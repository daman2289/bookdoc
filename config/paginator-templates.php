<?php
return [
    'number' => '<li><a href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active"><a class="page-item" href="{{url}}">{{text}}</a></li>',
    'prevActive' => '<li><a class="page-item" href="{{url}}" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>',
    'prevDisabled' => '<li class="disabled"><a class="page-item" href="javascript:void(0);" aria-label="Previous"><span aria-hidden="true">Prev</span></a></li>',
    'nextActive' => '<li><a class="page-item" href="{{url}}" aria-label="Next"><span aria-hidden="true">Next</span></a></li>',
    'nextDisabled' => '<li class="disabled"><a href="javascript:void(0);" aria-label="Next"><span aria-hidden="true">Next</span></a></li>',
];