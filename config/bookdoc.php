<?php

use Cake\Core\Configure;

return [
	'Version' => env('VERSION',1),
	'Roles' => [
		'admin' => 1,
		'subadmin' => 2,
		'docter' => 3,
		'patient' => 4
	],
	'recordsPerPage' => 8,
	'UserDummyImage' => 'man-blank-img.png',
	'DummyFemaleImage' => 'women-blank-img.png',
	'profileImageDimensions' => [
		'size' => ['width' => 222, 'height' => 222]
	],
	'doctorAddedImagesCheckPath' =>  WWW_ROOT .'files' . DS . 'doctor_images' . DS,
	'doctorDummyPath' =>  'img' . DS ,
	'doctorAddedImagesPath' => 'files' . DS . 'doctor_images' . DS,
	'booking' =>  [
		'status' => [
			'pending' => 1,
			'approved'	=> 2,
			'doctorDecline' => 3,
			'patientDecline' => 4,
		] 
	],
	'Plans' => [
		env('STRIPE_MONTHLY_PLAN_ID') => 1,
		env('STRIPE_SEMIANNUAL_PLAN_ID') => 2,
		env('STRIPE_ANNUAL_PLAN_ID') => 3,
		env('STRIPE_FREE_PLAN_ID') => 4
	],
	'PUSH_LOCAL_API' => env('ONE_SIGNAL_APP_KEY_LOCAL'),
	'PUSH_LOCAL_API_SECRET' => env('ONE_SIGNAL_SECRET_KEY_LOCAL'),

	'PUSH_API' => env('ONE_SIGNAL_APP_KEY'),
	'PUSH_API_SECRET' => env('ONE_SIGNAL_SECRET_KEY'),
];