<?php
use Migrations\AbstractSeed;

/**
 * Insurances seed.
 */
class InsurancesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title_german' => 'Privat/Wahlarzt',
                'title_eng' => 'Privat/Wahlarzt',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:51:42',
                'modified' => '2018-07-09 11:55:57',
            ],
            [
                'id' => '2',
                'title_german' => 'SVA',
                'title_eng' => 'SVA',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:52:55',
                'modified' => '2018-07-09 11:52:55',
            ],
            [
                'id' => '3',
                'title_german' => 'BVA',
                'title_eng' => 'BVA',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:53:25',
                'modified' => '2018-07-09 11:53:25',
            ],
            [
                'id' => '4',
                'title_german' => 'VA',
                'title_eng' => 'VA',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:54:28',
                'modified' => '2018-07-09 11:54:28',
            ],
            [
                'id' => '5',
                'title_german' => 'KFA',
                'title_eng' => 'KFA',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:54:58',
                'modified' => '2018-07-09 11:54:58',
            ],
            [
                'id' => '6',
                'title_german' => 'SVB',
                'title_eng' => 'SVB',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:55:28',
                'modified' => '2018-07-09 11:55:28',
            ],
            [
                'id' => '7',
                'title_german' => 'GKK',
                'title_eng' => 'GKK',
                'is_deleted' => '0',
                'created' => '2018-07-09 11:56:18',
                'modified' => '2018-07-09 11:56:18',
            ],
        ];

        $table = $this->table('insurances');
        $table->insert($data)->save();
    }
}
