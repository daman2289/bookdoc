<?php
use Migrations\AbstractSeed;

/**
 * Plans seed.
 */
class PlansSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Monthly Subscription',
                'stripe_id' => 'basic-monthly-plan',
                'price' => '49',
                'plan_type' => '1',
                'trial_period_in_days' => '0',
                'is_default' => '0',
                'created' => '2018-06-20 05:33:07',
                'modified' => '2018-06-20 05:33:07',
            ],
            [
                'id' => '2',
                'name' => 'Semi Annual Subscription',
                'stripe_id' => 'semi-annual-plan',
                'price' => '234',
                'plan_type' => '2',
                'trial_period_in_days' => '0',
                'is_default' => '0',
                'created' => '2018-06-20 05:33:07',
                'modified' => '2018-06-20 05:33:07',
            ],
            [
                'id' => '3',
                'name' => 'Annual Subscription',
                'stripe_id' => 'full-annual-plan',
                'price' => '348',
                'plan_type' => '3',
                'trial_period_in_days' => '0',
                'is_default' => '0',
                'created' => '2018-06-20 05:33:07',
                'modified' => '2018-06-20 05:33:07',
            ],
            [
                'id' => '4',
                'name' => 'Free',
                'stripe_id' => 'free',
                'price' => '0',
                'plan_type' => '4',
                'trial_period_in_days' => '0',
                'is_default' => '1',
                'created' => '2018-06-20 05:33:07',
                'modified' => '2018-06-20 05:33:07',
            ],
        ];

        $table = $this->table('plans');
        $table->insert($data)->save();
    }
}
