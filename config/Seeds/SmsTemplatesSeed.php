<?php
use Migrations\AbstractSeed;

/**
 * SmsTemplates seed.
 */
class SmsTemplatesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'template_for' => 'reminder_booking',
                'sms_text' => 'Bookdoc appt reminder:
#WEEKDAY, #DATE at #TIME
Dr. #DOCTOR_NAME,
#DOCTOR_ADDRESS',
                'created' => '2018-06-13 00:00:00',
                'modified' => '2018-06-13 00:00:00',
            ],
            [
                'id' => '2',
                'template_for' => 'cancel_booking_doctor',
                'sms_text' => 'Bookdoc appt cancel by doctor:
#WEEKDAY, #DATE at #TIME
Dr. #DOCTOR_NAME,
#DOCTOR_ADDRESS',
                'created' => '2018-06-13 00:00:00',
                'modified' => '2018-06-13 00:00:00',
            ],
            [
                'id' => '3',
                'template_for' => 'cancel_booking_patient',
                'sms_text' => 'Bookdoc appt cancel by patient:
#WEEKDAY, #DATE at #TIME
',
                'created' => '2018-06-13 00:00:00',
                'modified' => '2018-06-13 00:00:00',
            ],
        ];

        $table = $this->table('sms_templates');
        $table->insert($data)->save();
    }
}
