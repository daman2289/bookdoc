<?php
use Migrations\AbstractSeed;

/**
 * CmsPages seed.
 */
class CmsPagesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'meta_title' => 'About Us',
                'meta_description' => '<p><span style="font-size:16px">What is Lorem Ipsum?</span></p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Why do we use it?</p>

<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
',
                'meta_keyword' => 'About Us',
                'page_title' => 'About Us',
                'page_name' => 'About Us',
                'is_activated' => '1',
                'created' => '0000-00-00 00:00:00',
                'modified' => '2018-04-26 10:33:08',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '2',
                'meta_title' => 'Privacy Policy',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Privacy Policy',
                'page_title' => 'Privacy Policy',
                'page_name' => 'Privacy Policy',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:34:00',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '3',
                'meta_title' => 'Mission',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Mission',
                'page_title' => 'Mission',
                'page_name' => 'Mission',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:34:15',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '4',
                'meta_title' => 'Press',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Press',
                'page_title' => 'Press',
                'page_name' => 'Press',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:34:27',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '5',
                'meta_title' => 'Career',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Career',
                'page_title' => 'Career',
                'page_name' => 'Career',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:34:40',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '6',
                'meta_title' => 'Terms & conditions',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Terms & conditions',
                'page_title' => 'Terms & conditions',
                'page_name' => 'Terms & conditions',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:35:14',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '7',
                'meta_title' => 'Trust & Safety',
                'meta_description' => '<p><span style="font-size: 16px;">What is Lorem Ipsum?</span></p>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>Why do we use it?</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>',
                'meta_keyword' => 'Trust & Safety',
                'page_title' => 'Trust & Safety',
                'page_name' => 'Trust & Safety',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-05-05 10:35:36',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '8',
                'meta_title' => 'About Us',
                'meta_description' => '<p><span style="font-size:16px">What is Lorem Ipsum?</span></p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Why do we use it?</p>

<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
',
                'meta_keyword' => 'About Us',
                'page_title' => 'About Us',
                'page_name' => 'About Us',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-04-26 10:33:08',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '9',
                'meta_title' => 'About Us',
                'meta_description' => '<p><span style="font-size:16px">What is Lorem Ipsum?</span></p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Why do we use it?</p>

<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
',
                'meta_keyword' => 'About Us',
                'page_title' => 'About Us',
                'page_name' => 'About Us',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-04-26 10:33:08',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '10',
                'meta_title' => 'About Us',
                'meta_description' => '<p><span style="font-size:16px">What is Lorem Ipsum?</span></p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Why do we use it?</p>

<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
',
                'meta_keyword' => 'About Us',
                'page_title' => 'About Us',
                'page_name' => 'About Us',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-04-26 10:33:08',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
            [
                'id' => '11',
                'meta_title' => 'About Us',
                'meta_description' => '<p><span style="font-size:16px">What is Lorem Ipsum?</span></p>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>Why do we use it?</p>

<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
',
                'meta_keyword' => 'About Us',
                'page_title' => 'About Us',
                'page_name' => 'About Us',
                'is_activated' => '1',
                'created' => '2018-04-26 10:33:08',
                'modified' => '2018-04-26 10:33:08',
                'is_header' => '0',
                'page_order' => '1',
                'additional_key' => 'aboutus',
                'is_deleted' => '0',
            ],
        ];

        $table = $this->table('cms_pages');
        $table->insert($data)->save();
    }
}
