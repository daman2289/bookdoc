<?php
use Cake\Core\Configure;

return [
	'temp_new_appointment_booked' 	=> [
		'title'	=> 'New Appointment by #PATIENTNAME has been booked.', 
		'message'	=> 'New Appointment by patient <b> #PATIENTNAME </b> has been booked for #BOOKINGDATE',
	],
	'temp_reschedule_appointment' 	=> [
		'title'	=> 'Appointment by #PATIENTNAME has been rescheduled.', 
		'message'	=> 'You have rescheduled appointment for patient <b> #PATIENTNAME </b> on #BOOKINGDATE',
	],
	'temp_cancel_appointment' 	=> [
		'title'	=> 'Appointment by #PATIENTNAME has been canceled.', 
		'message'	=> 'You have canceled appointment for patient <b> #PATIENTNAME </b> on #BOOKINGDATE',
	],
	'temp_patient_cancel_appointment' 	=> [
		'title'	=> 'Appointment by #PATIENTNAME has been canceled.', 
		'message'	=> '<b> #PATIENTNAME </b> has canceled appointment on #BOOKINGDATE',
	],
	'temp_confirm_appointment' 	=> [
		'title'	=> 'Appointment by #PATIENTNAME has been confirmed.', 
		'message'	=> 'You have confirmed appointment for patient <b> #PATIENTNAME </b> on #BOOKINGDATE',
	],
	'temp_unavailable_time' 	=> [
		'title'	=> 'You made yourself unavailable', 
		'message'	=> 'You have made yourself unavailable from #FROMTIMEOFF to #TOTIMEOFF on #ONDATE',
	],
	'temp_decline_appointment' 	=> [
		'title'	=> 'Appointment by #PATIENTNAME has been declined.', 
		'message'	=> 'You have declined appointment for patient <b> #PATIENTNAME </b> on #BOOKINGDATE',
	],

	// PUSH NOTIFICATION TEMPLATE FOR PATIENT
	'push_new_appointment_booked' 	=> [
		'title'	=> 'New Appointment with #DOCTORNAME has been requested.', 
		'message'	=> 'New Appointment with Dr. #DOCTORNAME has been requested for #BOOKINGDATE',
	],
	'push_reschedule_appointment' 	=> [
		'title'	=> 'Appointment with #DOCTORNAME has been rescheduled.', 
		'message'	=> 'You have rescheduled appointment with Dr. #DOCTORNAME on #BOOKINGDATE',
	],
	'push_cancel_appointment' 	=> [
		'title'	=> 'Appointment with #DOCTORNAME has been canceled.', 
		'message'	=> 'You have canceled appointment with Dr.  #DOCTORNAME on #BOOKINGDATE',
	],
	'push_doctor_cancel_appointment' 	=> [
		'title'	=> 'Appointment with #DOCTORNAME has been canceled.', 
		'message'	=> 'Your appointment with Dr. #DOCTORNAME on #BOOKINGDATE has been cancelled.',
	],
	'push_confirm_appointment' 	=> [
		'title'	=> 'Appointment with #DOCTORNAME has been confirmed.', 
		'message'	=> 'Your appointment with Dr. #DOCTORNAME on #BOOKINGDATE has been confirmed.',
	],
	'push_unavailable_time' 	=> [
		'title'	=> 'You made yourself unavailable', 
		'message'	=> 'You have made yourself unavailable from #FROMTIMEOFF to #TOTIMEOFF on #ONDATE',
	],
	'push_decline_appointment' 	=> [
		'title'	=> 'Appointment with Dr. #DOCTORNAME has been declined.', 
		'message'	=> 'Your appointment with Dr. #DOCTORNAME on #BOOKINGDATE has been declined.',
	],
	'push_appointment_reminder' => [
		'title'	=> 'Your Appointment with Dr. #DOCTORNAME is scheduled on #TIME', 
		'message'	=> 'Your appointment with Dr. #DOCTORNAME is scheduled on #TIME.',
	],
];