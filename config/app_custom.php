<?php 
define('ROLE_ADMIN', 1);
define('ROLE_SUB_ADMIN', 2);
define('ROLE_DOCTOR', 3);
define('ROLE_PATIENT', 4);
define('ROLE_HOSPITAL', 5);

return [
    'Roles' => [
        'admin' => ROLE_ADMIN,
        'subadmin' => ROLE_SUB_ADMIN,
        'doctor' => ROLE_DOCTOR,
        'patient' => ROLE_PATIENT,
        'hospital' => ROLE_HOSPITAL
    ]
];
