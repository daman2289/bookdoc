<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::prefix('api', function ($routes) {
    $routes->setExtensions(['json']);
    $routes->resources('Users');
    $routes->resources('Specialities');
    $routes->resources('Insurances');
    $routes->resources('States');
    $routes->resources('Countries');
    $routes->resources('Doctors');
    $routes->resources('Search');
    $routes->resources('Notifications');
    $routes->resources('PhoneVerifications');

    $routes->fallbacks('InflectedRoute');
});

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Dashboard', 'action' => 'index', 'home']);
    $routes->connect('/search/**', ['controller' => 'Search', 'action' => 'index']);
    $routes->connect('/speciality/*', ['controller' => 'Search', 'action' => 'index', 'speciality']);
    $routes->connect('/insurance/*', ['controller' => 'Search', 'action' => 'index', 'insurance']);
    $routes->connect('/location/*', ['controller' => 'Search', 'action' => 'index', 'location']);
    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
    $routes->connect('/users/login/patient', ['controller' => 'Users', 'action' => 'PatientLogin', 'patient']);

    //CMS pages
    $routes->connect('/mission', ['controller' => 'Users', 'action' => 'cms-page', 1]);
    $routes->connect('/privacy-policy', ['controller' => 'Users', 'action' => 'cms-page', 2]);
    $routes->connect('/about', ['controller' => 'Users', 'action' => 'cms-page', 3]);
    $routes->connect('/press', ['controller' => 'Users', 'action' => 'cms-page', 4]);
    $routes->connect('/careers', ['controller' => 'Users', 'action' => 'cms-page', 5]);
    $routes->connect('/terms-conditions', ['controller' => 'Users', 'action' => 'cms-page', 6]);
    $routes->connect('/trust-safety', ['controller' => 'Users', 'action' => 'cms-page', 7]);

    $routes->connect(
        '/:id-:slug', // E.g. /3-Dr._David_john
        ['controller' => 'Doctors', 'action' => 'view']
    )
    // Define the route elements in the route template
    // to pass as function arguments. Order matters since this
    // will simply map ":id" to $articleId in your action
    ->setPass(['id', 'slug']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('admin', function ($routes) {
    // Because you are in the admin scope,
    // you do not need to include the /admin prefix
    // or the admin route element.
    $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/:controller/', ['action' => 'login']);
    $routes->connect('/:controller/:action/*');
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
