$(function () {

    function loadNotifications(URL) {
        var plimit = 10;
        var jqxhr = $.get(BASEURL + URL + "?limit=" + plimit, function (data) {

            var cdata = "<ul class=\"list-group\">";
            $.each(data.notifications, function (key, val) {

                var date = moment(val.created.split("+")[0]);
                var duration = moment.duration(moment(new Date()).diff(date));;

                date = (duration.asDays() >= 30) ? date.format("MMMM DD, YYYY h:mm:ss a") : date.fromNow();

                var link = 'javascript:void(0)';

                if (val.link) {
                    link = BASEURL + val.link;
                }

                cdata = cdata + "<li class=\"list-group-item " + ((val.unread == 0) ? "read" : "unread") + " notify-hover\" data-id=\"" + val.id + "\">";
                cdata = cdata + "<a href=\"" + link + "\">";
                cdata = cdata + "<span class=\"" + val.icon + "\"></span> " + val.message;
                cdata = cdata + "<span class=\"time pull-right\">" + date + "</span>";
                cdata = cdata + "</a></li>";
            });
            cdata = cdata + "</ul>";

            $("#notification-content-list").html(cdata);
            var totalpages = Math.ceil(parseInt(data.total) / plimit);
            if (totalpages > 1) {
                $("#notification-pagination").twbsPagination({
                    totalPages: totalpages,
                    visiblePages: 5,
                    initiateStartPageClick: false,
                    onPageClick: function (event, page) {
                        event.preventDefault();
                        loadNotifications("api/notifications.json?limit=" + plimit + "&page=" + page);
                    }
                });
            }

        }).fail(function () {
            alert("error");
        });

        $("#notficationModal").modal({ "show": true });
    }


    $(".notification-view").click(function (event) {
        event.preventDefault();
        loadNotifications("api/notifications.json");
    });



    $(document).on("mouseenter", ".notify-hover", function () {
        if ($(this).hasClass("unread")) {
            var $this = $(this);
            var $id = $this.attr("data-id");
            var jqxhr = $.get("/api/notifications/view/" + $id + ".json", function () {
                $this.removeClass("unread");
                $this.addClass("read");
                var count = 0;
                var selectorText = $(".badge").text();

                if ($(".badge").text() != "") {
                    count = parseInt(selectorText) - 1;
                }
                if (count < 0) {
                    count = 0;
                }
                $(".badge").text(count);

                // if (count == 0) {
                //     $(".fa-bell").next("span").remove();
                //     $(".count-notification-msg").text("Notifications");
                // } else {
                // //     console.log("Count is ", count);
                // //     $(".count-notification-msg").text("You have " + count + " new notification(s)");
                // //     $(".fa-bell").next("span").text(count);
                // }
            });
        }
    });

    $("#markasread").on("click", markAllNotificationsAsRead);

    
});
function markAllNotificationsAsRead() {
    var url = "/api/notifications/markAllAsRead.json";
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        success: function (response, textStatus, xhr) {
            if (xhr.status == 200) {
                $(".badge").text("0");
                $(".unread").removeClass("unread");
            }
        },
        error: function(error){
            console.log("Some Error has Occured");
            console.log(error);
            
        }
    });
}