$(function(){
    $("#datetimepicker4").datetimepicker({ 
        minDate: new Date(),
        format: "YYYY-MM-DD", 
        // onSelect: function(dateText, inst) {
        //     alert("select!");
        // } 
    });
    $("#datetimepicker4").on("dp.change", function(e){
        dateselected(e.date.format("d"));        
    });


});

function dateselected(date){
    console.log(date);
    var url = "/api/doctors/get-block-timings.json"; 
    var docId = $("#datetimepicker4").data("id");
    var data = {
        'doctorId': docId,
        'weekId' : date,
    };
    $.ajax({
            type: "POST",
            url: url,
            data : data,
            success: function (responseData) {
                renderTimings(responseData.blockTimes);
            },
            error: function (error){
                console.log(error);

                console.log("Some Error has Occured");
            }
    });
    console.log("DateSelected");
}
function renderTimings(responseData){
    console.log(responseData.length);
    
    if (responseData.length <= 0) {
        $("#enterData").addClass("hide");
        $("#errorMsg").removeClass('hide');
        console.log("Please Select Another Day. Please select");
        return;
    }else{
        $("#errorMsg").addClass("hide");
        
        $("#enterData").removeClass('hide');
    }
    var ele="";
    responseData.forEach(element => {
        ele = ele + '<label class="radio-inline"><input required=true type="radio" name="booking_time" value=' + element.timing + ">" + element.timing + "</label >";
        
    });
    $("#availabletime").append(ele);
    console.log("tim", responseData);
    
}