$(function(){
    getDoctors();
    if (selectedSlot != null  && selectedSlot.openDialog && old_patient == false) {
        showBookingModal('True', selectedSlot.doctorId);
    }
    $('.dates').hide();
    $('.times').hide();
    $('.more-times').hide();
    $('.dates').first().show();
    $(".time_table").each(function(){
        $(this).find(".times").first().show();
    });
});

function next(element) {
    var currentIndex = parseInt($(element).attr('data-current'));
    if (currentIndex < 9) {
        $(".dates").hide();
        $(".times").hide();
        $(".dates:eq( " + (currentIndex + 1) + " )").show();
        $('.calender-arrow').attr('data-current', (currentIndex + 1));
        $('.prev-arrow').find('i').css('opacity', 0.9);
        $('.more-times').hide();
        $('.more-link').show();
        $(".time_table").each(function () {
            $(this).find(".times:eq( " + (currentIndex + 1) + " )").show();
        });
    }
    if (currentIndex + 1 == 9) {
        $(element).find('i').css('opacity', 0.5);
    }
}

function prev(element) {
    var currentIndex = parseInt($(element).attr('data-current'));
    if (currentIndex > 0) {
        $(".dates").hide();
        $(".times").hide();
        $(".dates:eq( " + (currentIndex - 1) + " )").show();
        $('.calender-arrow').attr('data-current', (currentIndex - 1));
        $('.next-arrow').find('i').css('opacity', 0.9);
        $('.more-times').hide();
        $('.more-link').show();
        $(".time_table").each(function() {
            $(this).find(".times:eq( " + (currentIndex -1) + " )").show();
        });
    }
    if (currentIndex - 1 == 0) {
        $(element).find('i').css('opacity', 0.5);
    }
}

function showMore(element) {
    $(element).closest('div.row').find('.more-times').show();
    $(element).closest('div.row').find('.more-link').hide();
}

function getDoctors(){    
    $.get("/api/users/getDoctors.json", (Docdata) => {
        let data = {
            'doctors': Docdata.doctors
        };
        getSpeciality(data);
        // autocomplete(document.getElementById("myInput"), data);
        
    });
}

function getSpeciality(data){
    $.get("/api/specialities.json", response => {
        data.speciality = response.specialities;
        autocomplete(document.getElementById("myInput"), data);
    });
}



function autocomplete(inp, data) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var categ, b, i, val = this.value;
       
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        categ = document.createElement("DIV");
        categ.setAttribute("id", this.id + "autocomplete-list");
        categ.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(categ);

        autoSpeciality(data, categ, val);

        autocDoctor(data,categ, val);
    });

    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });


    function autoSpeciality(data, categ, val){
        var arr = data.speciality;
        if(lng == 'de') {
            for (i = 0; i < arr.length; i++) {

                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].title_german.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/


                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<span class='highlight'>" + arr[i].title_german.substr(0, val.length) + "</span>";
                    b.innerHTML += arr[i].title_german.substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i].title_german + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/


                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;

                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    categ.appendChild(b);
                }
            } 
        } else {
            for (i = 0; i < arr.length; i++) {

                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].title_eng.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/


                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<span class='highlight'>" + arr[i].title_eng.substr(0, val.length) + "</span>";
                    b.innerHTML += arr[i].title_eng.substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i].title_eng + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/


                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;

                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    categ.appendChild(b);
                }
            } 
        }
        
    }

    function autocDoctor(data, categ, val) {
        // Doctors
        
        var arr = data.doctors;
        var doc = document.createElement("DIV");
        doc.setAttribute("id", "docList");
        doc.setAttribute("class", "autocomplete-items hide");
        categ.appendChild(doc);

        var groupLable = document.createElement("DIV");
        groupLable.innerHTML = "Are you looking for?";
        groupLable.setAttribute("class", "doctorLabel text-muted");

        mydoc = $("#docList");
        // mydoc.addClass('hide');

        doc.appendChild(groupLable);

    

        for (i = 0; i < arr.length; i++) {            
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].user_profile.full_name
                    .substr(0, val.length)
                    .toUpperCase() == val.toUpperCase()) {
                mydoc.removeClass("hide");

                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<span class='highlight'>" + 'Dr. '+arr[i].user_profile.full_name.substr(0, val.length) + "</span>";
                b.innerHTML += arr[i].user_profile.full_name.substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i].user_profile.full_name + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/

                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;

                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                doc.appendChild(b);
            }
        }
    }




    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });

    $('.favt-doctor').on('click',function() {
        var doctor_id = $(this).attr('data-id');
        $('#doctor_id_favourite').val(doctor_id);
        $('#favourite').modal('show');
    })
    // $('#favourite').on('shown.bs.modal', function (event) {
    //     console.log($(event.relatedTarget));
    //   // var doctor_id = $('')
    //    //doctor_id_favourite
    // })
}

function setTime(that, time, weekday, date) {
    if (selectedSlot != null) {
        selectedSlot.selectedTime = time;
        selectedSlot.selectedDate = date;    
    }
    $("#loginModel").find('input[name="selectedTime"]').val(time);
    $("#loginModel").find('input[name="selectedDate"]').val(date);
    $("#bookingDate").val(date);
    $("#bookingTime").val(time);
}

function showBookingModal(isSignedIn, doctorId,my_patient) {
    if (Object.values(booking).indexOf(doctorId)) {
       $('#my_patient').prop('checked',true);
    }
    if(my_patient == 0) {
        $('.old_patient').addClass('hide');
    }else {
        $('.old_patient').removeClass('hide');
    }

    $('#refer-yes-0').on('change',function(){
        if($(this).prop('checked') == true){
        $('.refer').removeClass('hide');
        }else {
            $('.refer').addClass('hide');
        }   
    }) 

    $('#refer-yes-1').on('change',function(){
        if($(this).prop('checked') == true){
            $('.refer').addClass('hide');
        }else {
            $('.refer').removeClass('hide');
        }   
    }) 
     

    if (selectedSlot != null) {
        selectedSlot.doctorId = doctorId;
    }

    $("#loginModel").find('input[name="doctorId"]').val(doctorId);
    if (isSignedIn == "False") {        
        
        $("#loginModel").modal("show");
        $("#redirectto").val(doctorId);
    } else {
        $("#doctor").val(doctorId);
        if (selectedSlot != null) {
            $("#bookAppointment").find('input[name="booking_time"]').val(selectedSlot.selectedTime);
            $("#bookAppointment").find('input[name="booking_date"]').val(selectedSlot.selectedDate);
        }
        $("#bookAppointment").modal("show");

        // window.location.href = "doctors/view/"+doctorId;
    }   
}

function removeFav() {

    var url = '/api/doctors/removeFavourite.json';
    var data = {
        'doctorId': $("#alreadFav").data('id'),
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function (response) {
            location.reload();
        },
    });
}


function showHideDetails(data) {

    if (data == "smelse") {
        var somvar = $("#patientData").html();
        $("#datahere").html(somvar);
    } else {
        $("#datahere").children().remove();
    }
    $("#form").validator("update");
}

$('#form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        // handle the invalid form...
    } else {
        // everything looks good!
    }
});