function initMap(id, lat, lng) {
    var uluru = { lat: lat, lng: lng };
    var map = new google.maps.Map(document.getElementById(id), {
        zoom: 18,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}

function showMap(id, lat = -25.363, lng = 131.044) {
    if ($("#" + id).hasClass("hide")) {
        $('#view' + id).html("Hide My Location <i class='fa fa-angle-up'>");    
        $("#" + id).removeClass("hide");
        initMap(id, lat, lng);

    }else{
        $('#view' + id).html("Show My Location <i class='fa fa-angle-down'>");
        
        $("#" + id).addClass("hide");        
    }
}