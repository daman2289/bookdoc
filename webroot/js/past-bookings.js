$(function () {
    $('#myConfirmModel').on('show.bs.modal', function (e) {
        var patientId = $(e.relatedTarget).data('patient-id');
        var bookedBy = $(e.relatedTarget).data("bookedby");
        $("#patientId").val(patientId);
        $(".spammer").text(bookedBy);
        var link = "/patients/addToSpam/";
        $(e.currentTarget).find('form').attr("action", link);
    });
    // $("#rescheduleModel").on("show.bs.modal", function(e) {
    //     var bookingId = $(e.relatedTarget).data("booking-id");
    //     console.log(bookingId);
        
    //     var link = "/doctors/reschedule-appointment/" + bookingId;
    //     $(e.currentTarget)
    //         .find("form")
    //         .attr("action", link);
    // });


    $("#datetimepicker4").datetimepicker({
        minDate: new Date(),
        format: "YYYY-MM-DD",
    });

    $(document).on('click', '.NewAppointment', function () {
        $("#createEventModal #datetimepicker4").val(moment().format("YYYY-MM-DD"));
        $('#createEventModal').modal();
    })

    // $("#datetimepicker4").on("dp.change", function (e) {
    //     getBlockTimings(e.date.format("d"));
    // });
})

function getBlockTimings(date) {
    var url = "/api/doctors/get-doctor-available-times.json";
    var docId = $("#datetimepicker4").data("id");
    var data = {
        'doctorId': docId,
        'weekId': date,
    };
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (responseData) {
            renderTimings(responseData.availabiltyDays);
        },
        error: function (error) {
            console.log(error);

            console.log("Some Error has Occured");
        }
    });
    console.log("DateSelected");
}

function renderTimings(responseData) {
    var ele = "";
    
    if (!responseData || responseData.doctor_availability_timings.length <= 0) {
        $(".enterData").addClass("hide");
        $("#errorBlockMsg").removeClass("hide");
        return;
    } else {
        $("#errorBlockMsg").addClass("hide");
        responseData.doctor_availability_timings.forEach(element => {
            console.log(element.timing);
            
            var mytime = moment(element.timing).utc().format("HH:mm A");
            ele = ele + '<label class="radio-inline"><input required=true type="radio" name="booking_time" value=' + mytime + ">" + mytime + "</label >";
        });
        $(".enterData").removeClass("hide");
    }
    
    $("#availabletime").html("");
    $("#availabletime").append(ele);

}