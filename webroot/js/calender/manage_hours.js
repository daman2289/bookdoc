(function() {
	$('#add_working_hour_form').validate({
		rules :{
			'weekday' : {
				required : true
			},
			'available_from' : {
				required : true
			},
			'available_upto' : {
				required : true
			}
		}
	})

	$('#add_duration_form').validate({
		rules : {
			'duration' : {
				required : true
			}
		}
	})
})();