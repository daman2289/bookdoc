$(function(){  
    moment().locale("de");
    $('.dashbroad-left-sidebar').hide();                              
    $('#datetimepicker4').datetimepicker({
        //format: 'YYYY-MM-DD'
        format:'DD.MM.YYYY'
    });

    $('#datetimepicker12').datetimepicker({
        format: 'YYYY-MM-DD',
        inline: true,
    });

    $("#datetimepicker12").on('dp.change', function(e){ 
        $('#mycalendar').fullCalendar('gotoDate', e.date);

    })
    $(".datetimepicker44").datetimepicker({
        format: "YYYY-MM-DD",
        minDate: new Date()
    });
    
    getBusinessHours();

    $(".redio_btn label:last-child").click(function () {
        $(".redio_click").addClass("time_hidn");
    });
    $(".redio_btn label:first-child").click(function () {
        $(".redio_click").removeClass("time_hidn");
    });
    $("div#mycalendar .fc-view > table .fc-scroller").scrollTop(480);
    $(".fc-left").addClass("hide");
});

function checkAvailableHours($date, $time){
    $("#timeerrors").text("");
    var url = "/api/doctors/getAvailability.json";
    var data = {
        date: $date,
        time: $time
    }
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: 'json',
        success: function (response) {
            var selectedTime = moment(),
                hourMinute = data.time.split(":"),
                isBefore = "",
                isAfter = "",
                isSame = "",
                selected = "",
                option = "";

            selectedTime.set("hour", hourMinute[0]);

            if (hourMinute[1] !== undefined) {
                selectedTime.set("minute", hourMinute[1]);                
            }

            for (i in response.range) {
                var rangeDateTime = moment(),
                    splitted = response.range[i].split(":");

                rangeDateTime.set("hour", splitted[0]);

                if (splitted[1] !== undefined) {
                    rangeDateTime.set("minute", splitted[1]);                
                }

                if (selectedTime.isSame(rangeDateTime, "minute")) {
                    isSame = selected = response.range[i];
                }

                if (rangeDateTime.isBefore(selectedTime)) {
                    isBefore = response.range[i];
                }

                if (rangeDateTime.isAfter(selectedTime, "minute") && isAfter === "") {
                    isAfter = response.range[i];
                }
            }

            var isSelected = "";

            for(i in response.range) {
                if (isSame === "") {
                    if (isBefore !== "") {
                        isSelected = response.range[i] === isBefore ? "selected" : "";
                    } else {
                        if (isAfter !== "") {
                            isSelected = response.range[i] === isBefore ? "selected" : "";
                        }
                    }
                } else {
                    isSelected = response.range[i] === isSame ? "selected" : "";
                }

                // if (isSame === "") {
                //     if (!alreadySelected) {
                //         if (isBefore !== "") {
                //             isSelected = response.range[i] === isBefore ? "selected" : "";
                //             alreadySelected = response.range[i] === isBefore;
                //         } else if (isAfter !== "") {
                //             isSelected = response.range[i] === isAfter ? "selected" : "";
                //             alreadySelected = response.range[i] === isAfter;
                //         }
                //     }
                // } else {
                //     isSelected = response.range[i] === isSame ? "selected" : "";
                //     alreadySelected = response.range[i] === isSame;
                // }
                // if (isSelected === "") {
                //     if (selected !== "" && response.range[i] === selected) {
                //         isSelected = "selected";
                //     } else {
                        // if (isBefore !== "" && response.range[i] === isBefore) {
                        //     isSelected = "selected";
                        // } else if (isAfter !== "" && response.range[i] === isAfter) {
                        //     isSelected = "selected";
                        // }
                //     }

                //     option += '<option val ="'+ response.range[i] +'"' + isSelected + '>' + response.range[i] + '</option>';
                // } else {
                //     option += '<option val ="'+ response.range[i] +'">' + response.range[i] + '</option>';
                // }

                option += '<option val ="'+ response.range[i] +'"' + isSelected + '>' + response.range[i] + '</option>';
            }

            $('.browserTime').html(option);
            if (!response.is_available) {
                $("#timeerrors").text(response.message)   
            }
        },

    });
}
function getBusinessHours(){    
    var url = "/api/doctors/getDoctorWorkingDays.json";    
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        success: function (response) {
            renderData(response.availabiltyDays);
            
        },

    });
}


function renderData(weekdaysArray){
    var dataArray = [];
    
    weekdaysArray.forEach(day => {
        var obj = {};
        
        switch (day.weekday) {
            case 1:
                obj["day"] = "Monday";
                break;
            case 2:
                obj["day"] = "Tuesday";
                break;
            case 3:
                obj["day"] = "Wednesday";
                break;
            case 4:
                obj["day"] = "Thursday";
                break;
            case 5:
                obj["day"] = "Friday";
                break;
            case 6:
                obj["day"] = "Saturday";
                break;
            case 7:
                obj["day"] = "Sunday";
                break;
        
            default:
                break;
        }
        obj['dow'] = [day.weekday]
        obj["start"] = moment.utc(day.available_from).format("HH:mm");
        obj["end"] = moment.utc(day.available_upto).format("HH:mm");
        dataArray.push(obj);
    });
    loadCalender(dataArray);
}

function loadCalender(businessHours) {
    var initialLocaleCode = 'de';
    $("#mycalendar").fullCalendar({
        customButtons: {
            myCustomButton: {
              text: 'Print',
              click: function() {
                window.print();
              }
            }
          },
        header: {
            left: "prev,next",
            center: "title",
            right: "myCustomButton"
        },
        locale : initialLocaleCode,
        defaultDate: moment(),
        defaultView: "agendaWeek",
        columnHeaderFormat: "DD MMM dddd", 
        views: {
            agendaWeek: {
                titleFormat: "MMMM YYYY",
                titleRangeSeparator: " - "
            }
        },
        defaultDate: moment(window.myBookingDate),
        // slotWidth: "70",
        businessHours: businessHours,
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectHelper: true,
        longPressDelay: 0,
        select: function(start, end) {
           onAvalableTimeClick(start, end);
        },
        eventDrop: function(event, delta, revertFunc) {
            //do something when event is dropped at a new location
        },

        //When u resize an event in the calendar do the following:
        eventResize: function(event, delta, revertFunc) {
            //do something when event is resized
        },
        selectConstraint: "businessHours",
        eventRender: function(event, element) {
            var data = moment(event.start).format("H:mm");
            if (event.title) {
                data =
                    moment(event.start).format("h:mm A") +
                    "-" +
                    moment(event.end).format("h:mm A");

                data = event.title + "<br>" + data;
            }

            element.html(
                "<small><p class='p-font'>" + data + "</p></small>"
            );
        },

        //Activating modal for 'when an event is clicked'
        eventClick: function(event, element) {
            console.log(event);
            window.details = event;
            $(".status").html(event.currentstatus);
            $(".seeing").html(event.seeing);
            if (event.seeing == "Someone Else") {
                $(".else-name").removeClass("hide");
                $(".else-patient").html(event.name);
                $(".else-patient-email").removeClass("hide");
                $(".else-patient-gender").removeClass("hide");
                $(".booked").removeClass("hide");
                $(".name_patient").addClass("hide");
                $(".primary_email").addClass("hide");
                $(".primary_gender").addClass("hide");
                $(".primary_number").addClass("hide");
                $(".else-email").html(event.else_email);
                $(".else-gender").html(event.else_gender);
                $(".else-booked").html(event.title);
                $(".illness").html(event.illness);
                $(".doctor").html(event.note);
                $(".report").html(event.file);
                $('.report_download').prop('href','/doctors/reportDownload/'+ event.bid);
            } else {
                $(".name").html(event.name);
                $(".email").html(event.email);
                $(".gender").html(event.else_gender);
                $(".phone").html(event.phone);
                $(".illness").html(event.illness);
                $(".doctor").html(event.note);
                $(".report").html(event.file);
                $(".else-name").addClass("hide");
                $(".else-patient-email").addClass("hide");
                $(".else-patient-gender").addClass("hide");
                $(".booked").addClass("hide");
                $(".name_patient").removeClass("hide");
                $(".primary_email").removeClass("hide");
                $(".primary_gender").removeClass("hide");
                $(".primary_number").removeClass("hide");
                $('.report_download').prop('href','/doctors/reportDownload/'+ event.bid);
            }

            switch (event.status) {
                case 1: //PENDING
                    $("#confirmAppButton").removeClass("hide");
                    $(".cancelReason").addClass("hide");

                    break;

                case 2: //APPROVED
                    $(".cancelReason").addClass("hide");
                    $("#cancelAppButton").removeClass("hide");

                    break;

                case 3: //DECLINED BY DOCTOR
                    $("#confirmAppButton").removeClass("hide");
                    $(".cancelReason").removeClass("hide");
                    $(".creason").text(event.reason);
                    break;

                case 4: //DECLINED BY PATIENT
                    $("#confirmAppButton").removeClass("hide");
                    $(".cancelReason").removeClass("hide");
                    $(".creason").text(event.reason);
                    break;

                default:
                    break;
            }
            $(".bdate").html(moment(event.start).format("lll"));
            if (!event.bid) {
                return;
            }
            $("#patient-info").modal("show");
            return false;
        },

        weekNumbers: true,
        weekNumbersWithinDays: true,
        weekNumberCalculation: "ISO",
        slotEventOverlap: false,
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: "/api/users/chart.json",
        timeFormat: "H:mm(:mm)",
        slotDuration: '00:30:00',
        slotLabelInterval: 30,
        slotLabelFormat: "H:mm"
    });

    function onAvalableTimeClick(start, end){
        var startDate = moment(start),
            endDate = moment(end),
            date = startDate.clone();
        this.startDate = startDate.format("YYYY-MM-DD");
        this.startTTime = startDate.format("HH:mm");
        this.endDate = endDate.format("YYYY-MM-DD");
        var duration = $('#doctor_id_session').html();
        var finalDate = moment
            .utc(this.startTTime, "HH:mm")
            .add(duration, "minutes")
            .format("HH:mm");
        $("#createEventModal .extendTime").text(finalDate);
        $("#toTimeOff").val(finalDate);    
        console.log(startDate.format("DD.MM.YYYY"), '<--')    
        $("#createEventModal #datetimepicker4").val(
            //this.startDate
            startDate.format("DD.MM.YYYY")
        );
        $("#createEventModal .browserTime").val(
            this.startTTime
        );
        checkAvailableHours(this.startDate, this.startTTime);

        $("#createEventModal").modal("show");
    }
    // $('#submitButton').on('click', function (e) {
    //     // We don't want this to act as a link so cancel the link action
    //     e.preventDefault();

    //     doSubmit();
    // });

    // function doSubmit() {
    //     $("#createEventModal").modal('hide');
    //     $("#mycalendar").fullCalendar('renderEvent',
    //         {
    //             title: $('#eventName').val(),
    //             start: new Date($('#eventDueDate').val()),

    //         },
    //         true);

    //     $('.eventForm')[0].reset();
    // }
    // $('#createEventModal').on('hidden.bs.modal', function () {
    //     $(this).find("input,textarea,select").val('').end();

    // });

    $(document).on("click", "#confirmAppButton", function(event) {
        var url = "/doctors/update-booking/" + details.bid;
        $("#updateBookingForm").attr("action", url);
        $("#patient-info").modal("hide");
        $("#myConfirmModel").modal();
        // $(".appdate").html(moment(details.start).format("lll"));
        // setTimeout(hideshowModel(details), 1000);
    });

    $(document).on("click", ".cancelApp", function(event) {
        
        $(".pname").html(details.name);
        $(".appdate").html(moment(details.start).format("lll"));
        setTimeout(hideshowModel(details), 1000);

       
    });

    function hideshowModel(event){

        if (event.seeing == 'Someone Else') {
            $('.else-patient').html(event.name);
            $('.else-email').html(event.else_email);
            $(".pgender").html(event.else_gender);
            $(".illreason").html(event.illness);
            $(".docnote").html(event.note);

        } else {
            $(".pname").html(event.name);
            $(".pmail").html(event.email);

            $(".pgender").html(event.else_gender);
            $(".pmob").html(event.phone);
            $(".illreason").html(event.illness);
            $(".docnote").html(event.note);
        }

        $("#cancelAppModal").modal();
        $("#bid").val(event.bid);

        $("#patient-info").modal("hide");
    }

    $(document).on("click", ".fc-agenda-edit-hours-btn", function (event) {
        var notification = {
            'data' :  {
                title: false,
                width: "100%",
                hide: false,
                stack: {
                    "dir1": "down", 
                    "dir2": "left", 
                    "push": "top", 
                    "spacing1": 0, 
                    "spacing2": 0, 
                    "firstpos1": 0, 
                    "firstpos2": 0
                },
                styling: "bootstrap3",
                // addclass: "ui-pnotify-inline",
                buttons: {
                    sticker: false
                }
                
            }
        };
        var url = "/api/doctors/getDoctorDayOff.json";    
        var data_date = $(this).attr("data-date");
        var fullDate = $(this).prev().data('goto').date;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data : {
                selectedDate : $(this).attr("data-date")
            },
            success: function (response) {
                $('#daily_hours_override_dialog h4').text(data_date);
                $('#daily_hours_override_dialog h4').text(data_date);
                $("#onDate").val(moment(fullDate).format("YYYY-MM-DD"));
                $("input[name='day_off']").prop('checked', response.weekdayOf);  
                if(response.booking > 0) {
                    notification.data.text  = '<div class="text-center calmsg">Please reschedule or cancel your booking of this day first</div>';
                    new PNotify(notification.data);
                } else {
                   $('#daily_hours_override_dialog').modal(); 
                }     
            },
            error: function() {
                alert('Something went wrong!!');
                return false;
            }
        });
    });

    $(document).on('change', '.newPatient', function () {        
        if ($(this).val() == 'new') {
            var somvar = $("#patientData").html();
            $("#inserthere").html(somvar);
        } else {
            var somvar = $("#selectPatient").html();
            $("#inserthere").children().remove();
            $("#inserthere").html(somvar);
            // $('.choose_patient').addClass('hide')
        }
        $("#bookingForm").validator("update");
    })
    $(document).on('click', '.availablity', function () {
        if ($(this).is(':checked')) {
            $('.patientDetails').removeClass('hide')
            $("#bookingForm").validator().on();
            $("#bookingForm").validator("update");
            $("#bookingForm").attr("action", "/doctors/doctor-create-booking");
            
        } else {
            $('.patientDetails').addClass('hide')
            $("#bookingForm").validator().off();
            $("#bookingForm").validator("update");
            $('.submit').removeClass('disabled');
            $("#bookingForm").attr("action", "/doctors/doctor-time-Off");

        }
    })

    $("#rescheduleModel").on('shown.bs.modal', function() {
        $(this).find("#duration").val(details.duration);
    });
    $("#createEventModal").on('shown.bs.modal', function() {
        $(this).find("#duration").val(duration);
    });
    $(document).on("click", "#reschedule", function (event) {
        var name = details.name.split(" ", 2);
        var fname = name[0];
        var lname = name[1];
        var gender = details.else_gender;
        var bDate = moment(details.start);
        var bEndTime = moment(details.end);
        
        var bid = details.bid;
        
        $("#patientfname").val(fname);
        $("#patientlname").val(lname);

        if (gender == 'Male') {
            $("#pgendermale").prop("checked", true);            
        }else{
            $("#pgenderfemale").prop('checked', true);            
        }
        $("#bookingId").val(bid);
        $("#bDate").val(bDate.format("YYYY-MM-DD"));
        $("#bTime").val(bDate.format("HH:mm"));
        $('.extendTime').text(bEndTime.format("HH:mm"));
        var url = "/doctors/rescheduleAppointment/" + bid;
        $("#myReForm").attr("action", url);


        // $(".else-patient").html(event.name);

        $("#patient-info").modal("hide");
        $("#rescheduleModel").modal();

    });

    $(document).on('change', '.browserTime', function () {
        var $parent = $(this).parents(".modal-body");
        var startTTime = $(this).val();        
        var dur = $parent.find("#duration").val();
        var finalDate = moment.utc(startTTime, 'HH:mm').add(dur, 'minutes').format('HH:mm');
        $parent.find("#toTimeOff").val(finalDate);
        $parent.find(".extendTime").text(finalDate);
        var search_time = $("#datetimepicker4").val();
        //checkAvailableHours(search_time, startTTime);
    })



    $(document).on('change', '#duration', function () {
        var $parent = $(this).parents(".modal-body");
        var startTTime = $parent.find(".browserTime").val();
        var dur = $parent.find("#duration").val();
        var finalDate = moment.utc(startTTime, 'HH:mm').add(dur, 'minutes').format('HH:mm');
        $parent.find("#toTimeOff").val(finalDate);
        $parent.find(".extendTime").text(finalDate);        
    })


    $(document).on('click', '.ManageWorkingHours', function () {        
        $('#modal_third').modal();
    })
    $(document).on('click', '.NewAppointment', function () {
        $("#createEventModal #datetimepicker4").val(moment().format("YYYY-MM-DD"));
        $('#createEventModal').modal();
    })
    $(document).on('change', '.startTime', function () {
        var startTTime = $(this).val();
        if (startTTime == 'out') {
            $(this).parents('.work_hours').find('.nextTime').val('out')
        } else {
            var finalDate = moment.utc(startTTime, 'HH:mm').add(30, 'minutes').format('HH:mm');
            $(this).parents('.work_hours').find('.nextTime').val(finalDate)
        }
    })
    $(document).on('change', '.nextTime', function () {
        var startTTime = $(this).val();
        if (startTTime == 'out') {
            $(this).parents('.work_hours').find('.startTime').val('out')
        } else {
            var finalDate = moment.utc(startTTime, 'HH:mm').add(-30, 'minutes').format('HH:mm');
            var startTime = $(this).parents('.work_hours').find('.startTime').val();
            //alert(startTime+'---'+finalDate)
            if (startTime > startTTime) {
                $(this).parents('.work_hours').find('.startTime').val(finalDate)
            } else {
                $(this).parents('.work_hours').find('.startTime').val(startTime)
            }
        }
    });

    $(document).on('click','#sync-to-gcal',function() {

    })
}