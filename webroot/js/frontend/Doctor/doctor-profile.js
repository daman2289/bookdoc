$(function () {
	var ton = 'Auto';
	var toff= 'Manual';
	var t1on = 'Yes';
	var t1off= 'No';
	console.log(lng);
	if(lng == 'de') {
		$ton = 'automatisch';
		$toff= 'manuell';
		$t1on = 'Ja';
		$t1off= 'Nein';
	}
	
    $('#modeToggle').bootstrapToggle({
        on: ton,
        off: toff,
        height: 0,
    });

    $('#modeToggle1').bootstrapToggle({
        on: t1on,
        off: t1off,
        height: 0,
    });
});