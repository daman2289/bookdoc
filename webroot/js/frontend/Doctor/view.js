$(function(){
    if (selectedSlot != null  && selectedSlot.openDialog && old_patient == false) {
        showBookingModal('True', selectedSlot.doctorId);
    }
  $("#datepicker").datepicker({
    autoclose: true,
    todayHighlight: true,
  }).datepicker('update', );
  // getAvailableDays();

  $('#form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
      // handle the invalid form...
    } else {
      // everything looks good!
    }
  });
  $("#spammer").on("show.bs.modal", function (e) {
    var docName = $("#map").data("doc");
    $("#mydoc").text(docName);
  });

  $('.dates').hide();
  $('.times').hide();
  $('.more-times').hide();
  $('.dates').first().show();
  $('.times').first().show();


  $(".thumbnail").viewbox({
    margin: 20,
    resizeDuration: 300,
    openDuration: 200,
    closeDuration: 200,
    closeButton: true,
    navButtons: true,
    closeOnSideClick: true,
    nextOnContentClick: true
  });
});

function setTime(that, time, weekday, date) {
    if (selectedSlot != null) {
        selectedSlot.selectedTime = time;
        selectedSlot.selectedDate = date;    
    }
    $("#loginModel").find('input[name="selectedTime"]').val(time);
    $("#loginModel").find('input[name="selectedDate"]').val(date);
    $("#bookingDate").val(date);
    $("#bookingTime").val(time);
}

function showBookingModal(isSignedIn,doctorId,my_patient){
  var isSpammer = $("#map").data("spam");
  console.log(booking);
  if (Object.values(booking).indexOf(doctorId)) {
       $('#my_patient').prop('checked',true);
    }
    if(my_patient == 0) {
        $('.old_patient').addClass('hide');
    }else {
        $('.old_patient').removeClass('hide');
    }

    $('#refer-yes-0').on('change',function(){
        if($(this).prop('checked') == true){
        $('.refer').removeClass('hide');
        }else {
            $('.refer').addClass('hide');
        }   
    }) 

    $('#refer-yes-1').on('change',function(){
        if($(this).prop('checked') == true){
            $('.refer').addClass('hide');
        }else {
            $('.refer').removeClass('hide');
        }   
    }) 
     

    if (selectedSlot != null) {
        selectedSlot.doctorId = doctorId;
    }

    $("#loginModel").find('input[name="doctorId"]').val(doctorId);
    if (isSignedIn == "False") {        
        
        $("#loginModel").modal("show");
        $("#redirectto").val(doctorId);
    } else {
        $("#doctor").val(doctorId);
        if (selectedSlot != null) {
          console.log(selectedSlot);
            $("#bookAppointment").find('input[name="booking_time"]').val(selectedSlot.selectedTime);
            $("#bookAppointment").find('input[name="booking_date"]').val(selectedSlot.selectedDate);
        }
        $("#bookAppointment").modal("show");

        // window.location.href = "doctors/view/"+doctorId;
    }
  
  
  
}

function showHideDetails(data) {

  if (data == "smelse") {
      var somvar = $("#patientData").html();
      $("#datahere").html(somvar);      
  } else {
      $("#datahere").children().remove();
  }
  $("#form").validator("update");
}

function initMap() {
    var uluru = {lat: location_latitude, lng: location_longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 18,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
}

// function setTime(that, time, weekday, date) {
//   $("body").find(".activeElement").removeClass('active');
//   $(that).addClass('active');  
//   $(that).addClass('activeElement');  
//   $("#bookingTime").val(time);
//   $("#bookingDate").val(date);
//   $("#form").validator("update");

// }

function getAvailableDays(userId) {
  
  var data = {'userId': userId};
  var url = '';
  $.ajax({
    url: url,
    data: data,
    success: function(response){
      console.log(response);
    },
  });
}

function removeFav() {
  
  var url = '/api/doctors/removeFavourite.json';
  var data = {
      'doctorId': $("#alreadFav").data('id'),
  }
  
  $.ajax({
    url: url,
    type: 'POST',
    data : data,
    success: function(response){
     location.reload();
    },
  });
}

function next(element) {
    var currentIndex = parseInt($(element).attr('data-current'));
    if(currentIndex < 4){
        $(".dates").hide();
        $(".times").hide();
        $(".dates:eq( "+(currentIndex+1)+" )").show();
        $(".times:eq( "+(currentIndex+1)+" )").show();
        $('.calender-arrow').attr('data-current', (currentIndex+1));
        $('.prev-arrow').find('i').css('opacity', 0.9);
        $('.more-times').hide();
        $('.more-link').show();
    }
    if(currentIndex+1 == 4) {
        $(element).find('i').css('opacity', 0.5);
    }
}

function prev(element) {
    var currentIndex = parseInt($(element).attr('data-current'));
    if(currentIndex > 0) {
        $(".dates").hide();
        $(".times").hide();
        $(".dates:eq( "+(currentIndex-1)+" )").show();
        $(".times:eq( "+(currentIndex-1)+" )").show();
        $('.calender-arrow').attr('data-current', (currentIndex-1));
        $('.next-arrow').find('i').css('opacity', 0.9);
        $('.more-times').hide();
        $('.more-link').show();
    }
    if(currentIndex-1 == 0) {
        $(element).find('i').css('opacity', 0.5);
    }
}

function showMore(element) {
    $(element).closest('div.row').find('.more-times').show();
    $(element).closest('div.row').find('.more-link').hide();
}
