(function (window) {
    'use strict';
    var $ = window.jQuery;
    var week_day_parent_element = '[data-id="weekday-parent"]';
    var week_day_checkbox_ele = $('[data-id="week-day"]');
    var time_select_ele = $('[data-id="time-ele"]');
    var time_input_ele = $('[data-id="time-input"]');
    // var add_more_time_btn_ele = $('[data-id="add-more-time-btn"]');
    // var time_section_parent_ele = '[data-id="time-section-parent"]';
    // var remove_time_btn_ele = '[data-id="remove-more-time-btn"]';
    
    //event on change of week day check uncheck
    week_day_checkbox_ele.change(function() {
    	let element = $(this);
    	element.parents(week_day_parent_element).find(time_select_ele).addClass('hide');
    	if (element.is(':checked')) {
    		element.parents(week_day_parent_element).find(time_select_ele).removeClass('hide');
    	}
    });

    time_input_ele.timepicker({'timeFormat': 'H:i'});

    //event to add more time element to set for appointment
    /*add_more_time_btn_ele.click(function () {
    	let element = $(this);
    	let week_day_val = element.data('weekday');

		let element_to_clone = element.parent().parent(time_section_parent_ele); //defining which element to clone

    	let new_clonned_time_ele = element_to_clone.clone(); //clonning time element

    	//updating add button attributes e.g. replacing name from Add to Remove
		new_clonned_time_ele.find('button').attr('data-id', 'remove-more-time-btn').text('Remove');

		let clonned_parent_element = '[data-dynamicid="time-ele-' + week_day_val + '"]'; //element in which the clonned element will append under
		new_clonned_time_ele.appendTo(clonned_parent_element); //appending the clonned element
    });

    //event to remove time slot element
    $(document).on('click', remove_time_btn_ele, function () {
    	let element = $(this);
    	element.parent().parent(time_section_parent_ele).remove();
    });*/

})(window);

function nextclicked (renderHere){
    var data = { 
        'startTime': $("#starttime" + renderHere).val(), 
        'endTime': $("#endtime" + renderHere).val(), 
        'duration': $("#duration" + renderHere).val() 
    };
    $.ajax({
        url: "/api/doctors/get-times.json",
        data: data,
        type: 'post',
        success: function(response){
            console.log(response);
            renderElement(response, renderHere);
        }
    }).done(function() {
        $(this).addClass("done");
    });
}
function renderElement(response, renderHere){
    var ele = "";
    $("#blockoff" + renderHere).parent().removeClass("hide");
    $("#php" + renderHere).parent().addClass("hide");
    $("#blockoff" + renderHere).children().remove();
    response.availableTimes.forEach(element => {
        ele = "<label class='checkbox-inline'><input type='checkbox' name=" + renderHere+"[timing][] value=" + element.timing + ">" + element.timing + "</label>";
        $("#blockoff" + renderHere).append(ele);

    });
}