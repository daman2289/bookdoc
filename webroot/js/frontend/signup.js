(function () {

    var phone_codes_ele = $('[data-id="phone-codes"]');
    var passwrd_patient_ele = $('#passwd-patient');
    var passwrd_doctor_ele = $('#passwd-doc');
    var passwrd_hospital_ele = $('#passwd-hospital');

    passwrd_patient_ele.pwstrength({
        ui: {showVerdictsInsideProgressBar: true}
    });
    passwrd_doctor_ele.pwstrength({
        ui: {showVerdictsInsideProgressBar: true}
    });

    passwrd_hospital_ele.pwstrength({
        ui: {showVerdictsInsideProgressBar: true}
    });
    
    phone_codes_ele.intlTelInput({
        initialCountry: "at",
        hiddenInput: "full_number",
        utilsScript: "../../js/utils.js"
    });

    // validate signup form on keyup and submit
    var validateSignUp = function (element) {

        $(element).validate({
            rules: {
                'firstname': {
                    required: true,
                    namevalid: true,
                },

                'lastname': {
                    required: true,
                    namevalid: true,
                    minlength: 3
                },
                
                'country': {
                    required: true,
                },
                'state': {
                    required: true,
                },
                'password': {
                    required: true,
                    minlength: 8,
                    pwcheck: true,
                    noSpace: true
                },
                'phone': {
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 10
                },
                'User[user_profile][dob][month]': {
                    required: true
                },
                'User[user_profile][dob][date]': {
                    required: true
                },
                'User[user_profile][dob][year]': {
                    required: true
                },
                'User[user_profile][address]': {
                    required: true
                },

                'User[user_profile][zipcode]': {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    noSpace: true,
                    digits: true
                },

                'email': {
                    required: true,
                    email: true
                },

                'terms': {
                    required: true
                }

            },
            messages: {
                'email': {
                    required: "Please enter a valid email address"
                },
                'state': {
                    required: "Please enter a valid state"
                },
                'User[user_profile][dob][month]': {
                    required: "Required"
                },
                'User[user_profile][dob][date]': {
                    required: "Required"
                },
                'User[user_profile][dob][year]': {
                    required: "Required"
                },
                'User[user_profile][zipcode]': {
                    minlength: "Four digits are allowed only",
                    maxlength: "Four digits are allowed only",
                    digits: "Only numerics are allowed"
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "phone") {
                    error.insertAfter($(element).closest('.input-group'));
                } else if (element.attr("name") == "terms") {
                    error.insertAfter($(element).closest('.checkbox'));
                } else {
                    error.insertAfter($(element));
                }

            }
        });
    };

    var validateHospitalSignUp = function (element) {

        $(element).validate({
            rules: {
                'hospital_profile[hospital_name]': {
                    required: true
                },

                'hospital_profile[contact_name]': {
                    required: true,
                    namevalid: true
                },
                
                'hospital_profile[country_id]': {
                    required: true,
                },
                'hospital_profile[state_id]': {
                    required: true,
                },
                'password': {
                    required: true,
                    minlength: 8,
                    pwcheck: true,
                    noSpace: true
                },
                'hospital_profile[phone_number]': {
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 10
                },
                'hospital_profile[address]': {
                    required: true
                },

                'hospital_profile[zipcode]': {
                    required: true,
                    minlength: 4,
                    maxlength: 4,
                    noSpace: true,
                    digits: true
                },

                'email': {
                    required: true,
                    email: true
                },

                'terms': {
                    required: true
                }

            },
            messages: {
                'email': {
                    required: "Please enter a valid email address"
                },
                'hospital_profile[state_id]': {
                    required: "Please enter a valid state"
                },
                'hospital_profile[zipcode]': {
                    minlength: "Four digits are allowed only",
                    maxlength: "Four digits are allowed only",
                    digits: "Only numerics are allowed"
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "phone") {
                    error.insertAfter($(element).closest('.input-group'));
                } else if (element.attr("name") == "terms") {
                    error.insertAfter($(element).closest('.checkbox'));
                } else {
                    error.insertAfter($(element));
                }

            }
        });
    };

    jQuery.validator.addMethod("namevalid", function (value, element) {
        return this.optional(element) || /^[a-zA-Z]+$/.test(value);
    }, "Please use only letters.");
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No Space allowed");
    jQuery.validator.addMethod("pwcheck", function (value, element) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                &&
                /[a-z]/.test(value) // has a lowercase letter
                &&
                /\d/.test(value);
    }, "<ul><li>Has at least 8 characters</li><li>Has letters, numbers, and special characters</li></ul>");

    // jQuery.validator.addMethod("dollarsscents", function (value, element) {
    //     return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/i.test(value);
    // }, "You must include two decimal places");

    $(document).ready(function () {
        validateSignUp("#signupForm");
        validateSignUp("#signupForm_doctor");
        validateHospitalSignUp('#hospitalsignupForm');
    });
})();