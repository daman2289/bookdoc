function getStudent() {
	
} 
$(document).ready(function() {
  
  // page is now ready, initialize the calendar...
    facUrl = $('#admin-calendar').attr("data-href");
     
     $('#admin-calendar').fullCalendar({
      theme:false,
      defaultView : 'agendaWeek',
      header: {
       left: 'prev,next today',
       center: 'title',
       right: 'month,agendaWeek,agendaDay',

      },
      buttonText: {
       today: 'today',
       month: 'month',
       week: 'week',
       day: 'day'
      },
      
      events: facUrl,
      eventClick: function(event, element) {
      	$('.status').html(event.currentstatus);
      	$('.seeing').html(event.seeing);
      	if(event.seeing == 'Someone Else') {
      		$('.else-name').removeClass('hide');
      		$('.else-patient').html(event.name);
      		$('.else-patient-email').removeClass('hide');
      		$('.else-patient-gender').removeClass('hide');
      		$('.booked').removeClass('hide');
      		$('.name_patient').addClass('hide');
      		$('.primary_email').addClass('hide');
      		$('.primary_gender').addClass('hide');
      		$('.primary_number').addClass('hide');
      		$('.else-email').html(event.else_email);
      		$('.else-gender').html(event.else_gender);
      		$('.else-booked').html(event.title);
      		$('.illness').html(event.illness);
      		$('.doctor').html(event.note);
      	}else {
      		$('.name').html(event.title);
	      	$('.email').html(event.email);
	      	$('.gender').html(event.gender);
	      	$('.phone').html(event.phone);
	      	$('.illness').html(event.illness);
	      	$('.doctor').html(event.note);
	      	$('.else-name').addClass('hide');
	      	$('.else-patient-email').addClass('hide');
      		$('.else-patient-gender').addClass('hide');
      		$('.booked').addClass('hide');
      		$('.name_patient').removeClass('hide');
      		$('.primary_email').removeClass('hide');
      		$('.primary_gender').removeClass('hide');
      		$('.primary_number').removeClass('hide');
      	}
      	$('#patient-info').modal('show');
        return false;
      },
      eventRender: function(event, element){
      	
       var content = "<p class='p-font'>"+event.title+"</p>";
       
       element.html(content);
       
      },
      editable: true,
      droppable: true, 
     });

  });