(function() {
	var verify_phone_popup_ele = $('[data-id="phone-verify"]');
	var partitioned_input_ele = $('[data-id="partitioned-input"]');
	var phone_verify_form_ele = $('[data-id="phone-verify-form"]');
	var phone_verify_submit_btn_ele = $('[data-id="phone-verify-submit-btn"]');
	var resend_code_ele = $('[data-id="resend-code"]');
    var verify_phone_action_ele = $('[data-id="verify-phone"]');
    var phone_not_verified_ele = $('[data-id="phone-not-verified"]');

    verify_phone_action_ele.click(function() {
        verify_phone_popup_ele.modal('show');
    });

	partitioned_input_ele.pincodeInput({hidedigits:false,inputs:4}); //creating otp based input field

	//event to verify phone number
	phone_verify_form_ele.submit(function(e) {
		e.preventDefault();
		let element = $(this);
        let url = element.attr('action');
        let formData = element.serialize();
        
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            beforeSend: function() {
                phone_verify_submit_btn_ele.text('Verifying...');
                phone_verify_submit_btn_ele.attr('disabled', true);
            },
            success: function(resp) {
            	$.notify('Your phone number is verified successfully.', "success");
            },
            error: function(xhr, textStatus, errorThrown) {
                let error = 'Request not completed. ' + errorThrown;
                // console.log(error, xhr.responseJSON.message, textStatus);
                if (xhr.responseJSON.message) {
                    error = xhr.responseJSON.message;
                }

                $.notify(error, "error");

                phone_verify_submit_btn_ele.text('Verify');
                phone_verify_submit_btn_ele.attr('disabled', false);
            },
            complete: function(jqXHR, textStatus) {
            	console.log(textStatus);
                if (textStatus === 'success') {
                    phone_verify_submit_btn_ele.text('Verify');
                    phone_verify_submit_btn_ele.attr('disabled', false);
                    verify_phone_popup_ele.modal('hide');

                    phone_not_verified_ele.next().remove();
                    phone_not_verified_ele.html('<i class="fa fa-check-circle phone-verified-icon"></i> Verified');
                }
            },
        });
	});

	//event to resend verification code on user's phone number
	resend_code_ele.click(function(e) {
		let element = $(this);
        let url = element.data('url');
        let formData = element.serialize();
        
        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function() {
                element.text('Sending...');
                element.attr('disabled', true);
            },
            success: function(resp) {
                $.notify(resp.response.msg, "success");
            },
            error: function(xhr, textStatus, errorThrown) {
                let error = 'Request not completed. ' + errorThrown;
                if (xhr.responseJSON.message) {
                    error = xhr.responseJSON.message;
                }

                $.notify(error, "error");

                element.text('click here');
                element.attr('disabled', false);
            },
            complete: function(jqXHR, textStatus) {
            	if (textStatus === 'success') {
                    element.text('click here');
                    element.attr('disabled', false);
                }
            },
        });
	});

})();