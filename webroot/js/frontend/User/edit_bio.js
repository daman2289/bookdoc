(function () {
	$("#edit_bio").validate({
        rules: {

        },
        messages: {

        }
    });

	$("body").on("click", ".add-more", function () {
        var clonedBlock = $(this).closest('.form-group').clone();        
        var btnElem = clonedBlock.find("button.add-btn");
        clonedBlock.find("input:text").val($(this).closest('.form-group').find('input').val());
        $(this).closest('.form-group').after(clonedBlock);
        $(this).closest('.form-group').find('input').val("").attr('required', true);
    });

	 $("body").on("click", ".add-more-education", function () {
                
        var clonedBlock = $(this).closest('.form-group').clone();        
        var btnElem = clonedBlock.find("button.add-btn");
        var uni = $(this).closest('.form-group').find('.uni').val();
        var deg = $(this).closest('.form-group').find('.deg').val();        
        clonedBlock.find("input.deg:text").val(deg).attr('required', true);
        clonedBlock.find("input.uni:text").val(uni).attr('required', true);
        $(this).closest('.form-group').after(clonedBlock);
        $(this).closest('.form-group').find('input').val("")
    });

    //here it will remove the current value of the remove button which has been pressed
    $("body").on("click", ".remove", function () {
        if ($(this).parents(".form-group").parent().find(".form-group").length == 1) {
            $(this).parents(".form-group").find("input:text").val("");
        } else {
            //Remove only if its not last element            
            $(this).parents(".form-group").remove();
        }

        if ($(this).parents(".form-group").parent().find(".form-group").length == 1) {
           $(this).parents(".form-group").find("input:text").removeAttr('required');
        }
    });

    $("body").on("click", ".remove-education", function () {
        
        
        if ($(this).parents(".form-group").parent().find(".form-group").length == 1) {
            $(this).closest('.form-group').find("input.deg:text").val("");
            $(this).closest('.form-group').find("input.uni:text").val("");
        }else{
            //Remove only if its not last element            
            $(this).parents(".form-group").remove();
        }

        if ($(this).parents(".form-group").parent().find(".form-group").length == 1) {
            $(this).closest('.form-group').find("input.deg:text").removeAttr('required');
            $(this).closest('.form-group').find("input.uni:text").removeAttr('required');
        }

    });

})();