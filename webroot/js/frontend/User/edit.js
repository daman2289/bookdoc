(function () {

    var phone_codes_ele = $('[data-id="phone-codes"]');

    // validate signup form on keyup and submit
    var validatechangemobile = function (element) {

        $(element).validate({
            rules: {
                'user_profile[title]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[first_name]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[last_name]': {
                    required: true,
                    noSpace: true,
                },
                'email': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[month]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[year]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[date]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[address]': {
                    required: true,
                },
                'user_profile[zipcode]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[country]': {
                    required: true,
                    noSpace: true,
                },
                'user_profile[state]': {
                    required: true,
                    noSpace: true,
                }
            }
        });
    };
    jQuery.validator.addMethod("noSpace", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "No Space allowed");

    $(function () {
        validatechangemobile("#edit-profile");
    });

    phone_codes_ele.intlTelInput({
        initialCountry: "at",
        hiddenInput: "full_number",
        utilsScript: "../../js/utils.js"
    });

})();