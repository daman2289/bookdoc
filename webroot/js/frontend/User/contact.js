(function () {
	$('#contact_form').validate({
		rules :{
			'name' : {
				required : true
			},
			'email' : {
				required : true,
				email : true
			},
			'telephone' : {
				number: true,
                maxlength: 10,
                minlength: 10
			},
			'message' : {
				required : true
			}
		}
	})
}();