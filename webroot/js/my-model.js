$(function () {
    $('#myConfirmModel').on('show.bs.modal', function (e) {        
        var bookingId = $(e.relatedTarget).data('booking-id');
        var link = "/doctors/update-booking/" + bookingId;    
        $(e.currentTarget).find('form').attr("action", link);
    });

    $("#myRejectModel").on("show.bs.modal", function(e) {
        var bookingId = $(e.relatedTarget).data("booking-id");
        var link = "/doctors/update-booking/" + bookingId;
        console.log(link);
        
        $(e.currentTarget).find("form").attr("action", link);
    });
})