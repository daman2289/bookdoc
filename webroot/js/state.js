(function () {

    var country_ele = $('[data-id="change-country"]');
    var states_ele = $('[data-id="states"]');

    country_ele.change(function() {
        let element = $(this);
        getStates(element, element.val());
    });

    var getStates = function (country_ele, country_id) {

        url = country_ele.data('url') + '/' + country_id + '.json';
        $.ajax({
            type: 'GET',
            url: url,
            success: function(resp) {
                var popupTemplate = $("#states-script").html();
                let resultData = _.template(popupTemplate, {data: resp.states});
                states_ele.html(resultData);
            },
            complete: function(jqXHR, textStatus) {
                console.log(textStatus);
            },
        });
    }
})();