(function () {
    // validate signup form on keyup and submit
    var validateChangePwd = function (element) {

        $(element).validate({
            rules: {
                'old_password': {
                    required: true,
                    noSpace: true,
                },
                'password': {
                    required: true,
                    minlength: 8,
                    pwcheck: true,
                    noSpace: true,
                },
                'confirm_password': {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                }
            }
        });
    };

    jQuery.validator.addMethod("noSpace", function (value, element) {
        $('#err').html('');

        return value.indexOf(" ") < 0 && value != "";
    }, "No Space allowed");
    
    jQuery.validator.addMethod("pwcheck", function (value, element) {
        $('#err').html('');

        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            &&
            /[a-z]/.test(value) // has a lowercase letter
            &&
            /\d/.test(value);
    }, "<ul><li>Has at least 8 characters</li><li>Has letters, numbers, and special characters</li></ul>");

    $(document).ready(function () {
        validateChangePwd("#change-password");
    });
})();


$(document).on('click', '.submit', function () {
    // e.preventDefault();
    var password = $('#confirm_password').val();
    var confirm_password = $('#password').val();
    if (password == confirm_password) {
        return true;
    } else {
        $('#err').html('Password and Confirm Password Must be Same');
        return false;
    }
});