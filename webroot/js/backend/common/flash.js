function showFlashMessage() {
	//owl breaks bootstraps modals 
	$.support.transition = false
	$('#flashModal').modal('show');

    $("#flashModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#flashModal").remove();
    });
}