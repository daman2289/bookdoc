(function() {
	var phone_codes_ele = $('[data-id="phone-codes"]');
    var passwrd_doctor_ele = $('#passwd-doc');
    
    passwrd_doctor_ele.pwstrength({
        ui: {showVerdictsInsideProgressBar: true}
    });

    
    phone_codes_ele.intlTelInput({
        initialCountry: "at",
        hiddenInput: "full_number",
        utilsScript: "../../js/utils.js"
    });

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Enter Letters only please");

    $.validator.addMethod("validate_email",function(value, element)
     {       
        if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value ))
            {           
                return true;        
            } else {         
                 return false;        
        } },"Please enter a valid Email.");

    $.validator.addMethod("nowhitespace", function(value, element) {
        return this.optional(element) || /^[^-\s](.*)+$/.test(value);
      }, "No white space please"); 


    $('#add_hospital_doctor_form').validate({
        rules : {
            'user_profile[first_name]' : {
                required : true,
                lettersonly : true,
                nowhitespace : true
            },
            'user_profile[last_name]' : {
                required : true,
                lettersonly : true,
                nowhitespace : true
            },
            'user_profile[phone_number]' : {
                required : true,
                number: true,
                maxlength: 10,
                minlength: 10
            },
            'email' : {
                required : true,
                validate_email : true
            },
            'password' : {
                required :true,
                minlength: 8,
                nowhitespace: true
            },
            'user_profile[dob][day][month]' : {
                required : true
            },
            'user_profile[dob][month][day]' : {
                required : true
            },
            'user_profile[dob][year][year]' : {
                required : true
            },
            'user_profile[address]' : {
                required : true
            },
            'user_profile[zipcode]' : {
                required: true,
                minlength: 4,
                maxlength: 4,
                nowhitespace: true,
                digits: true
            },
            'user_profile[country_id]' : {
                required : true
            },
            'user_profile[state_id]' : {
                required : true
            }
        }
    });
})();