(function(){
	$('#login-form').validate({
		rules: {
		    'email': {
		      	'required': true,
		      	'email' : true
		    },
		    'password': {
		      	'required': true,
		      	'minlength':6
		    }
		},
		messages: {
			'email': {
		      	'required': 'Email can not be empty'
		    },
		    'password': {
		      	'required': 'Password can not be empty'
		    }
		}	
	});
})();