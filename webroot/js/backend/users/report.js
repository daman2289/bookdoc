(function() {
	$.validator.addMethod("htmltag", function(value, element) {
        return this.optional(element) || /^(?!.*<[^>]+>).*$/.test(value);
      }, "No Html tags are allowed"); 

	$('#add-report-form').validate({
		rules : {
			'patient_id' : {
				required : true
			},
			'title' : {
				required : true
			},
			'description' : {
				required : true,
				htmltag : true
			},
			'file' : {
				required : true,
				extension : 'pdf|jpg|doc|docx|jpeg|png|gif'
			}
		}
	});

	$('.review').on('click',function() {
		var doctor_id = $(this).attr('data-id');
		var booking_id = $(this).attr('data-booking'); 
		$('.review_doctor').val(doctor_id);
		$('.review_booking').val(booking_id);
		$('#review').modal('show');
	});
	
	$('.feedback').on('click', function () {

		var overall = $(this).attr('data-overall');
		var bedside = $(this).attr('data-bedside');
		var waitTime = $(this).attr('data-waittime');
		var review = $(this).attr('data-review');

		$('#myOverAll').rating('update', overall);
		$('#myBedSide').rating('update', bedside);
		$('#myWaitTime').rating('update', waitTime);

		$('#myReview').val(review);
		

		$('#myRatingModal').modal('show');
	});

	$('.more-button').on('click',function() {
		var desc = $(this).attr('data-desc');
		$('.desc').html(desc);
		$('#moredesc').modal('show');
	})
})();