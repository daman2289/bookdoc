(function () {   
  // validate signup form on keyup and submit
  var validateChangePwd = function(element) {

     $(element).validate({
            rules: {           
               'old_password': {
                    required: true,
                    noSpace: true,
                },
                'password': {
                    required: true,
                    minlength:6,
                    noSpace: true,
                },
               'confirm_password': {
                    required: true,
                    minlength:6,
                    equalTo:"#password"
              },
           }       
        });
    };
    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No Space allowed");
    $(document).ready(function() {
        validateChangePwd("#change-password");
    }); 
})();


