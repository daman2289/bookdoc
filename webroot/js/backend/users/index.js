//Switch Url deactivate/activate users
$(".switch_custom").on("change",function(){
  if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
  } else {
      var url = $(this).attr('data-url')+"/0";
  }
  $.ajax({
       type:"GET",
       url : url,
       dataType:'json',   
       success: function(data){
          console.log(data);                              
       },
             
    });
});

$(".switch_custom1").on("change",function(){
  if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
  } else {
      var url = $(this).attr('data-url')+"/0";
  }
  $.ajax({
       type:"GET",
       url : url,
       dataType:'json',   
       success: function(data){
          console.log(data);                              
       },
             
    });
});

$(".switch_custom2").on("change",function(){
  var role_id = $(this).attr('data-roleid');
  if (!$(this).is(':checked')) {
  console.log($(this).is(':checked'))
      var url = $(this).attr('data-url')+"/1";
      msg = "All doctors associated with this hospital will get activated";
  } else {
      var url = $(this).attr('data-url')+"/0";
      msg = "All doctors associated with this hospital will get deactivated";
  }
  if(role_id == 5) {
    if(confirm(msg)) {
        $.ajax({
         type:"GET",
         url : url,
         dataType:'json',   
         success: function(data){
            console.log(data);                              
         },
               
      });
    }else {
        $(this).prop('checked', false);
    }
    
  }
  
});

$(".switch_custom3").on("change",function(){
  var role_id = $(this).attr('data-roleid');
  if (!$(this).is(':checked')) {
  console.log(!$(this).is(':checked'))
      var url = $(this).attr('data-url')+"/1";
      msg = "All doctors associated with this hospital will get Deleted";
  } else {
      var url = $(this).attr('data-url')+"/0";
      msg = "All doctors associated with this hospital will get deactivated";
  }
  if(role_id == 5) {
    if(confirm(msg)) {
        $.ajax({
         type:"GET",
         url : url,
         dataType:'json',   
         success: function(data){
            console.log(data);     
            location.reload();                         
         },
               
      });
    }else {
        $(this).prop('checked', false);
    }
    
  }

  if(role_id == 3) {
     $.ajax({
         type:"GET",
         url : url,
         dataType:'json',   
         success: function(data){
            console.log(data);  
            location.reload();                            
         },
               
      });
  }
  
});

$('.delete_user').click(function(e) {   
  e.preventDefault();
  var pid = $(this).attr('data-id');
  var parent = $(this).parent("td").parent("tr");
  
  bootbox.dialog({
    message: "Are you sure you want to delete this user?",
    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
    buttons: {
    success: {
      label: "No",
      className: "btn-success",
      callback: function() {
       $('.bootbox').modal('hide');
      }
    },
    danger: {
      label: "Delete!",
      className: "btn-danger",
      callback: function() {                                    
         $.ajax({            
            type: 'DELETE',
            url: window.url+'api/users/'+pid+'.json',            
          })
          .done(function(response){
            
            bootbox.alert(response.msg);
            parent.fadeOut('slow');
            
          })
          .fail(function(data){
            
            bootbox.alert('Something Went Wrong ....');
                          
          })
                    
      }
    }
    }
  }); 
});