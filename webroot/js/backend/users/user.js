(function () {   
  // validate signup form on keyup and submit
  var validateSignUp = function(element) {

	 $(element).validate({
		rules: {
		   'user_profile[first_name]': {
				required: true,
				namevalid: true,
				// noSpace: true,
				minlength: 3
			},

			'user_profile[last_name]': {
				required: true,
				namevalid: true,
				// noSpace: true,
				minlength: 3
			},	
			'user_profile[phone_number]': {
				required: true,
				minlength:10,
				noSpace: true,
				number: true
			},	   
			'email': {
				required: true,
				email: true
			},
			'role_id': {
				required: true
			},
		   'password': {
				required: true,
				noSpace: true,
				minlength:8
			},
		   'confirm_password': {
				required: true,
				minlength:8,
				equalTo:"#password"
		  	},
		   'user_profile[zipcode]': {
				required: true,
				minlength:4,
		  	},
		   'user_profile[dob]': {
				required: true,
		  	},
	

	   },
	   messages: {
		  'email': {
			required: "Please enter a valid email address"
		  },		
	   }
	 });
	};
	jQuery.validator.addMethod("namevalid", function(value, element) {
			return this.optional(element) || /^[a-zA-Z]+$/.test(value);
	}, "Please use only letters.");
	jQuery.validator.addMethod("noSpace", function(value, element) { 
	    return value.indexOf(" ") < 0 && value != ""; 
	}, "No Space allowed");

	$(document).ready(function() {
		validateSignUp("#add-admin-form");
	});	
})();


