//Switch Url deactivate/activate users
$(".switch_custom").on("change",function(){
  if ($(this).is(':checked')) {
      var url = $(this).attr('data-url')+"/1";
  } else {
      var url = $(this).attr('data-url')+"/0";
  }
  $.ajax({
       type:"GET",
       url : url,
       dataType:'json',   
       success: function(data){
          console.log(data);                              
       },
             
    });
});


$('.delete_speciality').click(function(e) {   
  e.preventDefault();
  var pid = $(this).attr('data-id');
  var parent = $(this).parent("td").parent("tr");
  
  bootbox.dialog({
    message: "Are you sure you want to delete this Specialty?",
    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
    buttons: {
    success: {
      label: "No",
      className: "btn-success",
      callback: function() {
       $('.bootbox').modal('hide');
      }
    },
    danger: {
      label: "Delete!",
      className: "btn-danger",
      callback: function() {                                    
         $.ajax({            
            type: 'DELETE',
            url: window.url+'api/specialities/'+pid+'.json',            
          })
          .done(function(response){
            
            bootbox.alert(response.msg);
            parent.fadeOut('slow');
            
          })
          .fail(function(data){
            
            bootbox.alert('Something Went Wrong ....');
                          
          })                  
      }
    }
    }
  }); 
});