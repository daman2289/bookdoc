(function () {   
  // validate signup form on keyup and submit
  var validateSpecialityForm = function(element) {

	 $(element).validate({
		rules: {
		   'title_french': {
				required: true,
				minlength: 3
			},
			'title_eng': {
				required: true,
				minlength: 3
			},
			'title_italic': {
				required: true,
				minlength: 3
			}
	   	}
	 });
	};
	jQuery.validator.addMethod("namevalid", function(value, element) {
			return this.optional(element) || /^[a-z A-Z]+$/.test(value);
	}, "Please use only letters.");
	jQuery.validator.addMethod("noSpace", function(value, element) { 
	    return value.indexOf(" ") < 0 && value != ""; 
	}, "No Space allowed");

	$(document).ready(function() {
		validateSpecialityForm("#add-speciality-form");
	});	
})();


