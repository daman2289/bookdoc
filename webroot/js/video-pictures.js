//just for the demos, avoids form submit


$("#videoPic").validate({
    rules: {
        "video_link_one": {
            require_from_group: [1, ".video-group"]
        },
        "video_link_two": {
            require_from_group: [1, ".video-group"]
        },
        "video_link_three": {
            require_from_group: [1, ".video-group"]
        },
        "video_link_four": {
            require_from_group: [1, ".video-group"]
        },
        "video_link_five": {
            require_from_group: [1, ".video-group"]
        }
    }
});

$(document).on('click', '.ti-close', function () {
    var r = confirm("Are you sure to remove this image ?");
    if (r == true) {
        image_id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/users/remove-image",
            dataType: "json",
            data: { image_id: image_id },
            success: function (data) {
                $('#' + image_id).parent().remove()
            },
            error: function () {
                alert('Something went wrong please try again');
            }
        });
    } else {
        return false;
    }
});