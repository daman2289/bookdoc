(function (window) {
    'use strict';
    var $ = window.jQuery;
    var inputAutoComplete = document.getElementById('geocomplete');
    var doctor_inputAutoComplete = document.getElementById('doctor-geocomplete');
    var hospital_inputAutoComplete = document.getElementById('hospital_geocomplete');
    var autocomplete = new google.maps.places.Autocomplete(inputAutoComplete);
    var doctor_autocomplete = new google.maps.places.Autocomplete(doctor_inputAutoComplete);
    var hospital_autocomplete = new google.maps.places.Autocomplete(hospital_inputAutoComplete);
    var latitide_element = $('[data-id="latitude"]');
    var longitude_element = $('[data-id="longitude"]');
    var doc_latitide_element = $('[data-id="doc-latitude"]');
    var doc_longitude_element = $('[data-id="doc-longitude"]');
    var hos_latitide_element = $('[data-id="hos-latitude"]');
    var hos_longitude_element = $('[data-id="hos-longitude"]');
    
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }

        latitide_element.val(place.geometry.location.lat());
        longitude_element.val(place.geometry.location.lng());
    });

    doctor_autocomplete.addListener('place_changed', function() {
        var place = doctor_autocomplete.getPlace();
        if (!place.geometry) {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }
        
        doc_latitide_element.val(place.geometry.location.lat());
        doc_longitude_element.val(place.geometry.location.lng());
    });

    hospital_autocomplete.addListener('place_changed', function() {
        var place = hospital_autocomplete.getPlace();
        if (!place.geometry) {
          // User entered the name of a Place that was not suggested and
          // pressed the Enter key, or the Place Details request failed.
          window.alert("No details available for input: '" + place.name + "'");
          return;
        }
        
        hos_latitide_element.val(place.geometry.location.lat());
        hos_longitude_element.val(place.geometry.location.lng());
    });

})(window);