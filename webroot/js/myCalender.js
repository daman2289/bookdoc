
$(document).ready(function () {

    loadCalender();
    
    
});

// function getBookingData(){
//     var url = "/api/doctors/get-doctor-bookings/" + 39 + ".json";
//     $.ajax({
//             type: "GET",
//             url: url,
//             success: function (responseData) {
//                 loadCalender(responseData);
//             },
//             error: function (error){
//                 console.log(error);
                
//                 console.log("Some Error has Occured");
//             }
//     });
// }



function loadCalender() {
    // console.log(responseData.data);
    var facUrl = "/api/users/chart.json";


    $("#calendar").fullCalendar({ 
        theme: false,
        defaultView: "agendaWeek", buttonText: { today: "today", month: "month", week: "week", day: "day" },
        header: { left: "prev", center: "title", right: "next" },
        events: facUrl, 
        eventClick: function(event, element) {
            $('.status').html(event.currentstatus);
            $('.seeing').html(event.seeing);
            if(event.seeing == 'Someone Else') {
                $('.else-name').removeClass('hide');
                $('.else-patient').html(event.name);
                $('.else-patient-email').removeClass('hide');
                $('.else-patient-gender').removeClass('hide');
                $('.booked').removeClass('hide');
                $('.name_patient').addClass('hide');
                $('.primary_email').addClass('hide');
                $('.primary_gender').addClass('hide');
                $('.primary_number').addClass('hide');
                $('.else-email').html(event.else_email);
                $('.else-gender').html(event.else_gender);
                $('.else-booked').html(event.title);
                $('.illness').html(event.illness);
                $('.doctor').html(event.note);
            }else {
                $('.name').html(event.title);
                $('.email').html(event.email);
                $('.gender').html(event.gender);
                $('.phone').html(event.phone);
                $('.illness').html(event.illness);
                $('.else-name').addClass('hide');
                $('.else-patient-email').addClass('hide');
                $('.else-patient-gender').addClass('hide');
                $('.booked').addClass('hide');
                $('.name_patient').removeClass('hide');
                $('.primary_email').removeClass('hide');
                $('.primary_gender').removeClass('hide');
                $('.primary_number').removeClass('hide');
                $('.doctor').html(event.note);
                
            }
            $('#fullCalModal').modal('show');
        return false;
      },
        eventRender: function(event, element) {            
            var content = "<p class='p-font'>" + event.name +"</p>";
            var content = content + "<p class='p-font'>" + moment(event.start).format("h:mm A") + "</p>";            
            element.html(content);
        }, 
        editable: true, 
        droppable: true 
    });

    // $("#calendar").fullCalendar({
    //     minTime: "10:00:00",
    //     maxTime: "14:00:00",
    //     header: {
    //         left: "prev",
    //         center: "title",
    //         right: "next"
    //     },
    //     views: {
    //         agendaWeek: {
    //             // name of view
    //             titleFormat: "MMMM,YYYY"
    //             // other view-specific options here
    //         }
    //     },
    //     defaultView: "agendaWeek",
    //     defaultDate: "2018-05-28",
    //     navLinks: true, // can click day/week names to navigate views
    //     selectable: true,
    //     selectHelper: true,
    //     select: function(start, end) {
    //         var title = prompt("Event Title:");
    //         var eventData;
    //         if (title) {
    //             eventData = {
    //                 title: title,
    //                 start: start,
    //                 end: end
    //             };
    //             $("#calendar").fullCalendar("renderEvent", eventData, true); // stick? = true
    //         }
    //         $("#calendar").fullCalendar("unselect");
    //     },
    //     eventClick: function(event, jsEvent, view) {
    //         $("#modalTitle").html(event.title);
    //         $("#modalBody").html(event.description);
    //         $("#eventUrl").attr("href", event.url);
    //         $("#fullCalModal").modal();
    //     },

    //     weekNumbers: true,
    //     weekNumbersWithinDays: true,
    //     weekNumberCalculation: "ISO",

    //     editable: true,
    //     eventLimit: true, // allow "more" link when too many events
    //     events: facUrl,
    //     timeFormat: "h:00A(:mm)"
    // });
}