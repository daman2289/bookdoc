<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/home2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
<link rel="stylesheet" href="theme/css/responsive2.css">

<link rel="stylesheet" href="theme/css/owl.carousel.min2.css">
<link rel="stylesheet" href="theme/css/owl.theme.min2.css">
<script src="theme/js/jquery.min.js"></script>
 
<script>
	$(function(){
    $('.selectpicker').selectpicker();
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
</head>

<body class="doct_profile">
<div class="home_banner02">
	<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/white_logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><a href="">List your practice on Bookdoc </a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
          <li class="signin"><a href="">Login</a></li>
          <li>
          	<div class="countrydown">
            	<select class="selectpicker" data-width="fit">
    <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
  <option  data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
</select>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>	
</div>

<div class="doct_profile_section">
	<div class="container">
    	<div class="col-md-8 col-sm-8 col-xs-12 p-0">        	
            <div class="doct_profile_box">
            	<div class="doct_profile_inner">
                	<div class="col-md-12 col-sm-12 col-xs-12 doct_pro_inner">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_01.png" style="max-width:170px">
                            </div>
                            <div class="doctor_details">                            	
                            	<h2 class="doctorname" onclick="window.location=''">Dr. Niks</h2>
                                <h4 class="doctor_designation darkbluecolor">Primary Care Doctor</h4>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div>                               
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>                                
                            </div>
                            <div class="tab">
                            	<ul>
                                	<li><a href="" class="current">Overview</a></li>
                                    <li><a href="">View All Photo</a></li>
                                </ul>
                            </div>
                            
                            <div class="doct_details">
                            	<div class="doct_de_box">
                                	<h3 class="doc_heding">Professional statement</h3>
                                    <div class="doc_content">
                                    	Dr. Andrew Fagelman is board certified with the American Board of Internal Medicine and current physician at SOHO Health NY.
                                    </div>
                                </div>
                                <div class="doct_de_box">
                                	<h3 class="doc_heding">In-network insurances</h3>
                                    <div class="doc_content">
                                    	1199SEIUAARPAetna 
                                    </div>
                                    <div class="dviewal"><a href="">Viewall</a></div>
                                </div>
                                <div class="doct_de_box">
                                	<h3 class="doc_heding">Specialties</h3>
                                    <div class="doc_content">
                                    	Primary Care Doctor
                                    </div>
                                    <div class="doc_map">
                                    	<img src="theme/images/doc_map.png">
                                    </div>                                    
                                    
                                    <div class="sub_box">
                                    	<h3 class="doc_heding">BookDoc Awards</h3>
                                    <div class="doc_content">
                                    	<ul class="awardlist">
                                        	<li><a href=""><span class="awrd_img"><img src="theme/images/awrd_icon.png"></span> See You Again</a></li>
                                            <li><a href=""><span class="awrd_img"><img src="theme/images/awrd_icon.png"></span> Speedy Response</a></li>
                                            <li><a href=""><span class="awrd_img"><img src="theme/images/awrd_icon.png"></span> Scheduling Hero</a></li>
                                        </ul>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>  
                            	<div class="doct_de_box">
                                	<h3 class="doc_heding">Patient reviews</h3>
                                    <div class="doc_content">
                                    	All reviews have been submitted by patients after seeing the provider. For more information, check out our Patient Knowledge Base.
                                    </div>
                                    <div class="rating">
                                    	<ul>
                                        	<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Overall</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Wait Time</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Beside Manner</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="doct_de_box review_listing">   
                                    <div class="reviewrating">
                                    	<ul>
                                        	<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Overall</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Wait Time</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Beside Manner</span></li>
                                        </ul>
                                    </div>
                                    <div class="doc_content">
                                    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatm. 
                                    </div>
                                    <div class="author">
                                    	<span class="reviewdate">March 07, 2018</span>
                                        <span class="reviewby">By a Verified Patient <i class="fa fa-check-circle"></i></span>
                                    </div>
                                </div>
                                <div class="doct_de_box review_listing">   
                                    <div class="reviewrating">
                                    	<ul>
                                        	<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Overall</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Wait Time</span></li>
                                			<li><div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div> <span class="ratingcount">Beside Manner</span></li>
                                        </ul>
                                    </div>
                                    <div class="doc_content">
                                    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequatm. 
                                    </div>
                                    <div class="author">
                                    	<span class="reviewdate">March 07, 2018</span>
                                        <span class="reviewby">By a Verified Patient <i class="fa fa-check-circle"></i></span>
                                    </div>
                                </div>
                                
                        
                            
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 p-0 sidebarbg">
        	<div class="sidebar">
            	<h2 class="appointbook">Book Appointment</h2>
                <div class="fullappoint">
                	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="I’ll choose my insurance later" selected>I’ll choose my insurance later</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div>
                          
                          <div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="What’s the reason for your visit" selected>What’s the reason for your visit</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div>
                </div>
                <div class="slect_date">
            	<div class="navi_date">
                	<ul>
                    	<li><a href=""><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="">Thu 7 April</a></li>
                        <li><a href="">Fri 8 April</a></li>
                        <li><a href="">Sat 9 April</a></li>
                         <li><a href="">Sun 9 April</a></li>
                        <li><a href=""><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
            	<div class="doctor_time_Table bookappoint">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    
                                     <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    <li><a href="">10:30 AM</a></li>
                                    
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li> 
                          			<li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>                        
                                </ul>
                            </div>
                        </div>
                        
                        <div class="bookappointment">
                        	<a href="">Book Now</a>
                        </div>
            </div>
        </div>
    </div>
</div>


<div class="footer">
	<div class="subcription">
    	<div class="container">        	
            	<div class="subcription_box">
                	<h2 class="subcription_heading">Daily Health Tips to your Inbox</h2>
                    <div class="subcription_input_box">
                    	<label class="mailicon"><i class="fa fa-envelope-o" style="font-size:20px; color:#4e5668"></i></label>
                    	<input type="text" class="subcription_input" placeholder="Email">
                        <button class="subc_btn">Subscribe</button>                        
                    </div>
                </div>                 
        </div>
    </div>
    <div class="footer_section">
    	<div class="container">
    	<div class="col-md-9 col-sm-12 col-xs-12">
        	<div class="footer_links">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Look up your doctor</h3>
                        <ul>
                        	<li><a href="">Doctor name</a></li>
                            <li><a href="">Practice name</a></li>
                            <li><a href="">Hospital name</a></li>
                            <li><a href="">Health Q & A </a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Services</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Search By</h3>
                        <ul>
                        	<li><a href="">Specialty</a></li>
                            <li><a href="">Procedure</a></li>
                            <li><a href="">Language</a></li>
                            <li><a href="">Location </a></li>
                            <li><a href="">Insurance</a></li>
                            <li><a href="">Reviews</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Specialties</h3>
                        <ul>
                        	<li><a href="">Chiropractors</a></li>
                            <li><a href="">Dentists</a></li>
                            <li><a href="">Eye Doctors</a></li>
                            <li><a href="">Gynecologists</a></li>
                            <li><a href="">Primary care doctors</a></li>
                            <li><a href="">Psychiatrists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Partner with us</h3>
                        <ul>
                        	<li><a href="">Terms & conditions</a></li>
                            <li><a href="">Trust & Safety</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Mission</a></li>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Press</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
        	<div class="loginsbuttn">
            		<ul>
                    	<li>
                        	<div class="dentist">
                            	<a href="">
                                	<h2>Are you a top doctor or dentist?</h2>
                                    <h4>List of Practice at Bookdoc</h4>
                                </a>
                            </div>
                        </li>
                        <li>
                        	<div class="health">
                            	<a href="">
                                	<h2>Are you a looking for better Health?</h2>
                                    <h4>Join Perfect Health Partners</h4>
                                </a>
                            </div>
                        </li>
                    </ul>
            </div>
        </div>
        
        <div class="col-md-12 col-xs-12 copysoci">
        	<div class="copyright col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
            	© 2018 Bookdoc. All Rights Reserved.
            </div> 
            <div class="social_links col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
                    	<ul>
                        	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
        </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript">	
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});
	
</script>
</body>
</html>