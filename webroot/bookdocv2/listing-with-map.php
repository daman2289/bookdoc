<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/home2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
<link rel="stylesheet" href="theme/css/responsive2.css">

<link rel="stylesheet" href="theme/css/owl.carousel.min2.css">
<link rel="stylesheet" href="theme/css/owl.theme.min2.css">
<script src="theme/js/jquery.min.js"></script>
 
<script>
	$(function(){
    $('.selectpicker').selectpicker();
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
</head>

<body class="listing-with-map">
<div class="home_banner02">
	<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/white_logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><a href="">List your practice on Bookdoc </a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
          <li class="signin"><a href="">Login</a></li>
          <li>
          	<div class="countrydown">
            	<select class="selectpicker" data-width="fit">
    <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
  <option  data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
</select>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
	<div class="search_box">
    	<div class="container">
        	<div class="topsearchbar">
        	<h2 class="serach_tag">Find your Best Health Solution near you</h2>
            <div class="topserach">
            	<div class="forms">
               	  <form>
                  		<label class="search_form_icon"><i class="icon doctor_icon"><img src="theme/images/serch_icon01.png"></i></label>
                        <div class="serachinput first-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="" selected>Condition or Doctor name</option>
                                <option value="Doctor Name 01">Doctor Name 01</option>
                                <option value="Doctor Name 02">Doctor Name 02</option>
                                <option value="Doctor Name 03">Doctor Name 03</option>
                              </select>
                              </label>
                          </div>
                        </div>  	
                        <label class="search_form_icon"><i class="icon location_icon"><img src="theme/images/serch_icon02.png"></i></label>
                        <div class="serachinput second-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>Zip Code</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div>
                        </div>  	
                        <div class="serachinput">
                        	<div class="select-style">
                            <label class="css_select_lable">
                            <select>
                                <option value="I’ll Choose My Insurance Later">I’ll Choose My Insurance Later</option>
                                <option value="saab">Saab</option>
                                <option value="mercedes">Mercedes</option>
                                <option value="audi">Audi</option>
                              </select>
                              </label>
                          </div>
                        </div> 
                        <button class="serachbtn"><i class="fa fa-search"></i></button> 	
                        
                  </form>
                </div>
            </div>
        </div>
        </div>
    </div>  
</div>

<div class="listing_with_map_section">
	<div class="container-fluid">
    	<div class="col-md-8 col-sm-8 col-xs-12">
        	<div class="filters">
            	<div class="filter01">
                	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="" selected>General Consultation</option>
                                <option value="General Consultation 01">General Consultation 01</option>
                                <option value="General Consultation 02">General Consultation 02</option>
                                <option value="General Consultation 03">General Consultation 03</option>
                              </select>
                              </label>
                          </div>
                </div>
                <div class="filter02">
                	<div class="select-style">
                        <label class="css_select_lable">
                           <select>
                              <option value="" selected>Any Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="filter03">
                	<div class="days">
                    	<ul>
                        	<li><a href="">Any Day</a></li>
                            <li><a href="">Today</a></li>
                            <li><a href="">Next 3 Days</a></li>
                        </ul>
                    </div>
                </div>                
                <div class="filter04">
                	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="" selected>Select Time</option>
                                <option value="Doctor Name 01">Doctor Name 01</option>
                                <option value="Doctor Name 02">Doctor Name 02</option>
                                <option value="Doctor Name 03">Doctor Name 03</option>
                              </select>
                              </label>
                          </div>
                </div>
            </div>
            <div class="slect_date">
            	<div class="navi_date">
                	<ul>
                    	<li><a href=""><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="">Mon 7 April</a></li>
                        <li><a href="">Tue 8 April</a></li>
                        <li><a href="">Wed 9 April</a></li>
                        <li><a href=""><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="doc_listings">
            	<div class="doclist">
                	<div class="col-md-8 col-sm-12 col-xs-12 doc_profile">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doctor_1.png">
                            </div>
                            <div class="doctor_details">                            	
                            	<h2 class="doctorname" onclick="window.location='patient-editprofile.php'">Dr. Berret  <div class="markfav"><a href=""></a></div></h2>
                                <h4 class="doctor_designation">Internist</h4>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="deatils">“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore” ...<a href="" class="redcolor">read more</a></div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a id="map_btn">Show Location <i class="fa fa-angle-down"></i></a></div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 doc_time">
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>                                   
                                </ul>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="doclist">
                	<div class="col-md-8 col-sm-12 col-xs-12 doc_profile">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doctor_1.png">
                            </div>
                            <div class="doctor_details">                            	
                            	<h2 class="doctorname" onclick="window.location='patient-editprofile.php'">Dr. Berret  <div class="markfav"><a href=""></a></div></h2>
                                <h4 class="doctor_designation">Internist</h4>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="deatils">“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore” ...<a href="" class="redcolor">read more</a></div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a id="map_btn">Show Location <i class="fa fa-angle-down"></i></a></div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 doc_time">
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>                                   
                                </ul>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="doclist">
                	<div class="col-md-8 col-sm-12 col-xs-12 doc_profile">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doctor_1.png">
                            </div>
                            <div class="doctor_details">                            	
                            	<h2 class="doctorname" onclick="window.location='patient-editprofile.php'">Dr. Berret  <div class="markfav"><a href=""></a></div></h2>
                                <h4 class="doctor_designation">Internist</h4>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="deatils">“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore” ...<a href="" class="redcolor">read more</a></div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a id="map_btn">Show Location <i class="fa fa-angle-down"></i></a></div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 doc_time">
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>                                   
                                </ul>
                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="doclist">
                	<div class="col-md-8 col-sm-12 col-xs-12 doc_profile">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doctor_1.png">
                            </div>
                            <div class="doctor_details">                            	
                            	<h2 class="doctorname" onclick="window.location='patient-editprofile.php'">Dr. Berret  <div class="markfav"><a href=""></a></div></h2>
                                <h4 class="doctor_designation">Internist</h4>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="deatils">“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore” ...<a href="" class="redcolor">read more</a></div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a id="map_btn">Show Location <i class="fa fa-angle-down"></i></a></div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 doc_time">
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>
                                    <li><div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>More</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div></li>                                   
                                </ul>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
            
            <div class="pagination-wrapper">
  <div class="pagination">
    <!--<a class="prev page-numbers" href="javascript:;">prev</a>-->
    <span aria-current="page" class="page-numbers current">1</span>
    <a class="page-numbers" href="javascript:;">2</a>
    <a class="page-numbers" href="javascript:;">3</a>
    <a class="page-numbers" href="javascript:;">4</a>
    <a class="page-numbers" href="javascript:;">5</a>
    <a class="page-numbers" href="javascript:;">6</a>
    <a class="page-numbers" href="javascript:;">7</a>
    <a class="page-numbers" href="javascript:;">8</a>
    <a class="page-numbers" href="javascript:;">9</a>
    <a class="page-numbers" href="javascript:;">10</a>
    <a class="next page-numbers" href="javascript:;">Next <i class="fa fa-angle-right"></i></a>
  </div>
</div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="sidebarmap">
            	<img src="theme/images/sidebar_map.jpg">
            </div>
        </div>
    </div>
</div>


<div class="footer">
	<div class="subcription">
    	<div class="container">        	
            	<div class="subcription_box">
                	<h2 class="subcription_heading">Daily Health Tips to your Inbox</h2>
                    <div class="subcription_input_box">
                    	<label class="mailicon"><i class="fa fa-envelope-o" style="font-size:20px; color:#4e5668"></i></label>
                    	<input type="text" class="subcription_input" placeholder="Email">
                        <button class="subc_btn">Subscribe</button>                        
                    </div>
                </div>                 
        </div>
    </div>
    <div class="footer_section">
    	<div class="container">
    	<div class="col-md-9 col-sm-12 col-xs-12">
        	<div class="footer_links">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Look up your doctor</h3>
                        <ul>
                        	<li><a href="">Doctor name</a></li>
                            <li><a href="">Practice name</a></li>
                            <li><a href="">Hospital name</a></li>
                            <li><a href="">Health Q & A </a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Services</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Search By</h3>
                        <ul>
                        	<li><a href="">Specialty</a></li>
                            <li><a href="">Procedure</a></li>
                            <li><a href="">Language</a></li>
                            <li><a href="">Location </a></li>
                            <li><a href="">Insurance</a></li>
                            <li><a href="">Reviews</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Specialties</h3>
                        <ul>
                        	<li><a href="">Chiropractors</a></li>
                            <li><a href="">Dentists</a></li>
                            <li><a href="">Eye Doctors</a></li>
                            <li><a href="">Gynecologists</a></li>
                            <li><a href="">Primary care doctors</a></li>
                            <li><a href="">Psychiatrists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Partner with us</h3>
                        <ul>
                        	<li><a href="">Terms & conditions</a></li>
                            <li><a href="">Trust & Safety</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Mission</a></li>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Press</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
        	<div class="loginsbuttn">
            		<ul>
                    	<li>
                        	<div class="dentist">
                            	<a href="">
                                	<h2>Are you a top doctor or dentist?</h2>
                                    <h4>List of Practice at Bookdoc</h4>
                                </a>
                            </div>
                        </li>
                        <li>
                        	<div class="health">
                            	<a href="">
                                	<h2>Are you a looking for better Health?</h2>
                                    <h4>Join Perfect Health Partners</h4>
                                </a>
                            </div>
                        </li>
                    </ul>
            </div>
        </div>
        
        <div class="col-md-12 col-xs-12 copysoci">
        	<div class="copyright col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
            	© 2018 Bookdoc. All Rights Reserved.
            </div> 
            <div class="social_links col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
                    	<ul>
                        	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
        </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript">	
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});
	
</script>
</body>
</html>