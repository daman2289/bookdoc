<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/style02.css">
<link rel="stylesheet" href="theme/css/responsive.css">
<link rel="stylesheet" href="theme/css/owl.carousel.min.css">
<link rel="stylesheet" href="theme/css/owl.theme.min.css">

<script src="theme/js/jquery.min.js"></script>
<script src="theme/js/jquery.flexslider.min.js"></script>
<script type="text/javascript" src="theme/js/owl.carousel.min.js"></script>
</head>

<body class="about-page">
<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/green-logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><i class="fa fa-phone green_icon"></i> <a href="tel:124 567 890">124 567 890</a></li>
          <li><a href="">Login</a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="banner_section">
  <div class="banner_img" style="background:#cdfff1;"><div class="container"> <img src="theme/images/banner_doc_img.png" class="banner_img"> </div></div>
  <div class="book_appointment_section">
    <div class="wrapper">
<div class="col-md-7">
   
      <h1 class="serach_heading">Your Practice Made Practitioner Perfect.</h1>
      <div class="heading_content">
      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        <div class="butn"><a href="">List Your Practice</a></div>
      </div>
      </div>
    </div>
  </div>
</div>
<div class="whyjoin_section">
	<div class="container">
    	<div class="heading">
        	<h1 class="section_heading">Why Do Doctors Join Us</h1>      
        </div>    
        <div class="work_group">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/ab_icon01.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Build Pateint Loyalty</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/ab_icon02.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Attract The Patients You Need</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/ab_icon03.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Maximize Your Availability</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/ab_icon04.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Strengthen Your Reputation</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="doctor_country">
	<div class="container">
    	<div class="heading">
        	<h1 class="section_heading">Doctor Across The Country</h1>      
        </div> 
        <div class="find_doc">
               	<form>
                	<label class="find_doc_label">Show Me</label>
                   <div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Insurance">Insurance </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
              <label class="find_doc_label">In</label>
              <div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Insurance">11222 </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
                    <div class="search_btn_div">
                	<button class="searchbtn"><i class="fa fa-search serch_icon"></i></button>
                </div>
                </form>
               </div>
               <div class="doc_slider">
               		<div class="cd-doctor-wrapper cd-container">
                   <ul class="cd-doc-slider">
                        <li>
                       <div class="doctor-slider-content">
                           <div class="doctor_img">
                           	<img src="theme/images/doc_01.png">
                           </div> 
                            <div class="doctor-slider_name">
                            	<h3 class="doc_name">Sarah Ben</h3>
                                <h4 class="doc_type">Osteopathy</h4>
                            </div>
                          </div>
                     </li>
                        <li>
                       <div class="doctor-slider-content">
                           <div class="doctor_img">
                           	<img src="theme/images/doc_02.png">
                           </div> 
                            <div class="doctor-slider_name">
                            	<h3 class="doc_name">Mr David Landon</h3>
                                <h4 class="doc_type">MD, FAAD</h4>
                            </div>
                          </div>
                     </li>
                        <li>
                       <div class="doctor-slider-content">
                           <div class="doctor_img">
                           	<img src="theme/images/doc_03.png">
                           </div> 
                            <div class="doctor-slider_name">
                            	<h3 class="doc_name">Mr David Landon</h3>
                                <h4 class="doc_type">Osteopathy</h4>
                            </div>
                          </div>
                     </li>
                     <li>
                       <div class="doctor-slider-content">
                           <div class="doctor_img">
                           	<img src="theme/images/doc_04.png">
                           </div> 
                            <div class="doctor-slider_name">
                            	<h3 class="doc_name">Mr David Landon</h3>
                                <h4 class="doc_type">Osteopathy</h4>
                            </div>
                          </div>
                     </li>
                     
                     
                      </ul>
                 </div>
               </div>
    </div>
</div>

<div class="app_features">
	<div class="container">
    	<div class="heading text-center">
        	<h1 class="section_heading">App Features</h1>      
        </div>
        <div class="app_featureslist">
        	<div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 right-align">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="box-icon">
                           <img src="theme/images/app_icon_01.png">
                        </div>
                        <h4>Custom Bookdoc Profile</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="theme/images/app_icon_02.png">
                        </div>
                        <h4>Unlimited Locations</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                        <div class="box-icon">
                           <img src="theme/images/app_icon_03.png">
                        </div>
                        <h4>Patient Reviews</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                </div>
                <div class="hidden-xs hidden-sm col-md-4 center-align">
                    <figure class="mobile-image">
                        <img src="theme/images/app.png" alt="Feature Photo">
                    </figure>
                     <div class="service-box wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="theme/images/app_icon_04.png">
                        </div>
                        <h4>Email And Text Reminders</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 left-align">
                    <div class="service-box wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="theme/images/app_icon_07.png">
                        </div>
                        <h4>You Own Search Listing</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <div class="box-icon">
                           <img src="theme/images/app_icon_06.png">
                        </div>
                        <h4>24/7 Online Booking</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                    <div class="service-box wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                        <div class="box-icon">
                            <img src="theme/images/app_icon_05.png">
                        </div>
                        <h4>Professional Photos</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="Clients">
	<div class="container">
    	<div class="client_box">
        	<div class="heading">
        	<h1 class="section_heading">Clients</h1>      
        	</div> 
            <div class="client_list">
            	<ul>
                	<li><img src="theme/images/client01.png"></li>
                    <li><img src="theme/images/client02.png"></li>
                    <li><img src="theme/images/client03.png"></li>
                    <li><img src="theme/images/client04.png"></li>
                    <li><img src="theme/images/client05.png"></li>
                </ul>
            </div>
        </div>
        <div class="half_box">
        	<div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="signupbox">
                	<h3 class="sign_text">Interested? Learn More Today</h3>
                    <div class="butn">
                    	<a href="">Sign Up For Free Demo</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            	<div class="signupbox">
                	<h3 class="sign_text">How Patients Find You?</h3>
                    <div class="butn">
                    	<a href="">Create Your Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonial_section">
	<div class="container">
    	<h1 class="tag">Doctors Thrive With BookDoc.</h1>
        
        <div class="testimonials">
        	<div class="centertestimonial">
            	<div class="quote_icon">
                	<img src="theme/images/quote.png">
                    
                    <div class="testicontain">
                    	<p>BookDoc helps me to achieve what i set out to do as a doctor.</p>
                        <div class="author">Steven Stoll, MD, Philadelphia</div>
                        
                        <div class="butn"><a href="">See Dr. Trend Story</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="smart_phone_section">
	<div class="container">    	
        <div class="col-md-7 col-sm-7 col-xs-12">
        	<div class="smart_phone_detail">
            	<div class="smat_tag">Download Our App <br/> On Your Phone.</div>
                <div class="smart_content">
                	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minii ut aliquip ex ea commodo consequat.</p>
                </div>  
                <div class="playstore_links">
                	<ul class="playstore">
                    	<li><a href=""><img src="theme/images/googleplay.png"></a></li>
                        <li><a href=""><img src="theme/images/appstore.png"></a></li>
                    </ul>
                </div>              
            </div>
                 </div>
                 <div class="col-md-5 col-sm-3 col-xs-12 pull-left">
        	<div class="smrt_phone">
            	<img src="theme/images/smart_phone.png">
            </div>
        </div>
    </div>
</div>
<div class="subscription">
	<div class="container">
    	<div class="subscription_box">
        	<div class="heading">
        	<h1 class="section_heading">Subscribe Us</h1>      
        	</div> 
            <div class="subcrip_box">
            	<input type="text" class="sub_input" placeholder="Email Address">
                <button class="sub_btn">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<div class="footer">
	<div class="container">
    	<div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>        
        <div class="copy">
        	<div class="copyright">
            	<img src="theme/images/green-logo.png">
                <div class="copytext">© 2001–2018 All Rights Reserved. Bookdoc® is a registered trademark.</div>
            </div>
            <div class="social">
            	<ul>
                	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script src="theme/js/bootstrap.min.js"></script>
<script>
jQuery(document).ready(function($) {
	//create the slider
	$(window).trigger('resize');
	$('.cd-doctor-wrapper').flexslider({
		selector: ".cd-doc-slider > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		itemWidth: 180,
		itemMargin: 5,
    	minItems: 2,
    	maxItems: 3,
		move: 1,	
		smoothHeight: true,
		start: function() {
			$('.cd-doc-slider').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
			// tiny helper function to add breakpoints
  function getGridSize() {
    return (window.innerWidth < 600) ? 2 :
           (window.innerWidth < 900) ? 3 : 4; 
  }
		}
		
		
		
	});
});
</script>
</body>
</html>