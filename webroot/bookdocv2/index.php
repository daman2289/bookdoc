<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/style02.css">
<link rel="stylesheet" href="theme/css/responsive.css">
<script src="theme/js/jquery.min.js"></script>

</head>

<body>
<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/green-logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><i class="fa fa-phone green_icon"></i> <a href="tel:124 567 890">124 567 890</a></li>
          <li><a href="">Login</a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="banner_section">
  <div class="banner_img" style="background:#cdfff1;"><div class="container"> <img src="theme/images/banner_doc_img.png" class="banner_img"> </div></div>
  <div class="book_appointment_section">
    <div class="wrapper">
<div class="search_group">
   
      <h1 class="serach_heading">Book Your Appointment</h1>
      <div class="seach_field_group">
       
          <div class="container">
            <div class="col-md-5 col-sm-3 col-xs-12">
              <div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Specialist, Condition"> Specialist, Condition</option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
            	<div class="search_field">
               <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Location"> Location</option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
            	<div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Insurance">Insurance </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-1 col-sm-3 col-xs-12">
            	<div class="search_btn_div">
                	<button class="searchbtn"><i class="fa fa-search serch_icon"></i></button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="work_section">
	<div class="container">
    	<div class="heading">
        	<h1 class="section_heading">How Does it work</h1>      
        </div>    
        <div class="work_group">
            	<div class="col-md-4 col-sm-4 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/serach_icon.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Search</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/docter_list.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Docter List</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                	<div class="work_list_box">
                    	<div class="work_list_icon">
                        	<a href=""><img src="theme/images/book_icon.png"></a>
                        </div>
                        <div class="work_list_detail">
                        	<h2 class="work_tag">Book</h2>
                            <div class="content">
                            	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="smart_phone_section">
	<div class="container">    	
        <div class="col-md-7 col-sm-7 col-xs-12">
        	<div class="smart_phone_detail">
            	<div class="smat_tag">Download Our App <br/> On Your Phone.</div>
                <div class="smart_content">
                	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minii ut aliquip ex ea commodo consequat.</p>
                </div>  
                <div class="playstore_links">
                	<ul class="playstore">
                    	<li><a href=""><img src="theme/images/googleplay.png"></a></li>
                        <li><a href=""><img src="theme/images/appstore.png"></a></li>
                    </ul>
                </div>              
            </div>
                 </div>
                 <div class="col-md-5 col-sm-3 col-xs-12 pull-left">
        	<div class="smrt_phone">
            	<img src="theme/images/smart_phone.png">
            </div>
        </div>
    </div>
</div>
<div class="subscription">
	<div class="container">
    	<div class="subscription_box">
        	<div class="heading">
        	<h1 class="section_heading">Subscribe Us</h1>      
        	</div> 
            <div class="subcrip_box">
            	<input type="text" class="sub_input" placeholder="Email Address">
                <button class="sub_btn">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<div class="footer">
	<div class="container">
    	<div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        
        <div class="copy">
        	<div class="copyright">
            	<img src="theme/images/green-logo.png">
                <div class="copytext">© 2001–2018 All Rights Reserved. Bookdoc® is a registered trademark.</div>
            </div>
            <div class="social">
            	<ul>
                	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>