<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/home.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
<link rel="stylesheet" href="theme/css/responsive.css">
<link rel="stylesheet" type="text/css" href="theme/css/easy-responsive-tabs.css " />
<link rel="stylesheet" href="theme/css/owl.carousel.min.css">
<link rel="stylesheet" href="theme/css/owl.theme.min.css">
<script src="theme/js/jquery.min.js"></script>
 <script src="theme/js/easyResponsiveTabs.js"></script>
<script>
	$(function(){
    $('.selectpicker').selectpicker();
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
</head>

<body class="homepage">
<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/white_logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><a href="">List your practice on Bookdoc </a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
          <li>
          	<div class="countrydown">
            	<select class="selectpicker" data-width="fit">
    <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
  <option  data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
</select>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="smallbanner">
	<img src="theme/images/new-home_banner.jpg">
</div>
<div class="homeSearch_box">
	<div class="container">
    	<div class="outercolor">
    	<div class="topsearchbar">
        	<h2 class="serach_tag">Find your Best Health  Solution near you</h2>
            <div class="topserach">
            	<div class="forms">
               	  <form>
                  		<label class="search_form_icon"><i class="icon doctor_icon"><img src="theme/images/serch_icon01.png"></i></label>
                        <div class="serachinput first-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="" selected>Condition or Doctor name</option>
                                <option value="Doctor Name 01">Doctor Name 01</option>
                                <option value="Doctor Name 02">Doctor Name 02</option>
                                <option value="Doctor Name 03">Doctor Name 03</option>
                              </select>
                              </label>
                          </div>
                        </div>  	
                        <label class="search_form_icon"><i class="icon location_icon"><img src="theme/images/serch_icon02.png"></i></label>
                        <div class="serachinput second-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>Zip Code</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div>
                        </div>  	
                        <div class="serachinput">
                        	<div class="select-style">
                            <label class="css_select_lable">
                            <select>
                                <option value="I’ll Choose My Insurance Later">I’ll Choose My Insurance Later</option>
                                <option value="saab">Saab</option>
                                <option value="mercedes">Mercedes</option>
                                <option value="audi">Audi</option>
                              </select>
                              </label>
                          </div>
                        </div> 
                        <button class="serachbtn"><i class="fa fa-search"></i></button> 	
                        
                  </form>
                </div>
            </div>
        </div>
        
        <div class="doctabs">  
        		<div class="heading">
                	<h1 class="tags"><span class="bluecolor">Find doctors and</span> dentists by</h1>
                </div>
              	
        	<div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>City</li>
                <li>Specialty</li>
                <li>Insurance</li>
              </ul>
            <div class="resp-tabs-container hor_1">
                <div>
                <div class="contentlink">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>
                              
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                            	<li><a href="">New York</a></li>
                                <li><a href="">Philadelphia</a></li>
                                <li><a href="">Phoenix</a></li>
                                <li><a href="">San Antonio</a></li>
                                <li><a href="">San Diego</a></li>
                                <li><a href="">San Francisco</a></li>
                                <li><a href="">Washington, DC</a></li>
                            </ul>
                        </div>
                    </div>
                    
                  </div>
              </div>
                <div>
               <div class="contentlink">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>
                              
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                            	<li><a href="">New York</a></li>
                                <li><a href="">Philadelphia</a></li>
                                <li><a href="">Phoenix</a></li>
                                <li><a href="">San Antonio</a></li>
                                <li><a href="">San Diego</a></li>
                                <li><a href="">San Francisco</a></li>
                                <li><a href="">Washington, DC</a></li>
                            </ul>
                        </div>
                    </div>
                    
                  </div>
              </div>
                <div>
               <div class="contentlink">
                	<div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>
                              
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    	<div class="realtedlinks">
                        	<ul>
                            	<li><a href="">New York</a></li>
                                <li><a href="">Philadelphia</a></li>
                                <li><a href="">Phoenix</a></li>
                                <li><a href="">San Antonio</a></li>
                                <li><a href="">San Diego</a></li>
                                <li><a href="">San Francisco</a></li>
                                <li><a href="">Washington, DC</a></li>
                            </ul>
                        </div>
                    </div>
                    
                  </div>
              </div>
              </div>
          </div>
        </div>                
        </div>
    </div>
</div>

<div class="bookdoc_apps">
	<div class="container">
    	<div class="col-md-6 col-sm-6 col-xs-12">
        	<div class="mobile_img">
            	<img src="theme/images/join-book_img.png">
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	<div class="app_content">
            	<div class="heading">
                	<h1 class="brktag">Get the <span class="bluecolor bold">Bookdoc app.</span></h1>
                </div>
                <div class="points">
                	<h4>We've got you covered</h4>
                    <div class="uniquepoint">
                    	<ul>
                        	<li><div class="icons"><img src="theme/images/doct_bag_icon.png"></div> <div class="point_detail">We cover different medical specialties</div></li>
                            <li><div class="icons"><img src="theme/images/doc_thethos_icon.png"></div> <div class="point_detail">Verfied Professional Doctors</div></li>
                            <li><div class="icons"><img src="theme/images/safe-icon.png"></div> <div class="point_detail">Safe and Secure Appointments</div></li>
                        </ul>
                    </div>
                    <div class="appstore">
                    	<ul>
                        	<li><a href=""><img src="theme/images/googleplay.png"></a></li>
                            <li><a href=""><img src="theme/images/appstore.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fivestart_doctor">
	<div class="container">
    	<div class="doctor_details">
        	<div class="doctor_d col-md-6 col-sm-6 col-xs-12">
            	<div class="heading">
                	<h1 class="brktag">Are You a<span class="whitecolor bold">five star doctor?</span></h1>
                    <p class="whitecolor">List your practice to reach millions of patients.</p>
                </div>
                <div class="doctor_points">
                	<ul>
                    	<li>Attract and engage new patients</li>
                        <li>Build and strengthen your online reputation</li>
                        <li>Deliver a premium experience patients love</li>
                    </ul>
                </div>
                <div class="gradiant_btn">
                	<a href="">LIST YOUR PRACTICE ON BOOKDOC</a>
                </div>
                
                
            </div>
            <div class="doctor_im col-md-6 col-sm-6 col-xs-12">
            	<img src="theme/images/doc_img.png">
            </div>
        </div>
    </div>
</div>

<div class="health_services">
	<div class="container">
    	<div class="h_services">
        	<div class="heading">
            	<h1 class="tag">BEST <span class="bluecolor bold">HEALTH SERVICES</span> AT <br/> RESONABLE <span class="bluecolor bold">PRICES</span></h1>
                <p>Need help? Visit our Knowledge base</p>
            </div>
            <div class="gradiant_blue_butn">
            	<a href="">Join us for care</a>
            </div>
            <div class="group_img">
            	<img src="theme/images/doc_group.png">
            </div>
        </div>
    </div>
</div>

<div class="client_testi">
	<div class="container">
    	<div class="heading">
                	<h1 class="tags"><span class="bluecolor">OUR PATIENT'S</span> SPEAK</h1>
                </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <div class="pic">
                        <img src="theme/images/testimonial_author_img.png" alt="">
                    </div>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur.
                    </p>
                    <h3 class="title">Mr. David Landon
                        <span class="post">Osteopathy</span>
                    </h3>
                </div>
 
                <div class="testimonial">
                    <div class="pic">
                        <img src="theme/images/testimonial_author_img.png" alt="">
                    </div>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur.
                    </p>
                    <h3 class="title">Mr. David Landon
                        <span class="post"> Osteopathy</span>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="footer">
	<div class="subcription">
    	<div class="container">        	
            	<div class="subcription_box">
                	<h2 class="subcription_heading">Daily Health Tips to your Inbox</h2>
                    <div class="subcription_input_box">
                    	<label class="mailicon"><i class="fa fa-envelope-o" style="font-size:20px; color:#4e5668"></i></label>
                    	<input type="text" class="subcription_input" placeholder="Email">
                        <button class="subc_btn">Subscribe</button>                        
                    </div>
                </div>                 
        </div>
    </div>
    <div class="footer_section">
    	<div class="container">
    	<div class="col-md-9 col-sm-12 col-xs-12">
        	<div class="footer_links">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Look up your doctor</h3>
                        <ul>
                        	<li><a href="">Doctor name</a></li>
                            <li><a href="">Practice name</a></li>
                            <li><a href="">Hospital name</a></li>
                            <li><a href="">Health Q & A </a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Services</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Search By</h3>
                        <ul>
                        	<li><a href="">Specialty</a></li>
                            <li><a href="">Procedure</a></li>
                            <li><a href="">Language</a></li>
                            <li><a href="">Location </a></li>
                            <li><a href="">Insurance</a></li>
                            <li><a href="">Reviews</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Specialties</h3>
                        <ul>
                        	<li><a href="">Chiropractors</a></li>
                            <li><a href="">Dentists</a></li>
                            <li><a href="">Eye Doctors</a></li>
                            <li><a href="">Gynecologists</a></li>
                            <li><a href="">Primary care doctors</a></li>
                            <li><a href="">Psychiatrists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Partner with us</h3>
                        <ul>
                        	<li><a href="">Terms & conditions</a></li>
                            <li><a href="">Trust & Safety</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Mission</a></li>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Press</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
        	<div class="loginsbuttn">
            		<ul>
                    	<li>
                        	<div class="dentist">
                            	<a href="">
                                	<h2>Are you a top doctor or dentist?</h2>
                                    <h4>List of Practice at Bookdoc</h4>
                                </a>
                            </div>
                        </li>
                        <li>
                        	<div class="health">
                            	<a href="">
                                	<h2>Are you a looking for better Health?</h2>
                                    <h4>Join Perfect Health Partners</h4>
                                </a>
                            </div>
                        </li>
                    </ul>
            </div>
        </div>
        
        <div class="col-md-12 col-xs-12 copysoci">
        	<div class="copyright col-md-6 pl-0 pr-0 col-xs-12">
            	© 2018 Bookdoc. All Rights Reserved.
            </div> 
            <div class="social_links col-md-6 pl-0 pr-0 col-xs-12">
                    	<ul>
                        	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
        </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
	
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});
	
</script>
</body>
</html>