<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/style02.css">
<link rel="stylesheet" href="theme/css/responsive.css">
<link rel="stylesheet" href="theme/css/accordion.css">
<link rel="stylesheet" href="theme/css/owl.carousel.min.css">
<link rel="stylesheet" href="theme/css/owl.theme.min.css">

<script type="text/javascript" src="theme/js/jquery.min.js"></script>

<script src="theme/js/jquery.flexslider.min.js"></script>
<script type="text/javascript" src="theme/js/owl.carousel.min.js"></script>
<script>
	(function($) {

  'use strict';

  $(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
    var $target = $(e.target);
    var $tabs = $target.closest('.nav-tabs-responsive');
    var $current = $target.closest('li');
    var $parent = $current.closest('li.dropdown');
		$current = $parent.length > 0 ? $parent : $current;
    var $next = $current.next();
    var $prev = $current.prev();
   
    $tabs.find('>li').removeClass('next prev');
    $prev.addClass('prev');
    $next.addClass('next');
  });

})(jQuery);

//
</script>
</head>

<body>
<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/green-logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><i class="fa fa-phone green_icon"></i> <a href="tel:124 567 890">124 567 890</a></li>
          <li><a href="">Login</a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="mainbody">	
    
    <div class="doctor_profile_doctors">
    	<div class="doctor_profile">
        	<div class="container border pr-0">
            	<div class="col-md-8 col-sm-4 ol-xs-12">
        	  <div class="doctor_profile_details">
                <div class="doctor_detail_part">
                  <div class="doctor_img"> 
                  <img src="theme/images/doc_01.png"> 
                  	<div class="allphoto_btn">
                    	<a href="">View all Photos</a>
                    </div>
                  </div>
                  <div class="doctor_details">
                    <h2 class="doctorname">Dr. Niks</h2>
                    <div class="groupstar"> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> </div>
                    <div class="deatils">“Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.”</div>
                    <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                    <div class="add_map"><i class="icon fa fa-map-marker"></i> 128 Mott Street, #507, New York, NY, 10013</div>
                   	<div class="doctor_profiledetail"><i class="icon fa fa-bank"></i> Dental School - State University of New York</div>
                    <div class="doctor_profiledetail"><i class="icon fa fa-bank"></i> Dental Hospital Center, Residency in General Practice Englewood Dental, The Center for Implants and Aesthetics, Residency in Impant Dentistry.</div>
                    
                    
                  </div>
                 
                </div>
              </div>
              <div class="doctor_map">
            	<img src="theme/images/map.jpg">
            </div>
            	</div>
                
                <div class="col-md-4 col-sm-4 col-xs-12 light-graybg ">
                	<div class="bookappointment">
                    	<h2 class="heading_Text">Book Appointment</h2>
                        <div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li class="blocked"><a href="">Blocked</a></li>
                                    <li class="blocked"><a href="">Blocked</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li class="blocked"><a href="">Blocked</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li class="blocked"><a href="">Blocked</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                        
                        <div class="bookapp">
                        <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                            <div class="search_field">
                            <label class="css_select_lable">
                                <select class="css_select">
                                <option selected="Insurance"> I'l choose my insurance later </option>
                                <option>Short Option</option>
                                <option>This Is A Longer Option</option>
                              </select>
                              </label>
                          </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                            <div class="search_field">
                            <label class="css_select_lable">
                                <select class="css_select">
                                <option selected="Insurance"> What’s the reason for your visit </option>
                                <option>Short Option</option>
                                <option>This Is A Longer Option</option>
                              </select>
                              </label>
                          </div>
                          </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                            <div class="booknow_btn_div">
                            <button class="booknow_btn">Book Now</button>
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
                
            </div>            
        </div>        	
        </div>
        
        
     <div class="doctor_profile_tab">
     	<div class="container">
        	<ul class="demo-accordion accordionjs" data-active-index="2">
            
            <!-- Section 1 -->
            <li>
                <div>
                <h3> Languages Spoken</h3>
              </div>
                <div class="acc_content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget ultrices lacus, vel suscipit ligula. Duis a eros ut nisl consectetur porta. Integer ut nulla eget leo elementum blandit at et lacus. </p>
                <p>Suspendisse dui felis, posuere non neque sed, semper consequat metus. Donec non aliquet lectus. Donec vitae euismod dolor, eu maximus metus. Etiam euismod ligula vulputate, sagittis nunc fringilla, faucibus dui.</p>
              </div>
              </li>
            
            <!-- Section 2 -->
            <li>
                <div>
                <h3> Professional Statement</h3>
              </div>
                <div class="acc_content">
                <p><strong>Dr. Rong graduated from Franklin D. Roosevelt High School</strong> with the New York State Presidential Award in Chemistry and then went on to State University of New York (SUNY) at Stony Brook to get her Bachelors of Science in Biochemistry. She graduated Stony Brook undergraduate college Magna Cum Laude and Phi Beta Kappa, and attended SUNY at Stony Brook School of Dental Medicine. Following dental school, <strong>Dr. Rong entered General Practice Residency at Mount Sinai Queens Hospital Center</strong>. Then Dr. Rong went on to finish two years of implant training at Englewood Implant Center and two years of orthodontic training at Progressive Orthodontic Seminars CA.</p>
                <p>A career in dentistry demands great perseverance and dedication to both scientific and humanistic knowledge for the sake of patients’ well-being. <strong>Dr. Rong</strong> has acquired these characteristics through her academics, residency and working experiences over the years. She strongly believes that high level of continuous education and training delivers the best quality of dental care to patients. Dr. Rong </p>
               
              </div>
              </li>
            
            <!-- Section 3 -->
            <li>
                <div>
                <h3>Education</h3>
              </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget ultrices lacus, vel suscipit ligula. Duis a eros ut nisl consectetur porta. Integer ut nulla eget leo elementum blandit at et lacus. Donec ornare rutrum odio vel rutrum. Etiam dapibus velit augue, eu congue sapien bibendum non.</p>
              </li>
            
            <!-- Section 4 -->
            <li>
                <div>
                <h3>Professional Memberships</h3>
              </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget ultrices lacus, vel suscipit ligula. Duis a eros ut nisl consectetur porta. Integer ut nulla eget leo elementum blandit at et lacus. Donec ornare rutrum odio vel rutrum. Etiam dapibus velit augue, eu congue sapien bibendum non.</p>
              </li>
              
               <!-- Section 5 -->
            <li>
                <div>
                <h3>Awards and Publications</h3>
              </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget ultrices lacus, vel suscipit ligula. Duis a eros ut nisl consectetur porta. Integer ut nulla eget leo elementum blandit at et lacus. Donec ornare rutrum odio vel rutrum. Etiam dapibus velit augue, eu congue sapien bibendum non.</p>
              </li>
              
               <!-- Section 6-->
            <li>
                <div>
                <h3>In-Network Insurances</h3>
              </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget ultrices lacus, vel suscipit ligula. Duis a eros ut nisl consectetur porta. Integer ut nulla eget leo elementum blandit at et lacus. Donec ornare rutrum odio vel rutrum. Etiam dapibus velit augue, eu congue sapien bibendum non.</p>
              </li>
          </ul>
        </div>
     </div> 
     
     <div class="doctor_profile_reviews">
     	<div class="container-fluid">
        	<div class="doctor_review_slider review_heading">
            	<div class="container">
                	<div class="heading text-center">
        				<h1 class="section_heading">Verified Patient Reviews
                        	<span class="subheading">4.5 out of 5 stars</span> 
                            
                        </h1> 
                            
       				</div>
                </div>            	
            
            	<div class="doctor_rating_slider">
                        <div id="testimonial-slider" class="owl-carousel">
                        <div class="testimonial">
                            <p class="description"> Dr, Rong is one in a million of in the world of dentistry. She provides excellent care, with her gentle mood and expertise. Amazing staff all around that serve with professionalism and compassion. I have been coming to her for 6 years for all of my dental needs. Super plus for me that she speaks English, Cantonese and Mandarin. </p>
                            <h3 class="date">March 07, 2018</h3>
                            <h3 class="title">by a Verified Patient</h3>
                            <div class="doctor_rating_profile">
                              <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Overall Rating</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="star_for_name">Bedside Manner</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Wait Time</div>
                                </div>
                              </div>
                            </div>
                          </div>
                       <div class="testimonial">
                            <p class="description">Loved Dr. Rong, she is very knowledgeable and answered all the questions I have. She is very patient, professional with a relax funny side. That's what you need in a dentist! Will probably switch to her as my primary dr.</p>
                            <h3 class="date">March 07, 2018</h3>
                            <h3 class="title">by a Verified Patient</h3>
                            <div class="doctor_rating_profile">
                              <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Overall Rating</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="star_for_name">Bedside Manner</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Wait Time</div>
                                </div>
                              </div>
                            </div>
                          </div>
                       <div class="testimonial">
                            <p class="description"> Dr, Rong is one in a million of in the world of dentistry. She provides excellent care, with her gentle mood and expertise. Amazing staff all around that serve with professionalism and compassion. I have been coming to her for 6 years for all of my dental needs. Super plus for me that she speaks English, Cantonese and Mandarin. </p>
                            <h3 class="date">March 07, 2018</h3>
                            <h3 class="title">by a Verified Patient</h3>
                            <div class="doctor_rating_profile">
                              <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Overall Rating</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="star_for_name">Bedside Manner</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Wait Time</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="testimonial">
                            <p class="description"> Dr, Rong is one in a million of in the world of dentistry. She provides excellent care, with her gentle mood and expertise. Amazing staff all around that serve with professionalism and compassion. I have been coming to her for 6 years for all of my dental needs. Super plus for me that she speaks English, Cantonese and Mandarin. </p>
                            <h3 class="date">March 07, 2018</h3>
                            <h3 class="title">by a Verified Patient</h3>
                            <div class="doctor_rating_profile">
                              <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Overall Rating</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="star_for_name">Bedside Manner</div>
                                </div>
                              </div>
                               <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                              	<div class="rating">
                                	 <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span>
                                </div>
                                <div class="star_for_name">Wait Time</div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
              </div>
            </div>
        </div>
     </div>  
        <div class="bookaward">
        	<div class="container">
            	<div class="awards">
                	<div class="heading text-center">
        				<h1 class="section_heading">Bookdoc Award</h1> 
                            
       				</div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    	<ul class="awardlist">
                        	<li><img src="theme/images/award.png"></li>
                            <li><img src="theme/images/award.png"></li>
                            <li><img src="theme/images/award.png"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="subscription nobg inner_subscription">
	<div class="container">
    	<div class="subscription_box">
        	<div class="heading">
        	<h1 class="section_heading">Subscribe Us</h1>      
        	</div> 
            <div class="subcrip_box">
            	<input type="text" class="sub_input" placeholder="Email Address">
                <button class="sub_btn">Subscribe</button>
            </div>
        </div>
    </div>
</div>
        
        
    </div>
</div>

<div class="footer">
	<div class="container">
    	<div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        
        <div class="copy">
        	<div class="copyright">
            	<img src="theme/images/green-logo.png">
                <div class="copytext">© 2001–2018 All Rights Reserved. Bookdoc® is a registered trademark.</div>
            </div>
            <div class="social">
            	<ul>
                	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script src="theme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="theme/js/accordion.js"></script>
<script>
//Accordian script
jQuery(document).ready(function($){
			$(".demo-accordion").accordionjs();

			// Demo text. Let's save some space to make the code readable. ;)
			//$('.acc_content').not('.nodemohtml').html('<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce aliquet neque et accumsan fermentum. Aliquam lobortis neque in nulla  tempus, molestie fermentum purus euismod.</p>');
		});
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:2,
        itemsDesktop:[1000,2],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:true,
        navigationText:["",""],
        slideSpeed:1000,
        autoPlay:true
    });
	
});

</script>
</body>
</html>