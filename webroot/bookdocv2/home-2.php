<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initail-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/home2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
<link rel="stylesheet" href="theme/css/responsive2.css">

<link rel="stylesheet" href="theme/css/owl.carousel.min2.css">
<link rel="stylesheet" href="theme/css/owl.theme.min2.css">
<script src="theme/js/jquery.min.js"></script>
 
<script>
	$(function(){
    $('.selectpicker').selectpicker();
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
</head>

<body class="homepage">
<div class="home_banner02">
	<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/white_logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><a href="">List your practice on Bookdoc </a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
          <li class="signin"><a href="">Login</a></li>
          <li>
          	<div class="countrydown">
            	<select class="selectpicker" data-width="fit">
    <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
  <option  data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
</select>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
	<div class="parllax">
	<img src="theme/images/home_banner_2.jpg">
    </div>
     
	<div class="search_box">
    	<div class="container">
        	<div class="topsearchbar">
        	<!--<h2 class="serach_tag">Find your Best Health <br/> Solution near you</h2>-->
            <div class="topserach">
            	<div class="forms">
               	  <form>
                  		<label class="search_form_icon"><i class="icon doctor_icon"><img src="theme/images/serch_icon01.png"></i></label>
                        <div class="serachinput first-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="" selected>Condition or Doctor name</option>
                                <option value="Doctor Name 01">Doctor Name 01</option>
                                <option value="Doctor Name 02">Doctor Name 02</option>
                                <option value="Doctor Name 03">Doctor Name 03</option>
                              </select>
                              </label>
                          </div>
                        </div>  	
                        <label class="search_form_icon"><i class="icon location_icon"><img src="theme/images/serch_icon02.png"></i></label>
                        <div class="serachinput second-serach-box">
                        	<div class="select-style">
                            	<label class="css_select_lable">
                            <select>
                                <option value="Zip Code" selected>Zip Code</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                                <option value="123456">123456</option>
                              </select>
                             </label>
                          </div>
                        </div>  	
                        <div class="serachinput">
                        	<div class="select-style">
                            <label class="css_select_lable">
                            <select>
                                <option value="I’ll Choose My Insurance Later">I’ll Choose My Insurance Later</option>
                                <option value="saab">Saab</option>
                                <option value="mercedes">Mercedes</option>
                                <option value="audi">Audi</option>
                              </select>
                              </label>
                          </div>
                        </div> 
                        <button class="serachbtn"><i class="fa fa-search"></i></button> 	
                        
                  </form>
                </div>
            </div>
        </div>
        </div>
    </div>  
</div>
<div class="homeSearch_box">
	<div class="container">
    	<div class="outercolor">
        <div class="doctabs">  
        		<div class="heading">
                	<h1 class="tags">Find doctors and dentists by</h1>
                </div>
              	
                <div class="parents_lists">
                	<div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="patients_box">
                        	<h3 class="ds_name"><i class="icon icon01"><img src="theme/images/city_icon.png"></i> City</h3>
                            <div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>                              
                            </ul>
                            <a href="" class="viewall">View all</a>                            
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="patients_box">
                        	<h3 class="ds_name"><i class="icon icon01"><img src="theme/images/specia_icon.png"></i> Speciality</h3>
                            <div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>
                              
                            </ul>
                            <a href="" class="viewall">View all</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                    	<div class="patients_box">
                        	<h3 class="ds_name"><i class="icon icon01"><img src="theme/images/insurance_icon.png"></i> Insurance</h3>
                            <div class="realtedlinks">
                        	<ul>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Austin</a></li>
                              <li><a href="">Atlanta</a></li>
                              <li><a href="">Chicago</a></li>
                              <li><a href="">Columbus</a></li>
                              <li><a href="">Dallas</a></li>
                              <li><a href="">Houston</a></li>
                              <li><a href="">Jersey City</a></li>
                              <li><a href="">Los Angeles</a></li>
                              
                            </ul>
                            <a href="" class="viewall">View all</a>
                        </div>
                        </div>
                    </div>
                </div>        	
        </div>                
        </div>
    </div>
</div>

<div class="bookdoc_apps">
	<div class="mobile_img">
            	<img src="theme/images/join-book_img2.png">
            </div>
	<div class="container">
    	<div class="col-md-6 col-xs-12">
        	
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
        	<div class="app_content">
            	<div class="heading">
                	<h1 class="brktag">Get the <span class="bluecolor bold">Bookdoc app.</span></h1>
                </div>
                <div class="points">
                	<h4>We've got you covered</h4>
                    <div class="uniquepoint">
                    	<ul>
                        	<li><div class="icons"><img src="theme/images/doct_bag_icon_blue.png"></div> <div class="point_detail">We cover different medical specialties</div></li>
                            <li><div class="icons"><img src="theme/images/doc_thethos_icon_blue.png"></div> <div class="point_detail">Verfied Professional Doctors</div></li>
                            <li><div class="icons"><img src="theme/images/safe-icon_blue.png"></div> <div class="point_detail">Safe and Secure Appointments</div></li>
                        </ul>
                    </div>
                    <div class="appstore">
                    	<ul>
                        	<li><a href=""><img src="theme/images/googleplay.png"></a></li>
                            <li><a href=""><img src="theme/images/appstore.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fivestart_doctor">
	<div class="container">
    	<div class="doctor_details">
        	<div class="doctor_d col-md-12 col-sm-12 col-xs-12">
            	<div class="heading">
                	<h1 class="brktag">
                    <span class="light">Are You a</span> <span class="bluecolor bold">five star doctor?</span></h1>
                    <p class="">List your practice to reach millions of patients.</p>
                </div>
                <div class="doctor_points">
                	<ul>
                    	<li>Attract and engage new patients</li>
                        <li>Build and strengthen your online reputation</li>
                        <li>Deliver a premium experience patients love</li>
                    </ul>
                </div>
                <div class="gradiant_btn">
                	<a href="">LIST YOUR PRACTICE ON BOOKDOC</a>
                </div>
                
                
            </div>            
        </div>
    </div>
    <div class="doctor_im">
            	<img src="theme/images/five-star-doc.png">
            </div>
</div>

<div class="health_services">
	<div class="container">
    	<div class="h_services">
        	<div class="heading">
            	<h1 class="tag bold">BEST HEALTH SERVICES AT <br/> RESONABLE PRICES</span></h1>
                <p>Need help? Visit our Knowledge base</p>
            </div>
            <div class="gradiant_blue_butn">
            	<a href="">Join us for care</a>
            </div>
            <div class="group_img">
            	<img src="theme/images/doc_group2.png">
            </div>
        </div>
    </div>
</div>

<div class="client_testi">
	<div class="container">
    	<div class="heading">
                	<h1 class="tags">Find doctors and dentists by</h1>
                </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <div class="pic">
                        <img src="theme/images/testimonial_author_img.png" alt="">
                    </div>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur.
                    </p>
                    <h3 class="title">Mr. David Landon </h3>
                </div>
 
                <div class="testimonial">
                    <div class="pic">
                        <img src="theme/images/testimonial_author_img.png" alt="">
                    </div>
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti dolor ipsum molestias mollitia ut. Aliquam aperiam corporis cumque debitis delectus dignissimos. Lorem ipsum dolor sit amet, consectetur.
                    </p>
                    <h3 class="title">Mr. David Landon</h3>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="footer">
	<div class="subcription">
    	<div class="container">        	
            	<div class="subcription_box">
                	<h2 class="subcription_heading">Daily Health Tips to your Inbox</h2>
                    <div class="subcription_input_box">
                    	<label class="mailicon"><i class="fa fa-envelope-o" style="font-size:20px; color:#4e5668"></i></label>
                    	<input type="text" class="subcription_input" placeholder="Email">
                        <button class="subc_btn">Subscribe</button>                        
                    </div>
                </div>                 
        </div>
    </div>
    <div class="footer_section">
    	<div class="container">
    	<div class="col-md-9 col-sm-12 col-xs-12">
        	<div class="footer_links">
            	<div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Look up your doctor</h3>
                        <ul>
                        	<li><a href="">Doctor name</a></li>
                            <li><a href="">Practice name</a></li>
                            <li><a href="">Hospital name</a></li>
                            <li><a href="">Health Q & A </a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Services</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Search By</h3>
                        <ul>
                        	<li><a href="">Specialty</a></li>
                            <li><a href="">Procedure</a></li>
                            <li><a href="">Language</a></li>
                            <li><a href="">Location </a></li>
                            <li><a href="">Insurance</a></li>
                            <li><a href="">Reviews</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Specialties</h3>
                        <ul>
                        	<li><a href="">Chiropractors</a></li>
                            <li><a href="">Dentists</a></li>
                            <li><a href="">Eye Doctors</a></li>
                            <li><a href="">Gynecologists</a></li>
                            <li><a href="">Primary care doctors</a></li>
                            <li><a href="">Psychiatrists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                	<div class="footer_box">
                    	<h3 class="footer_text">Partner with us</h3>
                        <ul>
                        	<li><a href="">Terms & conditions</a></li>
                            <li><a href="">Trust & Safety</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="">Mission</a></li>
                            <li><a href="">Careers</a></li>
                            <li><a href="">Press</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
        	<div class="loginsbuttn">
            		<ul>
                    	<li>
                        	<div class="dentist">
                            	<a href="">
                                	<h2>Are you a top doctor or dentist?</h2>
                                    <h4>List of Practice at Bookdoc</h4>
                                </a>
                            </div>
                        </li>
                        <li>
                        	<div class="health">
                            	<a href="">
                                	<h2>Are you a looking for better Health?</h2>
                                    <h4>Join Perfect Health Partners</h4>
                                </a>
                            </div>
                        </li>
                    </ul>
            </div>
        </div>
        
        <div class="col-md-12 col-xs-12 copysoci">
        	<div class="copyright col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
            	© 2018 Bookdoc. All Rights Reserved.
            </div> 
            <div class="social_links col-md-6 pl-0 pr-0 col-xs-12 col-sm-6">
                    	<ul>
                        	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                            <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
        </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script type="text/javascript">	
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});
	
</script>
</body>
</html>