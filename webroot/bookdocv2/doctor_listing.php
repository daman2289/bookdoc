<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome to Bookdoc</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="theme/css/bootstrap.min.css">
<link rel="stylesheet" href="theme/css/custom-bootstrap-margin-padding.css">
<link rel="stylesheet" href="theme/css/font-awesome.min.css">
<link rel="stylesheet" href="theme/css/fonts.css">
<link rel="stylesheet" href="theme/css/style02.css">
<link rel="stylesheet" href="theme/css/responsive.css">

<script src="theme/js/jquery.min.js"></script>

<script>
	$(document).ready(function(){
    
    $("#map_btn").click(function(){
        $("#location_map").slideToggle(1000);      
  
    });
    
   
});
</script>

</head>

<body>
<div class="header">
  <div class="container">
    <div class="header_top">
      <div class="logo"><a href=""><img src="theme/images/green-logo.png" title="Bookdoc" alt="Bookdoc"></a></div>
      <div class="extra_links">
        <ul>
          <li><i class="fa fa-phone green_icon"></i> <a href="tel:124 567 890">124 567 890</a></li>
          <li><a href="">Login</a></li>
          <li class="signup_btn"><a href="">Sign up</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="banner_section inner_search_banner">
    <div class="book_appointment_section">
    <div class="wrapper">
<div class="search_group">
   
      <h1 class="inner_serach_heading">Find Your Best Health Solution Near You</h1>
      <div class="seach_field_group">
       
          <div class="container">
            <div class="col-md-5 col-sm-3 col-xs-12">
              <div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Specialist, Condition"> Condition, Doctor Name </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
            	<div class="search_field">
               <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Location"> zip code or city </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
            	<div class="search_field">
                <label class="css_select_lable">
                  <select class="css_select">
                    <option selected="Insurance"> I'l choose my insurance later </option>
                    <option>Short Option</option>
                    <option>This Is A Longer Option</option>
                  </select>
                </label>
              </div>
            </div>
            <div class="col-md-1 col-sm-3 col-xs-12">
            	<div class="search_btn_div">
                	<button class="searchbtn"><i class="fa fa-search serch_icon"></i></button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="mainbody">
	<div class="breadcame_section">
    	<div class="container">
        	<div class="bredcam">
            	<ul class="breadcrumb">
                	<li><a href="">Home</a></li>
                    <li><a href="">New York</a></li>
                    <li><a href="">Gynaecologists</a></li>
                </ul>
                <h1 class="heading_Text">Gynaecologists in New York, NY</h1>
            </div>
            <div class="filter">
            	<div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Apply Filter <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
            </div>
        </div>    
    </div>
    <div class="doctor_listing_block">
    	<div class="container">
        	<div class="listing">
            	<div class="list_box active">
                	<div class="col-md-8 col-sm-8 col-xs-12 p-0">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_01.png">
                            </div>
                            <div class="doctor_details">
                            	<h3 class="sponsers_tag" >Sponsored Result</h3>
                            	<h2 class="doctorname" onClick="window.location='patient-editprofile.php'">Dr. Niks</h2>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="deatils">"Nice, caring doctor who made me feel good about being seen."</div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a id="map_btn" >Hide my Location <i class="fa fa-angle-up"></i></a></div>
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs12 p-20 left-border">
                    	<div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br/> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="location_map" id="location_map">
                                	<div class="map_frame">
                                    	<img src="theme/images/map.jpg">
                                    </div>
                                </div>
                </div>
            
            	<div class="list_box">
                	<div class="col-md-8 col-sm-8 col-xs-12 p-0">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_02.png">
                            </div>
                            <div class="doctor_details">
                            	<h2 class="doctorname">Ex soleat habemus usu</h2>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="deatils">"Nice, caring doctor who made me feel good about being seen."</div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a href="">View my Location <i class="fa fa-angle-down"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs12 p-20 left-border">
                    	<div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br/> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="list_box">
                	<div class="col-md-8 col-sm-8 col-xs-12 p-0">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_03.png">
                            </div>
                            <div class="doctor_details">
                            	<h2 class="doctorname">Consectetur adipiscing elit</h2>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="deatils">"Nice, caring doctor who made me feel good about being seen."</div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a href="">View my Location <i class="fa fa-angle-down"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs12 p-20 left-border">
                    	<div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br/> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="list_box">
                	<div class="col-md-8 col-sm-8 col-xs-12 p-0">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_04.png">
                            </div>
                            <div class="doctor_details">
                            	<h2 class="doctorname">Excepteur sint occaecatu</h2>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="deatils">"Nice, caring doctor who made me feel good about being seen."</div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a href="">View my Location <i class="fa fa-angle-down"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs12 p-20 left-border">
                    	<div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br/> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="list_box">
                	<div class="col-md-8 col-sm-8 col-xs-12 p-0">
                    	<div class="doctor_detail_part">
                        	<div class="doctor_img">
                            	<img src="theme/images/doc_05.png">
                            </div>
                            <div class="doctor_details">
                            	<h2 class="doctorname">Eiusmod tempor</h2>
                                <div class="groupstar">
                                	<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span>
                                </div>
                                <div class="deatils">"Nice, caring doctor who made me feel good about being seen."</div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 19 Murray St, New York, NY 10007</div>
                                <div class="location_address"><a href="">View my Location <i class="fa fa-angle-down"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs12 p-20 left-border">
                    	<div class="time_table_calender">
                        	<div class="calender">
                            	<ul>
                                	<li class="arrows"><a href=""><i class="fa fa-angle-left"></i> </a></li>
                                    <li><a href="">Today <br/> 7, March 2018</a></li>
                                    <li class="arrows"><a href="">  <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    	<div class="doctor_time_Table">
                        	<div class="time_table">
                            	<ul>
                                	<li><a href="">10:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">11:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                    <li><a href="">04:30 PM</a></li>
                                    <li><a href="">12:00 AM</a></li>
                                    <li><a href="">09:30 AM</a></li>
                                    <li><a href="">----</a></li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="paginations">
            	 <ul class="pagination">
                <li class="page-item disabled"><a class="page-link" href="#">Prev</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#">6</a></li>
                <li class="page-item"><a class="page-link" href="#">7</a></li>
                <li class="page-item"><a class="page-link" href="#">8</a></li>
                <li class="page-item"><a class="page-link" href="#">9</a></li>
                <li class="page-item"><a class="page-link" href="#">10</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
              </ul> 
            </div>
        </div>
    </div>
    
    
</div>

<div class="footer">
	<div class="container">
    	<div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
        	<div class="links">
            	<ul>
                	<li><a href="">Cardiology</a></li>
                    <li><a href="">Chriopractic</a></li>
                    <li><a href="">Clinical Gentetics</a></li>
                    <li><a href="">Cosmetic Medicine</a></li>
                    <li><a href="">Audiology</a></li>
                    <li><a href="">Pain Management</a></li>
                    <li><a href="">Homeopathy</a></li>
                    <li><a href="">General Practice</a></li>
                    <li><a href="">Chest Pain</a></li>
                    <li><a href="">Asthma</a></li>
                </ul>
            </div>
        </div>
        
        <div class="copy">
        	<div class="copyright">
            	<img src="theme/images/green-logo.png">
                <div class="copytext">© 2001–2018 All Rights Reserved. Bookdoc® is a registered trademark.</div>
            </div>
            <div class="social">
            	<ul>
                	<li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


<script src="theme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="theme/js/accordion.js"></script>



</body>
</html>