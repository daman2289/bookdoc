# BookDoc

A skeleton for creating applications with [CakePHP](http://cakephp.org) 3.x.
The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Clone project from repo Run `git clone git@bitbucket.org:ajaybisht-zs/bookdoc.git`.
3. Goto project directory `cd bookdoc`  and Run `composer update`
4. Setup configuration : On project root directory  make a copy of `.env.default` with name `.env` and update all varibales accourding to your local system.
5. If database is successfully connected then Run `bin/cake migrations migrate` and `bin/cake migrations seed`
6. Creating/Updating  ACOs `bin/cake acl_extras aco_sync`
7. Setup Access Restrictions for all users
  - Run `bin/cake permissions` 

Ubuntu :-
`sudo apt-get update`

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

### User Roles

role id | User Roles
------- | ------------
  1     |   Admin
  2     |   Staff
  3     |   Doctor
  4     |   Patient



## Update

1. Creating/Updating  ACOs `bin/cake acl_extras aco_sync`
2. All API's are Aro/Aco restriction free
3. For New Method Which should be assesible to only `Doctor`
  a) `bin/cake acl_extras aco_sync`
  b) `bin/cake acl grant Roles.2 controllers/Payments/index`
    likewise for `Patient` also.



## Configuration

Read and edit `.env` and setup the `'#Database Connection'` and any other
configuration relevant for your application.
Add webhook url [http://your-domain-name.com/api/payments/webhook.json] into the stripe account, So that application can handle stripe callback requests.



## Configuring permissions using the ACL shell

First, find the IDs of each role you want to grant permissions on.  There are several ways of doing this.  Since we will be at the console anyway, the quickest way is probably to run `bin/cake acl view aro` to view the ARO tree.  In this example, we will assume the `Administrator`, `Manager`, and `User` roles have IDs 1, 2, and 3 respectively.

- Grant role permission to everything
  - Run `bin/cake permissions`

- Grant members of the `Staff` role permission to view `Admin` Panel
  - Run `bin/cake acl grant Roles.2 controllers/Admin`