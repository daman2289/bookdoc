<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DesignsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DesignsController Test Case
 */
class DesignsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.designs'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
