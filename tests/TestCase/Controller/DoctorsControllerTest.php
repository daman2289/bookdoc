<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DoctorsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DoctorsController Test Case
 */
class DoctorsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctors'
    ];

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
