<?php
namespace App\Test\TestCase\Controller\Api;

use App\Controller\Api\StatesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Api\StatesController Test Case
 */
class StatesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.states',
        'app.countries'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
