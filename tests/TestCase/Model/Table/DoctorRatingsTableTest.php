<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorRatingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorRatingsTable Test Case
 */
class DoctorRatingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorRatingsTable
     */
    public $DoctorRatings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_ratings',
        'app.doctors',
        'app.patients',
        'app.bookings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DoctorRatings') ? [] : ['className' => DoctorRatingsTable::class];
        $this->DoctorRatings = TableRegistry::get('DoctorRatings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorRatings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
