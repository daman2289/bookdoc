<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HospitalProfilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HospitalProfilesTable Test Case
 */
class HospitalProfilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HospitalProfilesTable
     */
    public $HospitalProfiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.hospital_profiles',
        'app.users',
        'app.states',
        'app.countries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HospitalProfiles') ? [] : ['className' => HospitalProfilesTable::class];
        $this->HospitalProfiles = TableRegistry::getTableLocator()->get('HospitalProfiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HospitalProfiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
