<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserAwardsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserAwardsTable Test Case
 */
class UserAwardsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserAwardsTable
     */
    public $UserAwards;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_awards',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserAwards') ? [] : ['className' => UserAwardsTable::class];
        $this->UserAwards = TableRegistry::get('UserAwards', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserAwards);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
