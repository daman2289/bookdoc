<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PatientReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PatientReportsTable Test Case
 */
class PatientReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PatientReportsTable
     */
    public $PatientReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.patient_reports',
        'app.patients',
        'app.doctors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PatientReports') ? [] : ['className' => PatientReportsTable::class];
        $this->PatientReports = TableRegistry::get('PatientReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PatientReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
