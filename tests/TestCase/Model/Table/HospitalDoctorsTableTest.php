<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HospitalDoctorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HospitalDoctorsTable Test Case
 */
class HospitalDoctorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HospitalDoctorsTable
     */
    public $HospitalDoctors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.hospital_doctors',
        'app.users',
        'app.hospitals'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HospitalDoctors') ? [] : ['className' => HospitalDoctorsTable::class];
        $this->HospitalDoctors = TableRegistry::getTableLocator()->get('HospitalDoctors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HospitalDoctors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
