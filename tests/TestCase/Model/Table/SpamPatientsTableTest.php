<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpamPatientsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpamPatientsTable Test Case
 */
class SpamPatientsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpamPatientsTable
     */
    public $SpamPatients;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.spam_patients',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpamPatients') ? [] : ['className' => SpamPatientsTable::class];
        $this->SpamPatients = TableRegistry::get('SpamPatients', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpamPatients);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
