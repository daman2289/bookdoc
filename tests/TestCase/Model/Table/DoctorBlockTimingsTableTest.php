<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorBlockTimingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorBlockTimingsTable Test Case
 */
class DoctorBlockTimingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorBlockTimingsTable
     */
    public $DoctorBlockTimings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_block_timings',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DoctorBlockTimings') ? [] : ['className' => DoctorBlockTimingsTable::class];
        $this->DoctorBlockTimings = TableRegistry::get('DoctorBlockTimings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorBlockTimings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
