<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorFavouritesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorFavouritesTable Test Case
 */
class DoctorFavouritesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorFavouritesTable
     */
    public $DoctorFavourites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_favourites',
        'app.patients',
        'app.doctors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DoctorFavourites') ? [] : ['className' => DoctorFavouritesTable::class];
        $this->DoctorFavourites = TableRegistry::get('DoctorFavourites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorFavourites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
