<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserHospitalAffiliationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserHospitalAffiliationsTable Test Case
 */
class UserHospitalAffiliationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserHospitalAffiliationsTable
     */
    public $UserHospitalAffiliations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_hospital_affiliations',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserHospitalAffiliations') ? [] : ['className' => UserHospitalAffiliationsTable::class];
        $this->UserHospitalAffiliations = TableRegistry::get('UserHospitalAffiliations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserHospitalAffiliations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
