<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorAvailabilityDaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorAvailabilityDaysTable Test Case
 */
class DoctorAvailabilityDaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorAvailabilityDaysTable
     */
    public $DoctorAvailabilityDays;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_availability_days',
        'app.users',
        'app.doctor_availability_timings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DoctorAvailabilityDays') ? [] : ['className' => DoctorAvailabilityDaysTable::class];
        $this->DoctorAvailabilityDays = TableRegistry::get('DoctorAvailabilityDays', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorAvailabilityDays);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
