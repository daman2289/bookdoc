<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorTimeOffsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorTimeOffsTable Test Case
 */
class DoctorTimeOffsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorTimeOffsTable
     */
    public $DoctorTimeOffs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_time_offs',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DoctorTimeOffs') ? [] : ['className' => DoctorTimeOffsTable::class];
        $this->DoctorTimeOffs = TableRegistry::getTableLocator()->get('DoctorTimeOffs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorTimeOffs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
