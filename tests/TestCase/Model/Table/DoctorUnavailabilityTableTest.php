<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorUnavailabilityTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorUnavailabilityTable Test Case
 */
class DoctorUnavailabilityTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorUnavailabilityTable
     */
    public $DoctorUnavailability;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_unavailability',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DoctorUnavailability') ? [] : ['className' => DoctorUnavailabilityTable::class];
        $this->DoctorUnavailability = TableRegistry::getTableLocator()->get('DoctorUnavailability', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorUnavailability);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
