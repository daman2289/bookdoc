<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorDayOffsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorDayOffsTable Test Case
 */
class DoctorDayOffsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorDayOffsTable
     */
    public $DoctorDayOffs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_day_offs',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DoctorDayOffs') ? [] : ['className' => DoctorDayOffsTable::class];
        $this->DoctorDayOffs = TableRegistry::getTableLocator()->get('DoctorDayOffs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorDayOffs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
