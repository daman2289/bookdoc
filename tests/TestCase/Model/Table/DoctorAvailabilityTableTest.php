<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorAvailabilityTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorAvailabilityTable Test Case
 */
class DoctorAvailabilityTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorAvailabilityTable
     */
    public $DoctorAvailability;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctor_availability',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DoctorAvailability') ? [] : ['className' => DoctorAvailabilityTable::class];
        $this->DoctorAvailability = TableRegistry::getTableLocator()->get('DoctorAvailability', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorAvailability);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
