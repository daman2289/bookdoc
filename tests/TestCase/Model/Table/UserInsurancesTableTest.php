<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserInsurancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserInsurancesTable Test Case
 */
class UserInsurancesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserInsurancesTable
     */
    public $UserInsurances;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_insurances',
        'app.users',
        'app.insurances'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserInsurances') ? [] : ['className' => UserInsurancesTable::class];
        $this->UserInsurances = TableRegistry::get('UserInsurances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserInsurances);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
