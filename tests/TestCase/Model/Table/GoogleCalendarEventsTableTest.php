<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GoogleCalendarEventsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GoogleCalendarEventsTable Test Case
 */
class GoogleCalendarEventsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GoogleCalendarEventsTable
     */
    public $GoogleCalendarEvents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.google_calendar_events',
        'app.bookings',
        'app.doctors',
        'app.events'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GoogleCalendarEvents') ? [] : ['className' => GoogleCalendarEventsTable::class];
        $this->GoogleCalendarEvents = TableRegistry::getTableLocator()->get('GoogleCalendarEvents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GoogleCalendarEvents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
