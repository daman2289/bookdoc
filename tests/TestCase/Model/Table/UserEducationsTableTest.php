<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserEducationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserEducationsTable Test Case
 */
class UserEducationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserEducationsTable
     */
    public $UserEducations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_educations',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserEducations') ? [] : ['className' => UserEducationsTable::class];
        $this->UserEducations = TableRegistry::get('UserEducations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserEducations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
