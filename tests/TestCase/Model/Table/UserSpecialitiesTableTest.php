<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserSpecialitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserSpecialitiesTable Test Case
 */
class UserSpecialitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserSpecialitiesTable
     */
    public $UserSpecialities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_specialities',
        'app.users',
        'app.specialities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserSpecialities') ? [] : ['className' => UserSpecialitiesTable::class];
        $this->UserSpecialities = TableRegistry::get('UserSpecialities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserSpecialities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
