<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DoctorAvailabilityFixture
 *
 */
class DoctorAvailabilityFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'doctor_availability';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'available_from' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => 'Doctor Available From', 'precision' => null],
        'available_upto' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => 'Doctor Available Upto', 'precision' => null],
        'weekday' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '1->Mon,2->Tue,3->Wed,4->Thu,5->Fri,6->Sat,7->Sun', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => 'Doctor User Id', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'doctor_availability_ibfk_2' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'available_from' => '10:54:22',
                'available_upto' => '10:54:22',
                'weekday' => 1,
                'user_id' => 1,
                'created' => '2018-07-11 10:54:22',
                'modified' => '2018-07-11 10:54:22'
            ],
        ];
        parent::init();
    }
}
