-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2018 at 08:07 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_activated` int(1) NOT NULL DEFAULT '0' COMMENT '0 => Not Activated, 1 => Activated',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `is_header` tinyint(2) DEFAULT '1' COMMENT '0=header\n1=footer',
  `page_order` int(11) DEFAULT '1',
  `additional_key` varchar(145) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `meta_title`, `meta_description`, `meta_keyword`, `page_title`, `page_name`, `is_activated`, `created`, `modified`, `is_header`, `page_order`, `additional_key`, `is_deleted`) VALUES
(1, 'About Us', '<p><span style=\"font-size:16px\">What is Lorem Ipsum?</span></p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Why do we use it?</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', 'About Us', 'About Us', 'About Us', 1, '0000-00-00 00:00:00', '2018-04-26 10:33:08', 0, 1, 'aboutus', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `template_used_for` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_body` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '0 => Not Deleted, 1 => Deleted',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `template_used_for`, `subject`, `mail_body`, `is_deleted`, `created`, `modified`) VALUES
(1, 'send registration email to user', 'User Registeration', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; height:100%; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td><!-- BEGIN TEMPLATE // -->\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateContainer\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; border:1px solid #BBBBBB; mso-table-lspace:0pt; mso-table-rspace:0pt; width:600px\">\r\n				<tbody>\r\n					<tr>\r\n						<td><!-- BEGIN HEADER // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateHeader\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"height:100px; text-align:center; vertical-align:middle\">\r\n									<p><span style=\"font-size:smaller\"><span style=\"color:rgb(128, 0, 0)\"><span style=\"font-family:calibri,sans-serif\">BOOK DOC</span></span></span></p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END HEADER --></td>\r\n					</tr>\r\n					<tr>\r\n						<td><!-- BEGIN BODY // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateBody\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; background-color:#fff; border-bottom:1px solid #CCCCCC; border-collapse:collapse !important; border-top:1px solid #FFFFFF; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"text-align:left\"><!-- <h1>Designing Your Template</h1> -->\r\n									<h3 style=\"margin-left:0; margin-right:0; text-align:left\">Dear #FIRSTNAME</h3>\r\n\r\n									<p>Your account has been created with <span style=\"color:rgb(128, 0, 0); font-family:calibri,sans-serif; font-size:10.8333px\">BOOK DOC</span>. Use the below credentials to log in.</p>\r\n\r\n									<p><strong>Email</strong>: #EMAIL</p>\r\n\r\n									<p><strong>Password</strong>: #PWD</p>\r\n\r\n									<p><strong>Link</strong>: #LINK</p>\r\n\r\n									<p><strong>Best Regards</strong><br />\r\n									Support team,<br />\r\n									BOOK DOC</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END BODY --></td>\r\n					</tr>\r\n					<tr>\r\n						<td><!-- BEGIN FOOTER // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateFooter\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; border-top:1px solid #FFFFFF; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"text-align:center\"><em>Copyright &copy; 2017 BOOK DOC</em>,<em> All rights reserved.</em></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END FOOTER --></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<!-- // END TEMPLATE --></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 0, '2016-11-02 00:00:00', '2018-04-26 10:34:38'),
(2, 'send reset password link to user', 'Password Reset Request - DO NOT REPLY', '<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; height:100%; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td><!-- BEGIN TEMPLATE // -->\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateContainer\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; border:1px solid #BBBBBB; mso-table-lspace:0pt; mso-table-rspace:0pt; width:600px\">\r\n				<tbody>\r\n					<tr>\r\n						<td><!-- BEGIN HEADER // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateHeader\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"height:100px; text-align:center; vertical-align:middle\">\r\n									<p><span style=\"font-size:smaller\"><span style=\"color:rgb(128, 0, 0)\"><span style=\"font-family:calibri,sans-serif\">BOOK DOC</span></span></span></p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END HEADER --></td>\r\n					</tr>\r\n					<tr>\r\n						<td><!-- BEGIN BODY // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateBody\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; background-color:#fff; border-bottom:1px solid #CCCCCC; border-collapse:collapse !important; border-top:1px solid #FFFFFF; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"text-align:left\"><!-- <h1>Designing Your Template</h1> -->\r\n									<h3 style=\"text-align:left\">Dear #FIRSTNAME</h3>\r\n\r\n									<p><span style=\"font-size:16px\">You may change your password using the link below.&nbsp;</span></p>\r\n\r\n									<p><strong>Link</strong><span style=\"color:rgb(80, 80, 80); font-family:helvetica,sans-serif; font-size:16px\">: #LINK</span></p>\r\n\r\n									<p><span style=\"color:rgb(80, 80, 80); font-family:helvetica,sans-serif; font-size:16px\">Your password won&#39;t change until you access the link above and create a new one.</span></p>\r\n\r\n									<p><span style=\"font-size:16px\">Thanks and have a nice day!</span></p>\r\n\r\n									<p>&nbsp;</p>\r\n\r\n									<p>&nbsp;</p>\r\n\r\n									<p><strong>Best Regards</strong><br />\r\n									Support team,<br />\r\n									<span style=\"color:rgb(128, 0, 0); font-family:calibri,sans-serif; font-size:10.8333px\">BOOK&nbsp;DOC</span></p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END BODY --></td>\r\n					</tr>\r\n					<tr>\r\n						<td><!-- BEGIN FOOTER // -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateFooter\" style=\"-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; border-collapse:collapse !important; border-top:1px solid #FFFFFF; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"text-align:center\"><em>Copyright &copy; 2017 BOOK DOC, All rights reserved.</em></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- // END FOOTER --></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<!-- // END TEMPLATE --></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 0, '2017-09-05 06:02:38', '2018-04-26 10:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE `insurances` (
  `id` int(11) NOT NULL,
  `title_french` varchar(55) NOT NULL,
  `title_eng` varchar(55) NOT NULL,
  `title_italic` varchar(55) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurances`
--

INSERT INTO `insurances` (`id`, `title_french`, `title_eng`, `title_italic`, `user_id`, `status`, `is_deleted`, `created`, `modified`) VALUES
(5, 'Aetna', 'Aetna', 'Aetna', 1, 0, 0, '2018-04-27 11:31:50', '2018-04-27 11:31:50'),
(6, 'Anthem Croix Bleue Bouclier Bleu', 'Anthem Blue Cross Blue Shield', 'Anthem Blue Cross Blue Shield', 1, 0, 0, '2018-04-27 11:31:58', '2018-04-27 11:58:20'),
(7, 'Blue Cross Blue Shield', 'Blue Cross Blue Shield', 'Blue Cross Blue Shield', 1, 0, 0, '2018-04-27 11:32:06', '2018-04-27 11:32:06'),
(8, 'Cigna', 'Cigna', 'Cigna', 1, 0, 0, '2018-04-27 11:32:13', '2018-04-27 11:32:13'),
(9, 'EmblemHealth (GHI)', 'EmblemHealth (GHI)', 'EmblemHealth (GHI)', 1, 0, 0, '2018-04-27 11:32:19', '2018-04-27 11:32:19'),
(10, 'EmblemHealth (HIP)', 'EmblemHealth (HIP)', 'EmblemHealth (HIP)', 1, 0, 0, '2018-04-27 11:32:27', '2018-04-27 11:32:27'),
(11, 'Empire Blue Cross Blue Shield', 'Empire Blue Cross Blue Shield', 'Empire Blue Cross Blue Shield', 1, 0, 0, '2018-04-27 11:32:34', '2018-04-27 11:32:34'),
(12, 'UnitedHealthcare', 'UnitedHealthcare', 'UnitedHealthcare', 1, 0, 0, '2018-04-27 11:32:40', '2018-04-27 11:32:40'),
(13, 'UnitedHealthcare Oxford', 'UnitedHealthcare Oxford', 'UnitedHealthcare Oxford', 1, 0, 0, '2018-04-27 11:32:48', '2018-04-27 11:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20180427095232, 'Initial', '2018-04-27 08:22:32', '2018-04-27 08:22:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(45) NOT NULL,
  `type` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `type`) VALUES
(1, 'admin'),
(2, 'subadmin'),
(3, 'doctor'),
(4, 'patient');

-- --------------------------------------------------------

--
-- Table structure for table `specialities`
--

CREATE TABLE `specialities` (
  `id` int(11) NOT NULL,
  `title_eng` varchar(55) NOT NULL,
  `title_french` varchar(55) NOT NULL,
  `title_italic` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialities`
--

INSERT INTO `specialities` (`id`, `title_eng`, `title_french`, `title_italic`, `status`, `is_deleted`, `user_id`, `created`, `modified`) VALUES
(5, 'Allergist', 'Allergologue', 'allergologo', 0, 0, 1, '2018-04-27 11:27:15', '2018-04-27 11:57:10'),
(6, 'Cardiologist', 'Cardiologue', 'Cardiologo', 0, 0, 1, '2018-04-27 11:28:21', '2018-04-27 11:28:21'),
(7, 'Chiropractor', 'Chiropracteur', 'Chiropratico', 0, 0, 1, '2018-04-27 11:28:36', '2018-04-27 11:48:34'),
(8, 'Dentist', 'Dentiste', 'Dentista', 0, 0, 1, '2018-04-27 11:28:50', '2018-04-27 11:51:46'),
(9, 'Dermatologist', 'Dermatologue', 'Dermatologo', 0, 0, 1, '2018-04-27 11:29:00', '2018-04-27 11:52:06'),
(10, 'Ear, Nose & Throat Doctor', 'Oreille, nez et gorge', 'Orecchio, naso e gola', 0, 0, 1, '2018-04-27 11:29:08', '2018-04-27 11:52:28'),
(11, 'Gastroenterologist', 'Gastro-entÃ©rologue', 'Gastroenterologo', 0, 0, 1, '2018-04-27 11:29:15', '2018-04-27 11:52:51'),
(12, 'OB-GYN', 'OB-GYN', 'Ginecologo', 0, 0, 1, '2018-04-27 11:29:26', '2018-04-27 11:53:13'),
(13, 'Opthalmologist', 'Ophtalmologiste', 'Oculista', 0, 0, 1, '2018-04-27 11:29:33', '2018-04-27 11:53:34'),
(14, 'Optometrist', 'OptomÃ©triste', 'optometrista', 0, 0, 1, '2018-04-27 11:29:41', '2018-04-27 11:53:55'),
(15, 'Orthopedic Surgeon', 'Chirurgien orthopÃ©dique', 'Chirurgo ortopedico', 0, 0, 1, '2018-04-27 11:29:53', '2018-04-27 11:54:33'),
(16, 'Pediatrician', 'PÃ©diatre', 'Pediatra', 0, 0, 1, '2018-04-27 11:30:04', '2018-04-27 11:55:03'),
(17, 'Podiatrist', 'Podologue', 'Podologo', 0, 0, 1, '2018-04-27 11:30:53', '2018-04-27 11:55:24'),
(18, 'Primary Care Doctor', 'MÃ©decin de soins primaires', 'Medico di cure primarie', 0, 0, 1, '2018-04-27 11:31:01', '2018-04-27 11:56:07'),
(19, 'Psychiatrist', 'Psychiatre', 'Psichiatra', 0, 0, 1, '2018-04-27 11:31:08', '2018-04-27 11:56:36'),
(20, 'Urologist', 'Urologue', 'Urologo', 0, 0, 1, '2018-04-27 11:31:15', '2018-04-27 11:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `is_approved` tinyint(4) DEFAULT '0',
  `is_email_verified` tinyint(4) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `persist_code` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `hashval` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `email`, `password`, `status`, `is_approved`, `is_email_verified`, `is_deleted`, `persist_code`, `created`, `modified`, `password_reset_token`, `hashval`) VALUES
(1, 1, 'naresh', 'admin@admin.com', '$2y$10$fdXKbdTPBepUU/AxdrBMvebMNAzMtEpsTkWYz386jd2j894KvbJ9C', 0, 1, NULL, 0, '', '2017-08-22 00:00:00', '2018-04-27 12:13:19', '3ec22074ca295d01737ad64e5c6c05450c47d21a', 'e664331a0d716cbf1df4e630ea65b40d30926ab5'),
(45, 3, '', 'rajnish.kumar@ucodesoft.com', '$2y$10$Qj0lLSYWztA3IsQxp2GqPOAWI2pTmsC.GcOASaAQAZHoK3i2EEkzq', 0, 1, NULL, 0, NULL, '2018-04-27 11:39:01', '2018-04-27 11:39:01', NULL, NULL),
(46, 4, '', 'anil@yopmail.com', '$2y$10$FfYSGCLCugZbiY0dmMevDOegla9pV/hokkzEADJPem64Loqila/iq', 0, 1, NULL, 0, NULL, '2018-04-27 12:14:32', '2018-04-27 12:14:32', NULL, NULL),
(48, 3, '', 'nk@gmail.com', '$2y$10$CCnhKtw/cZ8iBbiFTjFV3eETHE9nmn6vEiburtJMUlNfLVC7JQCjy', 0, 1, NULL, 0, NULL, '2018-04-29 12:19:50', '2018-04-29 12:19:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `zipcode` int(25) NOT NULL,
  `address` text,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `specialty` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `first_name`, `last_name`, `phone_number`, `dob`, `gender`, `zipcode`, `address`, `state`, `country`, `profile_pic`, `specialty`) VALUES
(6, 45, 'Rajnish', 'Kumar', '9878377602', '0000-00-00', '', 0, NULL, '', '', NULL, NULL),
(7, 46, 'Anil', 'kumar', '9878377602', '0000-00-00', '', 0, NULL, '', '', NULL, NULL),
(9, 48, 'naresh', 'kumar', '54564545454', '0000-00-00', '', 0, NULL, '', '', NULL, NULL),
(10, 54, 'Naresh', 'Kumar', '9597028953', '0000-00-00', '', 0, '27', '', '', 'hbjhbj', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurances`
--
ALTER TABLE `insurances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialities`
--
ALTER TABLE `specialities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `role_id_2` (`role_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `insurances`
--
ALTER TABLE `insurances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(45) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `specialities`
--
ALTER TABLE `specialities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
