<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Designs Controller
 *
 */
class DesignsController extends AppController
{
	public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }

    public function patientDashboard() {
    	$this->viewBuilder()->setLayout('patient');
    }

      public function bookingConformation()
    {
        $this->viewBuilder()->setLayout('frontend');
    }
}
