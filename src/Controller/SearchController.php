<?php
namespace App\Controller;
use Cake\Core\Configure;

use App\Controller\AppController;

/**
 * Search Controller
 *
 * 
 *
 * 
 */
class SearchController extends AppController
{

     public function initialize() {
        parent::initialize();
        // Auth component allow visitors to access add action to register  and access logout action 
        $this->Auth->allow(['index', 'doctorDetail']);
        $this->paginate = [
            'limit' => Configure::read('recordsPerPage'),
        ];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($test = null){
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('Users');
        $this->Users->hasOne('DoctorFavourites', [
            'foreignKey' => 'doctor_id',
            'saveStrategy' => 'replace',
            'conditions' => ['DoctorFavourites.patient_id' => $this->Auth->user('id')]
        ]);

        $this->Users->hasOne('Durations', [
            'foreignKey' => 'user_id'
        ]);
        
        $doctors = $this->Users
            ->findByRoleId(Configure::read('Roles.doctor'));

        // $availableTimes = $doctors->map(function ($row) {
        //         $row['age'] = $row['birth_date']->diff(new \DateTime)->y;
        //         return $row;
        // });
        // $dayofweek = date('w', strtotime(new \Datetime()));
        $doctors = $doctors
            ->where(['Users.is_delete' => 0])
        	->innerJoinWith('DoctorAvailability')
            ->contain([
                'UserProfiles.States',
                'UserProfiles.Countries',
                'DoctorFavourites',
                'Durations',
                'UserStripeDetails',
                'DoctorAvailability' => function($q){
                    return $q->select([
                        'DoctorAvailability.available_from', 
                        'DoctorAvailability.available_upto', 
                        'DoctorAvailability.full_day_off',
                        'DoctorAvailability.weekday',
                        'DoctorAvailability.user_id',

                        ]);
                },
                'UserInsurances' => function($q){
                    return $q->select([
                        'UserInsurances.user_id', 
                        'UserInsurances.id', 
                        'UserInsurances.insurance_id',

                        ]);
                    },
                'DoctorUnavailability' => function($q) {
                    return $q->where(['DoctorUnavailability.unavailable_from >=' => new \Datetime()]);
                }, 
                'DoctorRatings' => function($q){
                    return $q->select([
                        'DoctorRatings.id',
                        'DoctorRatings.doctor_id',
                        'DoctorRatings.booking_id',
                        'DoctorRatings.review',
                        'DoctorRatings.overall_rating',
                        'DoctorRatings.bedside_manner',
                        'DoctorRatings.wait_time',
                        ]);
                    }
                ])                                               
            ->select([
                'States.name', 'Countries.name', 
                'UserProfiles.first_name', 'UserProfiles.last_name', 
                'UserProfiles.dob', 'UserProfiles.address',  'UserProfiles.lat', 
                'UserProfiles.state_id', 'UserProfiles.country_id',  'UserProfiles.gender', 
                'UserProfiles.lng','UserProfiles.zipcode', 'UserProfiles.profile_pic', 'UserProfiles.my_patient_only',
                'Users.role_id', 'Users.id','Users.is_sponsored',
                'DoctorFavourites.patient_id',
                'DoctorFavourites.doctor_id',
                'Durations.user_id',
                'Durations.duration',
                'UserStripeDetails.current_period_end'

            ])
            ->order([
                'Users.is_sponsored' => 'DESC',
                'UserProfiles.first_name' => 'ASC'
                ])
            ->group('Users.id')
            ->formatResults(function ($results) {
              return $results->map(function ($row){
                    foreach ($row->doctor_availability as $key => $value) {
                    	$span = 30;
                    	if(!empty($row->duration)) {
                    		$span = $row->duration->duration;
                    	}
                        $range = [];
                        $begin = new \DateTime($value->available_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->available_upto->format("Y-m-d H:i:s"));
                        $timespan = $span*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($range, $new);
                        }
                        $row->doctor_availability[$key]->available_slots = $range;
                    }

                    foreach ($row->doctor_unavailability as $key => $value) {
                        $ranges = [];
                        $begin = new \DateTime($value->unavailable_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->unavailable_upto->format("Y-m-d H:i:s"));
                        $timespan = $row->duration->duration*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($ranges, $new);
                        }
                        $row->doctor_unavailability[$key]->unavailable_slots = $ranges;
                    }
                    return $row;
                    
                });
            });

        $states = [
            1050 => "Vienna (Wien)",
            2431 => "Lower Austria (Niederösterreich)",
            4310 => "Upper Austria (Oberösterreich)",
            8000 => "Styria (Steiermark)",
            6000 => "Tyrol (Tirol)",
            9324 => "Carinthia (Kärnten)",
            5321 => "Salzburg",
            6700 => "Vorarlberg",
            8380 => "Burgenland",
        ];
  
        $states_german = [
            1050 => "Wien",
            2431 => "Niederösterreich",
            4310 => "Oberösterreich",
            8000 => "Steiermark",
            6000 => "Tirol",
            9324 => "Kärnten",
            5321 => "Salzburg",
            6700 => "Vorarlberg",
            8380 => "Burgenland",
        ];

        $term = ($test == 'speciality')? $this->request->getParam('pass.1'):null;
        // $term = $this->request->getData('term', null);

        // $zip = $this->request->getData('zip', null);location
        $zip = null;
        if($test == 'location' && !empty($this->request->getParam('pass.1'))) {
            $zip =  ( !empty(array_search($this->request->getParam('pass.1'), $states) ) )
                ? array_search($this->request->getParam('pass.1'), $states) 
                : array_search($this->request->getParam('pass.1'), $states_german);
        }

        // $zip = ($test == 'location')? $this->request->getParam('pass.1'):null;
        // $insurance = $this->request->getData('insurance', null);
        $insurance = ($test == 'insurance')? $this->request->getParam('pass.1'):null;
        $gender = $this->request->getData('gender', null);
        $day = $this->request->getData('day', 'no');
        $time = $this->request->getData('time', null);
        
        if(!empty($time)) {
            $timeSplit = explode('-',$time);
            $this->loadModel('DoctorAvailability');
            $startTimeSlot = date('H:i:s',strtotime($timeSplit[0]));
            $endTimeSlot = date('H:i:s',strtotime($timeSplit[1]));
            // $todayofweek = date('N');
            //$next3Days = [$todayofweek, $todayofweek+1, $todayofweek+2];
            $availabileDoctors = $this->DoctorAvailability->find('list', [
                                'keyField' => 'id',
                                'valueField' => 'user_id'
                            ])
                            //->where(['DoctorAvailability.weekday IN' => $next3Days])
                            ->andWhere(function($exp) use($startTimeSlot, $endTimeSlot) {
                                $exp->lte('DoctorAvailability.available_from', $startTimeSlot);
                                $exp->gt('DoctorAvailability.available_upto', $endTimeSlot);
                                return $exp;
                             })
                            ->toArray();
            if (!empty($availabileDoctors)) {
                $doctors->where(['Users.id IN' => $availabileDoctors]);
            }
        }

        if (!empty($insurance)) {

            $doctors->innerJoinWith('UserInsurances', function ($q) use ($insurance){
                // return $q->where(['UserInsurances.insurance_id' => $insurance]);
                return $q->innerJoinWith('Insurances', function($p) use ($insurance){
                    return $p->where(['Insurances.slug' => $insurance]);
                });
                // where(['UserInsurances.insurance_id' => $insurance]);
            });

        }

        if (in_array($day, ['today', 'next'])) {
            if ($day == 'today') {
                $dayofweek = date('N');
                $doctors->innerJoinWith('DoctorAvailability', function ($q) use ($dayofweek){
                    return $q->where(['DoctorAvailability.weekday' => $dayofweek]);
                });
                // $doctors->notMatching('DoctorUnavailability', function ($q) use ($dayofweek){
                //     return $q->where(['DoctorUnavailability.weekday' => $dayofweek]);
                // });

            }else{
                $todayofweek = date('N');
                $next3Days = [$todayofweek, $todayofweek+1, $todayofweek+2];
                $doctors->innerJoinWith('DoctorAvailability', function ($q) use ($next3Days){
                    return $q->where(['DoctorAvailability.weekday IN' => $next3Days]);
                });
            }
        }

        if ($term) {
            $doctors->join([
                    'UserSpecialities' => [
                        'table' => 'user_specialities',
                        'type' => 'LEFT',
                        'conditions' => 'UserSpecialities.user_id = Users.id',
                    ],
                    'Specialities' => [
                        'table' => 'specialities',
                        'type' => 'LEFT',
                        'conditions' => "Specialities.id = UserSpecialities.speciality_id AND Specialities.slug LIKE '%{$term}%'",
                    ]
                ]);
            
            $term = explode(" ",$term)[0];
            $doctors->andWhere([
                'OR' => [
                    'UserProfiles.first_name LIKE' => "%" . $term . "%",
                    'UserProfiles.last_name LIKE' => "%" . $term . "%",
                    'Specialities.id IS NOT NULL'
                    ]
                ]);
        }

        if ($zip) {
           
            $latLng = $this->getLatandLong($zip);
            $search_radius = 15; //In Miles  

            if (!empty($latLng['lat']) && !empty($latLng['lng'])) {
                $distance = '('. 3959 .' * acos (cos ( radians('. $latLng['lat'] .') )
                                * cos( radians( UserProfiles.lat ) )
                                * cos( radians( UserProfiles.lng )
                                - radians('. $latLng['lng'] .') )
                                + sin ( radians('. $latLng['lat'] .') )
                                * sin( radians( UserProfiles.lat ) )))';

                $havingClause = ['distance <=' => $search_radius];

                $doctors
                ->contain([
                    'UserProfiles' => function ($q) use ($distance) {
                        return $q->select([
                            'lat',
                            'lng',
                            'country_id',
                            'state_id',
                            'first_name',
                            'last_name',
                            'zipcode',
                            'gender',
                            'distance' => $distance,
                            'address'
                        ]);
                    },

                ])
                ->having($havingClause);
                
            }else{
                $doctors->andWhere(["UserProfiles.zipcode" => $zip]);
            }

        }

        if ($gender){
            switch ($gender) {
                case 'm':
                    $doctors->andWhere(['UserProfiles.gender' => 'Male']);
                    break;

                case 'f':
                    $doctors->andWhere(['UserProfiles.gender' => 'Female']);
                    break;
                
                default:
                    break;
            }
        }


        $doctors->contain(['UserSpecialities.Specialities']);

        $this->loadModel('Bookings');
        $booking = $this->Bookings->find('list',[
                    'keyField' => 'id',
                    'valueField' => 'doctor_id'
                ])
                ->where(['Bookings.patient_id' => $this->Auth->user('id')])
                ->group('Bookings.doctor_id')
                ->toArray();

        $this->loadModel('IllnessReasons');

        $field_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? 'title_german' : 'title_eng';
        $specialities = $this->IllnessReasons->find('list',[
            'keyField' => 'id',
            'valueField' => $field_name
        ])->toArray();

        $this->loadModel('Insurances');
        $insurances = $this->Insurances->find('list',[
            'keyField' => 'slug',
            'valueField' => 'title_eng'
        ])->toArray();

    
        $timeOptions = ['before' => 'Before 10 AM','after' => 'After 5 PM'];

        $userPatients = [
            ['value' => 1, 'id' => 'me', 'text' => 'Me', 'onclick' => "showHideDetails('me')" , "checked" => "checked"],
            ['value' => 2, 'id' => 'smelse', 'text' => 'Someone Else', 'onclick' => "showHideDetails('smelse')"]
        ];

        $userPatients_german = [
            ['value' => 1, 'id' => 'me', 'text' => 'Ich', 'onclick' => "showHideDetails('me')" , "checked" => "checked"],
            ['value' => 2, 'id' => 'smelse', 'text' => 'Jemand anderes', 'onclick' => "showHideDetails('smelse')"]
        ];


        $seeing_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $userPatients_german : $userPatients;


        $state_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $states_german : $states;

        $this->loadModel('MetaTags');

        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.page_name' => $test])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }
        

        // $doctors = $doctors->toArray();  
        // pr($doctors); die;
        // debug($doctors); die; 
        $this->paginate($doctors);
        $this->set(compact('seeing_name','doctors','userPatients','insurances', 'specialities','timeOptions','favourite','booking','state_name','meta_title','meta_description'));

    }


    public function getLatandLong($zip){
        $url = "http://maps.googleapis.com/maps/api/geocode/json?components=country:AT|postal_code:".urlencode($zip)."&amp;sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        if ($result['status'] != 'OK') {

            return [
                'lat' => null, 
                'lng' => null
            ];
        }
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
       
        return $result3[0];
    }
                

    public function doctorDetail() {
        $this->viewBuilder()->setLayout('frontend');
    }

    // public function login() {
    //     if ($this->request->is('post')) {

    //         // Auth component identify if sent user data belongs to a user
    //         $user = $this->Auth->identify();
    //         if (!$user['is_email_verified']) {
    //             $this->Flash->error(__('Your email is not verified. Please verify your email by clicking on the link sent to your email address while signup'));
    //             return $this->redirect($this->referer());
    //         }

    //         if ($user) {
    //             $this->Auth->setUser($user);
    //             return $this->redirect($this->referer());
    //         }
    //         $this->Flash->error(__('Invalid username or password, try again.'));
    //     }
    // }

}