<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash'); 
        $this->loadComponent('Email');
        $this->Auth->allow(['index','contactUs']);
 
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index(){
        //pr($this->request->getSession()->read('Auth.User'));die;
        $this->viewBuilder()->setLayout('home');
        $this->loadModel('Insurances');
        $insurances = $this->Insurances->find('list',[
            'keyField' => 'slug',
            'valueField' => 'title_eng'
        ])->toArray();

        $this->loadModel('Specialities');

        $field_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? 'title_german' : 'title_eng'; 
        $specialities = $this->Specialities
            ->find('list',[
            'keyField' => 'slug',
            'valueField' => $field_name])->toArray();

        // $specialities = $this->Specialities
        //     ->find('list',[
        //     'keyField' => 'id',
        //     'valueField' => $field_name])->toArray();

        
        $states = [
            1050 => "Vienna (Wien)",
            2431 => "Lower Austria (Niederösterreich)",
            4310 => "Upper Austria (Oberösterreich)",
            8000 => "Styria (Steiermark)",
            6000 => "Tyrol (Tirol)",
            9324 => "Carinthia (Kärnten)",
            5321 => "Salzburg",
            6700 => "Vorarlberg",
            8380 => "Burgenland",
        ];

        $states_german = [
            1050 => "Wien",
            2431 => "Niederösterreich",
            4310 => "Oberösterreich",
            8000 => "Steiermark",
            6000 => "Tirol",
            9324 => "Kärnten",
            5321 => "Salzburg",
            6700 => "Vorarlberg",
            8380 => "Burgenland",
        ];

        $state_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $states_german : $states;
        $this->set(compact('insurances','specialities', 'state_name'));
    }

    public function contactUs() {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('MetaTags');
        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.slug' => 'Contact Us'])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }
        if ($this->request->is('post')) {
            $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 12])
                        -> first();
                $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];
                $field_name = str_replace(
                    array('#NAME','#EMAIL','#PHONE','#MESSAGE'),
                    array(
                        $this->request->data['name'],
                        $this->request->data['email'],
                        $this->request->data['telephone'],
                        $this->request->data['message']
                    ),
                    $field_name
                );
                // sending forgot password mail
            $this->Email->_sendEmailMessage('bookdoc.office@gmail.com', $field_name, $temp['subject']);
            $this->Flash->success(__('Your Request has been submitted.We will contact back to you soon.'));
            $this->redirect($this->referer());
        }
        $this->set(compact('meta_title','meta_description'));
    }

   
}