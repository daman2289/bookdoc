<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Twilio\Rest\Client;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;

/**
 * Designs Controller
 *
 */
class AppointmentsController extends AppController
{
	public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadComponent('Upload');
        $this->loadComponent('User');
    }

    public function index() {
    	$this->viewBuilder()->setLayout('frontend_dashboard');
    	$url = Router::url(['controller' => 'Users', 'action' => 'chart','prefix' => 'api']);
		$this->set(compact('url'));
    }

    public function past() {
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('Bookings');
        
        $doctorBookings = $this->Bookings
                                ->findByDoctorId($this->Auth->user('id'))
                                ->where(['booking_date < ' => new \DateTime()])
                                ->contain(['IllnessReasons', 'Patient.SpamPatients', 'Patient.UserProfiles']);

        
        // pr($doctorBookings->toArray()); die;

        $this->loadModel('IllnessReasons');
        $this->loadModel('Bookings');
        $illnessReasons = $this->IllnessReasons->find('list',[
            'keyField' => 'id',
            'valueField' => 'title_eng'
        ]);

        $doctorPatients = $this->Bookings
            ->find('list',[
                'valueField' => 'patient_first_name',
                'keyField' => 'patient_id'
            ])
            ->where(['doctor_id' => $this->Auth->user('id')])
            ->toArray();

        $this->paginate($doctorBookings);
		$this->set(compact('doctorBookings', 'doctorPatients', 'illnessReasons'));
    }

}