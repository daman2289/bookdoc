<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Insurances Controller
 *
 * @property \App\Model\Table\InsurancesTable $Insurances
 *
 * @method \App\Model\Entity\Insurance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InsurancesController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {   
        parent::initialize();
        $this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Paginator');
        $this->viewBuilder()->setLayout('backend');       
          
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $conditions = [];
        if ($this->request->getQuery('search')) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'Insurances.title_eng LIKE ' => '%'. $itemSearch .'%',
                   'Insurances.title_italic LIKE ' => '%'. $itemSearch .'%',
                   'Insurances.title_french LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = [
            'AND' => [                   
                'Insurances.is_deleted' => Configure::read('Insurance.is_deleted_default_insurance')
            ]
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'limit'      => Configure::read('Limit')
        ];
        $insurances = $this->paginate($this->Insurances);

        $this->set(compact('insurances'));
    }

    /**
     * View method
     *
     * @param string|null $id Insurance id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $insurance = $this->Insurances->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('insurance', $insurance);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $insurance = $this->Insurances->newEntity();
        if ($this->request->is('post')) {
            $insurance = $this->Insurances->patchEntity($insurance, $this->request->getData());
            $insurance->user_id = $this->Auth->user('id');
            if ($this->Insurances->save($insurance)) {
                $this->Flash->success(__('The insurance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insurance could not be saved. Please, try again.'));
        }
        $users = $this->Insurances->Users->find('list', ['limit' => 200]);
        $this->set(compact('insurance', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Insurance id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $insurance = $this->Insurances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $insurance = $this->Insurances->patchEntity($insurance, $this->request->getData());
            if ($this->Insurances->save($insurance)) {
                $this->Flash->success(__('The insurance has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The insurance could not be saved. Please, try again.'));
        }
        $users = $this->Insurances->Users->find('list', ['limit' => 200]);
        $this->set(compact('insurance', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Insurance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $insurance = $this->Insurances->get($id);
        if ($this->Insurances->delete($insurance)) {
            $this->Flash->success(__('The insurance has been deleted.'));
        } else {
            $this->Flash->error(__('The insurance could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Status change method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function status($id) {
        $this->viewBuilder()->setLayout('ajax');   
        if ($this->request->is('ajax')) {
            $insurance = $this->Insurances->get($id);
            $insurance->status = ($insurance->status) ? Configure::read('Insurance.is_active_default'): Configure::read('Insurance.deactive');
            $flag   = false;
            if ($this->Insurances->save($insurance)) {
                $flag   = true;                    
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }
}
