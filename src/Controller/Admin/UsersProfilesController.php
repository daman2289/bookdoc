<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * UsersProfiles Controller
 *
 *
 * @method \App\Model\Entity\UsersProfile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersProfilesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $usersProfiles = $this->paginate($this->UsersProfiles);

        $this->set(compact('usersProfiles'));
    }

    /**
     * View method
     *
     * @param string|null $id Users Profile id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersProfile = $this->UsersProfiles->get($id, [
            'contain' => []
        ]);

        $this->set('usersProfile', $usersProfile);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersProfile = $this->UsersProfiles->newEntity();
        if ($this->request->is('post')) {
            $usersProfile = $this->UsersProfiles->patchEntity($usersProfile, $this->request->getData());
            if ($this->UsersProfiles->save($usersProfile)) {
                $this->Flash->success(__('The users profile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users profile could not be saved. Please, try again.'));
        }
        $this->set(compact('usersProfile'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Profile id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersProfile = $this->UsersProfiles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersProfile = $this->UsersProfiles->patchEntity($usersProfile, $this->request->getData());
            if ($this->UsersProfiles->save($usersProfile)) {
                $this->Flash->success(__('The users profile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users profile could not be saved. Please, try again.'));
        }
        $this->set(compact('usersProfile'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Profile id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersProfile = $this->UsersProfiles->get($id);
        if ($this->UsersProfiles->delete($usersProfile)) {
            $this->Flash->success(__('The users profile has been deleted.'));
        } else {
            $this->Flash->error(__('The users profile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
