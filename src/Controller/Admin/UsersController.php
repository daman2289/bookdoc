<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Utility\Security;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {   
        parent::initialize();
        $this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Paginator');
        $this->viewBuilder()->setLayout('backend');      
    
       // $this->Auth->allow(['add']);
   
    }
    
    /**
     * Login method to login in admin section
     *
     * @return \Cake\Network\Response|null
     */

    public function login()
    {
        $this->set('title', 'Login');
        $this->viewBuilder()->setLayout('backend_login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
           
            if ($user) {  
              
            
                $this->Auth->setUser($user);                                                
                return $this->redirect(['action' => 'dashboard']);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }


    
    public function logout()
    {           
        $this->Flash->success(__('You are now logged out.'));
        return $this->redirect($this->Auth->logout() );
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set('title', 'Manage Users');
        $this->viewBuilder()->setLayout('backend');
        //search
        $conditions = [];
        if (!empty($this->request->getQuery('search'))) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'email LIKE ' => '%'. $itemSearch .'%',
                   'UserProfiles.first_name LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = ['AND' => 
                [
                    'Users.id <>' => $this->Auth->user('id'),
                    'Users.role_id ' => ROLE_SUB_ADMIN,
                    // 'is_deleted' => Configure::read('User.is_deleted_default_user')
                ]
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'contain'    => ['Roles', 'UserProfiles'],
            'limit'      => Configure::read('Limit'),
            'sortWhitelist' => [
               'UserProfiles.first_name', 
               'UserProfiles.last_name', 
               'UserProfiles.phone_number', 
               'Users.id', 
               'Users.email', 
               'Users.created',
               'Users.status',
               'Roles.type',
            ],
            'order'      => ['Users.id desc']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * doctors list method
     *
     * @return \Cake\Http\Response|void
     */
    public function doctors()
    {
        $this->set('title', 'Manage Doctors');
        $this->viewBuilder()->setLayout('backend');
        //search
        $conditions = [];
        if (!empty($this->request->getQuery('search'))) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'email LIKE ' => '%'. $itemSearch .'%',
                   'UserProfiles.first_name LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = ['AND' => 
                [
                    'Users.id <>' => $this->Auth->user('id'),
                    'Users.role_id IN' => [ROLE_DOCTOR,ROLE_HOSPITAL],
                    'Users.is_delete' => 0,
                    'Users.is_approved' => 1
                ]
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'contain'    => ['Roles', 'UserProfiles','HospitalProfiles','UserSpecialities' => ['Specialities'],'UserEducations'],
            'limit'      => Configure::read('Limit'),
            'sortWhitelist' => [
               'UserProfiles.first_name', 
               'UserProfiles.last_name', 
               'UserProfiles.phone_number', 
               'Users.id', 
               'Users.email', 
               'Users.created',
               'Users.status',
               'Roles.type',
            ],
            'order'      => ['Users.id desc']
        ];
        $users = $this->paginate($this->Users);
        //pr($users);die;
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }


    /**
     * patients list method
     *
     * @return \Cake\Http\Response|void
     */
    public function patients()
    {
        $this->set('title', 'Manage Patients');
        $this->viewBuilder()->setLayout('backend');
        //search
        $conditions = [];
        if (!empty($this->request->getQuery('search'))) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'email LIKE ' => '%'. $itemSearch .'%',
                   'UserProfiles.first_name LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = ['AND' => 
                [
                    'Users.id <>' => $this->Auth->user('id'),
                    'Users.role_id ' => ROLE_PATIENT
                ]
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'contain'    => ['Roles', 'UserProfiles'],
            'limit'      => Configure::read('Limit'),
            'sortWhitelist' => [
               'UserProfiles.first_name', 
               'UserProfiles.last_name',
               'UserProfiles.phone_number', 
               'Users.id', 
               'Users.email', 
               'Users.created',
               'Users.status',
               'Roles.type',
            ],
            'order'      => ['Users.id desc']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles','UserProfiles']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        $type = !empty($this->request->getQuery('type')) ? base64_decode($this->request->getQuery('type')) : ROLE_SUB_ADMIN;
        if ($this->request->is('post')) {
            $day = $this->request->getData('user_profile.dob.day', '11');
            $month = $this->request->getData('user_profile.dob.month', '07');
            $year = $this->request->getData('user_profile.dob.year', '1971');

            $this->request->data['user_profile']['dob']['date'] = "{$year}-{$month}-{$day}";
            $this->request->data['user_profile']['state_id'] = 277;
            $this->request->data['user_profile']['country_id'] = 14;
            
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['UserProfiles']]);
            $user->is_approved  =  Configure::read('User.is_approved');
            $saved = $this->Users->save($user);
            if ($saved) {
                $this->loadComponent('Email');
                //Send email to user
                $this->Email->sendVerifyEmail($saved->id);
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect($this->referer());
            } 
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', [
            'keyfield' => 'id', 
            'valueField' => 'type'
            ]
        )->where([
            'id' => $type
        ]);

        $conditionArrRole = [ROLE_SUB_ADMIN => 'Subadmin', ROLE_DOCTOR => 'Doctor', ROLE_PATIENT => 'Patient'];
        $pageTitle = ROLE_SUB_ADMIN;
        if ($type == ROLE_SUB_ADMIN) {
            $conditionArrRole = [ROLE_SUB_ADMIN => 'Subadmin'];
            $pageTitle = 'Subadmin';
        }

        if ($type == ROLE_DOCTOR) {
            $conditionArrRole = [ROLE_DOCTOR => 'Doctor'];
            $pageTitle = 'Doctor';
        }
        if ($type == ROLE_PATIENT) {
            $conditionArrRole = [ROLE_PATIENT => 'Patient'];
            $pageTitle = 'Patient';
        }
        $this->set(compact('user', 'roles', 'conditionArrRole', 'pageTitle'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserProfiles', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['UserProfiles']]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'patients']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', [
            'keyfield' => 'id', 
            'valueField' => 'type'
            ]
        )->where([
            'id NOT IN' => [ROLE_ADMIN]
        ]);
        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Users->get(base64_decode($id));
        $user->is_delete = 1;        
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'dashboard']);
    }

    public function dashboard()
    {
        $this->set('title', 'Dashboard');
        $this->viewBuilder()->setLayout('backend');
        $this->loadModel('Bookings');

        $totalDoctors = $this->Users->find()->where(
            [
                'role_id' => ROLE_DOCTOR
            ]
        )->count();

        $sponsered_doctors = $this->Users->find()->where(
            [
                'role_id' => ROLE_DOCTOR,
                'is_sponsored' => 1
            ]
        )->count();

        $totalPatients  = $this->Users->find()->where(
            [
                'role_id' => ROLE_PATIENT
            ]
        )->count();

        $appointments = $this->Bookings->find()
                        ->where(['Bookings.status' => 1])
                        ->count();
                        
        $conditions = [];
        if (!empty($this->request->getQuery('search'))) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'email LIKE ' => '%'. $itemSearch .'%',
                   'UserProfiles.first_name LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = [
            'AND' => 
                [
                    'Users.id <>' => $this->Auth->user('id'),
                    'Users.role_id ' => ROLE_DOCTOR
                ],
                'Users.is_approved' => 0
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'contain'    => ['Roles', 'UserProfiles','UserSpecialities' => ['Specialities'],'UserEducations'],
            'limit'      => Configure::read('Limit'),
            'sortWhitelist' => [
               'UserProfiles.first_name', 
               'UserProfiles.last_name', 
               'UserProfiles.phone_number', 
               'Users.id', 
               'Users.email', 
               'Users.created',
               'Users.status',
               'Users.is_approved',
               'Roles.type',
            ],
            'order'      => [
                'Users.is_approved' => 'ASC',
            ]
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);



        $this->set(compact('totalDoctors', 'totalPatients','sponsered_doctors','appointments'));
    }
    

    /**
     * Status change method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function status($id) {
        $this->viewBuilder()->setLayout('ajax');   
        if ($this->request->is('ajax')) {
            $user = $this->Users->get(base64_decode($id));
            $user->is_sponsored = ($user->is_sponsored) ? 0 : 1;
            $flag   = false;
            if ($this->Users->save($user)) {
                $flag   = true;                    
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }


    public function approve($id) {
        $this->viewBuilder()->setLayout('ajax');   
        if ($this->request->is('ajax')) {
            $user = $this->Users->findById(base64_decode($id))->contain(['UserProfiles'])->firstOrFail();
            $user->is_approved = ($user->is_approved) ? 0 : 1;
            $flag   = false;
            
            if ($saved = $this->Users->save($user)) {
                $flag   = true;                    
            }else{
                return;
            }
            $message = "Account has been Deactivated";
            if ($user->is_approved) {
                $message = "Account has been Approved";
                $this->loadComponent('Email');
                $this->Email->sendDoctorAccountApprovedEmail($user);
            }
            $this->set(compact('flag', 'message'));
            $this->set('_serialize', ['flag', 'message']);
        }
    }

    public function deactivate($id) {
        if ($this->request->is('ajax')) {
            $user = $this->Users->findById(base64_decode($id))->contain(['UserProfiles'])->firstOrFail();
            if($user->role_id == 5) {
                $this->loadModel('HospitalDoctors');
                $hospital_doctor = $this->HospitalDoctors->find()
                        ->where(['HospitalDoctors.hospital_id' => base64_decode($id)])
                        ->toArray();

                foreach ($hospital_doctor as $key => $value) {
                    $doctor = $this->Users->findById($value->user_id)->contain(['UserProfiles'])->firstOrFail();
                    $doctor->is_active = ($doctor->is_active) ? 0 : 1;
                    $flag   = false;
                    if ($saved = $this->Users->save($doctor)) {
                        $flag   = true;                    
                    }else{
                        return;
                    }
                }
            }
            $user->is_active = ($user->is_active) ? 0 : 1;
            $flag   = false;
            
            if ($saved = $this->Users->save($user)) {
                $flag   = true;                    
            }else{
                return;
            }
            $message = "Account has been Deactivated";
            if ($user->is_active) {
                $message = "Account has been Activated";
                $this->loadComponent('Email');
                $this->Email->sendDoctorAccountApprovedEmail($user);
            }
            $this->set(compact('flag', 'message'));
            $this->set('_serialize', ['flag', 'message']);
        }
    }

    public function deleteDoctor($id) {
        if ($this->request->is('ajax')) {
            $user = $this->Users->findById(base64_decode($id))->contain(['UserProfiles'])->firstOrFail();
            if($user->role_id == 5) {
                $this->loadModel('HospitalDoctors');
                $hospital_doctor = $this->HospitalDoctors->find()
                        ->where(['HospitalDoctors.hospital_id' => base64_decode($id)])
                        ->toArray();

                foreach ($hospital_doctor as $key => $value) {
                    $doctor = $this->Users->findById($value->user_id)->contain(['UserProfiles'])->firstOrFail();
                    $doctor->is_delete = ($doctor->is_delete) ? 0 : 1;
                    $flag   = false;
                    if ($saved = $this->Users->save($doctor)) {
                        $flag   = true;                    
                    }else{
                        return;
                    }
                }
            }
            $user->is_delete = ($user->is_delete) ? 0 : 1;
            $flag   = false;
            if ($saved = $this->Users->save($user)) {
                $flag   = true;                    
            }else{
                return;
            }
            $message = "Account has been Deactivated";
            if ($user->is_delete) {
                $message = "Account has been Deleted";
            }
            $this->set(compact('flag', 'message'));
            $this->set('_serialize', ['flag', 'message']);
        }
    }
    
    /**
     * changePassword admin method
     *
     * @return \Cake\Http\Response|void
     */
    public function changePassword() {       
        $this->viewBuilder()->setLayout('backend'); 
        if ($this->request->is('post')  && !empty($this->request->getData('password'))) {            
            if ($this->request->getData('password') !== $this->request->getData('confirm_password')) {
                $this->Flash->error('Password and confirm password did not matched');
                return $this->redirect($this->referer());
            }
            $user = $this->Users->get($this->Auth->user('id'));           
            if (!(new DefaultPasswordHasher)->check(
                    $this->request->getData('old_password'),
                    $user->password
                )) {                
                $this->Flash->error('Old Password did not matched');
                return $this->redirect($this->referer());
            }
            //Update Password
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->id = $this->Auth->user('id');
            if ($this->Users->save($user)) {
                $this->Flash->success('Password has been updated succesfully');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Password could not be updated. Please, try again.');                        
            }
        }
    }  
   
    /**
     * forgotPassword method
     *
     * @param NULL.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function forgotPassword() 
    {
        $this->viewBuilder()->setLayout('backend_login');
        $this->loadModel('EmailTemplates');
        if ($this->request->is('post')) {
            if (!empty($this->request->getData())) {
             $email = $this->request->getData('email');
             $user  = $this->Users->findByEmail($email)
                    ->contain(['UserProfiles'])
                    ->where(['Users.role_id IN' => [ROLE_ADMIN]])
                    ->first(); //admin, subadmin
                if (!empty($user)) {
                    $password = sha1(Text::uuid());
                    $password_token = Security::hash($password, 'sha256', true);
                    $hashval = sha1($user->email . rand(1, 100));
                    $user->password_reset_token = $password_token;
                    $user->hashval = $hashval;
                    $reset_token_link = Router::url(
                            [
                            'controller' => 'Users', 
                            'action' => 'resetPassword'
                            ], TRUE) . '/' . $password_token . '#' . $hashval;
                    
                    $this->loadComponent("Email");
                    $this->Email->sendForgotPasswordEmail($user, $reset_token_link);
                    
                    $this->Users->save($user);
                    $this->Flash->success(
                        'Please click on password reset link, sent in your email address to reset password.'
                    );
                }
                else{
                    $this->Flash->error('Sorry! Email address is not available here.');
                }
            }
        }
    }

    /**
     * resetPassword method
     *
     * @param NULL.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resetPassword($token = null) {
        $this->viewBuilder()->setLayout('backend_login');
        if (!empty($token)) {
            $user = $this->Users->findByPasswordResetToken($token)
            ->contain([
                'UserProfiles'
            ])
            ->first();
            if ($user) {                    
                if (!empty($this->request->getData())) {
                    $user = $this->Users->patchEntity($user, [
                        'password' => $this->request->getData('new_password'),
                        'new_password' => $this->request->getData('new_password'),
                        'confirm_password' => $this->request->getData('confirm_password')
                            ], ['validate' => 'password']
                    );
                    $hashval_new = sha1($user->email . rand(1, 100));
                    $user->password_reset_token = $hashval_new;
                    if ($this->Users->save($user)) {
                        $this->Flash->success('Your password has been changed successfully');
                        $emaildata = ['name' => $user->first_name, 'email' => $user->email];                            
                        $this->redirect(['action' => 'login']);
                    } else {
                        $this->Flash->error('Error changing password. Please try again!');
                    }
                }
            } else {
                $this->Flash->error('Sorry your password token has been expired.');
            }
        } else {
            $this->Flash->error('Error loading password reset.');
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function bookings(){
        $conditions = [];
        $this->loadModel('Bookings');
        if ($this->request->getQuery('search')) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'Bookings.patient_first_name LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => ['Users' => ['UserProfiles']],
            'limit'      => Configure::read('Limit')
        ];
        $bookings = $this->paginate($this->Bookings);
        $this->set(compact('bookings'));
    }

    public function deleteBooking($id){
        $id = base64_decode($id);
        $this->loadModel('Bookings');
        $entity = $this->Bookings->findById($id)->firstOrFail();
        $result = $this->Bookings->delete($entity);
        if ($result) {
            $this->Flash->success('Booking deleted successfully');
        }else{
            $this->Flash->error('Bookings was not deleted');
        }
        return $this->redirect(['action' => 'bookings']);
    }

    public function manageSubscription() {
        $this->viewBuilder()->setLayout('backend');
        $this->loadModel('FreeSubscriptions');
        $user = $this->FreeSubscriptions->find()
                ->first();
        $this->request->data['id'] = $user->id;
        $user_data = $this->FreeSubscriptions->patchEntity($user, $this->request->data);
        if ($this->FreeSubscriptions->save($user_data)) {
        } else {
            $this->Flash->error('Days could not be updated. Please, try again.');                        
        }
        $this->set(compact('user'));
    }

    public function approveDoctor($id) {
        $user = $this->Users->findById(base64_decode($id))->contain(['UserProfiles'])->firstOrFail();
            $user->is_approved = ($user->is_approved) ? 0 : 1;
            $flag   = false;
            
            if ($saved = $this->Users->save($user)) {
                $this->loadComponent('Email');
                $this->Email->sendDoctorAccountApprovedEmail($user);
                $this->Flash->success('Doctor has been approved');         
            }else{
                $this->Flash->error('Doctor could not be approved. Please, try again.');
            }
        return $this->redirect(['action' => 'dashboard']);   
    }
    
}