<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
/**
 * Specialities Controller
 *
 * @property \App\Model\Table\SpecialitiesTable $Specialities
 *
 * @method \App\Model\Entity\Speciality[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SpecialitiesController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {   
        parent::initialize();
        $this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Paginator');
        $this->viewBuilder()->setLayout('backend');       
          
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $conditions = [];
        if ($this->request->getQuery('search')) {
            $itemSearch = $this->request->getQuery('search');
            $this->set('itemSearch', $itemSearch);
            $conditions[] = [
               'or' => [
                   'Specialities.title_eng LIKE ' => '%'. $itemSearch .'%',
                   'Specialities.title_italic LIKE ' => '%'. $itemSearch .'%',
                   'Specialities.title_french LIKE ' => '%'. $itemSearch .'%',
               ]
            ];
        }
        $conditions[] = [
            'AND' => [                   
                'Specialities.is_deleted' => Configure::read('Speciality.is_deleted_default_speciality')
            ]
        ];
        $this->paginate = [
            'conditions' => $conditions,
            'limit'      => Configure::read('Limit')
        ];
        $specialities = $this->paginate($this->Specialities);

        $this->set(compact('specialities'));
    }

    /**
     * View method
     *
     * @param string|null $id Speciality id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $speciality = $this->Specialities->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('speciality', $speciality);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $speciality = $this->Specialities->newEntity();
        if ($this->request->is('post')) {
            $speciality = $this->Specialities->patchEntity($speciality, $this->request->getData());
            $speciality->user_id = $this->Auth->user('id');
            if ($this->Specialities->save($speciality)) {
                $this->Flash->success(__('The Specialty has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Specialty could not be saved. Please, try again.'));
        }
        $users = $this->Specialities->Users->find('list', ['limit' => 200]);
        $this->set(compact('speciality', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Speciality id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $speciality = $this->Specialities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $speciality = $this->Specialities->patchEntity($speciality, $this->request->getData());
            if ($this->Specialities->save($speciality)) {
                $this->Flash->success(__('The Specialty has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Specialty could not be saved. Please, try again.'));
        }
        $users = $this->Specialities->Users->find('list', ['limit' => 200]);
        $this->set(compact('speciality', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Speciality id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $speciality = $this->Specialities->get($id);
        if ($this->Specialities->delete($speciality)) {
            $this->Flash->success(__('The Specialty has been deleted.'));
        } else {
            $this->Flash->error(__('The Specialty could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Status change method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function status($id) {
        $this->viewBuilder()->setLayout('ajax');   
        if ($this->request->is('ajax')) {
            $speciality = $this->Specialities->get($id);
            $speciality->status = ($speciality->status) ? Configure::read('Speciality.is_active_default'): Configure::read('Speciality.deactive');
            $flag   = false;
            if ($this->Specialities->save($speciality)) {
                $flag   = true;                    
            }
            $this->set(compact('flag'));
            $this->set('_serialize', ['flag']);
        }
    }
}
