<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

/**
 * Designs Controller
 *
 */
class PatientsController extends AppController
{
	public function initialize() {
        parent::initialize();
        // $this->Auth->allow();
    }

    public function dashboard() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $patientBookings = $this->Bookings->findByPatientId($userId)
                                        ->where([
                                            'CONCAT(Bookings.booking_date,"T",Bookings.booking_time) > NOW()'
                                        ])
                                        ->contain(['Users' => ['UserProfiles']])
                                        ->order(['booking_date' => "DESC"]);

        $totalBookings = $this->Bookings->findByPatientId($userId)
                                        ->count();

        $upcomingBookings = $this->Bookings->findByPatientId($userId)
                                            ->where([
                                                'CONCAT(Bookings.booking_date,"T",Bookings.booking_time) > NOW()'
                                            ])
                                            ->count();

        $previousBooking = $this->Bookings->findByPatientId($userId)
                                            ->where(['status' => 2])
                                            ->where([
                                                'CONCAT(Bookings.booking_date,"T",Bookings.booking_time) < NOW()'
                                            ])
                                            ->count();
        
        $this->paginate($patientBookings);
        $this->set(compact('patientBookings','totalBookings', 'upcomingBookings', 'previousBooking'));
    }

    public function medicalTeam() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $patientBookings = $this->Bookings->findByPatientId($userId);

        // $completedCase = $patientBookings->func()->count('doctor_id');

        $patientBookings = $patientBookings
                        ->contain([
                            'Users' => ['UserProfiles', 'UserSpecialities' => ['Specialities']],
                        ])
                        ->select([
                            'booking_date', 'booking_time','doctor_id',
                            'Users.id',
                            'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.gender',
                            'UserProfiles.dob','UserProfiles.address','UserProfiles.zipcode','UserProfiles.profile_pic'
                            
                        ]);
                        // ->groupBy('doctor_id');
        $this->set(compact('patientBookings'));
    }

    public function pastAppointment() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('Bookings');
        $this->loadModel('DoctorRatings');
        $this->Bookings->belongsTo('Users', [
            'foreignKey' => 'doctor_id',
            'saveStrategy' => 'replace'
        ]);
        $userId = $this->Auth->user('id');
        $this->Bookings->hasOne('DoctorRatings', [
            'foreignKey' => 'booking_id',
            'saveStrategy' => 'replace',
            'conditions' => ['DoctorRatings.patient_id' => $userId]
        ]);
        $appointments = $this->Bookings->findByPatientId($userId)
                        ->contain([
                            'DoctorRatings',
                            'Users' => ['UserProfiles','UserEducations','UserSpecialities' => ['Specialities']]
                        ])
                        ->select([
                            'Bookings.id',
                            'Bookings.booking_date', 'Bookings.booking_time', 
                            'Bookings.status','Bookings.doctor_id',
                            'Users.id', 'DoctorRatings.overall_rating', 'DoctorRatings.bedside_manner','DoctorRatings.wait_time',
                            'DoctorRatings.id', 'DoctorRatings.booking_id', 'DoctorRatings.patient_id', 'DoctorRatings.review',
                            'UserProfiles.first_name', 'UserProfiles.last_name',  'UserProfiles.profile_pic',  'UserProfiles.dob'
                            ]);
        
        $time = $appointments->func()->concat([
            'Bookings.booking_date' => 'identifier',
            'T',
            'Bookings.booking_time' => 'identifier'
        ]);

        $appointments = $appointments->select(['bookingTime' => $time])
                            ->where([
                                'CONCAT(Bookings.booking_date,"T",Bookings.booking_time) < NOW()'
                            ]);
        
        $appointment = $this->paginate($appointments);
        $this->set(compact('appointment'));
    }

    public function addRating() {
        $this->loadModel('DoctorRatings');
        $rating = $this->DoctorRatings->newEntity();
        if($this->request->is('post')) {
            $this->request->data['patient_id'] = $this->Auth->user('id');
            $rating = $this->DoctorRatings->patchEntity($rating, $this->request->data);
            if ($this->DoctorRatings->save($rating)) { 
                $this->Flash->success(__('Rating has been added successfully'));
                $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Rating has not been added successfully'));
            }
        }
    }

      public function favoriteDoctors() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        
        $userId = $this->Auth->user('id');
        $this->loadModel('DoctorFavourites');
        $doctorFavourite = $this->DoctorFavourites
                                ->findByPatientId($userId)
                                ->contain(['Users' => ['UserProfiles', 'UserSpecialities' => 'Specialities']]);

        // pr($doctorFavourite->toArray()); die;
        $this->set(compact('doctorFavourite'));
    }




     /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editProfile() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Users');
        $countryList = $this->Countries->loadCountries();
        $countryId = (!empty($this->Auth->user('user_profile.country_id'))) ? $this->Auth->user('user_profile.country_id') : 14;
        $stateList = $this->States->loadStates($countryId);
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UserProfiles']
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            if (!empty($this->request->getData()['upload_image']['name'])) {
                $file = $this->request->getData()['upload_image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions

                if (in_array($ext, $arr_ext)) {

                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'profile_image_' . $file['name'])) {
                        //prepare the filename for database entry
                        $user->user_profile->profile_pic = 'profile_image_' . $file['name'];
                    }
                }
            }

            $this->request->data('user_profile.phone_number', $this->request->data('full_number'));
            $this->request->data('user_profile.dob', $this->request->getData('user_profile.year') . '-' . $this->request->getData('user_profile.month') . '-' . $this->request->getData('user_profile.date'));

            $user = $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['UserProfiles']]);

            if ($this->Users->save($user)) {

                $this->Auth->setUser($user);
                $this->Flash->success(__('The profile has been updated.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The profile could not be updated. Please, try again.'));
        }
        $this->set(compact('user', 'countryList', 'stateList'));
    }


    


    public function changePassword() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('Users');
        $user = $this->Users->get($this->Auth->user('id'));
        if ($this->request->is('post')) {
            if (!empty($this->request->getData())) {
                $user = $this->Users->patchEntity($user, [
                    'old_password' => $this->request->getData()['old_password'],
                    'password' => $this->request->getData()['password']
                        ], ['validate' => 'password']
                );

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Password successfully changed'));
                    $this->redirect(['action' => 'changePassword']);
                } else {
                    $data = $this->_setValidationError($user->errors());
                    $this->Flash->error($data);
                }
            }
        }
        $this->set('user', $user);
    }


    public function reports() {
        $this->viewBuilder()->setLayout('patient_dashboard');
        $this->loadModel('PatientReports');
       
        $report = $this->PatientReports->find()
                ->where(['PatientReports.patient_id' => $this->Auth->user('id')])
                ->contain(['Users' => ['UserProfiles']]);
        
        $reports = $this->paginate($report);
        $this->set(compact('reports'));
    }

    public function download($id) {
        $this->loadModel('PatientReports');
        $file = $this->PatientReports->find()
                ->where(['PatientReports.id' => base64_decode($id)])
                ->first();
        
        $filePath = WWW_ROOT .'reports_patients'. DS . 'uploads' . DS .$file->file;
        
        $response = $this->response->withFile($filePath ,
            array('download'=> true, 'name'=> $file->file));

        return $response;   
        
    }

    public function addToSpam(){
        if ($this->request->is(['post'])){
            $this->loadModel('SpamPatients');
            $entity = $this->SpamPatients->newEntity();
            $entity->doctor_id = $this->Auth->user('id');
            $entity = $this->SpamPatients->patchEntity($entity,$this->request->getData());
            $saved = $this->SpamPatients->save($entity);
            if (!$saved) {
                $this->Flash->error(__('Unable to Spam Patient. Please try again later'));
                throw new NotFoundException("Unable to save.");        
            }
            $this->Flash->success(__('Patient Successfully marked as spam.'));
            return $this->redirect($this->referer());

        }
        throw new NotFoundException("Invalid Request");
        return $this->redirect($this->referer());
        
    }

     public function cancelBooking(){
        $bid = $this->request->getData('booking_id', null);
        $reason = $this->request->getData('reason', null);
        if (empty($bid) || empty($reason)) {
            $this->Flash->error(__('Unable to update Booking details. Please try again later.'));
            return $this->redirect($this->referer());
        }
        
        $this->loadModel('Bookings');
        $doctorBooking = $this->Bookings->findById($bid)->first();
        $doctorBooking->reason = $reason;
        $doctorBooking->status = 4;

        if (!$this->Bookings->save($doctorBooking)) {
            $this->Flash->error(__('Unable to save.Please try again later.'));
            return $this->redirect($this->referer());
        }

        $bookingData = $this->Bookings
                            ->findById($bid)
                            ->contain([
                                'Patient' ,'Users' => ['UserProfiles' => ['States', 'Countries']],
                            ])
                            ->first();

        $this->loadComponent('Email');
        $this->Email->sendPatientCancelBookingEmail($bookingData);

        $this->Flash->success(__('Booking Cancelled Successfully'));
        return $this->redirect($this->referer());
    }

}
