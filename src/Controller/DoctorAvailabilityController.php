<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * DoctorAvailability Controller
 *
 * @property \App\Model\Table\DoctorAvailabilityTable $DoctorAvailability
 *
 * @method \App\Model\Entity\DoctorAvailability[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorAvailabilityController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $doctorAvailability = $this->paginate($this->DoctorAvailability);

        $this->set(compact('doctorAvailability'));
    }

    /**
     * View method
     *
     * @param string|null $id Doctor Availability id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctorAvailability = $this->DoctorAvailability->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('doctorAvailability', $doctorAvailability);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(){
        
        $userId = $this->Auth->user('id');
        $allData = $this->request->getData();
        $finalData = [];
        $pastData = $this->DoctorAvailability
                            ->find('list', ['keyField' => 'weekday', 'valueField' => 'id'])
                            ->where(['user_id' => $userId])
                            ->toArray();
        foreach ($allData as $key => $data) {
            if(!empty($data['my_doctor_availability_timings'])) {
                $myData = [
                'available_from' => new Time($data['my_doctor_availability_timings']['starttime']),
                'available_upto' => new Time($data['my_doctor_availability_timings']['endtime']),
                'weekday' => $data['weekday'],
                'full_day_off' => 0,
                'user_id' => $userId,
                'id' => isset($pastData[$data['weekday']])? $pastData[$data['weekday']]:null,
            ];
            array_push($finalData, $myData);
            }
        }
        
        if ($this->request->is('post')) {
            $doctorAvailability = $this->DoctorAvailability->patchEntities([], $finalData);

            $result = $this->DoctorAvailability->saveMany($doctorAvailability);

            if (!$result) {
                $this->Flash->error(__('The doctor availability could not be saved. Please, try again.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->loadModel('Durations');
            $duration = $this->Durations->newEntity();
            if(!empty($this->request->data['id'])) {
                $duration = $this->Durations->get($this->request->data['id']);
            } 
            $this->request->data['user_id'] = $this->Auth->user('id');
            $duration = $this->Durations->patchEntity($duration, $this->request->data);
            $this->Durations->save($duration);
            $this->Flash->success(__('The doctor availability has been saved.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('The doctor availability could not be saved. Invalid request method.'));
        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor Availability id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctorAvailability = $this->DoctorAvailability->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctorAvailability = $this->DoctorAvailability->patchEntity($doctorAvailability, $this->request->getData());
            if ($this->DoctorAvailability->save($doctorAvailability)) {
                $this->Flash->success(__('The doctor availability has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor availability could not be saved. Please, try again.'));
        }
        $users = $this->DoctorAvailability->Users->find('list', ['limit' => 200]);
        $this->set(compact('doctorAvailability', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor Availability id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctorAvailability = $this->DoctorAvailability->get($id);
        if ($this->DoctorAvailability->delete($doctorAvailability)) {
            $this->Flash->success(__('The doctor availability has been deleted.'));
        } else {
            $this->Flash->error(__('The doctor availability could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
