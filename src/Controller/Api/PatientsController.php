<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Chronos\Chronos;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Collection\Collection;
use Cake\I18n\FrozenTime;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Routing\Router;

/**
 * States Controller
 *
 * @property \App\Model\Table\StatesTable $States
 *
 * @method \App\Model\Entity\State[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PatientsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Phone');
        $this->Auth->allow(['view', 'index']);
 
    }

    /**
     * view method
     *
     * @return \Cake\Http\Response|void
     */

    public function bookAppointment(){
        $this->loadComponent('Email');

        $this->request->allowMethod(['post']);
       
        $userId = $this->Auth->user('id');
        $formData  = $this->request->getData();
        $formData['status'] = 1;
        
        $formData['duration'] = 30;
        $formData['patient_first_name'] = ( empty($formData['patient_first_name']) ? $this->Auth->user('user_profile')['first_name']:$formData['patient_first_name'] );
        $formData['patient_last_name'] = ( empty($formData['patient_last_name']) ? $this->Auth->user('user_profile')['last_name']:$formData['patient_last_name'] );
        


        $booking_type  = $this->request->getData('booking_type');
        if (empty($formData)) {
            throw new NotFoundException("Invalid Request");
        }
        $this->loadModel('Bookings');
        $bookingtime = new Chronos("{$formData['booking_date']}  {$formData['booking_time']}");
        
        if($bookingtime->isPast()){
            $message = __('Selected booking slot has already past, Please select another slot');
            throw new NotFoundException($message);

        };

        $dayofweek = date('N', strtotime($formData['booking_date']));
        
        //Check Doctor
        $this->loadModel('Users');
        $isDoc = $this->Users->exists(['id' => $formData['doctor_id'], 'role_id' => 3]);
        
        if (!$isDoc) {
            throw new NotFoundException(__("No Doctor is associated with this doctor_id."));
        }


        // AVAILABILITY CHECK
        $availabledays = $this->__getDoctorAvailabilities($formData['doctor_id'], $dayofweek, $formData['booking_date']);
        $unavailableDays = $this->getUnavailableTimes($formData['doctor_id'], $formData['booking_date'], $dayofweek);
        $appointInterval = $this->__getAppointmentInterval(
                $formData['booking_date'], 
                $formData['booking_time'], 
                $formData['duration']
            );
        
        foreach ($appointInterval as $key => $slot) {
            $existsInUnavailable = in_array($slot, $unavailableDays);
            $existsInAvailable = in_array($slot, $availabledays);
            
            if (!$existsInAvailable || $existsInUnavailable) {
                $message = __('Oops! that slot is Unavailable. Please select another booking slot.');
                throw new NotFoundException($message);
            }
        }

        //SPAM CHECK
        $this->loadModel('SpamPatients');
        $is_spammer = $this->SpamPatients->exists(['user_id' => $userId, 'doctor_id' => $formData['doctor_id']]);

        if ($is_spammer) {
            $message = __('Enable to create booking. You are marked as spammer by this doctor. Kindly try again with other doctor.');
            throw new BadRequestException($message);
        }

        //SAVING PATIENT BOOKING
        $booking = $this->Bookings->newEntity();
        
        if ($booking->who_will_seeing == 1) {
            $booking->patient_first_name = $this->Auth->user('user_profile.first_name');
            $booking->patient_last_name = $this->Auth->user('user_profile.last_name');
        }
        $booking->patient_id = $userId;
        
        if ($booking_type == 1) {
            $formData['status'] = 2;
            $formData['booking_type'] = 1;
        }

        $booking = $this->Bookings->patchEntity($booking, $formData);
        $saved = $this->Bookings->save($booking);
        
        if (!$saved) {
            $errors = $this->_getOneLineErrorMessageFromValidation($booking->errors());
            throw new BadRequestException($errors);
        }
        $message = __('Booking Successfully created.');
        if ($booking->booking_type == 1) {
            $message = __('Local Booking Successfully created.');
        }
        
        $bookingData = $this->Bookings
                            ->findById($saved->id)
                            ->contain(['Users.UserProfiles','IllnessReasons', 'Insurances'])
                            ->select([
                                'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                                'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                                'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                                'Bookings.who_will_seeing', 'Insurances.title_eng',
                                
                                'IllnessReasons.title_eng', 'Users.id', 'Users.email', 'UserProfiles.first_name', 'UserProfiles.last_name'
                                ])
                            ->first();
        
        //Approve Booking if AutoMatic Mode is SET by DOCTOR 
        $this->loadModel('UserProfiles');
        $doc = $this->UserProfiles->findByUserId($bookingData->doctor_id)->firstOrFail();
        if ($doc->mode) {
            $this->updateBooking($bookingData->id);
        }
        $this->Email->sendBookingEmail($this->Auth->user('email'), $bookingData);


        $this->set([
            'message' => $message,
            'data' => $bookingData,
            '_serialize' => ['message', 'data']
            ]);
    }

    public function updateBooking($bookingId){
        $this->loadComponent("Email");

        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $saved = false;
        $status = 2;

        if(is_null($status)){
            $message = __('Status Parameter not found.');           
        }

        $booking = $this->Bookings->findById($bookingId)->first();
        $booking->status = $status;

        if ($status == 2) {
            //DATE PAST CHECK
            $bookingtime = new Chronos("{$booking->booking_date} {$booking->booking_time->format('H:i')}");
            if($bookingtime->isPast()){
                $message = __('Selected booking slot has already past, Please select another slot');
            };

            //SLOT AVAILABILITY CHECK
            $dayofweek = date('w', strtotime($booking->booking_date->format("Y-m-d")));
            // pr($dayofweek); die;
            $availabledays = $this->__getDoctorAvailabilities($booking->doctor_id, $dayofweek, $booking->booking_date->format("Y-m-d"));
            
            $unavailableDays = $this->getUnavailableTimes($booking->doctor_id, $booking->booking_date->format("Y-m-d"), $dayofweek);
            $appointInterval = $this->__getAppointmentInterval(
                    $booking->booking_date->format("Y-m-d"), 
                    $booking->booking_time->format("H:i"), 
                    $booking->duration
                );
        
            foreach ($appointInterval as $key => $slot) {
                $existsInUnavailable = in_array($slot, $unavailableDays);

                $existsInAvailable = in_array($slot, $availabledays);
                
                if (!$existsInAvailable || $existsInUnavailable) {
                    $message = __('Oops! that slot is Unavailable. Please select another booking slot.');
                }
            }
        }


        $saved = $this->Bookings->save($booking);        
        if (!$saved) {
            $message = __('Booking could not get confirmed.');
        }
        
        if ($status == 2) {

            //ADDING TO UNAVAILABLE TIME
            $unavailable_from = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            $unavailable_upto = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            
            $unavailable_upto = $unavailable_upto->modify("$booking->duration minute");
            $result = $this->addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type = 1, $userId=$booking->doctor_id);

            if (!$result) {
                $message = __('Unable to mark this slot unavailable. Please contact Adminstrator');
            }
            
            $this->Phone->sendSmsToDoctorConfirmBooking($booking->id);
            $message = __('Booking Confirmed.');
        }
        if ($status == 3) {
            $this->Phone->sendCancelBookingByDoctorSms($booking->id);
            $message = __('Booking Declined.');
            
        }
        //$this->Email->sendConfirmBookingEmail2($booking->id, $status);

        $bookingData = $this->Bookings
                            ->findById($saved->id)
                            ->contain(['Users.UserProfiles','IllnessReasons', 'Insurances'])
                            ->select([
                                'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                                'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                                'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                                'Bookings.who_will_seeing', 'Insurances.title_eng',
                                
                                'IllnessReasons.title_eng', 'Users.id', 'Users.email', 'UserProfiles.first_name', 'UserProfiles.last_name'
                                ])
                            ->first();
        $this->set([
            'message' => $message,
            'data' => $bookingData,
            '_serialize' => ['message', 'data']
        ]);
    }

    public function addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type, $docId = null){
        $fd['unavailable_type'] = $unavailable_type;
        $fd['unavailable_from'] = $unavailable_from;
        $fd['unavailable_upto'] = $unavailable_upto;
        $fd['user_id'] = $this->Auth->user('id');
        if (!empty($docId)) {
            $fd['user_id'] = $docId;
        }
        
        $this->loadModel('DoctorUnavailability');
        $docEntity = $this->DoctorUnavailability->newEntity();
        $docEntity = $this->DoctorUnavailability->patchEntity($docEntity, $fd);
        if (!$this->DoctorUnavailability->save($docEntity)) {
            return false;
        }
        return true;
    }

    public function getUnavailableTimes($doctorId, $bookingDate, $dayofweek){
        $this->loadComponent('User');
        $this->loadModel('DoctorUnavailability');
        $this->loadModel('DoctorAvailability');

        $unavailableSlots=[];
        $docUnavailables = $this->DoctorUnavailability
                        ->findByUserId($doctorId)
                        ->where(['DATE(unavailable_from)'   => $bookingDate]);
        
        foreach ($docUnavailables as $key => $docUnavailable) {
            $unavalableTimeSlots = $this->User->getTimes(
                    $docUnavailable->unavailable_from, 
                    $docUnavailable->unavailable_upto, 
                    10
                );
            $collection = new Collection($unavalableTimeSlots);

            $new = $collection->map(function ($value, $key) use($bookingDate){
                return new FrozenTime($bookingDate. $value['timing']);
            });

            $unavailableSlots = array_merge($unavailableSlots, $new->toArray());
        }

        //ADD FULL DAY OFF
        $fullOfss = $this->DoctorAvailability
            ->findByUserId($doctorId)
            ->where(['DoctorAvailability.weekday' => $dayofweek])
            ->andWhere(['DoctorAvailability.full_day_off' => true])
            ->first();
        
        if (!empty($fullOfss)) {
            $nonTimeSlot = $this->User->getTimes(
                    '00:00:00', 
                    '23:59:59', 
                    10
                );
            $collection = new Collection($nonTimeSlot);
            $new = $collection->map(function ($value, $key) use ($bookingDate){
                return new FrozenTime($bookingDate. $value['timing']);
            });

            $unavailableSlots = array_merge($unavailableSlots, $new->toArray());
        }
        return $unavailableSlots;

    }



    protected function __getDoctorAvailabilities($doctorId, $dayofweek, $bookingDate) {
        $this->loadComponent('User');
        $this->loadModel('DoctorAvailability');
        $availableSlots = [];
        $availabilities = $this->DoctorAvailability
            ->findByUserId($doctorId)
            ->where(['DoctorAvailability.weekday' => $dayofweek])
            ->first();

        if (empty($availabilities)) {
             throw new NotFoundException(__("No available slots found. Please set available time slots and try again."));
        }
        $timeFrom = $availabilities->available_from->format('H:i:s');
        $timeTo = $availabilities->available_upto->format('H:i:s');

        $availableTimeSlot = $this->User->getTimes(
                $timeFrom, 
                $timeTo, 
                10
            );
        $collection = new Collection($availableTimeSlot);
        $new = $collection->map(function ($value, $key) use ($bookingDate){
            return new FrozenTime($bookingDate. $value['timing']);
        });

        $availableSlots = array_merge($availableSlots, $new->toArray());
        return $availableSlots;
    }





    private function __getAppointmentInterval($bookingDate, $bookingTime, $duration){
        $bookingStartTime = new FrozenTime($bookingDate .' ' .$bookingTime);
        $bookingEndTime = new \DateTime($bookingDate .' ' .$bookingTime);
        $bookingEndTime->add(new \DateInterval('PT' . $duration . 'M'));

        $getTimes = $this->User->getTimes(
            $bookingStartTime->format('H:i'), 
            $bookingEndTime->format('H:i'), 
            10
        );
        $collection = new Collection($getTimes);

        $new = $collection->map(function ($value, $key) use($bookingDate) {
            return new FrozenTime($bookingDate.$value['timing']);
        });

        return $new->toArray();
    }




    public function viewAppointments(){
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];

        $this->loadModel('Bookings');
        // echo $this->request->getQuery('past');die;
        $getPastAppointments = $this->request->getQuery('past');
        $bookingData = $this->Bookings
                            ->findByPatientId($this->Auth->user('id'))
                            ->contain(['Users.UserProfiles','IllnessReasons', 'Insurances', "Patient",'DoctorRatings'])
                            ->select([
                                'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                                'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                                'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                                'Bookings.who_will_seeing', 'Insurances.title_eng',
                                
                                'Patient.id', 'Patient.email',

                                'IllnessReasons.title_eng', 'Users.id', 'Users.email', 'UserProfiles.first_name', 'UserProfiles.last_name',
                                'DoctorRatings.id','DoctorRatings.review','DoctorRatings.overall_rating','DoctorRatings.bedside_manner','DoctorRatings.wait_time'
                                ]);


        if ($getPastAppointments == 'true') {
            $bookingData->where(['Bookings.booking_date <=' => new \DateTime()]);
        }
        
        $bookingData = $this->paginate($bookingData);
        $this->set([
            'success' => true,
            'message' => 'Success',
            'data' => [
                'prevPage' => $this->request->params['paging']['Bookings']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Bookings']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Bookings']['count'],
                'page' => $this->request->params['paging']['Bookings']['page'],
                'perPage' => $this->request->params['paging']['Bookings']['perPage'],
                'records' => $bookingData,
            ],
            '_serialize' => ['success', 'message', 'data']
        ]);   
    }

    public function patientUpcomingAppointments() {
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];

        $this->request->allowMethod(['get']);
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');

        $appointments = $this->Bookings->findByPatientId($userId)
                            ->where(['booking_date >' => new \DateTime()])
                            ->contain([
                                'IllnessReasons', 
                                'Users' => ['UserProfiles'], 
                                'Patient' => ['PatientProfiles']
                                ])
                            ->select([
                                'Bookings.id',
                                'Bookings.patient_id', 'Bookings.doctor_id', 'Bookings.booking_date', 
                                'Bookings.booking_time', 'Bookings.duration', 'Bookings.illness_reason_id',
                                'Bookings.status', 'Bookings.patient_dob', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob', 'Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.doctor_note',
                                'Bookings.booking_type', 'Bookings.reason',
                                
                                'IllnessReasons.id', 'IllnessReasons.title_eng',

                                'Users.email', 'Users.role_id',

                                'Patient.email', 'Patient.role_id',

                                'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.first_name',
                                'PatientProfiles.first_name', 'PatientProfiles.last_name',
                            ])
                            ->order(['booking_date' => "ASC"]);

        $message = __('success');  
        if ($appointments->isEmpty()) {
            $message = __('No Appointments found');                
        }

        $appointments = $this->paginate($appointments);

        $this->set([
            'data'   => [
                 'prevPage' => $this->request->params['paging']['Bookings']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Bookings']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Bookings']['count'],
                'page' => $this->request->params['paging']['Bookings']['page'],
                'records' => $appointments,
            ],
            'message'    => $message,
            '_serialize' => ['message','data']
        ]); 
    }

    public function rescheduleAppointment(){
        $this->request->allowMethod(['put', 'patch']);
        $userId = $this->Auth->user('id');
        $this->loadComponent('Email');

        $booking_date  =$this->request->getData('booking_date', null);
        $booking_time  =$this->request->getData('booking_time', null);
        $id  = $this->request->getData('booking_id', null);

        if (is_null($id)) {
            throw new BadRequestException(__("Invalid Request. Booking Id can't be empty."));
        }
        if (empty($booking_date) && empty($booking_time)) {
            throw new BadRequestException(__("New Booking Date or Booking Time can not be empty."));
        }

        $this->loadModel("Bookings");
        $data = $this->Bookings->findByIdAndPatientId($id, $userId)->first();
        
        if (empty($data)) {
            throw new RecordNotFoundException(__("Record not found. Please try again with correct data."));
        }
        if ($data->status == 2) {
            throw new BadRequestException(__("Confirmed Bookings can't be altered."));
        }

        $data->booking_date = $booking_date;
        $data->booking_time = $booking_time;
        $data->status = 1;

        if (!$saved = $this->Bookings->save($data)) {
            throw new BadRequestException(__("Unable to save. Please try again later."));            
        }

        $bookingData = $this->Bookings
                        ->findByPatientId($this->Auth->user('id'))
                        ->contain([
                            'Users' => [
                                'UserProfiles' => ['States', 'Countries'],
                                "UserSpecialities"  => function($q){
                                    return $q->select([
                                        'UserSpecialities.id',
                                        'UserSpecialities.user_id',
                                        ]);
                                }                            
                            ],
                            'IllnessReasons', 
                            'Insurances', 
                            "Patient",
                        ])
                        ->select([
                            'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                            'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                            'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                            'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                            'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                            'Bookings.who_will_seeing', 
                            
                            'Insurances.title_eng',
                            
                            'Patient.id', 'Patient.email',

                            'IllnessReasons.title_eng', 'Users.id', 'Users.email', 
                            'States.name',
                            'Countries.name',
                            'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.state_id', 
                            'UserProfiles.country_id', 
                            
                            ])
                            ->first();

        $this->Email->sendRescheduleBookingNotification($bookingData);
        
        $message = __('Appointment Successfully Rescheduled.');

        $this->set([
            'message' => $message,
            'data' => $bookingData,
            '_serialize' => ['message', 'data']
        ]);
    }

    public function cancelAppointment(){
        $this->request->allowMethod(['delete']);

        $bid = $this->request->getData('booking_id', null);
        $reason = $this->request->getData('reason', null);
        $userId = $this->Auth->user('id');

        if (empty($bid) || empty($reason)) {
            $message = __('Appointment Id and Reject reason is required.');
            throw new BadRequestException($message);
        }
        
        $this->loadModel('Bookings');
        $bookingData = $this->Bookings->findByIdAndPatientId($bid, $userId)->first();
        
        if (empty($bookingData)) {
            throw new BadRequestException(__("Record not found. Please try again with correct data."));
        }

        if (in_array($bookingData->status, [3,4])) {
            throw new BadRequestException(__("Appointment Status cannot be updated. Please note only bookings with pending confirmation status can be updated."));
        }

        $bookingData->reason = $reason;
        $bookingData->status = 4;

        if (!$this->Bookings->save($bookingData)) {
            $message = __('Unable to update Appointment details. Please try again later.');
            throw new BadRequestException($message);   
        }
        $message = __('Appointment cancelled successfully.');
        $this->set([
            'message' => $message,
            'data' => $bookingData,
            '_serialize' => ['message', 'data']
        ]);
    }

    public function giveFeedback() {
        $this->request->allowMethod(['post']);
        
        $userId = $this->Auth->user('id');

        if (empty($this->request->data)) {
            throw new NotFoundException("Invalid Request");
        }

        if (empty($this->request->data['doctor_id'])) {
            throw new NotFoundException("Doctor Id cannot be empty");
        }

        if (empty($this->request->data['booking_id'])) {
            throw new NotFoundException("Booking Id cannot be empty");
        }

        if (empty($this->request->data['review'])) {
            throw new NotFoundException("Review cannot be empty");
        }

        if (empty($this->request->data['ratings']['overall_rating'])) {
            throw new NotFoundException("Overall Rating cannot be empty");
        }

        if (empty($this->request->data['ratings']['bedside_manner'])) {
            throw new NotFoundException("Bedside Rating cannot be empty");
        }

        if (empty($this->request->data['ratings']['wait_time'])) {
            throw new NotFoundException("Wait time cannot be empty");
        }

        $this->loadModel('DoctorRatings');
        $feeback = $this->DoctorRatings->newEntity();
        $this->request->data['patient_id'] = $userId;
        $this->request->data['overall_rating'] = $this->request->data['ratings']['overall_rating'];
        $this->request->data['bedside_manner'] = $this->request->data['ratings']['bedside_manner'];
        $this->request->data['wait_time'] = $this->request->data['ratings']['wait_time'];

        $feeback = $this->DoctorRatings->patchEntity($feeback, $this->request->data);
        $saved = $this->DoctorRatings->save($feeback);
        
        if (!$saved) {
            $errors = $this->_getOneLineErrorMessageFromValidation($booking->errors());
            throw new BadRequestException($errors);
        }
        $message = __('Rating has been added successfully');
        $this->set([
            'message' => $message,
            'data' => $saved,
            '_serialize' => ['message', 'data']
        ]);
    }

    public function myReports() {
        $this->request->allowMethod(['get']);
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];
        $this->loadModel('PatientReports');
        $patient = $this->PatientReports->find()
                ->where(['PatientReports.patient_id' => $this->Auth->user('id')])
                ->contain(['Users' => ['UserProfiles']])
                ->formatResults(function (\Cake\Collection\CollectionInterface $results) {
                    return $results->map(function ($row) {
                        $row['file_url'] = Router::url('/',true) .'reports_patients'. DS . 'uploads' . DS .$row['file'];
                        return $row;
                    });
                });

        $patientResults = $this->paginate($patient);

        $this->set([
            'success' => true,
            'message' => 'Success',
            'data' => [
                'prevPage' => $this->request->params['paging']['PatientReports']['prevPage'], 
                'nextPage' => $this->request->params['paging']['PatientReports']['nextPage'],
                'totalRecords' => $this->request->params['paging']['PatientReports']['count'],
                'page' => $this->request->params['paging']['PatientReports']['page'],
                'records' => $patientResults,
            ],
            '_serialize' => ['success', 'message', 'data']
        ]);   
    }

    public function teamLeaders() {
        $this->request->allowMethod(['get']);
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $patientBookings = $this->Bookings->findByPatientId($userId);

        // $completedCase = $patientBookings->func()->count('doctor_id');

        $patientBookings = $patientBookings
                        ->contain([
                            'Users' => ['UserProfiles', 'UserSpecialities' => ['Specialities']],
                        ])
                        ->select([
                            'booking_date', 'booking_time','doctor_id',
                            'Users.id',
                            'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.gender',
                            'UserProfiles.dob','UserProfiles.address','UserProfiles.zipcode','UserProfiles.profile_pic'
                            
                        ])
                        ->group('doctor_id');

        $leaderResults = $this->paginate($patientBookings);

        $this->set([
            'success' => true,
            'message' => 'Success',
            'data' => [
                'prevPage' => $this->request->params['paging']['Bookings']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Bookings']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Bookings']['count'],
                'page' => $this->request->params['paging']['Bookings']['page'],
                'records' => $leaderResults,
            ],
            '_serialize' => ['success', 'message', 'data']
        ]);   
    }

}
