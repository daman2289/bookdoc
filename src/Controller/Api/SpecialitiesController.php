<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Specialities Controller
 *
 * @property \App\Model\Table\SpecialitiesTable $Specialities
 *
 * @method \App\Model\Entity\Speciality[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SpecialitiesController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['index']);
    }

    public function index(){
        $this->request->allowMethod(['get']);
        $this->paginate = ['limit' => $this->request->getQuery('limit'), env('APILIMIT',10)];

        $specialities = $this->Specialities->find()
            ->select(['id','title_eng', 'title_german', 'is_deleted'])
            ->order(['Specialities.title_eng' => 'ASC']);
        
        $specialities = $this->paginate($specialities);
        $this->set(compact('specialities'));
        $this->set('_serialize', ['specialities']);

        
    }

    
    /**
     * Delete method
     *
     * @param string|null $id Speciality id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $speciality = $this->Specialities->get($id);
        $speciality->is_deleted = Configure::read('Speciality.is_deleted');   
        if ($this->Specialities->save($speciality)) {       
            $msg = __('The Specialty has been deleted.');
        } else {
            $msg = __('The Specialty could not be deleted. Please, try again.');
        }
        $this->set([
            'msg' => $msg,
            '_serialize' => ['msg']
        ]);
    }    
}
