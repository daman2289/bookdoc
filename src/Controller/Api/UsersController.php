<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Firebase\JWT\JWT;
use Cake\Utility\Security;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'login', 'forgotPassword', 'getDoctors','share','addUser']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Roles']
        // ];
        // $users = $this->paginate($this->Users);

        $this->set([
            'message' => __('This endpoint is not active.'),
            '_serialize' => ['message', 'data']
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view(){

        $id = $this->Auth->user('id');
        $user = $this->Users
            ->findById($id)
            ->contain(['Roles', 'UserProfiles'])
            ->firstOrFail();

        $phone_number = $user->user_profile->phone_number;
        $country_code = $user->user_profile->country_id;
        
        $this->set([
            'message' => __('success'),
            'data' => $user,
            'phone_number' => $phone_number,
            'country_code' => $country_code,
            '_serialize' => ['message', 'data','phone_number','country_code']
        ]);
    }

    public function share($id) {

        $user = $this->Users
            ->findById($id)
            ->contain(['Roles', 'UserProfiles'])
            ->firstOrFail();

        $phone_number = $user->user_profile->phone_number;
        $country_code = $user->user_profile->country_id;

        $url = Router::url(array(
               'controller' => 'Users',
               'action' => 'share',
               $id,
               'prefix' => 'api',
            ), true); 

        $this->set([
            'message' => __('success'),
            'share_url' => $url,
            'data' => $user,
            '_serialize' => ['message', 'share_url','data','phone_number','country_code']
        ]);
    }

    /**
     * SIGN up
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function add(){
        $this->request->allowMethod(['post']);

        $email = $this->request->getData('email');
        $password = $this->request->getData('password');
        $country_code = $this->request->getData('countryCode', 14);
        $state_code = $this->request->getData('stateCode', 277);
        $phone = $this->request->getData('phoneNumber');
        $dob = $this->request->getData('dateOfBirth');
        $gender = $this->request->getData('gender', 'Male');
        $role_id = $this->request->getData('role_id',3);
        $zipcode = $this->request->getData('zipcode',1000);
        $first_name = $this->request->getData('first_name');
        $last_name = $this->request->getData('last_name');
        $address = $this->request->getData('address',3);

        if (!in_array($role_id, [3,4])){
            $message = __("Role Id can be 3 or 4 Only. Select 3 for Doctor and 4 for Patient.");
            throw new BadRequestException($message);
        };

        if (empty($email) || empty($password)) {
            $message = __("Email or Password can not be Empty.");
            throw new BadRequestException($message);
        }

        $data = [
            'email' => $email,
            'password' => $password,
            'role_id' => $role_id,
            'plan_id' => 4,
            'user_profile' => [
                'dob' => $dob,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'state_id' => $state_code,
                'country_id' => $country_code,
                'zipcode' => $zipcode,
                'phone_number' => $phone,
                'address' => $address
            ]
        ];
        $user = $this->Users->newEntity();

        $user = $this->Users->patchEntity($user, $data, ['associated' => ['UserProfiles'] ]);
        
        $saved = $this->Users->save($user);
        
        if (!$saved) {
            // $message = __("The user could not be saved. Please, try again with all required parameters.");
            $message = $this->_getOneLineErrorMessageFromValidation($user->errors());
            throw new BadRequestException($message);
        }
        if ($user->role_id == 3) {
		    $this->_subscribeToPlan($subscriptionType='free', $user->id, $user->email);
        }
        $this->loadComponent('Email');
        $this->Email->sendVerifyEmail($saved->id);

        $message = __('The user has been saved.');
        $data = (object) [
            'email' => $saved->email,
            'profile_img' => $saved->user_profile->display_url,
            'full_name' => $saved->user_profile->first_name . ' ' . $saved->user_profile->last_name,
            'role_id' => $saved->role_id,
            'id' => $saved->id,
            'token' => JWT::encode(
                    [
                        'sub' => $saved->email,
                        'exp' =>  time() + 604800
                    ],
                    Security::salt()
            )    
        ];
                

        $this->set('data', [
            'message' => $message,
            'data' => $data
        ]);
        $this->set('_serialize', ['data']);
    }

    public function login(){
        $user = $this->Auth->identify();
        if (!$user) {
            throw new UnauthorizedException(__('Invalid email or password'));
        }
        $this->loadModel('Users');
        $record = $this->Users->findById($user['id'])
                            ->contain(['UserProfiles'])
                            ->select([
                                'Users.id', 'Users.email', 'Users.role_id',
                                'UserProfiles.id', 'UserProfiles.first_name', 
                                'UserProfiles.last_name',
                                'UserProfiles.profile_pic'
                            ])
                            ->firstOrFail(); 
        $this->set([
            'message' => __('success'),
            'data' => [
                'token' => JWT::encode([
                    'sub' => $user['email'],
                    'exp' =>  time() + 604800
                ],
                Security::salt()),
                'email' => $record->email,
                'id' => $record->id,
                'role_id' => $record->role_id,
                'profile_img' => $record->user_profile->display_url,
                'full_name' => $record->user_profile->full_name,
            ],
            '_serialize' => ['success','message', 'data']
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit(){
        $id = $this->Auth->user('id');
        $user = $this->Users->findById($id)->contain(['UserProfiles'])->firstOrFail();

        // $email = $this->request->getData('email');
        // $password = $this->request->getData('password');
        $country_code = $this->request->getData('countryCode', 14);
        $state_code = $this->request->getData('stateCode', 277);
        $phone = $this->request->getData('phoneNumber');
        $dob = $this->request->getData('dateOfBirth');
        $gender = $this->request->getData('gender', 'Male');
        $zipcode = $this->request->getData('zipcode',1000);
        $first_name = $this->request->getData('first_name');
        $last_name = $this->request->getData('last_name');
        $address = $this->request->getData('address',"");
        $lat = $this->request->getData('lat');
        $lng = $this->request->getData('lng');
        $professional_statement = $this->request->getData('professionalStatement');
        
        $user_profile = null;
        (!empty($first_name))? $user_profile['first_name'] = $first_name : null;
        (!empty($last_name))? $user_profile['last_name'] = $last_name : null;
        (!empty($country_code))? $user_profile['country_id'] = $country_code : null;
        (!empty($state_code))? $user_profile['state_id'] = $state_code : null;
        (!empty($phone))? $user_profile['phone_number'] = $phone : null;
        (!empty($dob))? $user_profile['dob'] = $dob : null;
        (!empty($gender))? $user_profile['gender'] = $gender : null;
        (!empty($zipcode))? $user_profile['zipcode'] = $zipcode : null;
        (!empty($address))? $user_profile['address'] = $address : null;
        (!empty($lat))? $user_profile['lat'] = $lat : null;
        (!empty($lng))? $user_profile['lng'] = $lng : null;
        (!empty($professional_statement))? $user_profile['professional_statement'] = $professional_statement : null;

        $data = [
            'user_profile' => $user_profile
        ];
        // pr($user); die;
        $user = $this->Users->patchEntity($user, $data, ['associated' => 'UserProfiles']);        
        $saved = $this->Users->save($user);
        
        if (!$saved) {
            $message = $this->_getOneLineErrorMessageFromValidation($user->errors());
            throw new BadRequestException($message);
        }

        $message = __('The user has been successfully updated.');

        
        $return = $this->Users->findById($id)->contain(['UserProfiles' => ['Countries', 'States']])->select([
            'Users.id', 'Users.email',
            'Countries.name', 'States.name',
            'UserProfiles.id', 'UserProfiles.zipcode', 'UserProfiles.first_name', 
            'UserProfiles.lat', 'UserProfiles.lng', 'UserProfiles.professional_statement', 
            'UserProfiles.last_name', 'UserProfiles.address', 'UserProfiles.state_id',
            'UserProfiles.dob', 'UserProfiles.phone_number', 'UserProfiles.country_id'
            ])->firstOrFail();
        
        $return->user_profile->professional_statement = strip_tags($return->user_profile->professional_statement);
        $return->user_profile->professional_statement = preg_replace('/\s+/S', " ", $return->user_profile->professional_statement);
        
        $this->set([
            'message' => __('success'),
            'data' => $return,
            '_serialize' => ['message', 'data']
            ]);
    }

    public function uploadImage(){
        $this->request->allowMethod(['post']);
        $file = $this->request->getData('upload_image');
        
        if (empty($file)){
            throw new BadRequestException(__('File Parameter Can\'t be Blank' ));
        }

        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UserProfiles']
        ]);

        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
        $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
        
        $incomingImgSize = getimagesize($file['tmp_name']);
        $size = Configure::read('profileImageDimensions.size');
        if ($incomingImgSize[0] < $size['width'] || $incomingImgSize[1] < $size['height']) {
            $message = __("Profile Image should be of minimum of dimensions "). "{$size['width']} * {$size['height']}";
            throw new BadRequestException($message);
        }

        if (in_array($ext, $arr_ext)) {

            //do the actual uploading of the file. First arg is the tmp name, second arg is
            //where we are putting it
            if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'profile_image_' . $file['name'])) {
                //prepare the filename for database entry
                $user->user_profile->profile_pic = 'profile_image_' . $file['name'];
            }
        }
        $data = [
            'user_profile' =>  [ 'profile_pic' => "profile_image_{$file['name']}" ]
        ];
        $user = $this->Users->patchEntity($user, $data, ['associated' => ['UserProfiles']]);
        
        $result = $this->Users->save($user);
        
        if (!$result) {
            $message = $this->_getOneLineErrorMessageFromValidation($user->errors());
            throw new BadRequestException($message);
        }
        
        $result = $this->Users->findById($this->Auth->user('id'))
                                ->contain(['UserProfiles'])
                                ->select([
                                    'Users.id', 'UserProfiles.user_id', 'UserProfiles.first_name',
                                    'UserProfiles.profile_pic', 'UserProfiles.last_name'])
                                ->firstOrFail();
        $message = 'success';

        $this->set([
            'data'   => $result,
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);

    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Users->get($id);
        $user->is_deleted = Configure::read('User.is_deleted');   
        if ($this->Users->save($user)) {       
            $msg = __('The user has been deleted successfully.');
        } else {
            $msg = __('The user could not be deleted. Please, try again.');
        }
        $this->set([
            'msg' => $msg,
            '_serialize' => ['msg']
        ]);

    }

    public function getDoctors($id=null){
        if (is_null($id)) {
            $doctors = $this->Users->find()
                ->contain(['UserProfiles'])
                ->where(['role_id' => Configure::read('Roles.doctor')])
                ->select(['role_id','UserProfiles.last_name','UserProfiles.first_name']);
        }else{
            $doctors = $this->Users->findById($id);            
        }
        
        $this->set([
            'doctors' => $doctors,
            '_serialize' => ['doctors']
        ]);

    }

     public function chart() {
        $userId = $this->Auth->user('id');
        $this->request->is('ajax');
        $this->loadModel('Bookings');
        $this->loadModel('DoctorUnavailability');
        $this->Bookings->belongsTo('Users', [
            'foreignKey' => 'patient_id',
            'saveStrategy' => 'replace'
        ]);
        $this->Bookings->belongsTo('IllnessReasons', [
            'foreignKey' => 'illness_reason_id',
            'saveStrategy' => 'replace'
        ]);
        $query = $this->Bookings->find()
                    ->where(['Bookings.doctor_id' => $userId])
                    ->contain(['Users' => 'UserProfiles','IllnessReasons']);

        $concat = $query->func()->concat([
            'Bookings.patient_first_name' => 'identifier',
            ' ',
            'Bookings.patient_last_name' => 'identifier'
        ]);

        $time = $query->func()->concat([
            'Bookings.booking_date' => 'identifier',
            'T',
            'Bookings.booking_time' => 'identifier'
        ]);

        $query = $query->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['end'] = gmdate("Y-m-d\TH:i:s\Z",strtotime($row['duration']." minutes",strtotime($row['start'])));
                return $row;
            });
        });


        $color = $query->newExpr()
            ->addCase(
                [
                    $query->newExpr()->add(['Bookings.status' => 1]),
                    $query->newExpr()->add(['Bookings.status' => 2, 'Bookings.booking_type' => 0]),
                    $query->newExpr()->add(['Bookings.status' => 3]),
                    $query->newExpr()->add(['Bookings.status' => 4]),
                    $query->newExpr()->add(['Bookings.status' => 2, 'Bookings.booking_type' => 1])
                ],
                [
                    '#ffaf48',
                    '#44fcd3',
                    '#fb5570',
                    '#6c96ff',
                    '#76ff90'
                ],
                [
                    'string',
                    'string',
                    'string',
                    'string',
                    'string'
                ]
            );

        $status = $query->newExpr()
            ->addCase(
                [
                    $query->newExpr()->add(['Bookings.status' => 1]),
                    $query->newExpr()->add(['Bookings.status' => 2]),
                    $query->newExpr()->add(['Bookings.status' => 3]),
                    $query->newExpr()->add(['Bookings.status' => 4])
                ],
                [
                    'Pending for Approval',
                    'confirmed',
                    'Declined by You',
                    'Patient Cancel',
                ],
                [
                    'string',
                    'string',
                    'string',
                    'string'
                ]
            );

        $seeing = $query->newExpr()
            ->addCase(
                [
                    $query->newExpr()->add(['Bookings.who_will_seeing' => 1]),
                    $query->newExpr()->add(['Bookings.who_will_seeing' => 2])
                ],
                [
                    __('Itself'),
                    __('Someone Else')
                ],
                [
                    'string',
                    'string'
                ]
            );

        $else_patient_name = $query->func()->concat([
            'Bookings.patient_first_name' => 'identifier',
            ' ',
            'Bookings.patient_last_name' => 'identifier'
        ]);

        $data = $query->select([
                        'start' => $time,
                        'bid' => 'Bookings.id',
                        'title' => $concat,
                        'label' => '"My Appointments"',
                        'color' => $color,
                        'duration' => "Bookings.duration",
                        'email' => 'Users.email',
                        'phone' => 'UserProfiles.phone_number',
                        'gender' => 'UserProfiles.gender',
                        'currentstatus' => $status,
                        'illness' => 'IllnessReasons.title_eng',
                        'note' => 'Bookings.doctor_note',
                        'seeing' => $seeing,
                        'name' => $else_patient_name,
                        'else_email' => 'Bookings.patient_email',
                        'else_gender' => 'Bookings.patient_sex',
                        // 'end' => $end,
                        'reason' => 'Bookings.reason',
                        'status' => 'Bookings.status',
                        'file' => 'Bookings.file',
                        'created' => 'Bookings.created'
                    ])
                    ->toArray();
        
        // *******  CASE oF Time OFF/ UNAVAILABLE TIME/   **************

        $docUnavailable = $this->DoctorUnavailability->findByUserId($userId)
                            ->where(['DoctorUnavailability.unavailable_type IS NOT' => 1]);
        
        $status = $docUnavailable->newExpr()
            ->addCase(
                [
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 1]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 2]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 3]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 4])
                ],
                [
                    'Booked',
                    'LocalTime',
                    'Unavailable',
                    'Dayoff'
                ],
                [
                    'string',
                    'string',
                    'string',
                    'string'
                ]
        );

        $color = $docUnavailable->newExpr()
            ->addCase(
                [
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 1]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 2]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 3]),
                    $docUnavailable->newExpr()->add(['DoctorUnavailability.unavailable_type' => 4])
                ],
                [
                    '#a8c3c3',
                    '#a8c3c3',
                    '#a8c3c3',
                    '#a8c3c3'
                ],
                [
                    'string',
                    'string',
                    'string',
                    'string'
                ]
            );
        
        $docTimeOff = $docUnavailable->select([
                        'start' => 'DoctorUnavailability.unavailable_from',
                        'bid' => 'NULL',
                        'label' => '"My TimeOffs"',
                        'color' => $color,
                        'email' => 'NULL',
                        'title' => $status,
                        'phone' => 'NULL',
                        'gender' => 'NULL',
                        'currentstatus' => $status,
                        'illness' => 'NULL',
                        'note' => 'NULL',
                        'seeing' => 'NULL',
                        'name' => '"Unavailable"',
                        'else_email' => 'NULL',
                        'else_gender' => 'NULL',
                        'end' => 'DoctorUnavailability.unavailable_upto'
                    ])
                    ->toArray();
        $this->loadModel('CalendarEvents');
        $calendar = $this->CalendarEvents->find()
                  ->where(['CalendarEvents.doctor_id' => $this->Auth->user('id')])
                  ->toArray();

        $gcal = array();
        foreach ($calendar as $keycal => $valuecal) {
                $gcal[$keycal] = [
                        'start' => $valuecal->event_date,
                        'bid' => 'NULL',
                        'label' => $valuecal->event,
                        'color' => '#76ff90',
                        'email' => 'NULL',
                        'title' => $valuecal->event,
                        'phone' => 'NULL',
                        'gender' => 'NULL',
                        'currentstatus' => 'NULL',
                        'illness' => 'NULL',
                        'note' => 'NULL',
                        'seeing' => 'NULL',
                        'name' => '"Unavailable"',
                        'else_email' => 'NULL',
                        'else_gender' => 'NULL',
                        'end' => $valuecal->event_end_date
                ];       
        }
        
        $data = array_merge($data, $docTimeOff, $gcal);
        $this->set(array('resp' => $data, '_serialize' => 'resp'));
    }

    public function forgotPassword() {
        $this->request->allowMethod(['post']);
        $this->loadComponent('Email');

        $email = $this->request->getData('email');
        
        if (empty($email)) {
            $message = __("Email cann't be empty.");
            throw new BadRequestException($message);
        }

        $user = $this->Users->findByEmail($email)
                ->contain(['UserProfiles'])
                ->first();

        if (empty($user)) {
            $message = __('Sorry! Email address is not registered here.');
            throw new NotFoundException($message);                
        }
        $password = sha1(Text::uuid());
        $password_token = Security::hash($password, 'sha256', true);
        $hashval = sha1($user->email . rand(1, 100));
        $user->password_reset_token = $password_token;
        $user->hashval = $hashval;
        $reset_token_link = Router::url([
                    'controller' => 'Users',
                    'action' => 'resetPassword',
                    'prefix' => false,
                        ], TRUE) . '/' . $password_token . '#' . $hashval;
        //Send email                       
        $this->Email->sendForgotPasswordEmail($user, $reset_token_link);
        
        $saved = $this->Users->save($user);

        if (!$saved) {
            $message = __('Unable to update User details. Please try again later.');
            throw new BadRequestException($message);   
        }

        $message = __('Password reset link is sent on your email, Please click on link to reset password.');

        $this->set([
            'message' => $message,
            'data' => (object) [ 
                'email' => $saved->email
            ],
            '_serialize' => ['message', 'data']
        ]);
    }

    public function changePassword() {
        $this->request->allowMethod(['post']);
        $user = $this->Users->get($this->Auth->user('id'));
        $old_pass = $this->request->getData('old_password');
        $new_pass = $this->request->getData('new_password');

        if (empty($old_pass) || empty($new_pass)) {
            $message = __("Old Password or New Password cannot be null");
            throw new BadRequestException($message);   
        }

        $user = $this->Users->patchEntity($user, [
            'old_password' => $old_pass,
            'password' => $new_pass
                ], ['validate' => 'password']
        );
        $saved = $this->Users->save($user);
        
        if (!$saved) {
            // $message = __("Unable to update Password. Please try again");
            $message = strip_tags($this->_setValidationError($user->errors()));
            throw new BadRequestException($message);   
        }

        $message = __('The password is successfully changed');

        $this->set([
            'message' => $message,
            'data' => (object) [ 
                'email' => $saved->email
            ],
            '_serialize' => ['message', 'data']
        ]);
    }
    
    public function addUser() {
        $image_facebook = Router::url('/', true)."img/man-blank-img.png"; 
        if($this->request->data['provider'] == 'Google') {
            $email = $this->request->data['user']['email'];
        } elseif ($this->request->data['provider'] == 'facebook') { 
            $email = $this->request->data['email'];
        }

        $social_profile = $this->Users->find()
                ->where(['SocialProfiles.email' => $email])
                ->contain(['SocialProfiles'])
                ->first();
        if(!empty($social_profile)) {
            $message = __('Success');
            $data = [
                'email' => $social_profile->email,
                'profile_img' => $social_profile->social_profile->picture_url,
                'full_name' => $social_profile->social_profile->full_name,
                'role_id' => $social_profile->role_id,
                'id' => $social_profile->id,
                'token' => JWT::encode([
                    'sub' => $social_profile->email,
                    'exp' =>  time() + 604800
                ],
                Security::salt())
            ];
                    

            $this->set(compact('data','message'));
        } else {
           if($this->request->data['provider'] == 'Google') {
               $email = $this->request->data['user']['email'];
               $name = $this->request->data['user']['name'];
               $access_token = $this->request->data['accessToken'];
               $profile_url = (!empty($this->request->data['user']['photo'])) ? $this->request->data['user']['photo'] : $image_facebook;
               $provider = $this->request->data['provider'];
               $identifier = $this->request->data['user']['id'];
               $role_id = 4;
               $is_email_verified = 1;
               $active = 1;
            } elseif ($this->request->data['provider'] == 'facebook') {
               $email = $this->request->data['email'];
               $name = $this->request->data['first_name'].' '.$this->request->data['last_name'];
               $access_token = $this->request->data['accessToken'];
               $profile_url = (!empty($this->request->data['user']['photo'])) ? $this->request->data['user']['photo'] : $image_facebook;
               $provider = $this->request->data['provider'];
               $identifier = $this->request->data['id'];
               $role_id = 4;
               $is_email_verified = 1;
               $active = 1;
           }

            $data = [
                'email' => $email,
                'is_active' => $active,
                'is_email_verified' => $is_email_verified,
                'role_id' => $role_id,
                'social_profile' => [
                    'provider' => $provider,
                    'access_token' => $access_token,
                    'identifier' => $identifier,
                    'full_name' => $name,
                    'email' => $email,
                    'picture_url' => $profile_url,
                    'email_verified' => 1
                ]

            ];

            $user = $this->Users->newEntity();

            $user = $this->Users->patchEntity($user, $data, ['associated' => ['SocialProfiles'] ]);
            $saved = $this->Users->save($user);
            
            if (!$saved) {
                // $message = __("The user could not be saved. Please, try again with all required parameters.");
                $message = $this->_getOneLineErrorMessageFromValidation($user->errors());
                throw new BadRequestException($message);
            }

            $message = __('Success');
            $data = [
                'email' => $saved->email,
                'profile_img' => $saved->social_profile->picture_url,
                'full_name' => $saved->social_profile->full_name,
                'role_id' => $saved->role_id,
                'id' => $saved->id,
                'token' => JWT::encode([
                    'sub' => $saved->email,
                    'exp' =>  time() + 604800
                ],
                Security::salt())
            ];
                    

            $this->set(compact('data','message'));
        }
        
        $this->set('_serialize', ['data','message']);
    }
}
