<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Network\Exception\BadRequestException;
use Cake\Core\Configure;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 *
 * @method \App\Model\Entity\Api/Notification[] paginate($object = null, array $settings = [])
 */
class NotificationsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        // $this->loadComponent('RequestHandler');
        // $this->Auth->allow(['saveToken']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index(){
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];
        $userId = $this->Auth->user('id');

        $notifications = $this->Notifications
            ->findByUserId($userId)
            ->order(['Notifications.created' => 'DESC']);
        
        $onlyUnread = $this->request->getQuery('onlyUnread', false);
        
        if ($onlyUnread === 'true') {
            $notifications->where(['Notifications.unread' => 1]);
        }

        $total = $notifications->count();
        
        $notifications = $this->paginate($notifications);
        
        $message = __("success");

        $this->set([
            'message' => $message,
            'data' => [
                'prevPage' => $this->request->params['paging']['Notifications']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Notifications']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Notifications']['count'],
                'page' => $this->request->params['paging']['Notifications']['page'],
                'records' => $notifications,
            ],
            '_serialize' => ['success', 'message', 'data']
        ]);   

    }

    /**
     * View method
     *
     * @param string|null $id Api/notification id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notifications = $this->Notifications->get($id);

        if($notifications->unread == 1){
            $notifications->unread = 0;
            $this->Notifications->save($notifications);
        }
        
        $this->set('notifications', $notifications);
        $this->set('_serialize', ['notifications']);
    }

    public function markAllAsRead(){
        $userId= $this->Auth->user('id');

        $results = $this->Notifications->findByUserId($userId)
                    ->update()
                    ->set(['unread' => 0])
                    ->execute();
        
        $message = __("success");        
        
        $this->set([
            'message' => $message,
            'data' => $results,
            '_serialize' => ['message', 'data']
        ]);   
    }

    public function getPlanLeftTime(){
        $userId = $this->Auth->user('id');
        $this->loadModel('UserStripeDetails');

        $results = $this->UserStripeDetails
                    ->findByUserId($userId)
                    ->select(['current_period_end'])
                    ->firstOrFail();
        $today = new \DateTime();
        $interval = date_diff($today, $results->current_period_end);

        $data = $interval->days . ' '.'days';
        if ($interval->days < 1) {
            $data = $interval->h . ' '. 'hours';
        }

        
        $this->set('results', $data);
        $this->set('_serialize', ['results']);
    }

    public function saveToken(){
        $this->loadModel('UserTokens');
        $token = $this->request->data['user_token'][0];

        $userId = $this->Auth->user('id');

        $user = $this->UserTokens->findByUserIdAndToken($userId, $token)->first();
        $data = [
            'token' => $token,
            'user_id' => $userId
        ];
        if (empty($user)) {
            $user = $this->UserTokens->newEntity();
        }

        $user = $this->UserTokens->patchEntity($user, $data);

        $saved = $this->UserTokens->save($user);
        if (!$saved) {
            throw new BadRequestException($this->_getOneLineErrorMessageFromValidation($user->errors()));
        }

        $message = __('success');
        $this->set([
            'message' => $message,
            'data' => (object) [ 
                'result' => $saved
            ],
            '_serialize' => ['message', 'data']
        ]);
    }

}
