<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\ConflictException;

/**
 * PhoneVerificationsController Controller
 *
 */
class PhoneVerificationsController extends AppController
{

	public function initialize()
    {
    	parent::initialize();
        $this->loadComponent('Phone');
    }

	/**
     * Method verifyPhoneNumber to verify user phone number
     *
     * @return string
     */
    public function check() {
        $this->request->allowMethod(['post']);

        $verificationCode = $this->request->getData('code');
        $userPhoneNumber =  $this->Auth->user('user_profile.phone_number');

        if (empty($userPhoneNumber)) {
            throw new ConflictException(__('Phone Number not found. Please ensure phone number is added to profile and is valid.'));
        }
        //checking verification code on to verify user's phone number
        $checkVerificationCodeResp = $this->Phone->checkPhoneVerificationCode(
            $userPhoneNumber,
            $verificationCode);
        
        if (!$checkVerificationCodeResp->isOk()) {
            throw new ConflictException(json_decode($checkVerificationCodeResp->body)->message);
        }

        //updating user session to update user's is_phone_verify field to true
        $userId = $this->Auth->user('id');
        $this->loadModel('Users');
        $user = $this->Users->get($userId, [
                    'contain' => [
                        'UserProfiles'
                    ]
                ]);

        $user->is_phone_verified = true;
        if (!$this->Users->save($user)) {
            throw new ConflictException(__('Some errors occurred while update your phone verification! Kindly use resend code functionality again.'));
        }
        
        $this->Auth->setUser($user->toArray());
        $this->set([
            'message' => __('success'),
            '_serialize' => ['message']
        ]);
    }

    /**
     * Method verifyPhoneNumber to verify user phone number
     *
     * @return string
     */
    public function send() {

        $this->request->allowMethod(['get']);
        // $verificationCode = $this->request->getQuery('type');

        // debug($verificationCode); die;
        $userPhoneNumber =  $this->Auth->user('user_profile.phone_number');

        if (empty($userPhoneNumber)) {
            throw new ConflictException(__('Phone Number not found. Please ensure valid phone number is added to profile.'));
        }
    	//checking verification code on to verify user's phone number
        $sendVerificationCodeResp = $this->Phone->sendPhoneVerificationCode($userPhoneNumber);

        if (!$sendVerificationCodeResp->isOk()) {
            throw new ConflictException(json_decode($sendVerificationCodeResp->body)->message);
        }

        $response = [
        	'msg' => __('Code has been sent to your associated phone number successfully.')
        ];
        $this->set([
            'response' => $response,
            '_serialize' => ['response']
        ]);
    }
}