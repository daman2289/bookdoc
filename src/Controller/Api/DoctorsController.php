<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\BadRequestException;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash; 


require_once (ROOT . DS .'vendor' . DS . 'google-calendar-api.php');
/**
 * Doctors Controller
 *
 * @property \App\Model\Table\DoctorsTable $Doctors
 *
 * @method \App\Model\Entity\State[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorsController extends AppController
{

    public function initialize(){
        parent::initialize();
        $this->loadComponent('Email');
        $arr = ['getTimes'];

        if (empty($this->request->header('AUTHORIZATION'))) {
            $arr = ['getTimes', 'view'];
        }

        $this->Auth->allow($arr);
 
    }

    /**
     * view method
     *
     * @return \Cake\Http\Response|void
     */
    public function getAvailability(){
        $this->request->allowMethod(['post']);
        $userId = $this->Auth->user('id');
        // pr($userId); die;
        $this->loadModel('Durations');
        $duration = $this->Durations->find()
                    ->where(['Durations.user_id' => $this->Auth->user('id')])
                    ->first();
        $date = $this->request->getData('date');
        $time = $this->request->getData('time');

        if (is_null($date) || is_null($time)) {
            throw new NotFoundException(__("Date and time can not be null"));
        }

        $this->loadModel('DoctorUnavailability');
        $newdate = new \Datetime("{$date} {$time}");

        $this->loadModel('Users');
            $this->Users->hasOne('Durations', [
                'foreignKey' => 'user_id'
            ]);
            $doctor = $this->Users->find()
                ->where([
                    'Users.id' => $userId,
                    'Users.role_id' => Configure::read('UserRoles.Doctor')
                ])
                ->contain([
                    'DoctorAvailability' => [
                        'fields' => [
                            'available_from',
                            'available_upto',
                            'full_day_off',
                            'weekday',
                            'user_id',
                        ]
                    ],
                    'DoctorUnavailability' => function($q) use($date){
                        return $q->where(['DATE(DoctorUnavailability.unavailable_from)' => $date]);
                    },
                    'Durations'
            ])
            ->formatResults(function ($results) {
              return $results->map(function ($row){

                    foreach ($row->doctor_unavailability as $key => $value) {
                        $ranges = [];
                        $begin = new \DateTime($value->unavailable_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->unavailable_upto->format("Y-m-d H:i:s"));
                        $timespan = $row->duration->duration*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($ranges, $new);
                        }
                        $row->doctor_unavailability[$key]->unavailable_slots = $ranges;
                    }
                    return $row;
                });
            })
            ->first();
        $data_unavailable  = [];
        foreach ($doctor->doctor_unavailability as $key => $value) {

            $weekday = date('N', strtotime($value->formatted_time));
            $data_unavailable[$weekday][$value->id] = Hash::extract($value->unavailable_slots, '{n}.timing');
        }
        
        $data = $this->DoctorUnavailability
                        ->exists([
                            'unavailable_from' => $newdate, 
                            'user_id' => $userId
                        ]);
        

        $message = __("{$time} is available");

        if ($data) {
            $message = __("{$time} is unavailable.");
        }
        $is_available = !$data;
        $this->loadModel('DoctorAvailability');
        $dayofweek = date('N', strtotime($date));
        if(!empty($data_unavailable[$dayofweek])) {
            $undays = Hash::extract($data_unavailable[$dayofweek],'{n}.{n}');
        }

        $availability = $this->DoctorAvailability->find()
                        ->where(['DoctorAvailability.weekday' => $dayofweek,'DoctorAvailability.user_id' => $this->Auth->user('id')])
                        ->first();
        $start_time = date('H:i',strtotime($availability->available_from));
        $end_time = date('H:i',strtotime($availability->available_upto));
        
        $dur = $duration->duration;
        $begin = new \DateTime($start_time);
        $end = new \DateTime($end_time);
        
        $timespan = $dur*60;
        //$timespan = 30*60;
        $interval = new \DateInterval('PT'.$timespan.'S');
        $dateRange = new \DatePeriod($begin, $interval, $end);
        $range = [];
        foreach ($dateRange as $date) {
            $new = [];
            $new[] = $date->format('H:i');
            $range = array_merge($range, $new);
        }
        if(!empty($undays)) {
           foreach ($undays as $key => $value) {
            $keys = array_search($value, $range); 
            unset($range[$keys]);
          }
        }
        
        $this->set([
            'is_available'   => $is_available,
            'message'    => $message,
            'range' => $range,
            '_serialize' => ['message','is_available','range']
        ]);
    }

    public function getTimes(){
        $this->request->allowMethod(['post']);
        $startTime = $this->request->getData('startTime', '00:00');
        $endTime = $this->request->getData('endTime', '23:59');
        $duration = $this->request->getData('duration', 30);
        if (is_null($startTime) && is_null($endTime) && is_null($duration)) {
            throw new NotFoundException(__("Invalid Request"));
        }
        $this->loadComponent('User');
        $availableTimes = $this->User->getTimes($startTime, $endTime, $duration);
        $this->set(compact('availableTimes'));
        $this->set('_serialize', ['availableTimes']);
        
    }

    public function getBlockTimings(){
        $this->request->allowMethod(['post']);
        $doctorId = $this->request->getData('doctorId', null);
        $weekId = $this->request->getData('weekId', null);
        if (is_null($doctorId) && is_null($weekId)) {
            throw new NotFoundException(__("Invalid Request"));
        }
        $this->loadModel('DoctorBlockTimings');
        $blockTimes = $this->DoctorBlockTimings->findByUserIdAndWeekday($doctorId, $weekId)->select(['timing', 'weekday']);

        $this->set(compact('blockTimes'));
        $this->set('_serialize', ['blockTimes']);
    }

    public function removeFavourite() {
        $this->request->allowMethod(['delete','post']);
        $this->loadModel('DoctorFavourites');
        $message = __("Error. Invalid Request Method");
        
        if ($this->request->is('post')) {
            $doctorId = $this->request->getData('doctor_id', null);
            $patientId = $this->Auth->user('id');
            $entity = $this->DoctorFavourites
                            ->findByDoctorIdAndPatientId($doctorId, $patientId)
                            ->firstOrFail();

            if (!$this->DoctorFavourites->delete($entity)) {
                throw new NotFoundException(__("Unable to save. Try again later"));
            }
            $message = __("Success");
        }
        $data = $message;
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    
    public function addFavourite() {
        $this->loadModel('DoctorFavourites');

        $this->request->allowMethod(['post', 'put', 'patch']);

        $docId = $this->request->getData('doctor_id', null);
        $patId = $this->Auth->user('id');
        if (empty($docId)) {
            $message = __('Invalid Request. Please try again later.'); 
            throw new BadRequestException($message);
        }
        $favourite = $this->DoctorFavourites->findByDoctorIdAndPatientId($docId, $patId)->first();
        
        $message = __('Doctor is already been favorited.');
        if (empty($favourite)) {
            $message = __('Doctor has been made favourite');
            $favourite = $this->DoctorFavourites->newEntity();
        }

        $favourite = $this->DoctorFavourites->patchEntity($favourite, $this->request->getData());
        $saved = $this->DoctorFavourites->save($favourite);

        if (!$saved) {
            $message = __('Invalid Request: Patient or Doctor identifier is missing or invalid');                
            throw new BadRequestException($message);
        }


        $this->set([
            'data'   => $saved,
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);
    }


   
    public function getDoctorWorkingDays(){
        $this->request->allowMethod(['post']);
        $userId = $this->Auth->user('id');
        if (is_null($userId)) {
            throw new NotFoundException(__("Invalid Request"));
        }

        $this->loadModel('DoctorAvailability');
        $availabiltyDays = $this->DoctorAvailability
                            ->findByUserId($userId);
        
        
        $this->set(compact('availabiltyDays'));
        $this->set('_serialize', ['availabiltyDays']);
    }

    public function appointments(){
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];

        $this->request->allowMethod(['get']);
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $showPast = $this->request->getQuery('showOnlyPast', false);

        $appointments = $this->Bookings->findByDoctorId($userId)
                            ->contain([
                                'IllnessReasons', 
                                'Users' => ['UserProfiles'], 
                                'Patient' => ['PatientProfiles']
                                ])
                            ->select([
                                'Bookings.id',
                                'Bookings.patient_id', 'Bookings.doctor_id', 'Bookings.booking_date', 
                                'Bookings.booking_time', 'Bookings.duration', 'Bookings.illness_reason_id',
                                'Bookings.status', 'Bookings.patient_dob', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob', 'Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.doctor_note',
                                'Bookings.booking_type', 'Bookings.reason',
                                
                                'IllnessReasons.id', 'IllnessReasons.title_eng',

                                'Users.email', 'Users.role_id',

                                'Patient.email', 'Patient.role_id',

                                'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.first_name',
                                'PatientProfiles.first_name', 'PatientProfiles.last_name',
                            ])
                            ->order(['booking_date' => "ASC"]);

        $message = __('success');                
        
        if ($showPast) {
            $appointments->where(['booking_date <' => new \DateTime()]);            
        }

        if ($appointments->isEmpty()) {
            $message = __('No Appointments found');                
        }

        $appointments = $this->paginate($appointments);

        $this->set([
            'data'   => [
                 'prevPage' => $this->request->params['paging']['Bookings']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Bookings']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Bookings']['count'],
                'page' => $this->request->params['paging']['Bookings']['page'],
                'records' => $appointments,
            ],
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);
    }

    public function upcomingAppointments() {
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];

        $this->request->allowMethod(['get']);
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');

        $appointments = $this->Bookings->findByDoctorId($userId)
                            ->where(['booking_date >' => new \DateTime()])
                            ->contain([
                                'IllnessReasons', 
                                'Users' => ['UserProfiles'], 
                                'Patient' => ['PatientProfiles']
                                ])
                            ->select([
                                'Bookings.id',
                                'Bookings.patient_id', 'Bookings.doctor_id', 'Bookings.booking_date', 
                                'Bookings.booking_time', 'Bookings.duration', 'Bookings.illness_reason_id',
                                'Bookings.status', 'Bookings.patient_dob', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob', 'Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.doctor_note',
                                'Bookings.booking_type', 'Bookings.reason',
                                
                                'IllnessReasons.id', 'IllnessReasons.title_eng',

                                'Users.email', 'Users.role_id',

                                'Patient.email', 'Patient.role_id',

                                'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.first_name',
                                'PatientProfiles.first_name', 'PatientProfiles.last_name',
                            ])
                            ->order(['booking_date' => "ASC"]);

        $message = __('success');  
        if ($appointments->isEmpty()) {
            $message = __('No Appointments found');                
        }

        $appointments = $this->paginate($appointments);

        $this->set([
            'data'   => [
                 'prevPage' => $this->request->params['paging']['Bookings']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Bookings']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Bookings']['count'],
                'page' => $this->request->params['paging']['Bookings']['page'],
                'records' => $appointments,
            ],
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);              
    }

    public function rescheduleAppointment(){
        $this->request->allowMethod(['put', 'patch']);
        $userId = $this->Auth->user('id');
        $this->loadComponent('Email');

        $booking_date  =$this->request->getData('booking_date', null);
        $booking_time  =$this->request->getData('booking_time', null);
        $id  = $this->request->getData('booking_id', null);

        if (is_null($id)) {
            throw new BadRequestException(__("Invalid Request. Booking Id can't be empty."));
        }
        if (empty($booking_date) && empty($booking_time)) {
            throw new BadRequestException(__("New Booking Date or Booking Time can not be empty."));
        }

        $this->loadModel("Bookings");
        $data = $this->Bookings->findByIdAndDoctorId($id, $userId)->first();
        
        if (empty($data)) {
            throw new RecordNotFoundException(__("Record not found. Please try again with correct data."));
        }
        if ($data->status == 2) {
            throw new BadRequestException(__("Confirmed Bookings can't be altered."));
        }

        $data->booking_date = $booking_date;
        $data->booking_time = $booking_time;
        $data->status = 1;

        if (!$saved = $this->Bookings->save($data)) {
            throw new BadRequestException(__("Unable to save. Please try again later."));            
        }

        $bookingData = $this->Bookings
                        ->findByDoctorId($userId)
                        ->contain([
                            'Users' => [
                                'UserProfiles' => ['States', 'Countries'],
                                "UserSpecialities"  => function($q){
                                    return $q->select([
                                        'UserSpecialities.id',
                                        'UserSpecialities.user_id',
                                        ]);
                                }                            
                            ],
                            'IllnessReasons', 
                            'Insurances', 
                            "Patient",
                        ])
                        ->select([
                            'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                            'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                            'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                            'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                            'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                            'Bookings.who_will_seeing', 
                            
                            'Insurances.title_eng',
                            
                            'Patient.id', 'Patient.email',

                            'IllnessReasons.title_eng', 'Users.id', 'Users.email', 
                            'States.name',
                            'Countries.name',
                            'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.state_id', 
                            'UserProfiles.country_id', 
                            
                            ])
                            ->first();

        // $this->Email->sendRescheduleBookingNotification($bookingData);
        
        $message = __('Appointment Successfully Rescheduled.');

        $this->set([
            'message' => $message,
            'data' => $bookingData,
            '_serialize' => ['message', 'data']
        ]);
    }

    public function updateAppointment(){        
        $this->loadModel('Bookings');
        $this->loadComponent('Email');

        $userId = $this->Auth->user('id');
        $saved = false;
        $status = $this->request->getData('status');
        $bookingId = $this->request->getData('booking_id');

        if( empty($status) || empty($bookingId)){
            throw new NotFoundException(__("Status Parameter not found."));            
        }
        if (!in_array($status, [2,3])) {
            throw new NotFoundException(__("status cannot be other than 2 or 3."));            
        }
        
        $booking = $this->Bookings->findByIdAndDoctorId($bookingId, $userId)->first();
    
        if(empty($booking)){
            throw new NotFoundException(__("No Data associated with this account found. Please check input details."));            
        }
        
        $booking->status = $status;
        $saved = $this->Bookings->save($booking);

        if (!$saved) {
            $this->Flash->error(__('Booking could not get confirmed.'));        
            throw new NotFoundException(__("Unable to save. You might not authorised to perform this action."));
        }
        
        if ($status == 2) {
            //ADDING TO UNAVAILABLE TIME
            $unavailable_from = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            $unavailable_upto = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            $unavailable_upto = $unavailable_upto->modify("$booking->duration minute");
            $result = $this->addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type = 1);
            if (!$result) {
                $message = __('Unable to mark this slot unavailable. Please contact Adminstrator');
                throw new NotFoundException($message);
            }
            $message = __('Booking Confirmed.');
            $this->Email->sendConfirmBookingEmail($bookingId);

        }

        if ($status == 3) {
            $this->Email->sendCancelBookingEmail($bookingId);
            $message = __('Booking Declined.');

        }
     
        $this->set([
            'data'   => $saved,
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);
    }

    public function addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type){
        $fd['unavailable_type'] = $unavailable_type;
        $fd['unavailable_from'] = $unavailable_from;
        $fd['unavailable_upto'] = $unavailable_upto;
        $fd['user_id'] = $this->Auth->user('id');
        
        $this->loadModel('DoctorUnavailability');
        $docEntity = $this->DoctorUnavailability->newEntity();
        $docEntity = $this->DoctorUnavailability->patchEntity($docEntity, $fd);
        if (!$this->DoctorUnavailability->save($docEntity)) {
            return false;
        }
        return true;
    }



    public function view($id){
        $this->request->allowMethod(['get']);
        $message = __('success');
        $userLat = $this->request->getQuery('lat');
        $userLong = $this->request->getQuery('lng');

        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];
        $this->loadModel('Users');
        $this->Users->hasOne('DoctorFavourites', [
            'foreignKey' => 'doctor_id',
            'saveStrategy' => 'replace',
            'conditions' => ['DoctorFavourites.patient_id' => $this->Auth->user('id')]
        ]);
        $this->Users->hasOne('Durations', [
            'foreignKey' => 'user_id'
        ]);
        $searchResults = $this->Users->findByRoleIdAndId(Configure::read('Roles.doctor'), $id)
            ->contain(['UserProfiles'])
            ->order(['UserProfiles.first_name' => 'ASC']);
   
        $searchResults = $searchResults
            ->contain([
                'UserProfiles.States',
                'UserProfiles.Countries',
                'DoctorFavourites',
                'DoctorAvailability',
                'DoctorImages',
                'Durations',
                'UserLanguages' => function($q) {
                    return $q->contain(['Languages'])
                        ->select(['UserLanguages.user_id', 'UserLanguages.language_id', 'Languages.name']);
                },
                'UserSpecialities' => function($q) {
                    return $q->contain(['Specialities'])
                        ->select([
                            'UserSpecialities.user_id', 'UserSpecialities.speciality_id', 
                            'Specialities.title_eng',
                            'Specialities.title_german',
                        ]);
                },
                'UserInsurances' => function($q) {
                    return $q->contain(['Insurances'])
                        ->select([
                            'UserInsurances.user_id', 'UserInsurances.insurance_id', 
                            'Insurances.title_eng',
                            'Insurances.title_german',
                            'Insurances.is_deleted',
                        ]);
                },
                'DoctorUnavailability' => function($q) {
                    return $q
                        ->where(['DoctorUnavailability.unavailable_from >=' => new \Datetime()]);
                }, 
                'UserHospitalAffiliations' => function($q) {
                    return $q
                        ->select([
                            'UserHospitalAffiliations.user_id', 'UserHospitalAffiliations.name', 
                        ]);
                }, 
                'DoctorRatings' => function($q){
                    return $q->contain(['Patients' => ['UserProfiles','SocialProfiles']])->select([
                        'DoctorRatings.id',
                        'DoctorRatings.doctor_id',
                        'DoctorRatings.patient_id',
                        'DoctorRatings.booking_id',
                        'DoctorRatings.review',
                        'DoctorRatings.overall_rating',
                        'DoctorRatings.bedside_manner',
                        'DoctorRatings.wait_time',
                        'DoctorRatings.created',
                        'Patients.id',
                        'UserProfiles.id',
                        'UserProfiles.user_id',
                        'UserProfiles.profile_pic',
                        'UserProfiles.gender',
                        'UserProfiles.first_name',
                        'UserProfiles.last_name',
                        'SocialProfiles.id',
                        'SocialProfiles.user_id',
                        'SocialProfiles.picture_url',
                        'SocialProfiles.gender',
                        'SocialProfiles.first_name',
                        'SocialProfiles.last_name',
                        'SocialProfiles.full_name'
                        ]);
                    }
                ])                                               
            ->select([
                'States.name', 'Countries.name', 
                'UserProfiles.first_name', 'UserProfiles.last_name',  'UserProfiles.lat',  'UserProfiles.lng', 
                'UserProfiles.dob', 'UserProfiles.address',  'UserProfiles.lat', 
                'UserProfiles.professional_membership',
                'UserProfiles.state_id', 'UserProfiles.country_id',  'UserProfiles.gender', 
                'UserProfiles.lng','UserProfiles.zipcode', 'UserProfiles.profile_pic', 
                'Users.role_id', 'Users.id',
                'DoctorFavourites.patient_id', 'DoctorFavourites.doctor_id', 
                'UserProfiles.professional_statement',
                'Durations.user_id',
                'Durations.duration'

            ])
            ->order('UserProfiles.first_name')
            ->group('Users.id')
            ->where('UserProfiles.id IS NOT NULL')
            ->formatResults(function ($results) {
              return $results->map(function ($row){
                    foreach($row->doctor_ratings as $key => $rating) {
                        if(!empty($rating->patient->social_profile)){
                            $row->doctor_ratings[$key]->patient->user_profile = $rating->patient->social_profile;
                            unset($rating->patient->social_profile);
                        }
                    }
                    foreach ($row->doctor_availability as $key => $value) {
                        $range = [];
                        $begin = new \DateTime($value->available_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->available_upto->format("Y-m-d H:i:s"));
                        $timespan = $row->duration->duration*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($range, $new);
                        }
                        $row->doctor_availability[$key]->available_slots = $range;
                        
                    }
                    
                    return $row;
                });
            });

        $searchResults = $searchResults->first();
        if (empty($searchResults)) {
            $message = __('No Data found. Either User might not exists or has a role other than doctor.');
            throw new BadRequestException($message);
        }

        $dist = null;
        if (!empty($userLat) && !empty($userLong)) {
            $docLat = $searchResults->user_profile->lat;
            $docLong = $searchResults->user_profile->lng;
            
            $dist = $this->haversineGreatCircleDistance($docLat, $docLong, $userLat, $userLong);
        }
        $searchResults->user_profile->professional_statement = strip_tags($searchResults->user_profile->professional_statement);
        $searchResults->user_profile->professional_statement = preg_replace('/\s+/S', " ", $searchResults->user_profile->professional_statement);
        $this->set([
            'data'   => [
                'distance' => $dist,
                'record' => $searchResults,
            ],
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);

    }

    function haversineGreatCircleDistance(
        $lat1, $lon1, $lat2, $lon2, $unit="K")
    {
        // convert from degrees to radians
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
 
   public function addCalendar() {
     $this->loadModel('CalendarEvents');
     $data = $this->request->data;
     $gCalEvent = $this->CalendarEvents->find('list', [
                 'keyField' => 'id',
                 'valueField' => 'event_id'
               ])->toArray();
     foreach ($data['event'] as $key => $event) {
        if (in_array($event['id'], $gCalEvent)) {
            $events[$key]['id'] = array_search($event['id'],$gCalEvent);
        }

        $events[$key]['event'] = !empty($event['summary']) ? $event['summary'] : "No Title";

        $events[$key]['event_id'] = $event['id'];
        $events[$key]['event_date'] = !empty($event['start']['dateTime']) ? $event['start']['dateTime'] : $event['start']['date'];
        $events[$key]['event_end_date'] = !empty($event['end']['dateTime']) ? $event['end']['dateTime'] : $event['end']['date'];
        $events[$key]['doctor_id'] = $this->Auth->user('id');
     }

     $calendar = $this->CalendarEvents->newEntities($events);
     if($this->CalendarEvents->saveMany($calendar)) {
           $response = [
                    'error' => true,
                    'message' => __('Events are successfully synced')
                ];
     } else {
           $response = [
                    'error' => false,
                    'message' => __('There is a problem in syncing your events')
                ];
     }
     $this->set(compact('response'));
     $this->set('_serialize', ['response']);
   }

   public function renderEvents() {
       $this->loadModel('CalendarEvents');
       $calendar = $this->CalenderEvents->find()
                  ->where(['CalendarEvents.doctor_id' => $this->Auth->user('id')])
                  ->toArray();
       $this->set(array('resp' => $calendar, '_serialize' => 'resp'));
   }


   public function getDoctorDayOff() {
        $this->loadModel('Bookings');
        $selectedDate = $this->request->data['selectedDate'];
        $selectedDate = date('Y-m-d',strtotime($selectedDate));
        $booking = $this->Bookings->find()
                  ->where(['Bookings.doctor_id' => $this->Auth->user('id') , 'Bookings.booking_date' => $selectedDate])
                  ->toArray();
        $booking = count($booking);
        $this->loadModel('DoctorUnavailability');
        $unavailabilityData = array(
            'user_id' => $this->Auth->user('id'),
            'unavailable_from' => $selectedDate . " 00:00:00",
            'unavailable_upto' => $selectedDate . " 23:59:00",
            'unavailable_type' => 4
        );


        $unavailabilityForDayOff = $this->DoctorUnavailability->find()
                ->where($unavailabilityData)->first();

        if (!empty($unavailabilityForDayOff)) {
            $weekdayOf = true;
        } else {
            $weekdayOf = false;
        }
        
        
        
        $this->set(compact('weekdayOf','booking'));
        $this->set('_serialize', ['weekdayOf','booking']);    
    }


    public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) { 
            $url = 'https://accounts.google.com/o/oauth2/token';            
        $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
         $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url);        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
        curl_setopt($ch, CURLOPT_POST, 1);      
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);    
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        if($http_code != 200){
             $this->Flash->error(__('Failed receive access token'));
             return $this->redirect($this->referer());
        } 
            
        return $data;
    }

    public function GetUserCalendarTimezone($access_token) {
        $url_settings = 'https://www.googleapis.com/calendar/v3/users/me/settings/timezone';
        
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url_settings);       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        $data = json_decode(curl_exec($ch), true); //echo '<pre>';print_r($data);echo '</pre>';
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);      
        if($http_code != 200) {
             $this->Flash->error(__('Failed to get timezone'));
             return $this->redirect($this->referer());
        }

        return $data['value'];
    }

    public function CreateCalendarEvent($calendar_id, $summary, $event_time, $event_timezone, $access_token) {
        $url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events';

        $curlPost = array('summary' => $summary);
        
            $curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
            $curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);
      
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url_events);     
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
        curl_setopt($ch, CURLOPT_POST, 1);      
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));   
        $data = json_decode(curl_exec($ch), true);

        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);  
        
       if($http_code != 200){
             $this->Flash->error(__('Failed to create booking'));
             return $this->redirect($this->referer());
        }  
       return $data['id'];
       
        
    }

    public function addEvent() {
        $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        $client_redirect_url = Router::url(array(
                           'controller' => 'Doctors',
                           'action' => 'addEvent',
                           'prefix' => 'api',
                        ), true); 
        $client_secret = 'PUYdP5a-3PgsiBG_F0JdzndJ';

        $data = $this->GetAccessToken($client_id, $client_redirect_url,$client_secret, $_GET['code']);
        $user_timezone = $this->GetUserCalendarTimezone($data['access_token']);
        $bookingId = $this->request->session()->read('Config.booking_id')[$this->Auth->user('id')];
        $this->loadModel('Bookings');
        $this->loadModel('GoogleCalendarEvents');
        $bookings = $this->Bookings->findByDoctorId($this->Auth->user('id'))
                            ->where(['booking_date >' => new \DateTime()])
                            ->contain([
                                'IllnessReasons', 
                                'Users' => ['UserProfiles'], 
                                'Patient' => ['PatientProfiles']
                                ])
                            ->select([
                                'Bookings.id',
                                'Bookings.patient_id', 'Bookings.doctor_id', 'Bookings.booking_date', 
                                'Bookings.booking_time', 'Bookings.duration', 'Bookings.illness_reason_id',
                                'Bookings.status', 'Bookings.patient_dob', 'Bookings.patient_first_name', 
                                'Bookings.patient_last_name', 'Bookings.patient_dob', 'Bookings.patient_sex',
                                'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.doctor_note',
                                'Bookings.booking_type', 'Bookings.reason',
                                
                                'IllnessReasons.id', 'IllnessReasons.title_eng',

                                'Users.email', 'Users.role_id',

                                'Patient.email', 'Patient.role_id',

                                'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.first_name',
                                'PatientProfiles.first_name', 'PatientProfiles.last_name',
                            ])
                            ->toArray();
        foreach($bookings as $booking) {
            $summary = (!empty($booking->patient_first_name)) ? $booking->patient_first_name.' '.$booking->patient_last_name : 'null' .'  '.(!empty($booking['doctor_note'])) ? $booking['doctor_note'] : 'null';
   
           $date1 = $booking->booking_date->format('Y-m-d').' '.$booking->booking_time->format('H:i:s');

           $date = new \DateTime($date1, new \DateTimeZone($user_timezone));
           
           $start_time = str_replace(' ','T',$date->format('Y-m-d H:i:sP'));
          
            
           $end_time = date('Y-m-d H:i:s',strtotime('+'.$booking->duration. 'minutes',strtotime($date1)));

           $final_end_time = new \DateTime($end_time, new \DateTimeZone($user_timezone));

           $final_time = str_replace(' ','T',$final_end_time->format('Y-m-d H:i:sP'));
          
            $addedEvent = $this->GoogleCalendarEvents->find()
                        ->where(['GoogleCalendarEvents.booking_id' => $booking->id,'GoogleCalendarEvents.doctor_id' => $this->Auth->user('id')])
                        ->first();
            if(empty($addedEvent)) {
                $event_id = $this->CreateCalendarEvent('primary',$summary,array('start_time' => $start_time, 'end_time' => $final_time), $user_timezone, $data['access_token']);
                
                $calendar = $this->GoogleCalendarEvents->newEntity();
                $this->request->data['doctor_id'] = $this->Auth->user('id');
                $this->request->data['event_id'] = $event_id;
                $this->request->data['booking_id'] = $booking->id;
                $calendar = $this->GoogleCalendarEvents->patchEntity($calendar, $this->request->data);
                if ($this->GoogleCalendarEvents->save($calendar)) {
                   
                  //return $this->redirect($url);
                }
            }
            // Create event on primary calendar
        }
        $url =  Router::url([
                    'controller' => 'Doctors', 
                    'action' => 'BookingCalender',
                    'prefix' => false,
                    '?' => ["on" => base64_encode($booking->booking_date)]
                    ],true);
        $this->Flash->success(__('Booking saved successfully'));
        return $this->redirect($url);
        
    }
   
    public function DeleteCalendarEvent($event_id, $calendar_id, $access_token) {
        $url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_id;

        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url_events);     
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');      
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));     
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        if($http_code != 204) {
            $this->Flash->error(__('Failed to delete booking from booking calendar'));
            return $this->redirect($this->referer());
        }
    }

    public function deleteEvent() {
        $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        $client_redirect_url = Router::url(array(
                           'controller' => 'Doctors',
                           'action' => 'deleteEvent',
                           'prefix' => 'api',
                        ), true); 
        $client_secret = 'PUYdP5a-3PgsiBG_F0JdzndJ';
        $data = $this->GetAccessToken($client_id, $client_redirect_url,$client_secret, $_GET['code']);
        $bookingId = $this->request->session()->read('Config.booking_id')[$this->Auth->user('id')];
        $this->loadModel('GoogleCalendarEvents');
        $booking = $this->GoogleCalendarEvents->find()
                  ->where(['GoogleCalendarEvents.booking_id' => $bookingId])
                  ->first();
        $this->DeleteCalendarEvent($booking->event_id, 'primary', $data['access_token']);
        $this->Flash->success(__('Booking deleted successfully'));
        return $this->redirect($this->referer());
    }
    
    public function UpdateCalendarEvent($event_id, $calendar_id, $summary, $event_time, $event_timezone, $access_token) {
        $url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_id;

        $curlPost = array('summary' => $summary);
        
        $curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
        $curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);
        
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url_events);     
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');     
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json')); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));   
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);      
        if($http_code != 200) {
            $this->Flash->error(__('Failed to update booking in google calendar'));
            return $this->redirect($this->referer());
        }
    }

    public function updateCalendar() {
        $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        $client_redirect_url = Router::url(array(
                           'controller' => 'Doctors',
                           'action' => 'updateCalendar',
                           'prefix' => 'api',
                        ), true); 
        $client_secret = 'PUYdP5a-3PgsiBG_F0JdzndJ';

        $data = $this->GetAccessToken($client_id, $client_redirect_url,$client_secret, $_GET['code']);
        $user_timezone = $this->GetUserCalendarTimezone($data['access_token']);
        $bookingId = $this->request->session()->read('Config.booking_id')[$this->Auth->user('id')];
        $this->loadModel('Bookings');
        $booking = $this->Bookings->find()
                 ->where(['Bookings.id' => $bookingId])
                 ->first();

        $this->loadModel('GoogleCalendarEvents');
        $booking_event_id = $this->GoogleCalendarEvents->find()
                  ->where(['GoogleCalendarEvents.booking_id' => $bookingId])
                  ->first();

        if (!empty($booking_event_id->event_id)) {
            $calendar_id = 'primary';

        // Event on primary calendar
            $event_id = $booking_event_id->event_id;

            $event_title = (!empty($booking->patient_first_name)) ? $booking->patient_first_name.' '.$booking->patient_last_name : 'null' .'  '.(!empty($booking['doctor_note'])) ? $booking['doctor_note'] : 'null';

            // Event starting & finishing at a specific time
            $date1 = $booking->booking_date->format('Y-m-d').' '.$booking->booking_time->format('H:i:s');

            $date = new \DateTime($date1, new \DateTimeZone($user_timezone));
           
            $start_time = str_replace(' ','T',$date->format('Y-m-d H:i:sP'));
          
            
            $end_time = date('Y-m-d H:i:s',strtotime('+'.$booking->duration. 'minutes',strtotime($date1)));

            $final_end_time = new \DateTime($end_time, new \DateTimeZone($user_timezone));

            $final_time = str_replace(' ','T',$final_end_time->format('Y-m-d H:i:sP'));

            $event_time = [ 'start_time' => $start_time, 'end_time' => $final_time];

            // Full day event

            $this->UpdateCalendarEvent($event_id, $calendar_id, $event_title, $event_time, $user_timezone, $data['access_token']);
            $this->Flash->success(__('Appointment Successfully Rescheduled.'));
        } else {
            $this->Flash->success(__('Appointment confirmed successfully.'));
        }
        // updating event on the primary calendar
        
        return $this->redirect($this->referer());
    }

}
