<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Insurances Controller
 *
 * @property \App\Model\Table\InsurancesTable $Insurances
 *
 * @method \App\Model\Entity\Insurance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InsurancesController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {   
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');       
        $this->Auth->allow(['index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Insurance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function index(){
        $this->request->allowMethod(['get']);
        $this->paginate = ['limit' => $this->request->getQuery('limit'), env('APILIMIT',10)];

        $insurance = $this->Insurances
            ->find()
            ->select(['id', 'title_eng', 'title_german', 'is_deleted'])
            ->order(['Insurances.title_eng' => 'ASC']);
        
        $insurance = $this->paginate($insurance);
        $this->set(compact('insurance'));
        $this->set('_serialize', ['insurance']);

    }
     
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $insurance = $this->Insurances->get($id);
        $insurance->is_deleted = Configure::read('Insurance.is_deleted');   
        if ($this->Insurances->save($insurance)) {       
            $msg = __('The insurance has been deleted.');
        } else {
            $msg = __('The insurance could not be deleted. Please, try again.');
        }
        $this->set([
            'msg' => $msg,
            '_serialize' => ['msg']
        ]);
    }   
}
