<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\InternalErrorException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\I18n\Time;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Sendgrid;
use Cake\Routing\Router;
use Cake\View\View;

/**
 * Payments Controller
 *
 */
class PaymentsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Stripe');
        $this->Auth->allow(['webhook', 'connect', 'disconnect']);
    }

    public function add(){
        $stripeToken  = $this->request->getData('stripeToken', null);
        
        if (empty($stripeToken)) {
            throw new BadRequestException(__("Invalid request, Please try again later!"));
        }
        $userStripeDetailsTable = TableRegistry::get('UserStripeDetails');
        $userStripeDetails = $userStripeDetailsTable->getDetails(['user_id' => $this->Auth->user('id')]);
        // If details are not available, then create customer
        if (empty($userStripeDetails)) {

            // create customer with given email id
            $stripeCustomerId = $this->Stripe->createCustomer($this->Auth->user('email'));
            $savedCardId = null;
            $userStripeDetails = $userStripeDetailsTable->newEntity();
            $userStripeDetails->user_id = $this->Auth->user('id');
            $userStripeDetails->customer_id = $stripeCustomerId;

        } else {

            $stripeCustomerId = $userStripeDetails['customer_id'];
            $savedCardId = $userStripeDetails['card_id'];
        }
        
        $cardDetails = $this->Stripe->addCard($stripeCustomerId, $stripeToken, $savedCardId);
        
        // update stripe card id
        $userStripeDetailsTable->save($userStripeDetails);

        $this->set([
            'message' => $cardDetails->last4,
            '_serialize' => 'message'
        ]);
    }

    public function webhook() {
        $stripeEvent = $this->request->data;
        $data = 'success';

        if (empty($stripeEvent) || empty($stripeEvent['type'])) {
            throw new BadRequestException(__("Invalid webhook request"));
        }
        switch ($stripeEvent['type']) {
            
            case 'invoice.created': 
                if ($stripeEvent['data']['object']['paid'] != true) {
                    break;
                }
                $stripeCustomerId = $stripeEvent['data']['object']['customer'];
                $stripePaidAmount = $stripeEvent['data']['object']['amount_paid'];

                $stripeData = $stripeEvent['data']['object']['lines']['data'];
                $stipeSubs = $stripeEvent['data']['object']['subscription'];
                $invoice_pdf = $stripeEvent['data']['object']['invoice_pdf'];
                $isPaid = $stripeEvent['data']['object']['paid'];

                foreach ($stripeData as $key => $value) {
                    if ($value['id'] == $stipeSubs) {
                        $stripePaidAmount = $value['amount'];
                    }
                }

                $discount = $stripeEvent['data']['object']['discount'];
                $discount = ($discount && ($discount != null) ) ? $discount['coupon']['percent_off'] : 0;

                $stripePaidAmount = $stripePaidAmount - (($stripePaidAmount /100) * $discount);

                $usersTable = TableRegistry::get('Users');
                $userDetails = $usersTable->find()
                                    ->where(['UserStripeDetails.customer_id' => $stripeCustomerId])
                                    ->contain(['UserStripeDetails'])
                                    ->firstOrFail();

                if (!empty($userDetails)) {
                    // save user_transactions details
                    $userTransactionsTable = TableRegistry::get('UserTransactions');
                    $entity = $userTransactionsTable->newEntity();
                    $myData = [
                        'uuid' => Text::uuid(),
                        'user_id' => $userDetails->id,
                        'plan_id' => $userDetails->plan_id,
                        'amount' => $stripePaidAmount/100,
                        'is_paid' => 1,
                        'invoice_url' => $invoice_pdf,
                    ];
                    $entity = $userTransactionsTable->patchEntity($entity, $myData);
                    
                    if (!$userTransactionsTable->save($entity)) {
                        $data = $this->_setValidationError($entity->errors());
                    }
                }
                break;
                
            case 'customer.subscription.updated':
                $stripeSubscriptionPlanId = $stripeEvent['data']['object']['plan']['id'];
                $stripeSubscriptionStatus = $stripeEvent['data']['object']['status'];
                $stripeCustomerId = $stripeEvent['data']['object']['customer'];

                // if subscription is past_due or unpaid then downgrade subscription to free subscription
                if (in_array($stripeSubscriptionStatus, ['past_due','unpaid'])) {

                    $userStripeDetailsTable = TableRegistry::get('UserStripeDetails');
                    $contain = ['Users'];
                    $userStripeDetails = $userStripeDetailsTable->getDetails(['customer_id' => $stripeCustomerId], $contain);
                    // If user has paid plan then downgrade it to free plan
                    // if ($userStripeDetails->user->plan->is_default == 0) {
                    $this->_subscribeToPlan($stripeSubscriptionPlanId, $userStripeDetails->user->id, $userStripeDetails->user->email);
                    // }
                }
                break;

            
            default:
            
                break;
        }
        $this->response->type('json');
        $this->response->body($data);
        return $this->response;
    }


    private function createStripeCharge($customer_id, $total_amount, $transfer_group) {
        try {
            $response = $this->Stripe->createStripeCharges($customer_id, $total_amount, $transfer_group);
            // $this->__setChargeIdInOrder($order_id, $response->id);
            return $response;
        }
        catch (Stripe\Error\Base $e) {
            throw new BadRequestException("Stripe Error:- " + $e->getMessage());
        }
        catch (Exception $e) {
            throw new InternalErrorException(__("Internal server error"));
        }
    }


}