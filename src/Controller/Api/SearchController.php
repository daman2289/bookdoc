<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Specialities Controller
 *
 * @property \App\Model\Table\SpecialitiesTable $Specialities
 *
 * @method \App\Model\Entity\Speciality[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SearchController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if (empty($this->request->header('AUTHORIZATION'))) {
            $this->Auth->allow(['index']);
        }
    }

    public function index(){
        $this->request->allowMethod(['get']);
        $this->paginate = ['limit' => $this->request->getQuery('limit', env('APILIMIT',10))];
        $this->loadModel('Users');

        $this->Users->hasOne('DoctorFavourites', [
            'foreignKey' => 'doctor_id',
            'saveStrategy' => 'replace',
            'conditions' => ['DoctorFavourites.patient_id' => $this->Auth->user('id')]
        ]);
        $this->Users->hasOne('Durations', [
            'foreignKey' => 'user_id'
        ]);
        $searchResults = $this->Users->findByRoleId(Configure::read('Roles.doctor'))
            ->contain(['UserProfiles'])
            ->order(['UserProfiles.first_name' => 'ASC']);
        
         $searchResults = $searchResults
            ->contain([
                'UserProfiles.States',
                'UserProfiles.Countries',
                'DoctorFavourites',
                'DoctorAvailability',
                'Durations',
                'UserInsurances' => function($q){
                    return $q->select([
                        'UserInsurances.user_id', 
                        'UserInsurances.id', 
                        'UserInsurances.insurance_id',

                        ]);
                    },
                'DoctorUnavailability' => function($q) {
                    return $q
                        ->where(['DoctorUnavailability.unavailable_from >=' => new \Datetime()]);
                }, 
                'DoctorRatings' => function($q){
                    return $q->select([
                        'DoctorRatings.id',
                        'DoctorRatings.doctor_id',
                        'DoctorRatings.booking_id',
                        'DoctorRatings.review',
                        'DoctorRatings.overall_rating',
                        'DoctorRatings.bedside_manner',
                        'DoctorRatings.wait_time',
                        ]);
                    }
                ])                                               
            ->select([
                'States.name', 'Countries.name', 
                'UserProfiles.first_name', 'UserProfiles.last_name', 
                'UserProfiles.dob', 'UserProfiles.address',  'UserProfiles.lat', 
                'UserProfiles.state_id', 'UserProfiles.country_id',  'UserProfiles.gender', 
                'UserProfiles.lng','UserProfiles.zipcode', 'UserProfiles.profile_pic', 'UserProfiles.my_patient_only',
                'Users.role_id', 'Users.id',
                'DoctorFavourites.patient_id',
                'DoctorFavourites.doctor_id',
                'Durations.user_id',
                'Durations.duration'

            ])
            ->order('UserProfiles.first_name')
            ->group('Users.id')
            ->formatResults(function ($results) {
              return $results->map(function ($row){
                    foreach ($row->doctor_availability as $key => $value) {
                        $range = [];
                        $begin = new \DateTime($value->available_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->available_upto->format("Y-m-d H:i:s"));
                        if(empty($row->duration)) {
                            $timespan = 30*60;
                        } else {
                            $timespan = $row->duration->duration*60;
                        }
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($range, $new);
                        }
                        $row->doctor_availability[$key]->available_slots = $range;
                        
                    }
                    
                    return $row;
                });
            });
        
        $term = $this->request->getQuery('term', null);
        $zip = $this->request->getQuery('zip', null);
        $insurance = $this->request->getQuery('insuranceType', null);
        $gender = $this->request->getQuery('gender', null);
        $lat = $this->request->getQuery('lat', null);
        $lng = $this->request->getQuery('lng', null);
        $day = $this->request->getQuery('day', 'no');
        $slot = $this->request->getQuery('slot');
        
        if ($term) {
            $searchResults->join([
                    'UserSpecialities' => [
                        'table' => 'user_specialities',
                        'type' => 'LEFT',
                        'conditions' => 'UserSpecialities.user_id = Users.id',
                    ],
                    'Specialities' => [
                        'table' => 'specialities',
                        'type' => 'LEFT',
                        'conditions' => "Specialities.id = UserSpecialities.speciality_id AND Specialities.title_eng LIKE '%{$term}%'",
                    ]
                ]);
            
            $term = explode(" ",$term)[0];
            $searchResults->andWhere([
                'OR' => [
                    'UserProfiles.first_name LIKE' => "%" . $term . "%",
                    'UserProfiles.last_name LIKE' => "%" . $term . "%",
                    'Specialities.id IS NOT NULL'
                    ]
                ]);
        }

        if (!empty($zip) || ( !empty($lat) && !empty($lng) )) {
            $latLng = [
                'lat' => $lat, 
                'lng' => $lng
            ];
            if (!empty($zip) ) {
                $latLng = $this->getLatandLong($zip);
            }
            $search_radius = 15; //In Miles  

            if (!empty($latLng['lat']) && !empty($latLng['lng'])) {
                $distance = '('. 3959 .' * acos (cos ( radians('. $latLng['lat'] .') )
                                * cos( radians( UserProfiles.lat ) )
                                * cos( radians( UserProfiles.lng )
                                - radians('. $latLng['lng'] .') )
                                + sin ( radians('. $latLng['lat'] .') )
                                * sin( radians( UserProfiles.lat ) )))';

                $havingClause = ['distance <=' => $search_radius];

                $searchResults
                ->contain([
                    'UserProfiles' => function ($q) use ($distance) {
                        return $q->select([
                            'lat',
                            'lng',
                            'country_id',
                            'state_id',
                            'first_name',
                            'last_name',
                            'zipcode',
                            'gender',
                            'distance' => $distance,
                            'address'
                        ]);
                    },

                ])
                ->having($havingClause);
                
            }else{
                $searchResults->andWhere(["UserProfiles.zipcode" => $zip]);
            }

        }

        if ($gender){
            switch ($gender) {
                case 'm':
                    $searchResults->andWhere(['UserProfiles.gender' => 'Male']);
                    break;

                case 'f':
                    $searchResults->andWhere(['UserProfiles.gender' => 'Female']);
                    break;
                
                default:
                    break;
            }
        }

        if (!empty($insurance)) {
            $searchResults->innerJoinWith('UserInsurances', function ($q) use ($insurance){
                return $q->where(['UserInsurances.insurance_id' => $insurance]);
            });

        }

        if (!empty($slot)) {
            $dayofweek = date('N');
            $searchResults->innerJoinWith('DoctorAvailability', function ($q) use ($dayofweek){
                return $q->where(['DoctorAvailability.weekday' => $dayofweek]);
            });
            $slot = new Time($slot);
            //Removing Records which are present in unavailable time
            $searchResults ->notMatching('DoctorUnavailability', function ($q) use ($slot) {
                return $q->where(['DoctorUnavailability.unavailable_from' => $slot]);
            });

        }

        if (in_array($day, ['today', 'next'])) {
            if ($day == 'today') {
                $dayofweek = date('N');
                $searchResults->innerJoinWith('DoctorAvailability', function ($q) use ($dayofweek){
                    return $q->where(['DoctorAvailability.weekday' => $dayofweek]);
                });
    
            }else{
                $todayofweek = date('N');
                $next3Days = [$todayofweek, $todayofweek+1, $todayofweek+2];
                $searchResults->innerJoinWith('DoctorAvailability', function ($q) use ($next3Days){
                    return $q->where(['DoctorAvailability.weekday IN' => $next3Days]);
                });
            }
        }

        
        $searchResults = $this->paginate($searchResults);
        

        $this->set([
            'success' => true,
            'message' => 'Success',
            'data' => [
                'prevPage' => $this->request->params['paging']['Users']['prevPage'], 
                'nextPage' => $this->request->params['paging']['Users']['nextPage'],
                'totalRecords' => $this->request->params['paging']['Users']['count'],
                'page' => $this->request->params['paging']['Users']['page'],
                'records' => $searchResults,
            ],
            '_serialize' => ['success', 'message', 'data']
        ]);   
    }

    public function getLatandLong($zip){
        $url = "http://maps.googleapis.com/maps/api/geocode/json?components=country:AT|postal_code:".urlencode($zip)."&amp;sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        if ($result['status'] != 'OK') {

            return [
                'lat' => null, 
                'lng' => null
            ];
        }
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
       
        return $result3[0];
    }

    
}
