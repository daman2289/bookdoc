<?php
namespace App\Controller\Api;

use App\Controller\AppController;

/**
 * States Controller
 *
 * @property \App\Model\Table\StatesTable $States
 *
 * @method \App\Model\Entity\State[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['view', 'index', 'webview']);
 
    }

    /**
     * view method
     *
     * @return \Cake\Http\Response|void
     */
    public function index(){
        $this->request->allowMethod(['get']);
        // $this->paginate = ['limit' => $this->request->getQuery('limit'), env('APILIMIT',10)];

        $states = $this->States->find()
            ->select(['id','name', 'country_id'])
            ->order(['name' => 'ASC']);
        
        // $states = $this->paginate($states);

        $this->set([
            'message' => __('success'),
            'data' => $states,
            '_serialize' => ['message', 'data']
            ]);
    }

    public function view($countryId)
    {
        $states = $this->States->find()
                                ->where([
                                    'States.country_id' => $countryId
                                ]);
        
        $this->set([
            'message' => __('success'),
            'data' => $states,
            '_serialize' => ['message', 'data']
            ]);
    }

    public function webView($countryId){

        $states = $this->States->find('list', [
                                    'keyField' => 'id',
                                    'valueField' => 'name'
                                ])
                                ->where([
                                    'States.country_id' => $countryId
                                ]);
        
        $this->set(compact('states'));
        $this->set('_serialize', ['states']);
    }
}
