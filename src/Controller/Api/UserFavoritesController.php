<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Specialities Controller
 *
 * @property \App\Model\Table\SpecialitiesTable $Specialities
 *
 * @method \App\Model\Entity\Speciality[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserFavoritesController extends AppController
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index(){
        $userId = $this->Auth->user('id');
        $this->loadModel('DoctorFavourites');
        $docFav = $this->DoctorFavourites
                ->findByPatientId($userId)
                ->contain(['Doctors' => ['UserProfiles']])
                ->toArray();

        $message = __('success');
        if (empty($docFav)) {
            $message = __('No Data Found');
        }
        $this->set([
            'data'   => $docFav,
            'message'    => $message,
            '_serialize' => ['message','data']
        ]);

    }
}