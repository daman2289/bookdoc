<?php

namespace App\Controller;

//namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Twilio\Rest\Client;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\Http\Exception\BadRequestException;

//require_once(ROOT . 'vendor' . DS  . 'twilio' . DS . 'sdk' . DS . 'Services'. DS . 'facebook.php');
//require_once ROOT.DS.'/vendor/autoload.php';

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HospitalsController extends AppController {
	public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadComponent('Upload');
        $this->loadComponent('Email');
        
         $this->paginate = [
            'limit' => Configure::read('recordsPerPage'),
        ];
        // Auth component allow visitors to access add action to register  and access logout action 
    }

    public function index() {
    	$this->viewBuilder()->setLayout('frontend_dashboard');

        switch ($this->Auth->user('plan_id')) {            
            case '1':
                $activePlanId = env("STRIPE_MONTHLY_PLAN_ID");
                break;
            case '2':
                $activePlanId = env("STRIPE_SEMIANNUAL_PLAN_ID");
                break;
            case '3':
                $activePlanId = env("STRIPE_ANNUAL_PLAN_ID");
                break;
            default:
                $activePlanId = "";
                break;
        }

        $this->loadModel('Plans');
        $allPlans = $this->Plans->find()->where(['Plans.plan_type IS NOT' => 4]);
        $this->set(compact('activePlanId', 'allPlans'));
    }

    public function addDoctor() {
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Users');
        $countryList = $this->Countries->loadCountries();
        $stateList = $this->States->loadStates('14');
        $hospital = $this->Users->newEntity();
        if($this->request->is('post')) {
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['role_id'] = 3;
            $this->request->data['is_approved'] = 1;
            $this->request->data['plan_id'] = 4;
            $this->request->data['user_profile']['phone_number'] = $this->request->data['full_number'];
            // $this->request->data('User.is_email_verified', true);
            if (!empty($this->request->data['user_profile']['dob'])) {
                $this->request->data['user_profile']['dob'] = $this->request->data['user_profile']['dob']['year']['year'] . '-' . $this->request->data['user_profile']['dob']['day']['month']. '-' . $this->request->data['user_profile']['dob']['month']['day'];
            }
            $this->request->data['hospital_doctor']['hospital_id'] = $this->Auth->user('id');

            $user = $this->Users->patchEntity($hospital, $this->request->data,[
                'associated' => ['UserProfiles','HospitalDoctors']
            ]);
            $newuser = $this->Users->save($user);
            if($newuser) {
                $this->__createSubscription($newuser);
                $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 11])
                    -> first();    
                    $temp['mail_body'] = str_replace(
                            array('#DOCTOR_NAME','#HOSPITAL','#EMAIL','#PASSWORD'),
                            array(
                                $this->request->data['user_profile']['first_name'].' '.$this->request->data['user_profile']['last_name'],
                                $this->Auth->user('hospital_profile.hospital_name'),
                                $this->request->data['email'],
                                $this->request->data['password']
                            ), 
                        $temp['mail_body']
                    );
                $this->Email->_sendEmailMessage($this->request->data['email'], $temp['mail_body'], $temp['subject']);
                $this->Flash->success(__('Doctor has been added successfully'));
                $this->redirect(['action' => 'doctorListing']);
            } else {
                $errors = $this->_setValidationError($user->errors());
                $this->Flash->error(__('Doctor has not been added successfully due to following errors'.$errors));
                $this->redirect(['action' => 'doctorListing']);
            }
        }
        $this->set(compact(['countryList','stateList']));
    }

    public function doctorListing() {
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('HospitalDoctors');
        

        $doctorListing = $this->HospitalDoctors->find()
                ->where(['HospitalDoctors.hospital_id' => $this->Auth->user('id')])
                ->contain(['Users' => ['UserProfiles']]);

        $doctors = $this->paginate($doctorListing);
        $this->set(compact(['doctors','countryList','stateList']));
    }

    public function viewDashboard($uuid, $is_hospital=false) {
        $this->loadModel('Users');
        $userType = $is_hospital ? 5 : 3;
        $contain = $is_hospital ? 'HospitalProfiles' : 'UserProfiles';

        $user = $this->Users->find()
                ->where(['Users.uuid' => $uuid, 'Users.role_id' =>$userType])
                ->contain([$contain])
                ->first();
        $doctor = $user->toArray();
        if (!$is_hospital)
            $doctor['hospital_uuid'] = $this->Auth->user('uuid');

        $this->Auth->setUser($doctor);

        if (!$is_hospital) {

            return $this->redirect([
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'Dashboard',
            ]);

        } else {

            return $this->redirect([
                'prefix' => false,
                'controller' => 'Hospitals',
                'action' => 'index',
            ]); 
        }
    }

    private function __createSubscription($newUser) {
        $subscriptionType = 'free';

        // check if plan subscription is set
        // if user is tutor
        // if (empty($this->Auth->user('plan_id'))) {
            // subscribe to plan
        return $this->_subscribeToPlan($subscriptionType, $newUser->id, $newUser->email);
        // }

        
    }
}