<?php

namespace App\Controller;

//namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Twilio\Rest\Client;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\Http\Exception\BadRequestException;

//require_once(ROOT . 'vendor' . DS  . 'twilio' . DS . 'sdk' . DS . 'Services'. DS . 'facebook.php');
//require_once ROOT.DS.'/vendor/autoload.php';

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadComponent('Upload');
        $this->loadComponent('User');
         $this->paginate = [
            'limit' => Configure::read('recordsPerPage'),
        ];
        // Auth component allow visitors to access add action to register  and access logout action 
        $this->Auth->allow(['logout', 'signUp', 'login', 'patientLogin','forgetPassword', 'resetPassword', 'cmsPage', 'verifyEmail','hospitalSignup']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $users = $this->paginate($this->Users);
       
        $this->set(compact('users'));
    }

    public function dashboard() {
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $doctorBookings = $this->Bookings->findByDoctorId($userId)
                                        ->contain(['IllnessReasons'])
                                        ->where(['Bookings.status IN' => [2, 1]])
                                        ->andWhere(['booking_date >' => new \DateTime()])
                                        ->order(['booking_date' => "ASC"]);

        $totalBookings = $this->Bookings->findByDoctorId($userId)
                                        ->count();

        $upcomingBookings = $this->Bookings->findByDoctorId($userId)
                                        ->where(['status IN' => [2, 1]])
                                        ->andWhere(['booking_date >' => new \DateTime()])
                                        ->count();

        $previousBooking = $this->Bookings->findByDoctorId($userId)
                                        ->where(['status IN' => [2]])
                                        ->andWhere(['booking_date <' => new \DateTime()])
                                        ->count();

        $this->paginate($doctorBookings);
        switch ($this->Auth->user('plan_id')) {            
            case '1':
                $activePlanId = env("STRIPE_MONTHLY_PLAN_ID");
                break;
            case '2':
                $activePlanId = env("STRIPE_SEMIANNUAL_PLAN_ID");
                break;
            case '3':
                $activePlanId = env("STRIPE_ANNUAL_PLAN_ID");
                break;
            default:
                $activePlanId = "";
                break;
        }

        $this->loadModel('Plans');
        $allPlans = $this->Plans->find()->where(['Plans.plan_type IS NOT' => 4]);

        $this->set(compact('activePlanId', 'allPlans'));
        $this->set(compact('doctorBookings','totalBookings', 'upcomingBookings', 'previousBooking'));
    }

    public function cmsPage($id) {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('CmsPages');
        $this->loadModel('MetaTags');
        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.slug' => $id])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }

        $cmsPage = $this->CmsPages->get($id, [
            'contain' => []
        ]);
        $this->set(compact('cmsPage','meta_title','meta_description'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function signUp() {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('UserProfiles');
        $this->loadModel('Specialities');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadComponent('Email');
        $this->loadModel('MetaTags');
        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.slug' => $this->request->pass[0]])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }
        $specialtyList = $this->Specialities->loadSpecialty($lngTitle = 'eng');
        $countryList = $this->Countries->loadCountries();
        $stateList = $this->States->loadStates('14');

        $user = $this->UserProfiles->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data('User.user_profile.phone_number', $this->request->data('full_number'));
            // $this->request->data('User.is_email_verified', true);

            if (!empty($this->request->getData('User.user_profile.dob'))) {
                $this->request->data('User.user_profile.dob', $this->request->data('User.user_profile.dob.year') . '-' . $this->request->data('User.user_profile.dob.month') . '-' . $this->request->data('User.user_profile.dob.day'));
            }
            $user = $this->Users->newEntity($this->request->data('User'), [
                'associated' => ['UserProfiles']
            ]);
            
            $newUser = $this->Users->save($user);
            if ($newUser) {
                $authUser = $this->Users->find()
                            ->where(['Users.id' => $user->id])
                            ->contain(['UserProfiles'])
                            ->first();
                
                $this->Email->sendVerifyEmail($newUser->id);
                $this->Flash->success(__('Email verify link has been sent to your email.Please check and verify your email'));

                if($authUser->role_id == Configure::read('UserRoles.Patient')) {
                    //PATIENT
                    $this->Auth->setUser($authUser);
                    $this->redirect(['controller' => 'Dashboard','action' => 'index']);
                } else {
                    //DOCTOR
                    // check subscription

                    $this->__createSubscription($newUser);
                    $this->Flash->success(__('Thanks for showing your interest.Your account has been created successfully.Please wait for the admin Approval'));
                    return $this->redirect(['action' => 'login']);
                }
                // Log user in using Auth
                
                //$this->registerEmailSend($user->id);

                /* try{
                  $sid = Configure::read('TWILIO_SID');
                  $token = Configure::read('TWILIO_TOKEN');

                  $client = new Client($sid, $token);
                  // Use the client to do fun stuff like send text messages!
                  $client->messages->create(
                  // the number you'd like to send the message to
                  $this->request->getData()['phone'],
                  array(
                  // A Twilio phone number you purchased at twilio.com/console
                  'from' => '+43676800555228',
                  // the body of the text message you'd like to send
                  'body' => "This is your 4 digit account verification code".$code
                  )
                  );
                  }catch(Exception $e) {
                  echo 'Message: ' .$e->getMessage();
                  } */
            } else {
                $error = true;
                $role = 'doctor';
                if ($this->request->getData('User.role_id') == 4) {
                    $role = 'patient';  
                }
                
                $errors = $this->_setValidationError($user->errors());
                $this->Flash->error(__('Your account could not be created due to the below errors' . $errors));
                 $this->set(compact('user', 'specialtyList', 'countryList', 'stateList'));
                return $this->redirect(['action' => 'signUp', '?' => ["on" => $role]]);
            }
            
        }
        $this->set(compact('user', 'specialtyList', 'countryList', 'stateList','meta_title','meta_description'));
    }

    public function hospitalSignup() {
        $this->loadModel('MetaTags');

        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.page_name' => $test])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }

        $hospital = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['hospital_profile']['phone_number'] = $this->request->data['full_number'];
            $hospital = $this->Users->patchEntity($hospital, $this->request->data, [
                'associated' => ['HospitalProfiles']
            ]);

            $newUser = $this->Users->save($hospital);
            if($newUser) {
                $this->Flash->success(__('You account has been created'));
            } else {
                $errors = $this->_setValidationError($hospital->errors());
                $this->Flash->error(__('Your account could not be created due to the below errors' . $errors));
            }

            $this->redirect(['controller' => 'Users', 'action' => 'signUp']);
        }
        $this->set(compact('meta_title','meta_description'));
    }

    public function setFreePlan(){
        $this->loadModel('Users');
        $users = $this->Users->findByRoleId(3);
        foreach ($users as $user) {
            $this->__createSubscription($user);
        }
        return $this->redirect($this->referer());
    }


    private function __createSubscription($newUser) {
		$subscriptionType = 'free';

        // check if plan subscription is set
        // if user is tutor
		// if (empty($this->Auth->user('plan_id'))) {
			// subscribe to plan
		return $this->_subscribeToPlan($subscriptionType, $newUser->id, $newUser->email);
		// }

		
    }
    

    private function random_code() {
        return str_pad(rand(0, pow(10, 4) - 1), 4, '0', STR_PAD_LEFT);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editProfile() {
        if ($this->Auth->user('role_id') == Configure::read('UserRoles.Patient')) {
            return $this->redirect(['controller' => 'Patients', 'action' => 'EditProfile']);
        }
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $countryList = $this->Countries->loadCountries();

        $countryId = (!empty($this->Auth->user('user_profile.country_id'))) ? $this->Auth->user('user_profile.country_id') : 14;
        $stateList = $this->States->loadStates($countryId);
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['UserProfiles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            if (!empty($this->request->getData()['upload_image']['name'])) {
                $file = $this->request->getData()['upload_image']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                
                $incomingImgSize = getimagesize($file['tmp_name']);
                $size = Configure::read('profileImageDimensions.size');
                if ($incomingImgSize[0] < $size['width'] || $incomingImgSize[1] < $size['height']) {
                    $this->Flash->error(__("The Profile Image should be of minimum of dimensions "). "{$size['width']} * {$size['height']}");
                    return $this->redirect($this->referer());
                }

                if (in_array($ext, $arr_ext)) {

                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/uploads' . DS . 'profile_image_' . $file['name'])) {
                        //prepare the filename for database entry
                        $user->user_profile->profile_pic = 'profile_image_' . $file['name'];
                    }
                }
            }

            $this->request->data('user_profile.phone_number', $this->request->data('full_number'));
            $this->request->data('user_profile.dob', $this->request->getData('user_profile.year') . '-' . $this->request->getData('user_profile.month') . '-' . $this->request->getData('user_profile.date'));

            $user = $this->Users->patchEntity($user, $this->request->getData(), ['associated' => ['UserProfiles']]);

            if ($this->Users->save($user)) {

                $this->Auth->setUser($user);
                $this->Flash->success(__('The profile has been updated.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The profile could not be updated. Please, try again.'));
        }
        $this->set(compact('user', 'countryList', 'stateList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login() {

        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('MetaTags');
        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.slug' => $this->request->pass[0]])
                    ->order(['id' => 'DESC'])
                    ->first();

        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }
        $redirectAfter = $this->request->getData('redirectTo', null);
        if ($this->request->is('post')) {
            // Auth component identify if sent user data belongs to a user
            $user = $this->Auth->identify();
            if ($user['role_id'] == 3 && $user['is_approved'] == 0 && $user['is_delete'] == 0) {
                if($user['is_active'] == 0) {
                   $this->Flash->error(__('Your account has not been Deleted by the admin.'));
                    return $this->redirect($this->referer()); 
                }
                $this->Flash->error(__('Your account has not been approved by the admin. Please wait till your account approved by admin'));
                return $this->redirect($this->referer());
            }

            if ($user['role_id'] == 5 && $user['is_approved'] == 0) {
                $this->Flash->error(__('Your account has not been approved by the admin. Please wait till your account approved by admin'));
                return $this->redirect($this->referer());
            }

            if ($user) {
                $this->Auth->setUser($user);
                if (!empty($redirectAfter)) {
                    return $this->redirect(['controller' => 'Doctors', 'action' => 'view', $redirectAfter]);
                }
                if($user['role_id'] == 4) {
                    return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
                }
                if($user['role_id'] == 5) {
                    return $this->redirect(['controller' => 'Hospitals', 'action' => 'index']);
                }

                $this->__setPlanExpiry($this->Auth->user('id'));
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again.'));
        }
        $this->set(compact('meta_title','meta_description'));

    }

    public function patientLogin() {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('MetaTags');
        $meta_tags = $this->MetaTags->find()
                    ->where(['MetaTags.slug' => $this->request->pass[0]])
                    ->order(['id' => 'DESC'])
                    ->first();    
        if(!empty($meta_tags)) {
            $meta_title = $meta_tags->meta_title;
            $meta_description = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? $meta_tags->meta_description_german : $meta_tags->meta_description;
        }
        if ($this->request->is('post')) {
            // Auth component identify if sent user data belongs to a user
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);
                //pr($user);die;
                if($user['role_id'] == 4) {
                    return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
                }
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again.'));
        }
        $this->set(compact('meta_title','meta_description'));
    }

    private function __setPlanExpiry($userId){
        $this->loadModel('UserStripeDetails');
        $currentPeriodEnd = $this->UserStripeDetails->findByUserId($userId)->select(['current_period_end'])->first();
        $this->request->getSession()->write('Plan.expiry', $currentPeriodEnd->current_period_end);
        return;
    }

    public function logout() {
        $session = $this->request->session();
        $session->delete('Config.SelectedSlot');
        // $this->Flash->success('You successfully have logged out');
        return $this->redirect($this->Auth->logout());
    }

    public function verifyEmail($userId) {
        $userId = base64_decode($userId);

        $user = $this->Users->get($userId);

        $user->is_email_verified = true;
        $this->Users->save($user);

        $this->Flash->success(__('You email address has been verified successfully. Please login to proceed further now.'));
        return $this->redirect(['action' => 'login']);
    }

    public function changePassword() {
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $user = $this->Users->get($this->Auth->user('id'));
        if ($this->request->is('post')) {
            if (!empty($this->request->getData())) {
                $user = $this->Users->patchEntity($user, [
                    'old_password' => $this->request->getData()['old_password'],
                    'password' => $this->request->getData()['password']
                        ], ['validate' => 'password']
                );

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The password is successfully changed'));
                    return  $this->redirect($this->referer());
                } else {
                    $data = $this->_setValidationError($user->errors());
                    $this->Flash->error($data);
                }
            }
        }
        $this->set('user', $user);
    }

    /* Forget password */

    public function forgetPassword() {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('EmailTemplates');
        if ($this->request->is('post')) {
            $email = $this->request->getData('email');
            $user = $this->Users->findByEmail($email)
                    ->contain(['UserProfiles'])
                    ->first();
            if (!empty($user)) {
                $password = sha1(Text::uuid());
                $password_token = Security::hash($password, 'sha256', true);
                $hashval = sha1($user->email . rand(1, 100));
                $user->password_reset_token = $password_token;
                $user->hashval = $hashval;
                $reset_token_link = Router::url(
                                [
                            'controller' => 'Users',
                            'action' => 'resetPassword'
                                ], TRUE) . '/' . $password_token . '#' . $hashval;

                //Send email to user
                $this->loadComponent('Email');
                $this->Email->sendForgotPasswordEmail($user, $reset_token_link);
                
                $saved = $this->Users->save($user);
                $this->Flash->success(
                        __('Please click on password reset link, sent in your email address to reset password.')
                );
            } else {
                $this->Flash->error(__('Sorry! Email address is not available here.'));
            }
        }
    }

    /* Reset Password */

    public function resetPassword($token = null) {
        $this->viewBuilder()->setLayout('frontend');
        if (!empty($token)) {
            $user = $this->Users->findByPasswordResetToken($token)
                    ->contain([
                        'UserProfiles'
                    ])
                    ->first();
            if ($user) {
                if (!empty($this->request->getData())) {
                    $user = $this->Users->patchEntity($user, [
                        'password' => $this->request->getData('new_password'),
                        'new_password' => $this->request->getData('new_password'),
                        'confirm_password' => $this->request->getData('confirm_password')
                            ], ['validate' => 'password']
                    );
                    $hashval_new = sha1($user->email . rand(1, 100));
                    $user->password_reset_token = $hashval_new;
                    if ($this->Users->save($user)) {
                        $this->Flash->success('Your password has been changed successfully');
                        $emaildata = ['name' => $user->first_name, 'email' => $user->email];
                        $this->redirect(['action' => 'login']);
                    } else {
                        $errors = $this->_setValidationError($user->errors());
                        $this->Flash->error("{$errors} ");
                    }
                }
            } else {
                $this->Flash->error('Sorry your password token has been expired.');
                return $this->redirect(['action' => 'login']);
            }
        } else {
            $this->Flash->error('Error loading password reset.');
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit Bio method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to bio.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function editBio() {
        $id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $user = $this->Users->get($id, [
            'contain' => [
                'UserInsurances',
                'UserSpecialities',
                'UserLanguages',
                'UserProfiles',
                'UserEducations',
                'UserAwards',
                'UserHospitalAffiliations'
            ]
        ]);

        if ($this->request->is('post')) {

            $bioData = $this->User->resetUserBioFieldsToSave();

            $this->request->data('id', $id);
            $this->request->data('user_profile.id', $user->user_profile->id);

            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'associated' => [
                    'UserInsurances',
                    'UserSpecialities',
                    'UserLanguages',
                    'UserProfiles',
                    'UserEducations',
                    'UserAwards',
                    'UserHospitalAffiliations'
                ]
            ]);

            if ($this->Users->save($user)) {
                $user = $this->Users->get($id, [
                    'contain' => [
                        'UserInsurances',
                        'UserSpecialities',
                        'UserLanguages',
                        'UserProfiles',
                        'UserEducations',
                        'UserAwards',
                        'UserHospitalAffiliations'
                    ]
                ]);
                $this->Flash->success(__('Your bio has been saved successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Your bio counld not save.'));
            }
        }

        $this->loadModel('Specialities');
        $specialtyList = $this->Specialities->loadSpecialty($lngTitle = 'eng');

        $this->loadModel('Insurances');
        $insuranceList = $this->Insurances->loadInsurances($lngTitle = 'eng');

        $this->loadModel('Languages');
        $languageList = $this->Languages->loadLanguages();

        $this->set(compact('user', 'specialtyList', 'insuranceList', 'languageList'));
    }

    /*public function changeMobile() {
        $id = $this->Auth->user('id');
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $user = $this->Users->get($id, [
            'contain' => ['UserProfiles']
        ]);
        if ($this->request->is('post')) {
            if ($this->request->getData()['old_mobile'] != $user->user_profile->phone_number) {
                $this->Flash->error(__('Your Old Mobile Number Does Not Match With our Records'));
            } else {
                $this->loadModel('UserProfiles');
                $UserProfilesTable = TableRegistry::get('UserProfiles');
                $user_profiles = $UserProfilesTable->get($user->user_profile->id);
                $user_profiles->phone_number = $this->request->getData()['new_mobile'];*/
                /* try{
                  $sid = Configure::read('TWILIO_SID');
                  $token = Configure::read('TWILIO_TOKEN');

                  $client = new Client($sid, $token);
                  // Use the client to do fun stuff like send text messages!
                  $client->messages->create(
                  // the number you'd like to send the message to
                  $this->request->getData()['phone'],
                  array(
                  // A Twilio phone number you purchased at twilio.com/console
                  'from' => '+43676800555228',
                  // the body of the text message you'd like to send
                  'body' => "This is your 4 digit account verification code".$code
                  )
                  );
                  }catch(Exception $e) {
                  echo 'Message: ' .$e->getMessage();
                  } */
                /*if ($UserProfilesTable->save($user_profiles)) {
                    $user = $this->Users->get($id, [
                        'contain' => ['UserProfiles']
                    ]);
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Mobile Number Saved successfully'));
                } else {
                    $this->Flash->error(__('Mobile Number not Saved successfully'));
                }
            }
        }
        $this->set(compact('user'));
    }*/

    public function videoPicture() {

        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('DoctorVideos');
        $Doctorimages = $this->loadModel('DoctorImages');
        $id = $this->Auth->user('id');

        $video = $this->DoctorVideos
                ->find()
                ->where(['user_id' => $id])
                ->first();

        $images = $this->DoctorImages
                ->find('all')
                ->where(['user_id' => $id]);


        if ($this->request->is('post')) {
            $video = $this->DoctorVideos
                    ->find()
                    ->where(['user_id' => $id])
                    ->first();

            if (!empty($video)) {
                $videos_val = $this->DoctorVideos->get($video->id);
                $videos_val = $this->DoctorVideos->patchEntity($videos_val, $this->request->getData());
                if ($this->DoctorVideos->save($videos_val)) {
                    $this->Flash->success(__('Video Link Updated successfully'));
                } else {
                    $this->Flash->error(__('Video Link Not Updated successfully'));
                }
            } else {
                $entity = $this->DoctorVideos->newEntity();
                $entity->user_id = $id;
                $entity = $this->DoctorVideos->patchEntity($entity, $this->request->getData());
                if ($this->DoctorVideos->save($entity)) {
                    $this->Flash->success(__('Video Link Updated successfully'));
                } else {
                    $this->Flash->error(__('Video Link Not Updated successfully'));
                }
            }
        }
        $this->set('video', $video);
        $this->set('images', $images);
    }

    public function removeImage() {
        $image_id = $this->request->getData('image_id');
        $Doctorimages = $this->loadModel('DoctorImages');
        $entity = $this->DoctorImages->get($image_id);
        if ($this->DoctorImages->delete($entity)) {
            $data = array('success' => '1', 'message' => 'Image Deleted Successfully');
            $this->set('data', $data);
            $this->set('_serialize', ['data']);
        }
    }

    public function saveImages() {
        $Doctorimages = $this->loadModel('DoctorImages');
        $id = $this->Auth->user('id');

        if ($this->request->is('post')) {
            if (count($this->request->getData()['upload_image']) == 1) {
                if (empty($this->request->getData()['upload_image'][0]['tmp_name'])) {
                    $this->Flash->error(__('Please upload image.'));
                    return $this->redirect(['action' => 'videoPicture']);
                }
            }

            $images = $this->DoctorImages
                            ->find('all')
                            ->where(['user_id' => $id])->toArray();

            $count_images = 1 + sizeof($images);

            if ($count_images > 5) {

                $this->Flash->error(__('You are not allowed to add More than 5 images , Please delete any image and then upload your new images'));
                return $this->redirect(['action' => 'videoPicture']);
            } else {

                if (sizeof($this->request->getData()['upload_image']) >= 5) {
                    $this->Flash->error(__('You are not allowed to add More than 5 images.'));
                    return $this->redirect(['action' => 'videoPicture']);
                }

                foreach ($this->request->getData()['upload_image'] as $key => $file) {

                    if (!empty($file['name'])) {
                        //$file = $this->request->getData()['upload_image'];
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                        $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                        if (in_array($ext, $arr_ext)) {
                            
                            $incomingImgSize = getimagesize($file['tmp_name']);
                            $size = Configure::read('profileImageDimensions.size');
                            if ($incomingImgSize[0] < $size['width'] || $incomingImgSize[1] < $size['height']) {
                                $this->Flash->error(__("The Image should be of minimum of dimensions "). "{$size['width']} * {$size['height']}");
                                return $this->redirect($this->referer());
                            }
                            
                            if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'files/doctor_images' . DS . 'doctor_images_' . $file['name'])) {
                                $img_doctor = 'doctor_images_' . $file['name'];
                                $records_images = ['images' => $img_doctor, 'user_id' => $this->Auth->user('id')];
                                $Doctorimages = $this->DoctorImages->newEntity($records_images);
                                
                                if ($this->DoctorImages->save($Doctorimages)) {
                                    $this->Flash->success(__('Your image has been saved.'));
                                    return $this->redirect(['action' => 'videoPicture']);
                                
                                } else {
                                    $this->Flash->error(__('Please Upload Images Formats Only'));
                                    return $this->redirect(['action' => 'videoPicture']);
                                }
                            }
                        } else {
                            $this->Flash->error(__('Please Upload Images Formats Only'));
                            return $this->redirect(['action' => 'videoPicture']);
                        }
                    }
                }
            }/* End Else If */
        }
    }

    public function myCalender() {
        $this->viewBuilder()->setLayout('frontend_dashboard');

        $docAvailDaysObj = TableRegistry::get('DoctorAvailabilityDays');

        $this->loadModel('DoctorAvailabilityDays');
        $this->loadModel('DoctorHolidays');

        $id = $this->Auth->user('id');
        // pr('test'); die;
        $DoctorAvailabilityDays = $this->DoctorAvailabilityDays
                        ->find('all')->contain(['DoctorAvailabilityTimings'])
                        ->where(['user_id' => $id])->toArray();

        $DoctorHolidays = $this->DoctorHolidays
                        ->find('all')
                        ->where(['user_id' => $id])->toArray();

        $this->set('DoctorAvailabilityDays', $DoctorAvailabilityDays);
        $this->set('DoctorHolidays', $DoctorHolidays);
    }

    /* THis function is used to save doctor available */

    public function saveDoctorAvailability() {
        
        $id = $this->Auth->user('id');
        if ($this->request->is('post')) {

            $this->loadModel('DoctorAvailabilityDays');
            $this->loadModel('DoctorAvailabilityTimings');

            $docAvailDaysObj = TableRegistry::get('DoctorAvailabilityDays');
            $docAvailTimingsObj = TableRegistry::get('DoctorAvailabilityTimings');


             $docAvailDaysId = $docAvailDaysObj
                              ->find('all')
                              ->where(['DoctorAvailabilityDays.user_id' => $id])
                              ->toArray();
            if(!empty($docAvailDaysId)){
                foreach ($docAvailDaysId as $key => $docAvailDays_id) {
                    $Available_id[] = $docAvailDays_id->id;
                }
                $deleteAvailabeDays = array('DoctorAvailabilityDays.user_id' => $id);
                $docAvailDaysObj->deleteAll($deleteAvailabeDays,false);
                $deleteAvailableTime = array('DoctorAvailabilityTimings.doctor_availability_day_id in' => $Available_id);
                $docAvailTimingsObj->deleteAll($deleteAvailableTime,false);
            }
            /*$queryAvailDays = $docAvailDaysObj->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'id'
                    ])->where(['user_id' => $id]);

            $queryAvailTime = $docAvailTimingsObj->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'id'
                    ])->where(['user_id' => $id]);

            $oldIdsAvailDays = $queryAvailDays->toArray();
            $oldIdsAvailTime = $queryAvailTime->toArray();  */  
            $appointmentsDays = $this->request->getData()['weekday'];
            $appointmentsTimings = $this->request->getData()['data'];

            if (!empty($appointmentsTimings)) {
                $doctorAppointmentDays = $doctorAvailabilityTimings = [];
                foreach ($appointmentsDays as $dayselected => $isChecked) {
                    if ($isChecked && isset($appointmentsTimings[$dayselected])) {
                        $doctorAppointmentDays = ['user_id' => $id, 'weekday' => $dayselected, 'created' => date("Y-m-d H:i:s", time()), 'modified' => date("Y-m-d H:i:s", time())];

                        $doctorAppointDaysEntities = $docAvailDaysObj->newEntity($doctorAppointmentDays, ['validate' => false]);
                        if ($docAvailDaysObj->save($doctorAppointDaysEntities)) {

                            foreach ($appointmentsTimings[$dayselected] as $timeData) {

                                $doctorAvailabilityTimings = ['doctor_availability_day_id' => $doctorAppointDaysEntities->id, 'timing' => date('H:i:s', strtotime($timeData['time'])), 'created' => date("Y-m-d H:i:s", time()), 'modified' => date("Y-m-d H:i:s", time())];

                                $doctorAppointTimingsEntities = $docAvailTimingsObj->newEntity($doctorAvailabilityTimings, ['validate' => false]);

                                if ($docAvailTimingsObj->save($doctorAppointTimingsEntities)) {
                                                                       
                                }else{
                                    $this->Flash->error(__('Something went wrong'));
                                    return $this->redirect(['action' => 'myCalender']);
                                }
                            }
                        } else {
                             $this->Flash->error(__('Something went wrong'));
                             return $this->redirect(['action' => 'myCalender']);
                        }
                    }
                }
            }

            $this->Flash->success(__('Saved successfully'));
            return $this->redirect(['action' => 'myCalender']); 
           /* $doctorAppEntities = $docAvailbObj->newEntities($doctorAppointment);
              // In a controller.
              foreach ($doctorAppEntities as $entity) {
              // Save entity
              $ret = $docAvailbObj->save($entity);
              }
              if ($ret) {
              $this->Flash->success(__('Data saved successfully.'));
              $docAvailbObj->deleteAll(['id IN' => $oldIds]);
              } else
              $this->Flash->error(__('Data could not saved.'));
              return $this->redirect(['action' => 'myCalender']);
             */
        }
    }

    public function removeAvailablity($id) {
        if (!empty($id)) {
            $this->loadModel('DoctorAvailabilityTimings');
            $docAvailbObj = TableRegistry::get('DoctorAvailabilityTimings');
            $temp = $docAvailbObj->get($id);
            $ret = $docAvailbObj->delete($temp);
            if ($ret) {
                $this->Flash->success(__('Record deleted successfully.'));
            } else
                $this->Flash->error(__('Record could not delete.'));
        }
        return $this->redirect(['action' => 'myCalender']);
    }

    public function saveDoctorHoliday() {
        $id = $this->Auth->user('id');
        if ($this->request->is('post')) {
            $this->loadModel('DoctorHolidays');
            $docHolidayObj = TableRegistry::get('DoctorHolidays');
            $startDate = $this->request->getData()['start_date'];
            $startDate = explode('/', $startDate);
            $newStartDate = $startDate[2] . "-" . $startDate[0] . "-" . $startDate[1];
            $endDate = $this->request->getData()['end_date'];
            $endDate = explode('/', $endDate);
            $newEndDate = $endDate[2] . "-" . $endDate[0] . "-" . $endDate[1];
            $savedData = ['start_date' => $newStartDate, 'end_date' => $newEndDate, 'user_id' => $id, 'holiday_name' => $this->request->getData()['holiday_name']];
            $doctorHolidayEntity = $docHolidayObj->newEntity($savedData);
            if ($docHolidayObj->save($doctorHolidayEntity)) {
                $this->Flash->success(__('Date saved successfully.'));
            } else
                $this->Flash->error(__('Data could not saved.'));
            return $this->redirect(['action' => 'myCalender']);
        }
    }

//     public function calendar() {
// }

}
