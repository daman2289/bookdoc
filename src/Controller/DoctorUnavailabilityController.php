<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DoctorUnavailability Controller
 *
 *
 * @method \App\Model\Entity\DoctorUnavailability[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorUnavailabilityController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $doctorUnavailability = $this->paginate($this->DoctorUnavailability);

        $this->set(compact('doctorUnavailability'));
    }

    /**
     * View method
     *
     * @param string|null $id Doctor Unavailability id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctorUnavailability = $this->DoctorUnavailability->get($id, [
            'contain' => []
        ]);

        $this->set('doctorUnavailability', $doctorUnavailability);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $doctorUnavailability = $this->DoctorUnavailability->newEntity();
        if ($this->request->is('post')) {
            $doctorUnavailability = $this->DoctorUnavailability->patchEntity($doctorUnavailability, $this->request->getData());
            if ($this->DoctorUnavailability->save($doctorUnavailability)) {
                $this->Flash->success(__('The doctor unavailability has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor unavailability could not be saved. Please, try again.'));
        }
        $this->set(compact('doctorUnavailability'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor Unavailability id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctorUnavailability = $this->DoctorUnavailability->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctorUnavailability = $this->DoctorUnavailability->patchEntity($doctorUnavailability, $this->request->getData());
            if ($this->DoctorUnavailability->save($doctorUnavailability)) {
                $this->Flash->success(__('The doctor unavailability has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor unavailability could not be saved. Please, try again.'));
        }
        $this->set(compact('doctorUnavailability'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor Unavailability id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctorUnavailability = $this->DoctorUnavailability->get($id);
        if ($this->DoctorUnavailability->delete($doctorUnavailability)) {
            $this->Flash->success(__('The doctor unavailability has been deleted.'));
        } else {
            $this->Flash->error(__('The doctor unavailability could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
