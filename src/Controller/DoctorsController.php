<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;
use Cake\Http\Exception\NotFoundException;
use Cake\Mailer\Email;
use Cake\Chronos\Chronos;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Collection\Collection;
use Cake\I18n\FrozenTime;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

//require_once(ROOT .DS. 'vendor' . DS .'google'. DS.'apiclient-services'. DS .'Calendar.php');

/**
 * Doctors Controller
 *
 */
class DoctorsController extends AppController
{
	public $lng = 'en';
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['view','addCalendar']);
        $this->loadComponent('Phone');
        if(!empty($this->getRequest()->getSession()->read('Config.locale')) && $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') {
        	$this->lng = 'de';
        }
    }

    /**
     * View method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $doctorName = null)
    {
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('DoctorRatings');

        
        $rating = $this->DoctorRatings->find()
                ->where(['DoctorRatings.doctor_id' => base64_decode($id)])
                ->contain(['Patients' => 'UserProfiles'])
                ->toArray();
        $this->loadModel('DoctorFavourites');
        $favourite = $this->DoctorFavourites->find()
                    ->where(['DoctorFavourites.doctor_id' => base64_decode($id),'DoctorFavourites.patient_id' => $this->Auth->user('id')])
                    ->first();
        $userId = $this->Auth->user('id');
        if (!$userId) {
            $userId = 'null';
        }
        $id = base64_decode($id);
        try {
            $this->loadModel('Users');
            $this->Users->hasOne('Durations', [
                'foreignKey' => 'user_id'
            ]);
            $doctor = $this->Users->find()
                ->where([
                    'Users.id' => $id,
                    'Users.role_id' => Configure::read('UserRoles.Doctor')
                ])
                ->contain([
                    'UserSpecialities' => [
                        'Specialities' => [
                            'fields' => [
                                'id',
                                'title_eng'
                            ]
                        ]
                    ],
                    'DoctorAvailability' => [
                        'fields' => [
                            'available_from',
                            'available_upto',
                            'full_day_off',
                            'weekday',
                            'user_id',
                        ]
                    ],
                    'DoctorUnavailability' => function($q) {
                        return $q->where(['DoctorUnavailability.unavailable_from >=' => new \Datetime()]);
                    }, 
                    'UserAwards' => [
                        'fields' => [
                            'user_id',
                            'name'
                        ]
                    ],
                    'UserEducations' => [
                        'fields' => [
                            'id',
                            'user_id',
                            'degree',
                            'university'
                        ]
                    ],
                    'UserLanguages' => [
                        'Languages' => [
                            'fields' => [
                                'id',
                                'name'
                            ]
                        ]
                    ],
                    'UserHospitalAffiliations' => [
                        'fields' => [
                            'user_id',
                            'name'
                        ]
                    ],
                    'UserInsurances' => [
                        'Insurances' => [
                            'fields' => [
                                'id',
                                'title_eng'
                            ]
                        ]
                    ],
                    'UserProfiles',
                    'Durations'
            ])
            ->formatResults(function ($results) {
              return $results->map(function ($row){
                    foreach ($row->doctor_availability as $key => $value) {
                        $span = 30;
                        if(!empty($row->duration)) {
                            $span = $row->duration->duration;
                        }
                        $range = [];
                        $begin = new \DateTime($value->available_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->available_upto->format("Y-m-d H:i:s"));
                        $timespan = $span*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($range, $new);
                        }
                        $row->doctor_availability[$key]->available_slots = $range;
                        //pr($row);
                    }

                    foreach ($row->doctor_unavailability as $key => $value) {
                        $ranges = [];
                        $begin = new \DateTime($value->unavailable_from->format("Y-m-d H:i:s"));
                        $end = new \DateTime($value->unavailable_upto->format("Y-m-d H:i:s"));
                        $timespan = $row->duration->duration*60;
                        $interval = new \DateInterval('PT'.$timespan.'S');
                        $dateRange = new \DatePeriod($begin, $interval, $end);
                        foreach ($dateRange as $date) {
                            $new = [];
                            $new['timing'] = $date->format('H:i');
                            array_push($ranges, $new);
                        }
                        $row->doctor_unavailability[$key]->unavailable_slots = $ranges;
                    }
                    return $row;
                });
            })
            ->first();
        } catch (RecordNotFoundException $e) {
            $this->Flash->error(__('Invalid request'));
            return $this->redirect('/');
        }
        // $days = $this->__getDoctorAvailabilities($id);
        // $unavailableDays = $this->getUnavailableTimes($id, $days);
        $this->loadModel('Insurances');
        $insuranceOptions = $this->Insurances->find('list',[
            'keyField' => 'id',
            'valueField' => 'title_eng'
        ]);

        $this->loadModel('IllnessReasons');  
        $field_name = (empty($this->getRequest()->getSession()->read('Config.locale')) || $this->getRequest()->getSession()->read('Config.locale') == 'en_DE') ? 'title_german' : 'title_eng';      
        $illnessReason = $this->IllnessReasons->find('list',[
            'keyField' => 'id',
            'valueField' => $field_name
        ]);
        

        $this->loadModel('Bookings');    
        $this->loadModel('Bookings');
        $booking = $this->Bookings->find('list',[
                    'keyField' => 'id',
                    'valueField' => 'doctor_id'
                ])
                ->where(['Bookings.patient_id' => $this->Auth->user('id')])
                ->group('Bookings.doctor_id')
                ->toArray();    
        $patients = $this->Bookings->findByPatientId($userId)->where(['who_will_seeing' => 2]);

        $userPatients = [
            ['value' => 1, 'id' => 'me', 'text' => __('Me'), 'onclick' => "showHideDetails('me')" , "checked" => "checked"],
            ['value' => 2, 'id' => 'smelse', 'text' => __('Someone Else'), 'onclick' => "showHideDetails('smelse')"]
        ];

        
        $this->loadModel('DoctorImages');
        $images = $this->DoctorImages->findByUserId($id)->limit(5);

        $this->loadModel('SpamPatients');
        $is_spammer = $this->SpamPatients->exists(['user_id' => $userId, 'doctor_id' => $id]);

        $this->set(compact('illnessReason','insuranceOptions', 'doctor', 'is_spammer'));
        
        $this->set(compact('favourite','rating', 'booking','patients','images', 'days', 'userPatients', 'userPatients','userId'));
    }

    protected function __getDoctorAvailabilities($doctorId, $dayofweek, $bookingDate) {
        $this->loadComponent('User');
        $this->loadModel('DoctorAvailability');
        $availableSlots = [];
        $availabilities = $this->DoctorAvailability
            ->findByUserId($doctorId)
            ->where(['DoctorAvailability.weekday' => $dayofweek])
            ->first();
        

        if (empty($availabilities)) {
            $message = __('Oops! This Doctor has no Availabile time slots set. Please try again after sometime.');
            throw new RecordNotFoundException($message);
        }

        $timeFrom = $availabilities->available_from->format('H:i:s');
        $timeTo = $availabilities->available_upto->format('H:i:s');

        $availableTimeSlot = $this->User->getTimes(
                $timeFrom, 
                $timeTo, 
                10
            );
        $collection = new Collection($availableTimeSlot);
        $new = $collection->map(function ($value, $key) use ($bookingDate){
            return new FrozenTime($bookingDate. $value['timing']);
        });

        $availableSlots = array_merge($availableSlots, $new->toArray());

        return $availableSlots;
    }


    /**
     * This Function will combine bookedtime, unavailabletime 
     */
    public function getUnavailableTimes($doctorId, $bookingDate, $dayofweek){
        $this->loadComponent('User');
        $this->loadModel('DoctorUnavailability');
        $this->loadModel('DoctorAvailability');

        $unavailableSlots=[];
        $docUnavailables = $this->DoctorUnavailability
                        ->findByUserId($doctorId)
                        ->where(['DATE(unavailable_from)'   => $bookingDate]);
        
        foreach ($docUnavailables as $key => $docUnavailable) {
            $unavalableTimeSlots = $this->User->getTimes(
                    $docUnavailable->unavailable_from, 
                    $docUnavailable->unavailable_upto, 
                    10
                );
            $collection = new Collection($unavalableTimeSlots);

            $new = $collection->map(function ($value, $key) use($bookingDate){
                return new FrozenTime($bookingDate. $value['timing']);
            });

            $unavailableSlots = array_merge($unavailableSlots, $new->toArray());
        }

        //ADD FULL DAY OFF
        $fullOfss = $this->DoctorAvailability
            ->findByUserId($doctorId)
            ->where(['DoctorAvailability.weekday' => $dayofweek])
            ->andWhere(['DoctorAvailability.full_day_off' => true])
            ->first();
        
        if (!empty($fullOfss)) {
            $nonTimeSlot = $this->User->getTimes(
                    '00:00:00', 
                    '23:59:59', 
                    10
                );
            $collection = new Collection($nonTimeSlot);
            $new = $collection->map(function ($value, $key) use ($bookingDate){
                return new FrozenTime($bookingDate. $value['timing']);
            });

            $unavailableSlots = array_merge($unavailableSlots, $new->toArray());
        }
        return $unavailableSlots;

    }

    public function doctorTimeOff(){
        $fd = $this->request->getData(); 
        
        $bookingDate = $this->request->getData('booking_date', null);
        $bookingTime = $this->request->getData('booking_time', null);
        $toTimeOff = $this->request->getData('to_time_off', null);
        $duration = $this->request->getData('duration', null);
        if (empty($bookingDate) || empty($bookingTime)  || empty($toTimeOff)  || empty($duration)) {
            $this->Flash->error(__('Unable to update unavailable times. Please try again later.'));
            return $this->redirect($this->referer());
        }
        $fd['unavailable_type'] = 3;
        $fd['unavailable_from'] = new \DateTime($fd['booking_date'].$fd['booking_time']);
        $fd['unavailable_upto'] = new \DateTime($fd['booking_date'].$toTimeOff);
        $fd['user_id'] = $this->Auth->user('id');
        $result = $this->addToUnavailable($fd['unavailable_from'], $fd['unavailable_upto'], $fd['unavailable_type']);
       
        if (!$result) {
            $this->Flash->error(__('Unable to save.Please try again later.'));
            return $this->redirect($this->referer());
        }

        $this->Flash->success(__('Time Off Interval Updated Successfully'));
        return $this->redirect($this->referer());
    }

    public function bookAppointment(){
        $this->loadComponent("Email");
        $this->viewBuilder()->setLayout('frontend');
        $this->loadModel('Bookings');
        $this->loadModel('Users');
        $this->loadModel('Durations');
        $userId = $this->Auth->user('id');
        $user = $this->Users->find()
                        ->where(['Users.id' => $userId])
                        ->first();
        $formData  = $this->request->getData();
        
        $doctor = $this->Users->find()
                ->where(['Users.id' => base64_decode($formData['doctor_id'])])
                ->contain(['UserProfiles'])
                ->first();
        $duration = $this->Durations->find()
                    ->where(['Durations.user_id' => base64_decode($formData['doctor_id'])])
                    ->first();
                    
        if(empty($formData['old_patient']) && $doctor->user_profile->my_patient_only == 1) {
            $this->Flash->error(__('It is not possible to book appointment with this doctor , pls search for another doctor ...'));
                return $this->redirect($this->referer().'&old_patient=true');
        }
        $formData['status'] = 1;        
        $formData['duration'] = (!empty($duration)) ? $duration->duration : 30;
        $formData['patient_first_name'] = ( empty($formData['patient_first_name']) ? $this->Auth->user('user_profile')['first_name']:$formData['patient_first_name'] );
        $formData['patient_last_name'] = ( empty($formData['patient_last_name']) ? $this->Auth->user('user_profile')['last_name']:$formData['patient_last_name'] );
        $formData['patient_dob'] = ( empty($formData['patient_dob'])) ? '' : date('Y-m-d',strtotime($formData['patient_dob'] ));
        $formData['doctor_id'] = base64_decode($formData['doctor_id']);
        $booking_type  = $this->request->getData('booking_type');
        if (!empty($formData['file']['name'])) {
                $file = $formData['file']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('pdf','doc','docx','jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if (in_array($ext, $arr_ext)) {

                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    $savedFileName = $user->uuid.'_' . $file['name'];
                    if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'reports_patients/uploads' . DS . $savedFileName)) {
                        //prepare the filename for database entry
                        $formData['file'] = $user->uuid.'_' . $file['name'];

                    }
                } else {
                    $this->Flash->error(__('Please upload the file with valid Extension(pdf,doc,docx,jpg, jpeg, gif, png)'));
                }
            }
        if (empty($formData)) {
            throw new NotFoundException("Invalid Request");
        }
        

        //CHECK IF SLOT IS ALREADY PAST
        $bookingtime = new Chronos("{$formData['booking_date']}  {$formData['booking_time']}");

        if($bookingtime->isPast()){
            $this->Flash->error(__('Selected booking slot has already past, Please select another slot'));
            return $this->redirect($this->referer());
        };
        
        // AVAILABILITY CHECK
        $dayofweek = date('N', strtotime($formData['booking_date']));
        $availabledays = $this->__getDoctorAvailabilities($formData['doctor_id'], $dayofweek, $formData['booking_date']);
        $unavailableDays = $this->getUnavailableTimes($formData['doctor_id'], $formData['booking_date'], $dayofweek);
        $appointInterval = $this->__getAppointmentInterval(
                $formData['booking_date'], 
                $formData['booking_time'], 
                $formData['duration']
            );
        foreach ($appointInterval as $key => $slot) {
            $existsInUnavailable = in_array($slot, $unavailableDays);
            $existsInAvailable = in_array($slot, $availabledays);
            
            if (!$existsInAvailable || $existsInUnavailable) {
                $this->Flash->error(__('Oops! that slot is Unavailable. Please select another booking slot.'));
                return $this->redirect($this->referer());
            }
        }

        //SPAM CHECK
        $this->loadModel('SpamPatients');
        $is_spammer = $this->SpamPatients->exists(['user_id' => $userId, 'doctor_id' => $formData['doctor_id']]);
        if ($is_spammer) {
            $this->Flash->error(__('Enable to create booking. You are marked as spammer by this doctor. Kindly try again with other doctor.'));
            return $this->redirect($this->referer());
        }

        //SAVING PATIENT BOOKING
        $booking = $this->Bookings->newEntity();
        
        if ($booking->who_will_seeing == 1) {
            $booking->patient_first_name = $this->Auth->user('user_profile.first_name');
            $booking->patient_last_name = $this->Auth->user('user_profile.last_name');
        }
        $booking->patient_id = $userId;
        
        if ($booking_type == 1) {
            $formData['status'] = 2;
            $formData['booking_type'] = 1;
        }
        $booking = $this->Bookings->patchEntity($booking, $formData);
        $saved = $this->Bookings->save($booking);
        $session = $this->request->session();
        $session->delete('Config.SelectedSlot');
        if (!$saved) {
            $errors = $this->_setValidationError($booking->errors());
            $this->Flash->error(__('Enable to create booking. Please Try again.' . $errors));
            return $this->redirect($this->referer());
        }
        if ($booking->booking_type == 1) {
            $this->Flash->success(__('Local Booking Successfully created.'));
            return $this->redirect($this->referer());
        }
        
        $bookingData = $this->Bookings
                            ->findById($saved->id)
                            ->contain(['Users.UserProfiles','Users.UserSpecialities.Specialities','IllnessReasons'])
                            ->first();
        
        //Approve Booking if AutoMatic Mode is SET by DOCTOR 
        $this->loadModel('UserProfiles');
        $doc = $this->UserProfiles->findByUserId($bookingData->doctor_id)->firstOrFail();
        if ($doc->mode) {
            $this->updateBooking($bookingData->id);
        }
        $this->Email->sendPendingBookingEmail($bookingData, $this->Auth->user('email'));
        $this->set('bookingData', $bookingData);
    }

   

    public function rescheduleAppointment($id=null){
        $userId = $this->Auth->user('id');
        $booking_date  =$this->request->getData('booking_date', null);
        $booking_time  =$this->request->getData('booking_time', null);
        if (is_null($id)) {
            $response = [
                'error' => true,
                'message' => __('Invalid Request.')
            ];
        }
        $this->loadModel("Bookings");
        $data = $this->Bookings->findByIdAndDoctorId($id, $userId)->first();
        
       
        if (empty($data)) {
            $response = [
                'error' => true,
                'message' => __('Record not found. Please try again with correct data.')
            ];
        }

        $data->booking_date = $this->request->data['booking_date'];
        $data->booking_time = $this->request->data['booking_time'];
        $data->duration = $this->request->data['duration'];
        $data->status = 2;
        if (!$saved = $this->Bookings->save($data)) {
            $response = [
                'error' => true,
                'message' => __('Unable to save. Please try again later.')
            ];            
        }
        $bookingData = $this->Bookings
                        ->findByPatientId($data->patient_id)
                        ->contain([
                            'Users' => [
                                'UserProfiles' => ['States', 'Countries'],
                                "UserSpecialities"  => function($q){
                                    return $q->contain(['Specialities'])->select([
                                        'UserSpecialities.id',
                                        'UserSpecialities.user_id',
                                        'Specialities.title_eng',
                                        ]);
                                }                            
                            ],
                            'IllnessReasons', 
                            'Insurances', 
                            "Patient",
                        ])
                        ->select([
                            'Bookings.id', 'Bookings.doctor_id', 'Bookings.patient_id', 'Bookings.doctor_note',
                            'Bookings.booking_date', 'Bookings.booking_time', 'Bookings.duration', 
                            'Bookings.illness_reason_id', 'Bookings.patient_first_name', 
                            'Bookings.patient_last_name', 'Bookings.patient_dob','Bookings.patient_sex',
                            'Bookings.patient_email', 'Bookings.insurance_id', 'Bookings.reason', 'Bookings.status', 
                            'Bookings.who_will_seeing', 
                            
                            'Insurances.title_eng',
                            
                            'Patient.id', 'Patient.email',

                            'IllnessReasons.title_eng', 'Users.id', 'Users.email', 
                            'States.name',
                            'Countries.name',
                            'UserProfiles.first_name', 'UserProfiles.last_name', 'UserProfiles.state_id', 
                            'UserProfiles.country_id', 
                            
                            ])
                            ->first();

        $this->loadComponent('Email');
        $this->Email->sendRescheduleBookingNotification($bookingData);
        $this->Phone->sendSmsToDoctorRescheduleBooking($bookingData->id);
        // $this->__sendRescheduleBookingNotification($bookingData);
        $this->request->session()->write('Config.booking_id',[$this->Auth->user('id') => $id]);
        $url =  Router::url('/',true);
        $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        $client_redirect_url = Router::url(array(
               'controller' => 'Doctors',
               'action' => 'updateCalendar',
               'prefix' => 'api'
            ), true); 
        $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($client_redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=online';

        $response = [
                            'error' => false,
                            'message' => __('Appointment Successfully Rescheduled.'),
                            'url' => $url,
                            'client_id' => $client_id,
                            'client_redirect_url' => $client_redirect_url,
                            'login_url' => $login_url
                    ];
        echo  json_encode($response);die;
    }

    public function updateBooking($bookingId){
        $this->loadComponent("Email");

        $this->loadModel('Bookings');
        $userId = $this->Auth->user('id');
        $saved = false;
        $status = 2;

        if(is_null($status)){
            $response = [
                'error' => true,
                'message' => __('Status Parameter not found.')
            ];            
        }

        $booking = $this->Bookings->findById($bookingId)->first();
        $booking->status = $status;

        if ($status == 2) {
            //DATE PAST CHECK
            $bookingtime = new Chronos("{$booking->booking_date} {$booking->booking_time->format('H:i')}");
            if($bookingtime->isPast()){
                $response = [
                    'error' => true,
                    'message' => __('Selected booking slot has already past, Please select another slot')
                ];
            };

            //SLOT AVAILABILITY CHECK
            $dayofweek = date('N', strtotime($booking->booking_date->format("Y-m-d")));
            
            $availabledays = $this->__getDoctorAvailabilities($booking->doctor_id, $dayofweek, $booking->booking_date->format("Y-m-d"));
            
            $unavailableDays = $this->getUnavailableTimes($booking->doctor_id, $booking->booking_date->format("Y-m-d"), $dayofweek);
            $appointInterval = $this->__getAppointmentInterval(
                    $booking->booking_date->format("Y-m-d"), 
                    $booking->booking_time->format("H:i"), 
                    $booking->duration
                );
        
            foreach ($appointInterval as $key => $slot) {
                $existsInUnavailable = in_array($slot, $unavailableDays);

                $existsInAvailable = in_array($slot, $availabledays);
                
                if (!$existsInAvailable || $existsInUnavailable) {
                    $response = [
                        'error' => true,
                        'message' => __('Oops! that slot is Unavailable. Please select another booking slot.')
                    ];
                }
            }
        }


        $saved = $this->Bookings->save($booking);        
        if (!$saved) {
            $response = [
                'error' => true,
                'message' => __('Booking could not get confirmed.')
            ];
        }
        
        if ($status == 2) {

            //ADDING TO UNAVAILABLE TIME
            $unavailable_from = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            $unavailable_upto = new \DateTime($booking->booking_date->format('Y-m-d').$booking->booking_time->format('H:i'));
            
            $unavailable_upto = $unavailable_upto->modify("$booking->duration minute");
            $result = $this->addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type = 1, $userId=$booking->doctor_id);

            if (!$result) {
                $response = [
                    'error' => true,
                    'message' => __('Unable to mark this slot unavailable. Please contact Adminstrator')
                ];
            }
            
            $this->Phone->sendSmsToDoctorConfirmBooking($saved->id);
            $this->request->session()->write('Config.booking_id',[$this->Auth->user('id') => $booking->id]);
            $url =  Router::url('/',true);
            // $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
            // $client_redirect_url = Router::url(array(
            //        'controller' => 'Doctors',
            //        'action' => 'updateCalendar',
            //        'prefix' => 'api'
            //     ), true); 
            // $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($client_redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=online';

            // $response = [
            //                     'error' => false,
            //                     'message' => __('Booking Confirmed.'),
            //                     'url' => $url,
            //                     'client_id' => $client_id,
            //                     'client_redirect_url' => $client_redirect_url,
            //                     'login_url' => $login_url
            //             ];
        }
        if ($status == 3) {
            $this->Phone->sendCancelBookingByDoctorSms($booking->id);
            $this->Flash->success(__('Booking Declined'));
            return $this->redirect($this->referer());
            // $response = [
            //     'error' => true,
            //     'message' => __('Booking Declined.')
            // ];
            
        }
        $this->Email->sendConfirmBookingEmail2($bookingId, $status);
        $this->Flash->success(__('Appointment Successfully Rescheduled.'));
        return $this->Redirect($this->referer());
        // if ($this->request->is('ajax')) {
        //     echo  json_encode($response);die;
        // }
        // return true;
    }

    public function makeFavourite() {
        $this->loadModel('DoctorFavourites');

        if ($this->request->is('post')) {
            $docId = $this->request->getData('doctor_id', null);
            $patId = $this->request->getData('patient_id', null);
            if (empty($docId) && empty($patId)) {
                $this->Flash->error(__('Invalid Request. Please try again later.'));
                return $this->redirect($this->referer());
            }
            $favourite = $this->DoctorFavourites->findByDoctorIdAndPatientId($docId, $patId)->first();
            
            if (empty($favourite)) {
                $favourite = $this->DoctorFavourites->newEntity();
            }

            $favourite = $this->DoctorFavourites->patchEntity($favourite, $this->request->getData());
            if (!$this->DoctorFavourites->save($favourite)) {
                $this->Flash->error(__('Invalid Request. Please try again later.'));                
                return $this->redirect($this->referer());
            }
            $this->Flash->success(__('Doctor has been made favourite'));
            return $this->redirect($this->referer());

        }
        $this->Flash->error(__('Invalid Request. Please try again later.'));
        return $this->redirect($this->referer());
    }

    public function login() {
        if ($this->request->is('post')) {

            // Auth component identify if sent user data belongs to a user
            $user = $this->Auth->identify();
            if (!$user['is_email_verified']) {
                $this->Flash->error(__('Your email is not verified. Please verify your email by clicking on the link sent to your email address while signup'));
                return $this->redirect($this->referer());
            }

            if ($user) {
                $selectedTime = $this->request->getData('selectedTime');
                $selectedDate = $this->request->getData('selectedDate');
                $doctorId = $this->request->getData('doctorId');
                $session = $this->request->session();
                $session->write('Config.SelectedSlot', array(
                    'selectedTime' => $selectedTime,
                    'openDialog' => true,
                    'selectedDate' => $selectedDate,
                    'doctorId' => $doctorId
                ));
                
                $this->Auth->setUser($user);
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Invalid username or password, try again.'));
        }

    }


    public function doctorCreateBooking(){
        $formData = $this->request->getData();

        $formData['status'] = 2; //SET IT AS CONFIRMED

        $formData['doctor_id'] = $this->Auth->user('id');
        if (is_null($formData)) {
            $this->Flash->error(__('Form data missing'));
            return $this->redirect($this->referer());
        }
        if (empty($formData['booking_date'])) {
            $this->Flash->error(__('Booking Date cannot be empty'));
            return $this->redirect($this->referer());
        }
        
        $btime = Time::createFromFormat(
	    'd.m.Y',
	    $formData['booking_date']
	);
	
	$formData['booking_date'] = $btime->format('Y-m-d');

        if (empty($formData['booking_time'])) {
            $response = [
                            'error' => true,
                            'message' => __('Booking Time cannot be empty')
                        ];
        }
        $dayofweek = date('w', strtotime($formData['booking_date']));

        $this->loadModel('Bookings');
        $booking = $this->Bookings->newEntity();
        $booking->booking_type = 1; //BOOKED BY DOCTOR

        if (!empty($formData['patient_id'])) {
            $this->loadModel('UserProfiles');
            $patientDetail = $this->UserProfiles->findByUserId($formData['patient_id'])->firstOrFail();
            if (!empty($patientDetail)) {   
                $formData['patient_first_name']  = $patientDetail->first_name;
                $formData['patient_last_name']  = $patientDetail->last_name;
            }
        }
        //DATE PAST CHECK
        $bookingtime = new Chronos("{$formData['booking_date']}  {$formData['booking_time']}");

        if($bookingtime->isPast()){
            $response = [
                            'error' => true,
                            'message' => __('Selected booking slot has already past, Please select another slot')
                        ];
        };

        //SLOT AVAILABILITY CHECK
        $availabledays = $this->__getDoctorAvailabilities($this->Auth->user('id'), $dayofweek, $formData['booking_date']);
        $unavailableDays = $this->getUnavailableTimes($this->Auth->user('id'), $formData['booking_date'], $dayofweek);
        $appointInterval = $this->__getAppointmentInterval(
                $formData['booking_date'], 
                $formData['booking_time'], 
                $formData['duration']
            );
        foreach ($appointInterval as $key => $slot) {
            $existsInUnavailable = in_array($slot, $unavailableDays);

            $existsInAvailable = in_array($slot, $availabledays);
            
            if (!$existsInAvailable || $existsInUnavailable) {
                $this->Flash->error(__('Oops! that slot is Unavailable. Please select another booking slot.'));
                return $this->redirect($this->referer());
                // $response = [
                //             'error' => true,
                //             'message' => __('Oops! that slot is Unavailable. Please select another booking slot.')
                //         ];
            }
        }
        
        //SAVING BOOKING
        $booking = $this->Bookings->patchEntity($booking, $formData);
        
        if (!$this->Bookings->save($booking)) {
            $response = [
                            'error' => true,
                            'message' => __('Unable to save.Please try again later.')
                        ];
        }

        

        $this->request->session()->write('Config.booking_id',[$this->Auth->user('id') => $booking->id]);

        $unavailable_type = 1;
        $unavailable_from = new \DateTime( $formData['booking_date'].$formData['booking_time']);
        $unavailable_upto = new \DateTime( $formData['booking_date'].$formData['booking_time']);
        $unavailable_upto = $unavailable_upto->modify("$booking->duration minute");
        $unavailable_date = $formData['booking_date'];
        $docId = $formData['doctor_id'];
        $url =  Router::url([
            'controller' => 'Doctors', 
            'action' => 'BookingCalender', 
            '?' => ["on" => base64_encode($formData['booking_date'])]
            ],true);
        // $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        // $client_redirect_url = Router::url(array(
        //        'controller' => 'Doctors',
        //        'action' => 'addEvent',
        //        'prefix' => 'api'
        //     ), true); 
        // $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($client_redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=online';
        $result = $this->addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type, $docId , $unavailable_date);
        $this->Flash->success(__('Booking saved successfully'));
        return $this->redirect($url);
        // $response = [
        //                     'error' => false,
        //                     'message' => __('Booking saved Successfully.'),
        //                     'url' => $url,
        //                     'client_id' => $client_id,
        //                     'client_redirect_url' => $client_redirect_url,
        //                     'login_url' => $login_url
        //             ];
        // echo  json_encode($response);die;
        
    }

    private function __getAppointmentInterval($bookingDate, $bookingTime, $duration){
        $bookingStartTime = new FrozenTime($bookingDate .' ' .$bookingTime);
        $bookingEndTime = new \DateTime($bookingDate .' ' .$bookingTime);
        $bookingEndTime->add(new \DateInterval('PT' . $duration . 'M'));

        $getTimes = $this->User->getTimes(
            $bookingStartTime->format('H:i'), 
            $bookingEndTime->format('H:i'), 
            $duration
        );
        $collection = new Collection($getTimes);

        $new = $collection->map(function ($value, $key) use($bookingDate) {
            return new FrozenTime($bookingDate.$value['timing']);
        });

        return $new->toArray();
    }

    public function manageDayOff(){
        $formData = $this->request->getData();
        //$this->loadModel('DoctorAvailability');

        $userId = $this->Auth->user('id');
        $dayofweek = date('D', strtotime($formData['on_date']));
        // $config = array(
        //     'Mon' => 1,
        //     'Tue' => 2,
        //     'Wed' => 3,
        //     'Thu' => 4,
        //     'Fri' => 5,
        //     'Sat' => 6,
        //     'Sun' => 7
        // );

        // $entity = $this->DoctorAvailability->findByUserIdAndWeekday($userId, $config[$dayofweek])->firstOrFail();
        // $entity->available_from = new Time('00:00');
        // $entity->available_upto = new Time('00:00');
        // if($formData['day_off'] == 0) {
        //  $entity->full_day_off = $formData['day_off'];
        // } else {
        //    $entity->full_day_off = $formData['day_off']; 
        // }

        // $entity = $this->DoctorAvailability->patchEntity($entity, $formData);
        
        // $saved = $this->DoctorAvailability->save($entity);
        // if (!$saved) {
        //     $this->Flash->error(__('Unable to save.Please try again later.'));
        //     return $this->redirect($this->referer());
        // }

        $this->loadModel('DoctorUnavailability');
        $unavailabilityData = array(
            'user_id' => $userId,
            'unavailable_from' => $formData['on_date'] . " 00:00:00",
            'unavailable_upto' => $formData['on_date'] . " 23:59:00",
            'unavailable_type' => 4
        );

        $unavailabilityForDayOff = $this->DoctorUnavailability->find()
                ->where($unavailabilityData)->first();

        if ($formData['day_off'] == 1) {
            // Save the day off 

            if (empty($unavailabilityForDayOff)) {
                $unavailabilityEntity = $this->DoctorUnavailability->newEntity();
                $unavailabilityEntity = $this->DoctorUnavailability->patchEntity($unavailabilityEntity, $unavailabilityData);
                if ($this->DoctorUnavailability->save($unavailabilityEntity)) {
                    $this->Flash->success(__('Dayoff set Successfully.'));
                    return $this->redirect($this->referer());
                }
            }

           
        } else {
           

            if (!empty($unavailabilityForDayOff)) {
                if ($this->DoctorUnavailability->delete($unavailabilityForDayOff)) {
                    $this->Flash->success(__('Day on set Successfully.'));
                    return $this->redirect($this->referer());
                }
            }
        }

        $this->Flash->error(__('Something went wrong!!'));
        return $this->redirect($this->referer());
    }

    public function bookingCalender(){
        $this->loadModel('DoctorAvailability');
        $this->viewBuilder()->setLayout('frontend_new_dashboard');
        $this->loadModel('Durations');
        $duration = $this->Durations->find()
                    ->where(['Durations.user_id' => $this->Auth->user('id')])
                    ->first();
        $userId = $this->Auth->user('id');
        $savedDoctorAvailabilityDay = $this->DoctorAvailability->find('all')
                                            ->where(['user_id' => $userId])
                                            ->toArray();
        
        if (!empty($savedDoctorAvailabilityDay)) {
            $doctorAvailabilityDay = [];
            foreach ($savedDoctorAvailabilityDay as $key => $availabilityDay) {
                $doctorAvailabilityDay[$availabilityDay->weekday] = $availabilityDay;
            }
        }

        $this->viewBuilder()->setLayout('frontend_dashboard');
        
        $this->loadModel('IllnessReasons');
        $this->loadModel('UserProfiles');
        $this->loadModel('Bookings');
        
        $patient = $this->Bookings->find('list',[
                   'keyField' => 'patient_id',
                   'valueField' => 'patient_first_name'
                   ])
                ->where(['Bookings.doctor_id' => $userId])
                ->andWhere(['Bookings.patient_id IS NOT NULL']);
       
        $doctorPatients = array_unique($patient->toArray());
        $illnessReasons = $this->IllnessReasons->find('list', [
            'keyField' => 'id',
            'valueField' => ($this->lng == 'de')? 'title_german':'title_eng'
        ]);
        
        $this->set(compact('doctorPatients','illnessReasons', 'doctorAvailabilityDay','duration'));

    }


    public function doctorCancelBooking(){
        $this->loadComponent('Phone');        
        $bid = $this->request->getData('booking_id', null);
        $reason = $this->request->getData('reason', null);
        if (empty($bid) || empty($reason)) {
            $response = [
                            'error' => true,
                            'message' => __('Unable to update Booking details. Please try again later.')
                        ];
        }
        
        $this->loadModel('Bookings');
        $doctorBooking = $this->Bookings->findById($bid)->first();
        $doctorBooking->reason = $reason;
        $doctorBooking->status = 3;

        if (!$this->Bookings->save($doctorBooking)) {
            $response = [
                            'error' => true,
                            'message' => __('Unable to save.Please try again later.')
                        ];
        }

        $bookingData = $this->Bookings
                            ->findById($bid)
                            ->contain([
                                'Patient' ,'Users' => ['UserProfiles' => ['States', 'Countries']],
                            ])
                            ->first();

        $this->loadComponent('Email');

        $this->Email->sendDoctorCancelBookingEmail($bookingData);
        $this->Phone->sendCancelBookingByDoctorSms($bid);
        $bookingId = $this->request->session()->write('Config.booking_id',[$this->Auth->user('id') => $bid]);
        $url = Router::url('/',true);
        $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
        $client_redirect_url = Router::url(array(
               'controller' => 'Doctors',
               'action' => 'deleteEvent',
               'prefix' => 'api'
            ), true); 
        $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($client_redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=online';
        $response = [
                            'error' => false,
                            'message' => __('Booking cancelled Successfully.'),
                            'url' => $url,
                            'client_id' => $client_id,
                            'client_redirect_url' => $client_redirect_url,
                            'login_url' => $login_url
                    ];
        echo  json_encode($response);die;
    }

    public function addToUnavailable($unavailable_from, $unavailable_upto, $unavailable_type, $docId = null){
        $fd['unavailable_type'] = $unavailable_type;
        $fd['unavailable_from'] = $unavailable_from;
        $fd['unavailable_upto'] = $unavailable_upto;
        $fd['user_id'] = $this->Auth->user('id');
        if (!empty($docId)) {
            $fd['user_id'] = $docId;
        }
        
        $this->loadModel('DoctorUnavailability');
        $docEntity = $this->DoctorUnavailability->newEntity();
        $docEntity = $this->DoctorUnavailability->patchEntity($docEntity, $fd);
        if (!$this->DoctorUnavailability->save($docEntity)) {
            return false;
        }
        return true;
    }

    public function manageWorkingHours() {
        $this->viewBuilder()->setLayout('frontend_dashboard'); 
        $this->loadModel('DoctorAvailability');
        $this->loadModel('Durations');
        $duration = $this->Durations->find()
                    ->where(['Durations.user_id' => $this->Auth->user('id')])
                    ->first();
        $availability = $this->DoctorAvailability->find()
                        ->where(['DoctorAvailability.user_id' => $this->Auth->user('id')]);

        $timings = $this->paginate($availability);
        $this->set(compact('timings','duration'));
    }

    public function addWorkingHours() {
       $this->loadModel('DoctorAvailability');  
       $availability = $this->DoctorAvailability->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $availability = $this->DoctorAvailability->patchEntity($availability, $this->request->data);
            if($this->DoctorAvailability->save($availability)) {
                $this->Flash->success(__('Doctor Availability is added'));
                $this->redirect($this->referer());
            } else {
                $errors = $this->_setValidationError($availability->errors());
                $this->Flash->error(__('Doctor Availability has not been added successfully due to following errors'.$errors));
                $this->redirect($this->referer());
            }
        }
    }

    public function addDuration() {
        $this->loadModel('Durations');
        if ($this->request->is('post')) {
            $duration = $this->Durations->newEntity();
            if(!empty($this->request->data['id'])) {
                $duration = $this->Durations->get($this->request->data['id']);
            } 
            $duration = $this->Durations->patchEntity($duration, $this->request->data);
            if($this->Durations->save($duration)) {
                $this->Flash->success(__('Duration has been added successfully'));
                $this->redirect($this->referer());
            } else {
                $errors = $this->_setValidationError($duration->errors());
                $this->Flash->error(__('Duration has not been added successfully due to following errors'.$errors));
                $this->redirect($this->referer());
            }
        }
    }

    public function reportdownload($id) {
        $this->loadModel('Bookings');
        $file = $this->Bookings->find()
                ->where(['Bookings.id' => $id])
                ->first();
        
        $filePath = WWW_ROOT .'reports_patients'. DS . 'uploads' . DS .$file->file;

        $response = $this->response->withFile($filePath ,
            array('download'=> true, 'name'=> $file->file));

        return $response;   
        
    }
}