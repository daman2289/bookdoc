<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;
use Cake\Filesystem\File;
use Cake\I18n\Time;

class PhoneComponent extends Component
{

    /**
     * getPhoneAttributes method to break phone number and get country code and phone number
     *
     * @param $phoneNumber integer
     * @return array
     */
    protected $_defaultConfig = [];


    public function initialize(array $config) {
        $this->http = new Client([
            'headers' => 
                ['X-Authy-API-Key' => TWILLIO_AUTHY_API_KEY],
                ['Content-Type' => 'application/json' ],
                ['User-Agent' => $this->__getUserAgent() ]
        ]);
    }


    public function getPhoneAttributes($phoneNumber) {
        $countryCode = substr($phoneNumber, 0, 3);
        $countryCode = str_replace('-','',$countryCode);
        $phoneNumber = substr($phoneNumber, 3);
        
        return ['country_code' => $countryCode, 'phone_number' => $phoneNumber];
    }

    private function __getUserAgent()
    {
        return sprintf(
            'AuthyPHP/%s (%s-%s-%s; PHP %s)',
            '3.0.0',
            php_uname('s'),
            php_uname('r'),
            php_uname('m'),
            phpversion()
        );
    }

    public function sendPhoneVerificationCode($phoneNumber) {
        $phoneNumber = $this->getPhoneAttributes($phoneNumber);
        $phone = $phoneNumber['phone_number'];
        $countryCode = $phoneNumber['country_code'];
        
        $response = $this->http->post("https://api.authy.com/protected/json/phones/verification/start", 
                        [
                            'phone_number' => "{$phone}",
                            'country_code' => "{$countryCode}",
                            'via' => "sms",
                            'locale' => 'en'
                        ]
                    );
        
        $message = json_decode($response->body)->message;
        
        //saving logs while sending phone verify code
        $phoneVerifyLogFile = new File(LOGS . 'twillio_phone_verify', true, 0644);

        //getting current time to save in the logs
        $now = Time::now();
        $now->i18nFormat('yyyy-MM-dd HH:mm:ss');
        $messageToLog = PHP_EOL . $now->i18nFormat('yyyy-MM-dd HH:mm:ss') . ' Message: ' . $countryCode . '-' . $phone . ' ' . $message;
        $phoneVerifyLogFile->append($messageToLog);
        $phoneVerifyLogFile->close();

        return $response;
    }

    public function checkPhoneVerificationCode($phoneNumber, $verification_code) {
        $phoneNumber = $this->getPhoneAttributes($phoneNumber);
        $phone = $phoneNumber['phone_number'];
        $countryCode = $phoneNumber['country_code'];

        $response = $this->http->get("https://api.authy.com/protected/json/phones/verification/check", 
                        [
                            'phone_number' => "{$phone}",
                            'country_code' => "{$countryCode}",
                            'verification_code' => "{$verification_code}",
                            'locale' => 'en'
                        ]
                    );

        $message = json_decode($response->body)->message;
        //saving logs while sending phone verify code
        $phoneVerifyLogFile = new File(LOGS . 'twillio_phone_check', true, 0644);

        //getting current time to save in the logs
        $now = Time::now();
        $now->i18nFormat('yyyy-MM-dd HH:mm:ss');

        $messageToLog = PHP_EOL . $now->i18nFormat('yyyy-MM-dd HH:mm:ss') . ' Message: ' . $countryCode . '-' . $phone  . '(' . $verification_code . ')' . ' ' . $message;
        $phoneVerifyLogFile->append($messageToLog);
        $phoneVerifyLogFile->close();

        return $response;
    }



    protected function _sendSmsNotification($phone, $countryCode, $smsBody) {

        $number = $countryCode.$phone;
        $http = new Client();
        $accSID = env('TWILLIO_SID');
        $authToken = env('TWILLIO_AUTH_TOKEN');
        $fromNumber = env('TWILLIO_SEND_SMS_FROM');
        $response = $http->post("https://api.twilio.com/2010-04-01/Accounts/{$accSID}/Messages", [
                        'Body' => "{$smsBody}",
                        'To' => "{$number}",
                        'From' => "{$fromNumber}"
                            ],[
                                'auth' => [
                                    'type' => 'basic',
                                    'username' => "{$accSID}", 
                                    'password' => "{$authToken}"
                                ]
                            ]
                    );
        
        //saving logs while sending phone verify code
        $cronMessageLogFile = new File(LOGS . 'sms_notifications', true, 0644);
        $now = Time::now();
        $now->i18nFormat('yyyy-MM-dd HH:mm:ss');
        $messageToLog = "SMS Sent to {$fromNumber} as {$smsBody} on {$now}";

        // $cronMessageLogFile = new File(LOGS . 'cron_sms_notifications', true, 0644);

        // //getting current time to save in the logs

        // $messageToLog = PHP_EOL . $now->i18nFormat('yyyy-MM-dd HH:mm:ss') . ' Message: ' . $code . '-' . $phone . ' ' . $response->message();
        $cronMessageLogFile->append($messageToLog);
        $cronMessageLogFile->close();
       
        return $response;
    }

    public function sendReminderBookingSms($bookingId){

        // $this->loadModel('SmsTemplates');
        $smsTempTable = TableRegistry::get('SmsTemplates');
        $bookingsTable = TableRegistry::get('Bookings');

        $bookingData = $bookingsTable->get($bookingId);

        $bookingData = $bookingsTable
                        ->findById($bookingId)
                        ->contain([
                            'Patient.UserProfiles', 
                            'Users.UserProfiles.States', 
                            'Users.UserProfiles.Countries',
                            'Users.UserSpecialities.Specialities'
                        ])
                        ->first();

        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $doctorName = $bookingData->user->user_profile->full_name;
        //$docAddress = $bookingData->user->user_profile->address.' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
        $patientPhone = $bookingData->patient->user_profile->phone_number;

        $temp = $smsTempTable->get(1);
        $temp['sms_text'] = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME', '#DOCTOR_NAME', '#DOCTOR_ADDRESS'], 
            [ 
                $weekday, 
                $bookingDate, 
                $bookingTime, 
                $doctorName, 
                $docAddress
            ], 
            $temp['sms_text']
        );
        $countryCode = substr($patientPhone, 0, 3);
        $phoneNumber = substr($patientPhone, 3);  
        $sid = 'AC5822cb98272fa1cb79ca80b7e957b32e';
            $token = '148f1987135cf8c40cab0f48851403fc';
            $client = new \Twilio\Rest\Client($sid, $token);
            try {

            $client->messages->create(
                   $patientPhone,
                   array(
                       'from' => '+43676800555228',
                       'body' => $temp['sms_text']
                   )
               );
            }catch (\Twilio\Exceptions\RestException $e) {
               return false;
            }
        return true;
        //return $this->_sendSmsNotification($phoneNumber, $countryCode, $temp['sms_text']);
    }

    public function sendCancelBookingByDoctorSms($bookingId){

        $smsTempTable = TableRegistry::get('SmsTemplates');
        $bookingsTable = TableRegistry::get('Bookings');

        $bookingData = $bookingsTable->get($bookingId);

        $bookingData = $bookingsTable
                        ->findById($bookingId)
                        ->contain([
                            'Patient.UserProfiles', 
                            'Users.UserProfiles.States', 
                            'Users.UserProfiles.Countries',
                            'Users.UserSpecialities.Specialities'
                        ])
                        ->first();
                            // pr($bookingData->toArray());
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $doctorName = $bookingData->user->user_profile->full_name;
        $docAddress = $bookingData->user->user_profile->address.' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
        $patientPhone = ($bookingData->patient)?$bookingData->patient->user_profile->phone_number: null;
        if (is_null($patientPhone)) {
            return;
        }

        $temp = $smsTempTable->get(2);
        $temp['sms_text'] = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME', '#DOCTOR_NAME', '#DOCTOR_ADDRESS'], 
            [ 
                $weekday,
                $bookingDate, 
                $bookingTime, 
                $doctorName, 
                $docAddress
            ], 
            $temp['sms_text']
        );
        $countryCode = substr($patientPhone, 0, 3);
        $phoneNumber = substr($patientPhone, 3);         
        $sid = 'AC5822cb98272fa1cb79ca80b7e957b32e';
            $token = '148f1987135cf8c40cab0f48851403fc';
            $client = new \Twilio\Rest\Client($sid, $token);
            try {

            $client->messages->create(
                   $patientPhone,
                   array(
                       'from' => '+43676800555228',
                       'body' => $temp['sms_text']
                   )
               );
            }catch (\Twilio\Exceptions\RestException $e) {
               return false;
            }
        return true;
        //return $this->_sendSmsNotification($phoneNumber, $countryCode, $temp['sms_text']);
    }

    public function sendCancelBookingByPatinetSms($bookingId){

        
        $smsTempTable = TableRegistry::get('SmsTemplates');
        $bookingsTable = TableRegistry::get('Bookings');

        $bookingData = $bookingsTable->get($bookingId);

        $bookingData = $bookingsTable
                        ->findById($bookingId)
                        ->contain([
                            'Patient.UserProfiles', 
                            'Users.UserProfiles.States', 
                            'Users.UserProfiles.Countries',
                            'Users.UserSpecialities.Specialities'
                        ])->first();

        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $doctorName = $bookingData->user->user_profile->full_name;
        $docAddress = $bookingData->user->user_profile->address.' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
        $patientPhone = $bookingData->patient->user_profile->phone_number;

        $temp = $smsTempTable->get(3);
        $temp['sms_text'] = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'], 
            [ 
                $weekday,
                $bookingDate, 
                $bookingTime, 
            ], 
            $temp['sms_text']
        );
        $countryCode = substr($patientPhone, 0, 3);
        $phoneNumber = substr($patientPhone, 3);         
        $sid = 'AC5822cb98272fa1cb79ca80b7e957b32e';
            $token = '148f1987135cf8c40cab0f48851403fc';
            $client = new \Twilio\Rest\Client($sid, $token);
            try {

            $client->messages->create(
                   $patientPhone,
                   array(
                       'from' => '+43676800555228',
                       'body' => $temp['sms_text']
                   )
               );
            }catch (\Twilio\Exceptions\RestException $e) {
               return false;
            }
        return true;
        //return $this->_sendSmsNotification($phoneNumber, $countryCode, $temp['sms_text']);
    }

    public function sendSmsToDoctorConfirmBooking($bookingId) {
        $bookingsTable = TableRegistry::get('Bookings');
        $bookingData = $bookingsTable
                            ->findById($bookingId)
                            ->contain([
                                'Patient.UserProfiles', 
                                'Users.UserProfiles.States', 
                                'Users.UserProfiles.Countries',
                                'Users.UserSpecialities.Specialities'
                            ])->first();

            $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
            $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
            $doctorName = $bookingData->user->user_profile->full_name;
            $docAddress = $bookingData->user->user_profile->address.' ';
            $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
            $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
            $patientPhone = $bookingData->user->user_profile->phone_number;

            

            $smsTempTable = TableRegistry::get('SmsTemplates');
            $temp = $smsTempTable->get(4);
            $temp['sms_text'] = str_replace(
                ['#DATE', '#TIME','#DOCNAME','#ADDRESS'], 
                [ 
                    $bookingDate,
                    $bookingTime, 
                    $doctorName, 
                    $docAddress
                ], 
                $temp['sms_text']
            );
            $countryCode = substr($patientPhone, 0, 3);
            $phoneNumber = substr($patientPhone, 3);      
            $sid = 'AC5822cb98272fa1cb79ca80b7e957b32e';
            $token = '148f1987135cf8c40cab0f48851403fc';
            $client = new \Twilio\Rest\Client($sid, $token);
            try {

            $client->messages->create(
                   $patientPhone,
                   array(
                       'from' => '+43676800555228',
                       'body' => $temp['sms_text']
                   )
               );
            }catch (\Twilio\Exceptions\RestException $e) {
               return false;
            }
            return true;
            //return $this->_sendSmsNotification($phoneNumber,$countryCode, $temp['sms_text']);
    }

    public function sendSmsToDoctorRescheduleBooking($bookingId) {
        $bookingsTable = TableRegistry::get('Bookings');
        $bookingData = $bookingsTable
                            ->findById($bookingId)
                            ->contain([
                                'Patient.UserProfiles', 
                                'Users.UserProfiles.States', 
                                'Users.UserProfiles.Countries',
                                'Users.UserSpecialities.Specialities'
                            ])->first();

            $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
            $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
            $doctorName = $bookingData->user->user_profile->full_name;
            $docAddress = $bookingData->user->user_profile->address.' ';
            $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
            $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
            $patientPhone = $bookingData->user->user_profile->phone_number;

            

            $smsTempTable = TableRegistry::get('SmsTemplates');
            $temp = $smsTempTable->get(4);
            $temp['sms_text'] = str_replace(
                ['#DATE', '#TIME','#DOCNAME','#ADDRESS'], 
                [ 
                    $bookingDate,
                    $bookingTime, 
                    $doctorName, 
                    $docAddress
                ], 
                $temp['sms_text']
            );
            $countryCode = substr($patientPhone, 0, 3);
            $phoneNumber = substr($patientPhone, 3);      
            $sid = 'AC5822cb98272fa1cb79ca80b7e957b32e';
            $token = '148f1987135cf8c40cab0f48851403fc';
            $client = new \Twilio\Rest\Client($sid, $token);
            try {

            $client->messages->create(
                   $patientPhone,
                   array(
                       'from' => '+43676800555228',
                       'body' => $temp['sms_text']
                   )
               );
            }catch (\Twilio\Exceptions\RestException $e) {
               return false;
            }
            return true;
            //return $this->_sendSmsNotification($phoneNumber,$countryCode, $temp['sms_text']);
    }
}