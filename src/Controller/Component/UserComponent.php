<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Utility\Hash;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class UserComponent extends Component
{

    public function resetUserBioFieldsToSave() {

        //resetting user specialities data to save
        if (null != $this->request->data('user_specialities.speciality_id')) {
            $selecedSpecialities = $this->request->data('user_specialities.speciality_id');
            $specialitiesToSave = [];
            foreach ($selecedSpecialities as $key => $value) {
                $specialitiesToSave[] = [
                    'speciality_id' => $value
                ];
            }
            $this->request->data('user_specialities' , $specialitiesToSave);
        }

        //resetting user insurance data to save
        if (null != $this->request->data('user_insurances.insurance_id')) {
            $selecedInsurances = $this->request->data('user_insurances.insurance_id');
            $insurancesToSave = [];
            foreach ($selecedInsurances as $key => $value) {
                $insurancesToSave[] = [
                    'insurance_id' => $value
                ];
            }
            $this->request->data('user_insurances' , $insurancesToSave);
        }

        //resetting user language data to save
        if (null != $this->request->data('user_languages.language_id')) {
            $selecedLanguages = $this->request->data('user_languages.language_id');
            $languagesToSave = [];
            foreach ($selecedLanguages as $key => $value) {
                $languagesToSave[] = [
                    'language_id' => $value
                ];
            }
            $this->request->data('user_languages' , $languagesToSave);
        }

        //resetting user education data to save
        if (null != $this->request->data('degree')) {
            $selectedDegrees = $this->request->data('degree');
            $selectedUniversities = $this->request->data('university');
            $educationInfoToSave = [];
            foreach ($selectedDegrees as $key => $value) {
                $educationInfoToSave[] = [
                    'degree' => $value,
                    'university' => $selectedUniversities[$key]
                ];
            }

            $this->request->data('user_educations' , $educationInfoToSave);
            unset($this->request->data['degree']);
            unset($this->request->data['university']);
        }

        //resetting user hospital affiliations data to save
        if (null != $this->request->data('hospital_affiliation')) {
            $selectedHospitals = $this->request->data('hospital_affiliation');
            $hospitalsToSave = [];
            foreach ($selectedHospitals as $key => $value) {
                $hospitalsToSave[] = [
                    'name' => $value
                ];
            }

            $this->request->data('user_hospital_affiliations' , $hospitalsToSave);
            unset($this->request->data['hospital_affiliation']);
        }

        //resetting user awards data to save
        if (null != $this->request->data('award')) {
            $selectedAwards = $this->request->data('award');
            $awardsToSave = [];
            foreach ($selectedAwards as $key => $value) {
                $awardsToSave[] = [
                    'name' => $value
                ];
            }

            $this->request->data('user_awards' , $awardsToSave);
            unset($this->request->data['award']);
        }

        return true;
    }

    /**
     * method resetUserAvailabilityFieldsTosave to reset the availability form field before save
     *
     * @param $userId int id of the save
     * @return array
     */
    public function resetUserAvailabilityFieldsTosave($userId) {
        $daysToDelete = [];
        $this->doctorBlockTime = TableRegistry::get('DoctorBlockTimings');
        $data = $this->request->getData();
        if (isset($data)) {
            foreach ( $data as $weekday => $availability) {
                if (isset($availability['timing'])) {
                    $this->saveBlockTimings($weekday, $availability['timing'], $userId);
                }
                //removing empty value from weekdays array to not save them
                if (empty($availability['weekday'])) {

                    //getting uncheked days id to delete from db
                    if (!empty($availability['id'])) {
                        $daysToDelete[] = $availability['id'];
                    }

                    unset($this->request->data[$weekday]);
                    continue;
                }
                $this->request->data[$weekday]['user_id'] = $userId;

                // $availabletime = $availability->doctor_availability_timings;
                if ($availability['my_doctor_availability_timings']['starttime'] && $availability['my_doctor_availability_timings']['endtime']) {
                    $availableTimes = $this->getTimes(
                        $availability['my_doctor_availability_timings']['starttime'], 
                        $availability['my_doctor_availability_timings']['endtime'], 
                        '30');
                    $this->request->data[$weekday]['doctor_availability_timings'] = $availableTimes;
                }
                // $availability['doctor_availability_timings'] = ['d'];
                // foreach ($availability->doctor_availability_timings as $weekday => $timings) {
                //     $this->request->data[$weekday]['udser_id'] = $userId;
                // }

                //removing empty value from timings array to not save them
                // foreach ($availability['doctor_availability_timings'] as $keyTiming => $valueTiming) {
                //     if (empty($valueTiming['timing'])) {
                //         unset($this->request->data[$weekday]['doctor_availability_timings'][$keyTiming]);
                //     }
                // }

            }
        }
        // pr($this->request->getData()); die;
        //return below array with ids if any records need to be deleted when user uncheck it.
        return $daysToDelete;
    }

    public function getTimes($startTime, $endTime, $duration){
        $begin = new \DateTime($startTime);
        $end = new \DateTime($endTime);
        $timespan = $duration*60;
        $interval = new \DateInterval('PT'.$timespan.'S');
        $dateRange = new \DatePeriod($begin, $interval, $end);
        $range = [];
        foreach ($dateRange as $date) {
            $new = [];
            $new['timing'] = $date->format('H:i');
            array_push($range, $new);
        }
        return $range;
    }

    public function saveBlockTimings($weekday, $timings, $userId){        
        $docBlockTime = $this->doctorBlockTime->findByUserIdAndWeekday($userId, $weekday);
        if (!is_null($docBlockTime->toArray())){
            $this->doctorBlockTime->deleteAll(['user_id' => $userId, 'weekday' => $weekday]);
        }
        foreach ($timings as $time) {
           
            $docBlockTime = $this->doctorBlockTime->newEntity();
            $docBlockTime->user_id = $userId;
            $docBlockTime->weekday = $weekday;
            $docBlockTime->timing = $time;
            $save = $this->doctorBlockTime->save($docBlockTime);
        }
    }

    public function checkAndGetImage($imageName) {
        $defaultPic = Configure::read('UserDummyImage');
        if (!empty($imageName)) {
            $file = new File(Configure::read('UserProfilePicPathToCheck') . $imageName);
            if ($file->exists()) {
                return Router::url(Configure::read('UserProfilePicPathToShow') . $file->name, true);
                
            }
        }
        return Router::url($defaultPic, true);
    }
}