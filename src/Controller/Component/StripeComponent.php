<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use \Stripe as Stripe;

/**
 * Stripe component
 */
class StripeComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize(array $config) {
        Stripe\Stripe::setApiKey(env('STRIPE_KEY'));
    }

	public function createCustomer($email) {
		$customer = Stripe\Customer::create(array(
		  "email" => $email,
		));
		return $customer->id;
	}

	public function subscribe($customerId, $stripePlanId) {
		$subscription = Stripe\Subscription::create(array(
		  "customer" => $customerId,
		  "plan" => $stripePlanId,
		));
		return $subscription;
	}

	public function createSource($iban, $name){
		$source = \Stripe\Source::create([
			"type" => "sepa_debit",
			"sepa_debit" => ["iban" => $iban],
			"currency" => "eur",
			"owner" => ["name" => $name,],
		]);
	}

	public function createSofortSource($amount, $returnUrl){
		$source = \Stripe\Source::create([
			"type" => "sofort",
			"amount" => $amount,
			"currency" => "eur",
			"redirect" => [
				"return_url" => $returnUrl,
			],
			"sofort" => [
				"country" => 'au',
				"preferred_language" => "en"
			],
		]);
	}

	public function addCard($customerId, $stripeToken, $cardId = null) {
		if ($cardId != null) {
			try {
				$customer = \Stripe\Customer::retrieve($customerId);
				$customer->sources->retrieve($cardId)->delete();
			} catch (\Exception $e) {

			}
		}
		$customer = \Stripe\Customer::retrieve($customerId);
		$response = $customer->sources->create(array("source" => $stripeToken));
		return $response;
	}

	// public function getCard($customerId, $cardId) {
	// 	try {
	// 		$customer = \Stripe\Customer::retrieve($customerId);
	// 		$card = $customer->sources->retrieve($cardId);
	// 		return $card->last4;
	// 	} catch (\Exception $e) {
	// 		return false;
	// 	}
	// }

	public function cancelSubscription($subscriptionId) {
		try {
			$sub = \Stripe\Subscription::retrieve($subscriptionId);
			$sub->cancel();
		} catch (\Exception $e) {
			return false;
		}
	}

	public function updateSubscription($subscriptionId, $planId) {
		try {
			
			$subscription = \Stripe\Subscription::retrieve($subscriptionId);
			$subscription->plan = $planId;
			$subscription->save();

			return $subscription;
		} catch(\Stripe\Error\Card $e) {
			return $e->getMessage();
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			return __('Please add card details before upgrading your plan.');
		} catch (\Exception $e) {
			// Something else happened, completely unrelated to Stripe
			return __('Sorry! We are unable to process your request at this moment. Kindly try again later.');
		}
	}


	public function createStripeCharges($customerId, $amount, $currency='eur') {
		$charge = \Stripe\Charge::create(array(
			"amount" => $amount,
			"currency" => $currency,
			"customer" => $customerId,
		));
		return $charge;

	}

	public function getCards($customerId){
		$cards = \Stripe\Customer::retrieve($customerId)
						->sources->all([
						  	'limit'=>3, 
						  	'object' => 'card'
						]);
		return $cards;
	}

	public function deleteCard($customerId, $cardId){

		$customer = \Stripe\Customer::retrieve($customerId);
		return $customer->sources->retrieve($cardId)->delete();

	}
}
