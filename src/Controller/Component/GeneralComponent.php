<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Core\Exception\Exception;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
/**
 * General component
 */
class GeneralComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    public function setAuthentication(){

        if(php_sapi_name() === 'cli'){ // Check cli command or web request.
            return [];
        }

        // For API 
        if ($this->isApi()) {
                
            return [
                'storage' => 'Memory',
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'email'
                        ],
                        'finder' => 'auth'
                    ],
                    'ADmad/JwtAuth.Jwt' => [
                        'parameter' => 'token',
                        'userModel' => 'Users',
                        'fields' => [
                            'username' => 'email'
                        ],
                        'finder' => 'auth',
                        

                        
                        // Boolean indicating whether the "sub" claim of JWT payload
                        // should be used to query the Users model and get user info.
                        // If set to `false` JWT's payload is directly returned.
                        'queryDatasource' => true,
                    ]
                ],
                // 'authError' => 'O',

                'unauthorizedRedirect' => false,
                'checkAuthIn' => 'Controller.initialize',

                // If you don't have a login action in your application set
                // 'loginAction' to false to prevent getting a MissingRouteException.
                'loginAction' => false
            ];

        } 

        // For Admin Panel
        if(empty($this->request->session()->read('Auth.User.id')) && isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin') {

            return  [
                'loginAction' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'plugin' => false,
                    'prefix' => 'admin'
                ],
                'loginRedirect' => [
                    'controller' => 'Dashboard',
                    'action' => 'index',
                    'plugin' => false,
                    'prefix' => 'admin'

                ],
                'logoutRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'plugin' => false,
                    'prefix' => 'admin'

                ],
                'authenticate' => [
                                'Form' => [
                                    'fields' => ['username' => 'email', 'password' => 'password'],
                                ],
                                        
                                ],
                'authorize' => [
                                    'Acl.Actions' => ['actionPath' => 'controllers/']
                                ],
                'unauthorizedRedirect' => [
                                    'controller' => 'Users',
                                    'action' => 'login',
                                    'plugin' => false,
                                    'prefix' => 'admin'
                                ],
                'authError' => 'Please login to proceed.',
            ];
   
        }

        // For User
        return [
            'authorize' => ['Controller'],    
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                    'finder' => 'auth'
                ],
                // 'ADmad/HybridAuth.HybridAuth' => [
                // // All keys shown below are defaults
                //     'fields' => [
                //         'provider' => 'provider',
                //         'openid_identifier' => 'openid_identifier',
                //         'email' => 'email'
                //     ],

                //     'profileModel' => 'ADmad/HybridAuth.SocialProfiles',
                //     'profileModelFkField' => 'user_id',

                //     'userModel' => 'Users',

                //     // The URL Hybridauth lib should redirect to after authentication.
                //     // If no value is specified you are redirect to this plugin's
                //     // HybridAuthController::authenticated() which handles persisting
                //     // user info to AuthComponent and redirection.
                //     'hauth_return_to' => ['controller' => 'Dashboard','action' => 'index']
                // ]
            ],
            'authorize' => [
                'Acl.Actions' => ['actionPath' => 'controllers/']
            ],
            'unauthorizedRedirect' => [
                                    'controller' => 'Users',
                                    'action' => 'login',
                                    'plugin' => false,
            ],
            'authError' => 'Please login to proceed.',
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'dashboard'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => [
                'className' => 'Session',
            //'key' => 'Auth.Admin',               
            ]
        ];
    
    }

public function isApi(){
    return (empty($this->request->getSession()->check('Auth.User')) && 
            isset($this->request->params['prefix']) && 
            $this->request->params['prefix'] == 'api');
}

}