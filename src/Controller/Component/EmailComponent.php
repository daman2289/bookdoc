<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Core\Exception\Exception;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Email component
 */
class EmailComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    public function initialize(array $config) {
        $this->EmailTemplates = TableRegistry::get('EmailTemplates');
    }

    public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom('support@bookdoc.at');
        $email->setSender('support@bookdoc.at');
        $email->setTo($to);
        $email->setSubject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

    public function sendRescheduleBookingNotification($bookingData){
        
        $temp = $this->EmailTemplates->get(9);

        if (is_null($bookingData->patient_id)) {
                // $this->Flash->error(__('No Relevant Emails details foundd. Terminating email notification'));
                return;
        }
        $email = $bookingData->patient->email;
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $server = Router::url('/', true);

        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $docAddress = $bookingData->user->user_profile->address.' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->state->name . ' ';
        $docAddress = $docAddress .' '. $bookingData->user->user_profile->country->name. ' ';
        $docPhone = $bookingData->user->user_profile->phone_number;
        $docSpeciality = [];
        $doctorPic = $this->checkAndGetImage($bookingData->user->user_profile->profile_pic);

        foreach ($bookingData->user->user_specialities as $special) {
            array_push($docSpeciality, $special->speciality->title_eng);
        }

        $docSpeciality = Text::toList($docSpeciality);
        $link = Router::url('/patients/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');

        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
                [
                    '#WEEKDAY', '#DATE', '#TIME',
                    '#PATIENT_NAME', '#DOCTOR_NAME',
                    '#DOCTOR_SPECIALITY', '#DOCTOR_ADDRESS',
                    '#DOCTOR_PHONE', '#DOCTOR_PIC' , '#SERVER', '#LINK'],
                [
                    $weekday, $bookingDate, $bookingTime,
                    $patientName, $doctorName,
                    $docSpeciality, $docAddress,
                    $docPhone, $doctorPic, $server, $link
                ],
                $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'],
            [ $weekday, $bookingDate, $bookingTime],
            $subject
        );

        $this->_sendEmailMessage($email, $field_name, $subject);
    }


    public function sendBookingEmail($email, $bookingData=null){

        $temp = $this->EmailTemplates->get(3);

        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $server = Router::url('/', true);
        $link = Router::url('/patients/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME', '#PATIENT_NAME', '#DOCTOR_NAME', '#SERVER', '#LINK'],
            [$weekday, $bookingDate, $bookingTime, $patientName, $doctorName, $server, $link],
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'], 
            [$weekday, $bookingDate, $bookingTime],
            $subject
        );

        $this->_sendEmailMessage($email, $field_name, $subject);
    }

    public function sendDoctorAccountApprovedEmail($user){
        $name = $user->user_profile->full_name;
        $email = $user->email;
        $server = Router::url('/', true);

        $encodedUserId = json_encode($user->id);

        $link = Router::url('/Users/dashboard/', true);
        $temp = $this->EmailTemplates->get(10);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
                ['#DOCTOR_NAME', '#SERVER', '#LINK'],
                [ $name, $server, $link ],
                $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];
        
        $message = "Account Activated. Mail Sent";
        $this->_sendEmailMessage($email, $field_name, $subject);
    }

    public function sendForgotPasswordEmail($user, $reset_token_link){
        $this->EmailTemplates = TableRegistry::get('EmailTemplates');
        $temp = $this->EmailTemplates->get(2); 

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
            ['#FIRSTNAME', '#EMAIL', '#LINK'], [
            $user['user_profile']['first_name'],
            $user->email,
            $reset_token_link
            ],
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $this->_sendEmailMessage($user->email,$field_name, $subject);
    }

    public function sendConfirmBookingEmail($bookingId){
        $this->Bookings = TableRegistry::get('Bookings');
        
        if (is_null($bookingId)) {
            throw new NotFoundException("Booking id or status can't be null");
        }
        
        $bookingData = $this->Bookings
                            ->findById($bookingId)
                            ->contain(['Users.UserProfiles','Users.UserSpecialities.Specialities','IllnessReasons', 'Patient'])
                            ->first();

        if (is_null($bookingData->patient_id)) {
            return;
        }
        $temp = $this->EmailTemplates->get(4);
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm').'CEST';
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $doctorAddress = $bookingData->user->user_profile->address .' '.$bookingData->user->user_profile->zipcode;
        $doctorPhone = $bookingData->user->user_profile->phone_number;
        $doctorPic = $this->checkAndGetImage($bookingData->user->user_profile->profile_pic);
        $userEmail = $bookingData->patient->email;
        $doctorSpeciality = [];
        foreach ($bookingData->user->user_specialities as $eachSpeciality) {
            array_push($doctorSpeciality, $eachSpeciality->speciality->title_eng);
        }
        $doctorSpeciality = implode( ", ", $doctorSpeciality );
        $server = Router::url('/', true);
        $link = Router::url('/patients/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body']; 

        $field_name = str_replace([
                '#WEEKDAY', '#DATE', '#TIME',
                '#PATIENT_NAME', '#DOCTOR_NAME',
                '#DOCTOR_SPECIALITY', '#DOCTOR_ADDRESS',
                '#DOCTOR_PHONE', '#DOCTOR_PIC', '#SERVER', '#LINK'
            ], [
                $weekday, $bookingDate, $bookingTime,
                $patientName, $doctorName, $doctorSpeciality,
                $doctorAddress, $doctorPhone, $doctorPic, $server, $link
            ], 
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'],
            [$weekday, $bookingDate, $bookingTime],
            $subject
        );

        $this->_sendEmailMessage($userEmail, $field_name, $subject);
        return null;
    }

    public function sendCancelBookingEmail($bookingId ){

        $this->Bookings = TableRegistry::get('Bookings');
        $bookingData = $this->Bookings
                        ->findById($bookingId)
                        ->contain(['Users.UserProfiles','Users.UserSpecialities.Specialities','IllnessReasons', 'Patient'])
                        ->first();

        $temp = $this->EmailTemplates->get(5);
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm').' CEST';
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $userEmail = $bookingData->patient->email;
        $server = Router::url('/', true);
        $link = Router::url('/patients/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
            ['#PATIENT_NAME', '#DOCTOR_NAME', '#SERVER', '#LINK'],
            [$patientName, $doctorName, $server, $link],
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'],
            [$weekday, $bookingDate, $bookingTime],
            $subject
        );
        $this->_sendEmailMessage($userEmail, $field_name, $subject);
        return null;
    }

    public function sendDoctorCancelBookingEmail($bookingData){
        //This Email will be received by Patient        

        $temp = $this->EmailTemplates->get(7);

        if (is_null($bookingData->patient_id)) {
                return;
        }
        $email = $bookingData->patient->email;
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];

        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $cancelReason = $bookingData->reason;
        $server = Router::url('/', true);
        $link = Router::url('/users/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
                [
                    '#DOCTOR_NAME', '#PATIENT_NAME', '#CANCEL_REASON', '#SERVER', '#LINK'    
                ],
                [
                    $doctorName, $patientName, $cancelReason, $server, $link                    
                ],
                $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'],
            [ $weekday, $bookingDate, $bookingTime],
            $subject
        );

        $this->_sendEmailMessage($email, $field_name, $subject);
    }

    public function sendPatientCancelBookingEmail($bookingData){
        //This Email will be received by Doctor        
        $temp = $this->EmailTemplates->get(8);

        if (is_null($bookingData->patient_id)) {
                return;
        }
        $email = $bookingData->user->email;
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];

        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $cancelReason = $bookingData->reason;
        $server = Router::url('/', true);
        $link = Router::url('/users/dashboard/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
                [
                    '#DOCTOR_NAME', '#PATIENT_NAME', '#CANCEL_REASON', '#SERVER', '#LINK'    
                ], 
                [ 
                    $doctorName, $patientName, $cancelReason, $server, $link
                ],
                $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'], 
            [ $weekday, $bookingDate, $bookingTime], 
            $subject
        );
        

        $this->_sendEmailMessage($email, $field_name, $subject);   
    }

    public function checkAndGetImage($imageName) {
        $defaultPic = Configure::read('UserDummyImage');
        if (!empty($imageName)) {
            $file = new File(Configure::read('UserProfilePicPathToCheck') . $imageName);
            if ($file->exists()) {
                return Router::url(Configure::read('UserProfilePicPathToShow') . $file->name, true);

            }
        }
        return Router::url($defaultPic, true);
    }

    public function sendVerifyEmail($userId) {
        $this->Users = TableRegistry::get('Users');
        $this->EmailTemplates = TableRegistry::get('EmailTemplates');

        //pr($this->request->session()->read('Config.locale'));die;
        $user = $this->Users->findById($userId)->contain(['UserProfiles'])->firstOrFail();
        $name = $user->user_profile->full_name;
        $email = $user->email;

        $encodedUserId = base64_encode($user->id);
        $server = Router::url('/', true);
        $link = Router::url('/Users/verifyEmail/' . $encodedUserId, true);
        
        $temp = $this->EmailTemplates->get(1);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
            ['#FIRSTNAME', '#EMAIL', '#LINK', '#SERVER'], 
            [ $name, $email, $link, $server], 
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject']; 

        $this->_sendEmailMessage($email, $field_name, $subject);
        return;
    }

    public function sendPendingBookingEmail($bookingData=null, $userEmail){
        $temp = $this->EmailTemplates->get(3);
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $link = Router::url('/patients/dashboard/', true);
        $server = Router::url('/', true);

        $language = $this->request->session()->read('Config.locale');
        $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

        $field_name = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME', 
            '#PATIENT_NAME', '#DOCTOR_NAME', 
            '#LINK', '#SERVER'], 
            [$weekday, $bookingDate, $bookingTime, 
            $patientName, $doctorName, 
            $link, $server], 
            $field_name
        );

        $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

        $subject = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'], 
            [$weekday, $bookingDate, $bookingTime], 
            $subject
        );

        $this->_sendEmailMessage($userEmail, $field_name, $subject);   
    }

    public function sendConfirmBookingEmail2($bookingId, $status){
        $this->Bookings = TableRegistry::get('Bookings');

        if (is_null($bookingId) || is_null($status)) {
            throw new NotFoundException("Booking id or status can't be null");
        }
        
        $bookingData = $this->Bookings
                            ->findById($bookingId)
                            ->contain(['Users.UserProfiles','Users.UserSpecialities.Specialities','IllnessReasons', 'Patient'])
                            ->first();
        $server = Router::url('/', true);
        $link = Router::url('/patients/dashboard/', true);

        if ($status == 2) {
            if (is_null($bookingData->patient_id)) {
                // $this->Flash->error(__('No Relevant Emails details found. Terminating email notification'));
                return;           
            }
            $temp = $this->EmailTemplates->get(4);
            $dayofweek = date('w', strtotime($bookingData->booking_date));
            $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
            $weekday = $days[$dayofweek];
            $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
            $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm').'CEST';
            $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
            $doctorName = $bookingData->user->user_profile->full_name;
            $doctorAddress = $bookingData->user->user_profile->address .' '.$bookingData->user->user_profile->zipcode;
            $doctorPhone = $bookingData->user->user_profile->phone_number;
            $doctorPic = $this->checkAndGetImage($bookingData->user->user_profile->profile_pic);
            $userEmail = $bookingData->patient->email;

            $doctorSpeciality = [];
            foreach ($bookingData->user->user_specialities as $eachSpeciality) {
                array_push($doctorSpeciality, $eachSpeciality->speciality->title_eng);
            }
            $doctorSpeciality = implode( ", ", $doctorSpeciality );

            $language = $this->request->session()->read('Config.locale');
            $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

            $field_name = str_replace([
                    '#WEEKDAY', '#DATE', '#TIME', 
                    '#PATIENT_NAME', '#DOCTOR_NAME', 
                    '#DOCTOR_SPECIALITY', '#DOCTOR_ADDRESS', 
                    '#DOCTOR_PHONE', '#DOCTOR_PIC', 
                    '#LINK', '#SERVER' 
                ], [
                    $weekday, $bookingDate, $bookingTime, 
                    $patientName, $doctorName, $doctorSpeciality, 
                    $doctorAddress, $doctorPhone, 
                    $doctorPic, $link, $server
                ], 
                $field_name
            );

            $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

            $subject = str_replace(
                ['#WEEKDAY', '#DATE', '#TIME'],
                [$weekday, $bookingDate, $bookingTime],
                $subject
            );
        }


        if ($status == 3) { #DECLINED
            $temp = $this->EmailTemplates->get(5);
            $dayofweek = date('w', strtotime($bookingData->booking_date));
            $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
            $weekday = $days[$dayofweek];
            $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
            $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm').' CEST';
            $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
            $doctorName = $bookingData->user->user_profile->full_name;
            $userEmail = $bookingData->patient->email;
           
            $language = $this->request->session()->read('Config.locale');
            $field_name = (empty($language) || $language == 'en_DE') ? $temp['mail_body_german'] : $temp['mail_body'];

            $field_name = str_replace(
                ['#PATIENT_NAME', '#DOCTOR_NAME', '#LINK', '#SERVER'],
                [$patientName, $doctorName, $link, $server], 
                $field_name
            );

            $subject = (empty($language) || $language == 'en_DE') ? $temp['subject_german'] : $temp['subject'];

            $subject = str_replace(
                ['#WEEKDAY', '#DATE', '#TIME', ], 
                [$weekday, $bookingDate, $bookingTime ],
                $subject
            );
        }

        $this->_sendEmailMessage($userEmail, $field_name, $subject);
        return null;
    }

}