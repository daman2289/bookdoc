<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Core\Configure;
use Cake\Http\Client;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 *
 * @method \App\Model\Entity\Role[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->viewBuilder()->setLayout('payment');
        $this->loadComponent("Stripe");


    }

    public function index($planId){
        $this->render(false);
        $this->request->getSession()->write('Plan.id', $planId);
        return $this->redirect(['action' => 'buy']);
    }
    
    public function buy(){
        $this->loadModel('Plans');
        $planId = $this->request->getSession()->read('Plan.id', null);
        $plan = $this->Plans->findByStripeId($planId)->first(); 
        
        if (empty($plan)) {
            throw new NotFoundException(__("Plan not Found."));
        }


        $this->set(compact('plan'));
    }

    public function createSubscription($token=null){

        if (empty($token)){
            throw new BadRequestException(__("Oops! Parameters missing in request. Please try again later."));
        }

        $this->loadModel('UserStripeDetails');
        $this->loadModel('Users');
        $userId = $this->Auth->user('id');
        $userEmail = $this->Auth->user('email');

        $stripePlanId = $this->request->getSession()->read('Plan.id', null);
        
        if (empty($stripePlanId)){
            throw new BadRequestException(__("Oops! Parameters missing in request. Please try again later."));
        }

        $isSubscribedToPlan = $this->_subscribeToPlan($stripePlanId, $userId, $userEmail);
        
        
        if (!$isSubscribedToPlan){
            throw new BadRequestException(__("Unable to save. Try again later"));
        }
        $this->Flash->success(__('Payment Successful.'));
        return $this->redirect(['action' => 'mySubscriptions']);

    }

    public function mySubscriptions(){
        $this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('UserStripeDetails');
        $this->loadModel('UserTransactions');
        
        $userId = $this->Auth->user('id');

        $userPlan = $this->UserStripeDetails->findByUserId($userId)
                                        ->contain(['Users' => ['Plans']])
                                        ->first();

        $userTransactions = $this->UserTransactions->findByUserId($userId)
                                        ->contain(['Plans']);

        $this->set(compact('userPlan', 'userTransactions'));
    }

    public function cancelSubscription(){

    }

    public function listAndDeleteCards(){
        $this->loadModel('UserStripeDetails');
        $userId = $this->Auth->user('id');

        $userPlan = $this->UserStripeDetails->findByUserId($userId)
                                        ->first();

        $cards = $this->Stripe->getCards($userPlan->customer_id);
        foreach ($cards->data as $card) {
            $this->__deleteCard($userPlan->customer_id, $card->id);
        }
        $userPlan->auto_renew = 0;
        $this->UserStripeDetails->save($userPlan);

        $this->Flash->success(__('Auto-Renewal turned Off'));
        return $this->redirect($this->referer());

    }

    private function __deleteCard($customerId, $cardId){
        $response = $this->Stripe->deleteCard($customerId, $cardId);
        if (!$response->deleted) {
            throw new BadRequestException(__("Unable to delink card, Please contact adminstrator"));
        }
        return;

    }

    public function payBySepa(){
        $data = $this->request->getData();
        // pr($data); die;
        $this->Flash->error(__('This Payment method has not activated. Please try again with another method.'));
        return $this->redirect($this->referer());
        // $response = $this->Stripe->createSource($iban, $name);

    }

    public function payBySofort(){
        $http = new Client();
        $response = $http->post(
            'https://api.paymill.com/v2.1/checksums', [
            'title' => 'testing',
            'body' => [
                'checksum_type' => 'sofort',
                'amount' => '100',
                'currency' => 'EUR',
                'billing_address' => [
                    'name' => 'sofort',
                    'street_address' => 'Test street 123',
                    'postal_code' => '12345',
                    'country' => 'DE',
                    'city' => 'Munich',
                    'phone' => '123123123',
                ],
                'customer_email' => 'test@customer.de',
                'description' => 'Test Transaction',
                'client' => 'client_88a388d9dd48f86c3136a4s',
                'return_url' => 'https://www.example.com/store/checkout/result',
                'cancel_url' => 'https://www.example.com/store/checkout/retry',
                ]
        ],[
            'auth' => ['4a9c14ab599b4d00d2a54629d44cdab1']
        ]);

        $response = $this->Stripe->createSofortSource($amount, $returnUrl);

    }

   
}
