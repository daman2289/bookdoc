<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Twilio\Rest\Client;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;

/**
 * Designs Controller
 *
 */
class ReportsController extends AppController
{
	public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash'); // Include the FlashComponent
        $this->loadComponent('Upload');
    }

    public function index() {
    	$this->viewBuilder()->setLayout('frontend_dashboard');
        $this->loadModel('PatientReports');
        $userId = $this->Auth->user('id');
        $this->PatientReports->belongsTo('Users', [
            'foreignKey' => 'patient_id',
            'saveStrategy' => 'replace'
        ]);
        $report = $this->PatientReports->find()
                ->where(['PatientReports.doctor_id' => $userId])
                ->contain(['Users' => 'UserProfiles']);
                
        $reports = $this->paginate($report);
        $this->loadModel('Bookings');
        $this->Bookings->belongsTo('Users', [
            'foreignKey' => 'patient_id',
            'saveStrategy' => 'replace'
        ]);
        
        $patient = $this->Bookings->find()
                ->where(['Bookings.doctor_id' => $userId])
                ->andWhere(['Bookings.patient_id IS NOT NULL'])
                ->contain(['Users' => 'UserProfiles']);
        $concat = $patient->func()->concat([
            'UserProfiles.first_name' => 'identifier',
            ' ',
            'UserProfiles.last_name' => 'identifier'
        ]);
        $patient_name = $patient->select(['id' => 'Users.id','name' => $concat]);
        $patient_name->find('list',[
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
        $this->set(compact('reports','patient_name'));
    }

    public function addReport() {
        $this->loadModel('PatientReports');
        $this->loadModel('Users');
        $userId = $this->Auth->user('id');
        
        $report = $this->PatientReports->newEntity();
        if($this->request->is('post')) {
            if(!empty($this->request->data['patient_id'])) {
                $user = $this->Users->find()
                        ->where(['Users.id' => $this->request->data['patient_id']])
                        ->first();
            }
            if (!empty($this->request->getData()['file']['name'])) {
                $file = $this->request->getData()['file']; //put the  data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('pdf','doc','docx','jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                if (in_array($ext, $arr_ext)) {

                    //do the actual uploading of the file. First arg is the tmp name, second arg is
                    //where we are putting it
                    $savedFileName = $user->uuid.'_' . $file['name'];
                    if (move_uploaded_file($file['tmp_name'], WWW_ROOT . 'reports_patients/uploads' . DS . $savedFileName)) {
                        //prepare the filename for database entry
                        $this->request->data['file'] = 'report_' . $file['name'];

                    }
                } else {
                    $this->Flash->error(__('Please upload the file with valid Extension(pdf,doc,docx,jpg, jpeg, gif, png)'));
                }
            }
            $this->request->data['doctor_id'] = $userId;
            $this->request->data['file'] = $savedFileName;
            $report = $this->PatientReports->patchEntity($report, $this->request->data);
            if ($this->PatientReports->save($report)) { 
                $this->Flash->success(__('Report has been uploaded successfully'));
                $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Report has not been uploaded successfully'));
            }
        }
    } 
}