<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Session;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use Cake\Utility\Hash;
use Cake\I18n\FrozenTime;
use Cake\Chronos\Chronos;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {


    public $components = [
                'Acl' => [
                    'className' => 'Acl.Acl'
                ]
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        
        parent::initialize();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('General');
  
        $this->loadComponent('Auth', $this->General->setAuthentication());
        //pr($this->Auth->user());die;

        $session = $this->getRequest()->getSession();
        $isPhoneVerified = true;
        if ($session->check('Auth.User') && in_array($session->read('Auth.User.role_id'), [Configure::read('UserRoles.Patient'), Configure::read('UserRoles.Doctor')]) && !$session->read('Auth.User.is_phone_verified')) {
            $isPhoneVerified = false;
        }
        $isEmailVerified = true;
        if ($session->check('Auth.User') && in_array($session->read('Auth.User.role_id'), [Configure::read('UserRoles.Patient'), Configure::read('UserRoles.Doctor')]) && !$session->read('Auth.User.is_email_verified')) {
            $isEmailVerified = false;
        }
        $this->set(compact('isPhoneVerified','isEmailVerified'));
        
        if (empty($this->request->params['prefix'])) {
            // $this->loadComponent('Security');
            $this->loadComponent('Csrf');
        }

        if ($this->General->isApi()) {
            $this->RequestHandler->ext = 'json';
        }
        
    }
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        if ($this->getRequest()->getSession()->check('Config.locale')) {
            I18n::setLocale($this->getRequest()->getSession()->read('Config.locale'));
        
        }else{
            $this->request->getSession()->write('Config.locale', 'en_DE');
        }
        
        $this->Auth->allow(['login', 'setLocale']);

        $this->loadModel('DoctorAvailability');
        $availability = $this->DoctorAvailability->find()
                        ->where(['DoctorAvailability.user_id' => $this->Auth->user('id')])
                        ->toArray();
        $this->set(compact('availability'));
    }

    // public function forceSSL(){
    //     $this->request->addDetector('ssl', array(
    //         'env' => 'HTTP_X_FORWARDED_PROTO',
    //         'value' => 'https'
    //     ));

    //     if($_SERVER['REQUEST_SCHEME'] == "http") {
    //         return $this->redirect('https://' . env('SERVER_NAME') . $this->here); 
    //     }
    // }

    public function afterFilter(Event $event){
        $this->_isrouteAllowed();   
    }

    public function setLocale(){
        $data = $this->request->getData('locale', 'en_DE');
        $this->request->getSession()->write('Config.locale', $data);
        return $this->redirect($this->referer());
    }

    public function isAuthorized($user) {

        // Admin can access every action
        // if (isset($user['role_id']) && $user['role_id'] == 3 || $user['role_id'] == 1 || $user['role_id'] == 4 && $user['status'] == 1) {
        if (isset($user['role_id']) && $user['role_id'] == 3 || $user['role_id'] == 1 || $user['role_id'] == 4) {
            return true;
        }
        // // Default deny

        return false;
    }

    // protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {
    //     $email = new Email('default');
    //     $email->setEmailFormat('both');
    //     $email->setFrom('support@bookdoc.at');
    //     $email->setSender('support@bookdoc.at');
    //     $email->setTo($to);
    //     $email->setSubject($subject);
    //     if ($email->send($email_body)) {
    //         return true;
    //     }
    //     return false;
    // }

    protected function _isrouteAllowed(){
        if($this->Auth->user('role_id') == Configure::read('Roles.docter')){
            $planExpiry = $this->request->getSession()->read('Plan.expiry', null);
            $planExpiry = new Chronos($planExpiry);
            if ($planExpiry->isPast()) {
                $action = $this->request->getParam('action');
                $controller = $this->request->getParam('controller');
                $allowedController = ['Users','Payments'];
                $allowedActions = ['dashboard', 'logout', 'login','mySubscriptions','index'];
                if (!in_array($controller, $allowedController) && !in_array($action, $allowedActions)) { 
                    $this->Flash->error(__('Oops! Your Plan has expired. Consider upgrading your plan.'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'Dashboard','#' => 'choosePlan']);
                }
            }
        }
    }

    protected function _subscribeToPlan($type = 'free', $userId = null, $email = null) {
        $this->loadModel('FreeSubscriptions');
        $user = $this->FreeSubscriptions->find()
                ->first();
        $days = $user->days;
        if (!in_array($type, [
                env('STRIPE_MONTHLY_PLAN_ID'), 
                env('STRIPE_SEMIANNUAL_PLAN_ID'), 
                env('STRIPE_ANNUAL_PLAN_ID'),
                env('STRIPE_FREE_PLAN_ID'),
                ])) {
            return __('Invalid selection. Please try again.');
        }
        // load stripe component
        $this->loadComponent('Stripe');

        if (empty($userId) || empty($email)) {
            return false;
        }

        // find default plan details
        $plansTable = TableRegistry::get('Plans');
        $usersTable = TableRegistry::get('Users');

        $user = $usersTable->get($userId, ['contain' => ['UserStripeDetails']]);

        // If user does not have subscription id, and $type = 'free' then
        // automatically change its value from 'free' to 'basic'.
        $type = (empty($user->user_stripe_detail->subscription_id) && $type == 'free') ? 'free' : $type;

        switch ($type) {
            case env('STRIPE_ANNUAL_PLAN_ID'):
                $defaultPlan = $plansTable->getPlan(['Plans.stripe_id' => env('STRIPE_ANNUAL_PLAN_ID')]);
                $isPaidPlanSubscribed = 1;
                break;

            case env('STRIPE_SEMIANNUAL_PLAN_ID'):
                $defaultPlan = $plansTable->getPlan(['Plans.stripe_id' => env('STRIPE_SEMIANNUAL_PLAN_ID')]);
                $isPaidPlanSubscribed = 1;
                break;

            case env('STRIPE_MONTHLY_PLAN_ID'):
                $defaultPlan = $plansTable->getPlan(['Plans.stripe_id' => env('STRIPE_MONTHLY_PLAN_ID')]);
                $isPaidPlanSubscribed = 1;
                break;

            default:
                $defaultPlan = $plansTable->getPlan(['Plans.stripe_id' => env('STRIPE_FREE_PLAN_ID')]);
                $isPaidPlanSubscribed = 0;
                break;
        }


        // create customer with given email id
        $stripeCustomerId = !empty($user->user_stripe_detail->customer_id) ? $user->user_stripe_detail->customer_id : $this->Stripe->createCustomer($email);

        // If user does not have subscription id, then create subscription
        if (empty($user->user_stripe_detail->subscription_id)) {
            
            if ($isPaidPlanSubscribed) {
                // attach customer with subscription plan
                $subscriptionDetails = $this->Stripe->subscribe($stripeCustomerId, $defaultPlan->stripe_id, 0);
                $stripeSubscriptionId = $subscriptionDetails->id;
                $currentPeriodStart = new FrozenTime(gmdate("Y-m-d\TH:i:s\Z", $subscriptionDetails->current_period_start));
                $currentPeriodEnd = new FrozenTime(gmdate("Y-m-d\TH:i:s\Z", $subscriptionDetails->current_period_end));

            }else{
                // In Case of Free Plan
                $stripeSubscriptionId = null;
                $currentPeriodStart = new \DateTime();
                $currentPeriodEnd = new \DateTime();
                $currentPeriodEnd->modify('+'.$days.' day');

            }

        } else { // update subscription
            $stripeSubscriptionId = $user->user_stripe_detail->subscription_id;

            //set trial period
            $trialEnd = strtotime("+{$defaultPlan->trial_period_in_days} days", time());

            // If user already subscribed paid plan once
            if ($user->user_stripe_detail->is_paid_plan_subscribed) {
                $trialEnd = 'now';
            }
     
            $subscriptionDetails = $this->Stripe->updateSubscription($stripeSubscriptionId, $defaultPlan->stripe_id, $trialEnd);
            
            if (!is_object($subscriptionDetails)) {
                return false;
            }

            $currentPeriodStart = new FrozenTime(gmdate("Y-m-d\TH:i:s\Z", $subscriptionDetails->current_period_start));
            $currentPeriodEnd = new FrozenTime(gmdate("Y-m-d\TH:i:s\Z", $subscriptionDetails->current_period_end));
        }
               

        // update customer_id and subscription_id
        $userObj = [
            'user_stripe_detail' => [
                'id' => !empty($user->user_stripe_detail->id) ? $user->user_stripe_detail->id : null,
                'user_id' => $userId,
                'customer_id' => $stripeCustomerId,
                'subscription_id' => $stripeSubscriptionId,
                'current_period_start' => $currentPeriodStart,
                'current_period_end' => $currentPeriodEnd,
                'is_paid_plan_subscribed' => $isPaidPlanSubscribed,
                'auto_renew' => 1,
            ],
            'plan_id' => $defaultPlan->id
        ];
        
        $entity = $usersTable->patchEntity($user, $userObj, [
            'associated' => ['UserStripeDetails']
        ]);

        $saved = $usersTable->save($entity);
        

        // If user session is set
        if ($this->Auth->user()) {
            $this->request->getSession()->write('Auth.User.plan_id', $defaultPlan->id);
            $this->request->getSession()->write('Plan.expiry', $currentPeriodEnd);
        }
        return true;
    }

    protected function _isStripeConnected() {
        $this->loadModel('UserStripeDetails');
        return $this->UserStripeDetails->isStripeConnected([
            'user_id' => $this->Auth->user('id'),
            'connected_account_id IS NOT NULL'
        ]);
    }


    /**
     * Method _setValidationErrorForInvoice to set validation errors for invoice
     *
     * @param $errors array containing the error list
     * @return $str string containg the list of errors as string
     */
    protected function _setValidationError($errors = array()) {
        $allErrors = Hash::flatten($errors);
        $allErrors = array_unique($allErrors);
        $str = null;
        if (!empty($allErrors)) {
            $str = '<ul>';
            foreach ($allErrors as $key => $val):
                $str.= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }

    protected function _getOneLineErrorMessageFromValidation($errors){
        $allErrors = Hash::extract($errors, '{s}');

        foreach ($allErrors[0] as $key => $value) {
            if (is_array($value)) {
                $value = Hash::extract($value, '{s}');
                return $key.' : '.$value[0];
            }
            return $key.' : '.$value;
        }
        return "";
    }
    public function change_locale($locale){

        $this->request->session()->write('Config.locale', $locale);
        return $this->redirect($this->referer());
    }  

}
