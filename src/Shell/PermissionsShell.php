<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception;


class PermissionsShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
    }

    public function main(){
        $this->info("Generating ACO_SYNC");
        $this->loadModel('Users');
        $this->loadModel('Aros');
        // $this->Users->updateAll(["role_id"=>1],["group_id"=>1]);
        $users = $this->Users->find()->toArray();
        // For Current Users
        foreach ($users as $user) {

            $arosUser = $this->Aros->findByForeignKey($user->id)->where(['parent_id IS NOT' => NULL])->toArray();

            if (empty($arosUser)) {
                $this->dispatchShell([
                    'command' => "acl create aro Roles.{$user->role_id} Users.{$user->id}"
                    ]);
            }
        }

        $this->dispatchShell([
            'command' => 'acl_extras aco_sync',
            ]);
            
        $selection = $this->in('Set Permission for?', ['Doctor', 'Admin', 'Patient', 'All'], 'All');            
        $this->info("Setting Permission for ${selection}");
        
        //Admin
        $role1Permissions = [
            "acl grant Roles.1 controllers",
            "acl grant Roles.1 controllers/Api",            
        ];

        //Doctor
        $role3Permissions = [
            "acl deny Roles.3 controllers",
            "acl grant Roles.3 controllers/Api",
            "acl grant Roles.3 controllers/Doctors",
            "acl grant Roles.3 controllers/Users/logout",
            "acl grant Roles.3 controllers/Payments",
            "acl grant Roles.3 controllers/Users/dashboard",
            "acl grant Roles.3 controllers/Users/editProfile",
            "acl grant Roles.3 controllers/Users/editBio",
            "acl grant Roles.3 controllers/Users/changePassword",
            "acl grant Roles.3 controllers/Appointments/past",
            "acl grant Roles.3 controllers/Reports",
            "acl grant Roles.3 controllers/DoctorAvailabilityDays",
            "acl grant Roles.3 controllers/Users/videoPicture",
            "acl grant Roles.3 controllers/Users/saveImages",
            "acl grant Roles.3 controllers/Users/videoPicture",
            "acl grant Roles.3 controllers/Users/removeImage",
            "acl grant Roles.3 controllers/Patients/addToSpam",
            "acl grant Roles.3 controllers/DoctorAvailability",
            "acl grant Roles.3 controllers/DoctorUnavailability",
            
        ];

        //Patient
        $role4Permissions = [ 
            "acl deny Roles.4 controllers",
            "acl grant Roles.4 controllers/Api",
            "acl grant Roles.4 controllers/Doctors/bookAppointment",
            "acl grant Roles.4 controllers/Doctors/makeFavourite",
            "acl grant Roles.4 controllers/Patients",
            "acl grant Roles.4 controllers/Users/logout",
            "acl grant Roles.4 controllers/Users/changePassword",
        ];

        switch ($selection) {
            case 'Admin':
                $setPermissions = $role1Permissions;
                break;

            case 'Doctor':
                $setPermissions = $role3Permissions;
                break;

            case 'Patient':
                $setPermissions = $role4Permissions;
                break;

            default:
                $setPermissions = array_merge($role1Permissions, $role3Permissions, $role4Permissions);
                break;
        }

        $this->setPermission($setPermissions);
        $this->hr();

        $this->dispatchShell([
            'command' => 'acl view aro',
            ]);
        $this->success("Permissions set successfully.");
    }



    public function setPermission($allPermissions){

        foreach ($allPermissions as $permission) {
            $this->dispatchShell([
            'command' => $permission,
            ]);
        }
    }

    public function getOptionParser(){
        $parser = new ConsoleOptionParser('console');
        $parser->setDescription(
                'Use this command to grant ACL permissions. Once executed, the  ' .
                'ARO specified (and its children, if any) will have ALLOW access  ' .
                'to the specified ACO action (and the ACO\'s children, if any). ' .
                "\n\n" .
                'You will need to have ARO/ACO Tables created and Seed run for this Shell to work.'
        );
        $parser->setEpilog('ARO/ACO Permissions Console');
        return $parser;
    }


}
