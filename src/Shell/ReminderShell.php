<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Filesystem\File;
// use Cake\Http\Client;
use Twilio\Rest\Client;



class ReminderShell extends Shell
{
    public function main(){
        $today = Time::now();
    	$this->loadModel('Bookings');
        $bookings = $this->Bookings
                        ->find()
                        ->where(['booking_date >=' => $today])
                        ->contain([
                            'Patient.UserProfiles', 
                            'Users.UserProfiles.States', 
                            'Users.UserProfiles.Countries',
                            'Users.UserSpecialities.Specialities'
                        ]);
                        
                            
        foreach ($bookings as $booking) {
            $mergeDate = new \DateTime($booking->booking_date->format('Y-m-d') .' ' .$booking->booking_time->format('H:i:s'));
            $bookingDate = new Time($mergeDate);
            $days_left = date_diff($bookingDate, $today)->days;
            
            if ($days_left < 1) {
                $this->__sendEmail($booking);
            }

        }
        

    }

    public function __sendEmail($bookingData=null){
        $this->loadModel('EmailTemplates');
        $temp = $this->EmailTemplates->get(6);
        if (is_null($bookingData->patient) || is_null($bookingData->patient->email)) {
            return;
        }
        $email = $bookingData->patient->email;
        $dayofweek = date('w', strtotime($bookingData->booking_date));
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $weekday = $days[$dayofweek];
        $bookingDate = $bookingData->booking_date->i18nFormat('yyyy-MM-dd');
        $bookingTime = $bookingData->booking_time->i18nFormat('HH:mm');
        $patientName = $bookingData->patient_first_name.' '.$bookingData->patient_last_name;
        $doctorName = $bookingData->user->user_profile->full_name;
        $docAddress = $bookingData->user->user_profile->address.' ';
        $state = ($bookingData->user->user_profile->state ) ? $bookingData->user->user_profile->state->name:" ";
        $country = ($bookingData->user->user_profile->country ) ? $bookingData->user->user_profile->country->name:" ";
        // pr($state); die;
        $docAddress = $docAddress. $state;
        $docAddress = $docAddress .' '. $country. ' ';
        $docPhone = $bookingData->user->user_profile->phone_number;
        $docSpeciality = [];
        
        foreach ($bookingData->user->user_specialities as $special) {
            array_push($docSpeciality, $special->speciality->title_eng);
        }
        
        $docSpeciality = Text::toList($docSpeciality);
        $server = Router::url('/', true);
        $link = Router::url('/patients/dashboard/', true);

        $temp['mail_body'] = str_replace(
                ['#WEEKDAY', '#DATE', '#TIME', 
                '#PATIENT_NAME', '#DOCTOR_NAME', 
                '#DOCTOR_SPECIALITY', '#DOCTOR_ADDRESS', 
                '#DOCTOR_PHONE', '#SERVER', '#LINK'
            ], 
                [ $weekday, $bookingDate, $bookingTime, 
                $patientName, $doctorName, $docSpeciality, 
                $docAddress, $docPhone, $server, $link], 
                $temp['mail_body']
        );

        $temp['subject'] = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME'], 
            [ $weekday, $bookingDate, $bookingTime], 
            $temp['subject']
        );
        
        $this->_sendEmailMessage($email, $temp['mail_body'], $temp['subject']);
        $this->_sendSmsMessage($bookingData->patient->user_profile->phone_number, $weekday, $bookingDate, $bookingTime, $doctorName, $docAddress);
        $this->out('Execution Complete');
    }

    public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null) {
        $this->out("Sending Mail to {$to}");        
        $email = new Email('default');
        $email->setEmailFormat('both');
        $email->setFrom(env('EMAIL_FROM', 'support@bookdoc.com'));
        $email->setSender(env('EMAIL_FROM', 'support@bookdoc.com'));
        $email->setTo($to);
        $email->setSubject("Reminder : ".$subject);

        $cronEmailLogFile = new File(LOGS . 'cron_email_notification', true, 0644);
        $now = Time::now();
        $now = $now->i18nFormat('yyyy-MM-dd HH:mm:ss');

        if ($email->send($email_body)) {

            //GENRATING LOGS        
            $messageToLog = PHP_EOL . PHP_EOL ."{$now} :: Email Id: 6, Sent to: {$to}, Status: Sent";
            $cronEmailLogFile->append($messageToLog);
            $cronEmailLogFile->close();
            
            return true;
        }

        //GENRATING LOGS                
        $messageToLog = PHP_EOL . PHP_EOL ."{$now} :: Email Id: 6, Sent to: {$to}, Status: Failed";
        $cronEmailLogFile->append($messageToLog);
        $cronEmailLogFile->close();

        return false;
    }

    public function _sendSmsMessage($userPhoneNumber, $weekday, $bookingDate, $bookingTime, $doctorName, $docAddress){
        $countryCode = substr($userPhoneNumber, 0, 3);
        $phoneNumber = substr($userPhoneNumber, 3);   

        $notificationMessage = $this->_sendNotificationMessage($phoneNumber, $countryCode, $weekday, $bookingDate, $bookingTime, $doctorName, $docAddress);
        return;
    }
    
    protected function _sendNotificationMessage($phone, $code, $weekday, $bookingDate, $bookingTime, $doctorName, $docAddress) {
        $phone = str_replace('-','',$phone);
        $number = $code.$phone;
        $this->out("Sending SMS to {$number}");
        
        $accSID = env('TWILLIO_SID');
        $authToken = env('TWILLIO_AUTH_TOKEN');
        $fromNumber = env('TWILLIO_SEND_SMS_FROM');


        $this->http = new Client($accSID, $authToken);
        
        $this->loadModel('SmsTemplates');
        $temp = $this->SmsTemplates->get(1);
        
        
        $temp['sms_text'] = str_replace(
            ['#WEEKDAY', '#DATE', '#TIME', '#DOCTOR_NAME', '#DOCTOR_ADDRESS'], 
            [ $weekday, $bookingDate, $bookingTime, $doctorName, $docAddress], 
            $temp['sms_text']
        );
        $message = $this->http->messages->create(
            $number,
            [
                'from' => $fromNumber,
                'body' => $temp['sms_text'],
                'ProvideFeedback' => true
            ]
        );

        //saving logs while sending phone verify code
        $cronMessageLogFile = new File(LOGS . 'cron_sms_notifications', true, 0644);


        // getting current time to save in the logs
        $now = Time::now();
        $now = $now->i18nFormat('yyyy-MM-dd HH:mm:ss');

        $messageToLog = PHP_EOL . PHP_EOL. "{$now} :: Message: '{$message->body}', Sent to: {$number}, Message SID: {$message->sid}";
        $cronMessageLogFile->append($messageToLog);
        $cronMessageLogFile->close();

        return $message;
    }

    public function getClient() {
        $client = new \Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes(\Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig('credentials.json');
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = 'token.json';
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        
        $service = new \Google_Service_Calendar($client);

        $event = new \Google_Service_Calendar_Event(array(
          'summary' => 'Google I/O 2015',
          'location' => '800 Howard St., San Francisco, CA 94103',
          'description' => 'A chance to hear more about Google\'s developer products.',
          'start' => array(
            'dateTime' => '2018-08-30T09:00:00-07:00',
            'timeZone' => 'America/Los_Angeles',
          ),
          'end' => array(
            'dateTime' => '2018-09-30T17:00:00-07:00',
            'timeZone' => 'America/Los_Angeles',
          ),
          'recurrence' => array(
            'RRULE:FREQ=DAILY;COUNT=2'
          ),
          'attendees' => array(
            array('email' => 'lpage@example.com'),
            array('email' => 'sbrin@example.com'),
          ),
          'reminders' => array(
            'useDefault' => FALSE,
            'overrides' => array(
              array('method' => 'email', 'minutes' => 24 * 60),
              array('method' => 'popup', 'minutes' => 10),
            ),
          ),
        ));

        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);
        printf('Event created: %s\n', $event->htmlLink);


        // Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
          'maxResults' => 10,
          'orderBy' => 'startTime',
          'singleEvents' => true,
          'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();

        if (empty($events)) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }

    }
}