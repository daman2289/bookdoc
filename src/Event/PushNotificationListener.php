<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Http\Client;


class PushNotificationListener implements EventListenerInterface
{   

    public function implementedEvents(){

        return [
            'Push.Bookings.status' => 'getTemplate',
            'Push.Bookings.new' => 'newBookingTemplate',
        ];
    }

    public function getTemplate($event, $options){
        // PENDING -> APPROVED
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.approved')){
            $options['template'] =   'push_confirm_appointment';
        }

        if($options['status']['from'] == Configure::read('booking.status.doctorDecline') && $options['status']['to'] == Configure::read('booking.status.approved')){
            $options['template'] =   'push_confirm_appointment';
        }

        // PENDING -> DECLINED  by Doctor
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.doctorDecline')){
            $options['template'] =   'push_decline_appointment';
        }

        // APPROVED -> DECLINED by Doctor
        if($options['status']['from'] == Configure::read('booking.status.approved') && $options['status']['to'] == Configure::read('booking.status.doctorDecline')){
            $options['template'] =   'push_cancel_appointment';

        }

        // APPROVED -> DECLINED by Patient
        if($options['status']['from'] == Configure::read('booking.status.approved') && $options['status']['to'] == Configure::read('booking.status.patientDecline')){
            $options['template'] =   'push_patient_cancel_appointment';

        }

        // PENDING -> DECLINED by Patient
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.patientDecline')){
            $options['template'] =   'push_patient_cancel_appointment';

        }
        $options['send_delayed'] = false;
        
        return $this->getMessageString($options);
    }

    public function newBookingTemplate($event, $options){
        // NEW APPOINTMENT
        $options['template'] = 'push_new_appointment_booked';
        $options['send_delayed'] = true;

        return $this->getMessageString($options);

    }

    private function getMessageString($data){
        $this->UserTokens = TableRegistry::get('UserTokens');
        $userTokenData = $this->UserTokens->find()
                      ->where(['UserTokens.user_id' => $data['patient_id']])
                      ->toArray();

        $message = "";
        switch ($data['template']) {
            
            case 'push_new_appointment_booked':
                $notificationTemplate = Configure::read('push_new_appointment_booked');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                    [$data['vars']['doctor_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'push_reschedule_appointment':
                $notificationTemplate = Configure::read('push_reschedule_appointment');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                    [$data['vars']['doctor_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'push_cancel_appointment':
                $notificationTemplate = Configure::read('push_cancel_appointment');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                    [$data['vars']['doctor_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'push_patient_cancel_appointment':
                $notificationTemplate = Configure::read('push_patient_cancel_appointment');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                    [$data['vars']['doctor_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'push_confirm_appointment':
                $notificationTemplate = Configure::read('push_confirm_appointment');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                   [$data['vars']['doctor_name'], $data['vars']['booking_date']], 
                   $notificationTemplate['message']
                );
                break;

            case 'push_unavailable_time':
                $notificationTemplate = Configure::read('push_unavailable_time');
                $message = str_replace(
                    ['#FROMTIMEOFF', '#TOTIMEOFF', '#ONDATE'], 
                    [$data['vars']['from_time_off'], $data['vars']['to_time_off'], $data['vars']['on_date']], 
                    $notificationTemplate['message']
                );
                break;
            
            case 'push_decline_appointment':
                $notificationTemplate = Configure::read('push_decline_appointment');
                $message = str_replace(
                    ['#DOCTORNAME', '#BOOKINGDATE'], 
                    [ $data['vars']['doctor_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;
            
            default:
                
                break;
        }
        
        $clientTokenArray = Hash::extract($userTokenData, '{n}.token');  
        foreach($clientTokenArray as $token){ 
           $this->pushNotification([$token], $message, $data);
        }
        return true;
    }

    public function pushNotification($clientTokenArray, $message, $optionData){
        $http = new Client();

        $appId = env('ONE_SIGNAL_APP_KEY');
        $secretKey = env('ONE_SIGNAL_SECRET_KEY');

        if(Configure::read('debug')){
            $appId = env('ONE_SIGNAL_APP_KEY_LOCAL');
            $secretKey = env('ONE_SIGNAL_SECRET_KEY_LOCAL');

        }
       
        $data = json_encode([
            'app_id' => $appId,
            'include_player_ids' => $clientTokenArray,
            "contents" => ["en" =>  $message],
            "headings" => ["en" => "BookDoc Says:", "de" => "BookDoc sagt"],
        ]);
        $response = $http->post('https://onesignal.com/api/v1/notifications', 
                $data, 
                ['headers' => 
                    ['Content-Type' => 'application/json'],
                    ['Authorization' => "Basic {$secretKey}" ]
                ]
            );

        if ($optionData['send_delayed']) {
            $notificationTemplate = Configure::read('push_appointment_reminder');
            $message = str_replace(
                ['#DOCTORNAME', '#TIME'], 
                [ $optionData['vars']['doctor_name'], $optionData['vars']['booking_date']], 
                $notificationTemplate['message']
            );
            $fire1On = new \DateTime($optionData['vars']['booking_date']);
            $fire1On = $fire1On->modify('-1 day');
            
            $fire2On = new \DateTime($optionData['vars']['booking_date']);
            $fire2On = $fire2On->modify('-1 hour');

            $scheduledTime = [$fire1On, $fire2On];
            foreach ($scheduledTime as $time) {
                $data = json_encode([
                    'app_id' => $appId,
                    'include_player_ids' => $clientTokenArray,
                    "contents" => ["en" =>  $message],
                    "send_after" => $time->format('Y-m-d H:i:s'),
                    "headings" => ["en" => "BookDoc Says:", "de" => "BookDoc sagt"],
                ]);

                $response = $http->post('https://onesignal.com/api/v1/notifications', 
                    $data, 
                    ['headers' => 
                        ['Content-Type' => 'application/json'],
                        ['Authorization' => "Basic {$secretKey}" ]
                    ]
                );
            }
        }
        return true;
    }


}