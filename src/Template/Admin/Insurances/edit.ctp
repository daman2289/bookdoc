<section>
	<div class="col-sm-12">
		<h3><?= __('Insurance') ?></h3>
		<ol class="breadcrumb">
			<li>
				<?php
					echo $this->Html->link(__('Insurance'), [
						'controller' => 'insurances',
						'action' => 'index'
					]);
				?>
			</li>
			<li class="active">
				<?= __('Edit'); ?>
			</li>
		</ol>
	</div>
</section>
<section class="content">
	<div class="box">
	<div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
		<?= $this->Form->create($insurance,['id'  => 'add-insurance-form']) ?>
		  <div class="form-group right-inner-addon">
					<?php 
						echo $this->Form->control('title_french', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>55]);
					?>
			</div>
			<div class="form-group right-inner-addon">
					<?php 
						echo $this->Form->control('title_eng', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>55]);
					?>
			</div>  
			<div class="form-group right-inner-addon">
					<?php 
						echo $this->Form->control('title_italic', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>55]);
					?>
			</div>                                    
			<div class="form-group btn-margin-bottom right-inner-addon text-center">
					<?=
						$this->Form->button(__('Submit'),[
							'type' => 'submit',
							'class' => 'btn btn-lg btn-primary'
						]);
					?>
					<?=
					$this->Html->link(__('Back'), [
						'action' => 'index'
					], [
						'class' => 'btn btn-lg btn-primary'
					]);
					?>
			</div>
		<?= $this->Form->end() ?>
		</div>
	</div>
</section>
<?= 
	$this->Html->script(
		[
		  'backend/common/jquery.validate',
		  'backend/insurances/add'
		],
		[
		  'block' => 'scriptBottom'
		]
	);
?>

