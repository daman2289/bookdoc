<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Insurance $insurance
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Insurance'), ['action' => 'edit', $insurance->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Insurance'), ['action' => 'delete', $insurance->id], ['confirm' => __('Are you sure you want to delete # {0}?', $insurance->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Insurances'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Insurance'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="insurances view large-9 medium-8 columns content">
    <h3><?= h($insurance->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title French') ?></th>
            <td><?= h($insurance->title_french) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title Eng') ?></th>
            <td><?= h($insurance->title_eng) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title Italic') ?></th>
            <td><?= h($insurance->title_italic) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $insurance->has('user') ? $this->Html->link($insurance->user->id, ['controller' => 'Users', 'action' => 'view', $insurance->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($insurance->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($insurance->is_deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($insurance->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($insurance->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($insurance->modified) ?></td>
        </tr>
    </table>
</div>
