<section>
    <div class="col-sm-12">
        <h3><?= __('EmailTemplates') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Email Templates'), [
                        'controller' => 'EmailTemplates',
                        'action' => 'index'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Edit'); ?>
            </li>
        </ol>
    </div>
</section>
<!-- Main content -->
<section class="content">
   <div class="box">         
	  <div class="">                     
		<div class="" id="emails">
			<?= $this->Form->create($emailTemplate) ?>
				<div class="form-group">
				   <label for="exampleInputEmail1">Template Used For</label>
				   <?= $this->Form->control('template_used_for', ['class'=>'form-control', 'label' => false]) ?>
				</div>
				<div class="form-group">
				   <label for="exampleInputEmail1">Subject</label>
				   <?= $this->Form->control('subject', ['class'=>'form-control', 'label' => false]) ?>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Body</label>
					<div id="myeditablediv">
					<?php echo $this->Form->control('mail_body', ['class'=>'form-control', 'id' => 'text-div', 'label' => false]) ?>
					</div>
				</div> 
				<div class="form-group">
					<label for="exampleInputEmail1">Body in German</label>
					<div id="myeditablediv">
					<?php echo $this->Form->control('mail_body_german', ['class'=>'form-control', 'id' => 'text-div1', 'label' => false]) ?>
					</div>
				</div>               
				<div class="form-group btn-margin-bottom right-inner-addon text-center">
					<?= $this->Form->button(__('Save'), ['class' => 'btn btn-lg btn-primary', 'type' => 'submit']) ?>
					<?=
					$this->Html->link(__('Back'), [
						'action' => 'index'
					], [
						'class' => 'btn btn-lg btn-primary'
					]);
					?>
			</div>
			<?= $this->Form->end() ?>
		   
		 </div>
	  </div>
   </div>
</section>
<!-- /.content -->
<?= 
    $this->Html->script(
        [
          'https://cloud.tinymce.com/stable/tinymce.min.js',
          'backend/tinymce/edit.js'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
