<section>
    <div class="col-sm-12">
        <h3><?= __('Email Templates') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('EmailTemplate'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
            <div class="col-sm-12">
                <div class="filter-panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-1 filter-txt">
                                <h4><i><?= __('Filter :') ?></i></h4>
                            </div>
                            <div class="col-md-11">
                                <?php
                                    echo $this->Form->create(null, [                                        
                                        'type' => 'get',
                                        'class' => 'row'
                                    ]);
                                    
                                    echo $this->Form->control('search', [
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'value' => $this->request->getQuery('search'),
                                        'placeholder' => __('Search by: Template'),
                                        'templates' => [
                                            'inputContainer' => '<div class="col-md-3">
                                                                    <div class="form-group">
                                                                        {{content}}
                                                                    </div>
                                                                </div>'
                                        ]
                                    ]);
                                ?>

                                    <div class="col-md-3">
                                        <?php
                                            echo $this->Form->button(__('Filter'), [
                                                'class' => 'btn btn-primary',
                                                'type' => 'submit'
                                            ]);
                                        ?>

                                        <?php
                                            echo $this->Html->link(__('Clear'), [
                                                'controller' => 'EmailTemplates',
                                                'action' => 'index'
                                                ], [
                                                    'class' => 'btn btn-default',
                                            ]);
                                        ?>
                                    </div>
                                    
                                <?php  
                                    $this->Form->end();
                                ?>                                                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th scope="col"><?= $this->Paginator->sort('template_used_for', 'Template Used For') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('subject', 'Subject') ?></th>                
                            <th scope="col"><?= $this->Paginator->sort('modified', 'Last Modified') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </thead>
                        <tbody>
                        <?php foreach ($emailTemplates as $emailTemplate): ?>
                        <tr>
                            <td><?= h($emailTemplate->template_used_for) ?></td>
                            <td><?= h($emailTemplate->subject) ?></td>
                            <td><?= h($emailTemplate->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(
                                    __('<span class="fa fa-pencil-square-o icon-setting"></span>'), 
                                    ['action' => 'edit', $emailTemplate->id], ['escape' => false]
                                    ) 
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="7" class="text-right">
                                <div class="clearfix col-xs-12">
                                    <nav aria-label="Page navigation" class="pull-right">
                                        <ul class="pagination">
                                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('next') . ' >') ?>
                                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                                        </ul>
                                    </nav>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div> 
        </div>
    </div>
</section>
