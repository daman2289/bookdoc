<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Speciality $speciality
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Speciality'), ['action' => 'edit', $speciality->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Speciality'), ['action' => 'delete', $speciality->id], ['confirm' => __('Are you sure you want to delete # {0}?', $speciality->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Specialities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Speciality'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="specialities view large-9 medium-8 columns content">
    <h3><?= h($speciality->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title Eng') ?></th>
            <td><?= h($speciality->title_eng) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title French') ?></th>
            <td><?= h($speciality->title_french) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title Italic') ?></th>
            <td><?= h($speciality->title_italic) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $speciality->has('user') ? $this->Html->link($speciality->user->id, ['controller' => 'Users', 'action' => 'view', $speciality->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($speciality->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($speciality->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($speciality->is_deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($speciality->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($speciality->modified) ?></td>
        </tr>
    </table>
</div>
