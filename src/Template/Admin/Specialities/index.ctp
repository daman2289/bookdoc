<section>
    <div class="col-sm-12">
        <h3><?= __('Specialty') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Specialty'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
            <div class="col-sm-12">
                <div class="filter-panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-1 filter-txt">
                                <h4><i><?= __('Filter :') ?></i></h4>
                            </div>
                            <div class="col-md-11">
                                <?php
                                    echo $this->Form->create(null, [                                        
                                        'type' => 'get',
                                        'class' => 'row'
                                    ]);
                                    
                                    echo $this->Form->control('search', [
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'value' => $this->request->getQuery('search'),
                                        'placeholder' => __('Search by: Title'),
                                        'templates' => [
                                            'inputContainer' => '<div class="col-md-3">
                                                                    <div class="form-group">
                                                                        {{content}}
                                                                    </div>
                                                                </div>'
                                        ]
                                    ]);
                                ?>

                                    <div class="col-md-3">
                                        <?php
                                            echo $this->Form->button(__('Filter'), [
                                                'class' => 'btn btn-primary',
                                                'type' => 'submit'
                                            ]);
                                        ?>

                                        
                                    </div>
                                    
                                <?php  
                                    $this->Form->end();
                                ?>
                                <?=
                                    $this->Html->link(__('Add New Specialty'), [
                                        'action' => 'add'
                                    ], [
                                        'class' => 'btn btn-primary pull-right'
                                    ]);
                                ?>                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title_eng') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title_french') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title_italic') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>                                                        
                        </thead>
                        <tbody>
                            <?php foreach ($specialities as $speciality): ?>
                            <tr>
                                <td><?= $this->Number->format($speciality->id) ?></td>
                                <td><?= h($speciality->title_eng) ?></td>
                                <td><?= h($speciality->title_french) ?></td>
                                <td><?= h($speciality->title_italic) ?></td>                            
                                <td><?= h($speciality->created) ?></td>
                                <td><?= h($speciality->modified) ?></td>
                                <td>
                                <span class="switch-toggle custom-switch">
                                    <input type="hidden" value="0" id="<?php echo "test".$speciality->id.'_' ?>" name="data[]" checked>
                                    <input type="checkbox" hidden="hidden"  value="1" class="switch_custom" 
                                        id="<?php echo "test".$speciality->id ?>" 
                                        data-url="<?php echo $this->Url->build(
                                                     [      
                                                         'controller' => 'specialities',                                                
                                                         "action" => "status",
                                                         $speciality->id
                                                     ],true) ?>"  
                                        name="data[]"
                                        <?php echo ($speciality->status) ? "checked" : ""?>>
                                    <label class="switch" for="<?php echo "test".$speciality->id ?>"></label>
                                </span>                                                                       
                                </td> 
                                <td class="actions">
                                    <?= $this->Html->link(__('<span class="fa fa-pencil-square-o icon-setting"></span>'), ['action' => 'edit', $speciality->id], ['escape' => false]) ?>
                                    <a class="delete_speciality" data-id="<?php echo $speciality->id; ?>" href="javascript:void(0)">
                                       <i class="fa fa-trash-o icon-setting"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td colspan="12" class="text-right">
                                    <?php echo $this->element('Admin/pagination'); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
        </div>
    </div>
</section>
<script type="text/javascript">
    window.url          = '<?php echo  $this->Url->build('/',true); ?>';
</script>
<?= 
    $this->Html->script(
        [
            'backend/common/bootbox.min',
            'backend/speciality/index'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
