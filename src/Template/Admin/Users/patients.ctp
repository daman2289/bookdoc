<section>
    <div class="col-sm-12">
        <h3><?= __('Patients') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Patients'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
            <div class="col-sm-12">
                <div class="filter-panel">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-1 filter-txt">
                                <h4><i><?= __('Filter :') ?></i></h4>
                            </div>
                            <div class="col-md-11">
                                <?php
                                    echo $this->Form->create(null, [                                        
                                        'type' => 'get',
                                        'class' => 'row'
                                    ]);
                                    
                                    echo $this->Form->control('search', [
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'label' => false,
                                        'value' => $this->request->getQuery('search'),
                                        'placeholder' => __('Search by: First Name, Email'),
                                        'templates' => [
                                            'inputContainer' => '<div class="col-md-3">
                                                                    <div class="form-group">
                                                                        {{content}}
                                                                    </div>
                                                                </div>'
                                        ]
                                    ]);
                                ?>

                                    <div class="col-md-3">
                                        <?php
                                            echo $this->Form->button(__('Filter'), [
                                                'class' => 'btn btn-primary',
                                                'type' => 'submit'
                                            ]);
                                        ?>

                                        <?php
                                            echo $this->Html->link(__('Clear'), [
                                                'controller' => 'Users',
                                                'action' => 'patients'
                                                ], [
                                                    'class' => 'btn btn-default',
                                            ]);
                                        ?>
                                    </div>
                                    
                                <?php  
                                    $this->Form->end();
                                ?>
                                <!-- <?=
                                    $this->Html->link(__('Add New Patient'), [
                                        'action' => 'add',
                                        '?' => ['type' => base64_encode(ROLE_PATIENT)]
                                    ], [
                                        'class' => 'btn btn-primary pull-right'
                                    ]);                                    
                                ?>      -->                         
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th></th>
                            <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.first_name', 'Name'); ?></th>
                            <th scope="col"><?php echo $this->Paginator->sort('Users.email', 'Email') ?></th>
                            <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.tall', 'Tall') ?></th>
                            <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.weight', 'Weight') ?></th>
                            <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.phone_number', 'Mobile Number') ?></th>               
                            <th scope="col"><?php echo $this->Paginator->sort('Users.status', 'Status') ?></th>                            
                            <?php
                            if ($this->request->getSession()->read('Auth.User.role_id') === ROLE_ADMIN) :                
                            ?> 
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                            <?php
                            endif;
                            ?>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $key=>$user): ?>
                        <tr>
                            <td><?php echo $this->Html->image($this->User->checkAndGetImage($user->user_profile->profile_pic),['class' => 'profile-left-img']);?></td>
                            <td><?= ucfirst(h($user->user_profile->first_name)).' '.h($user->user_profile->last_name).'<br>'.$this->User->getUserAge($user->user_profile->dob).' yrs'; ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= h($user->user_profile->height).' cm' ?></td>
                            <td><?= h($user->user_profile->weight).' kg' ?></td>
                            <td><?= h($user->user_profile->phone_number) ?></td>
                            <td>
                                <span class="switch-toggle custom-switch">
                                    <input type="hidden" value="0" id="<?php echo "test".$user->id.'_' ?>" name="data[]" checked>
                                    <input type="checkbox" hidden="hidden"  value="1" class="switch_custom" 
                                        id="<?php echo "test".$user->id ?>" 
                                        data-url="<?php echo $this->Url->build(
                                                     [      
                                                         'controller' => 'Users',                                                
                                                         "action" => "status",
                                                         $user->id
                                                     ],true) ?>"  
                                        name="data[]"
                                        <?php echo ($user->status) ? "checked" : ""?>>
                                    <label class="switch" for="<?php echo "test".$user->id ?>"></label>
                                </span>                                                                       
                            </td>
                            <?php if ($this->request->getSession()->read('Auth.User.role_id') === ROLE_ADMIN): ?>                
                            <td class="actions">
                                <?php if ($user->is_approved === 1): ?>

                                <?= '<span class="fa fa-check icon-setting" title="Approved" aria-hidden="true"></span>' ?>
                                <?php endif;?>

                                <?php if ($user->is_approved === 0): ?>
                                <?= $this->Html->link(__('<span class="fa  fa-times icon-setting" title="Click here to approve" aria-hidden="true"></span>'), ['action' => 'approve', $user->id], ['escape' => false]) ?>
                                <?php endif;?>

                                <?= $this->Html->link(__('<span class="fa fa-pencil-square-o icon-setting"></span>'), ['action' => 'edit', $user->id], ['escape' => false]) ?>
                                                    
                                <!-- <a class="delete_user" data-id="<?php echo $user->id; ?>" href="javascript:void(0)">
                                   <i class="fa fa-trash-o icon-setting"></i>
                                </a>   -->                  
                            </td>
                            <?php endif;?>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="12" class="text-right">
                                <?php echo $this->element('Admin/pagination'); ?>
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>                 
        </div>
    </div>
</section>
<script type="text/javascript">
    window.url          = '<?php echo  $this->Url->build('/',true); ?>';
</script>
<?= 
    $this->Html->script(
        [
            'bootbox.min',
            'backend/users/index'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
