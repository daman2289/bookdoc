<section>
    <div class="col-sm-12">
        <h3><?= __('Appointments') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Appointments'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-1 filter-txt">
                            <h4><i><?= __('Filter :') ?></i></h4>
                        </div>
                        <div class="col-md-11">
                            <?php
                                echo $this->Form->create(null, [                                        
                                    'type' => 'get',
                                    'class' => 'row'
                                ]);
                                
                                echo $this->Form->control('search', [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'value' => $this->request->getQuery('search'),
                                    'placeholder' => __('Search by: Patient Name'),
                                    'templates' => [
                                        'inputContainer' => '<div class="col-md-3">
                                                                <div class="form-group">
                                                                    {{content}}
                                                                </div>
                                                            </div>'
                                    ]
                                ]);
                            ?>
                            <div class="col-md-3">
                                <?php
                                    echo $this->Form->button(__('Filter'), [
                                        'class' => 'btn btn-primary',
                                        'type' => 'submit'
                                    ]);
                                ?>
                            </div>                            
                            <?php  
                                $this->Form->end();
                            ?>
                                                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th scope="col"><?php echo $this->Paginator->sort('Bookings.patient_first_name', 'Patient'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Bookings.booking_date', 'Appointment Date'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Bookings.duration', 'Duration'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Bookings.Users.first_name', 'Doctor'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Bookings.status', 'Status') ?></th>                
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </thead>
                    <tbody>
                    <?php foreach ($bookings as $key=>$booking): ?>
                        <tr>
                            <td><?php echo h($booking->patient_first_name); echo " ";  echo h($booking->patient_last_name); ?></td>                            
                            <td><?php echo $booking->booking_date->format('M d, Y'); echo $booking->booking_date->format(' H:i'); ?></td>
                            <td><?php echo h($booking->duration); echo __(" Minutes"); ?></td>
                            <td><?php echo h($booking->user->user_profile->first_name); echo " ";  echo h($booking->user->user_profile->last_name); ?></td>                            
                            <td>
                                <?php 
                                    switch ($booking->status) {
                                        case '1':
                                            $status = __("Pending for approval");
                                            break;
                                        case '2':
                                            $status = __("Approved");
                                            break;
                                        case '3':
                                            $status = __("Declined");
                                            break;
                                        case '4':
                                            $status = __("Canceled by Patient");
                                            break;
                                        
                                        default:
                                            $status = "";
                                            break;
                                    }
                                ?>
                                <?= $status ?>
                            </td>
                            
                            <td class="actions">
                                
                                <?= $this->Html->link(
                                    '<i class="fa fa-trash-o icon-setting"></i>', 
                                    ['action' => 'deleteBooking', base64_encode($booking->id)],
                                    [
                                        'escape' => false,
                                        'confirm' => 'Are you sure you wish to delete this booking?'
                                    ]

                                ); 
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>


                    <?php if ($bookings->isEmpty()) { ?>
                            <tr>
                                <td class="text-center" colspan="6"><?= __("No Bookings Found"); ?> </td>
                            </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="12" class="text-right">
                            <?php echo $this->element('Admin/pagination'); ?>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>  
        </div>
    </div>
</section>
<script type="text/javascript">
    window.url          = '<?php echo  $this->Url->build('/',true); ?>';
</script>
<?= 
    $this->Html->script(
        [
            'bootbox.min',
            'backend/users/index'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
