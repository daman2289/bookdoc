<section>
    <div class="col-sm-12">
        <h3><?= __('Manage Free Time Subscription') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Manage Free Time Subscription'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="box">
        <div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
            <?php 
                echo $this->Form->create($user, [
                    'url'   => ['controller' => 'Users', 'action' => 'manageSubscription'],
                    'type'  => 'post',
                    'id'    => 'manage-subscription'
                    ]
                );
            ?>   
                <div class="form-group">
                    <label class="control-label"><?php echo __('Free Time'); ?><span>*</span></label>
                    <?php
                        echo $this->Form->control('days', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __('days'),
                             'maxlength'=>100
                            )
                        );
                    ?>
                </div>
                <div class="form-group">
                    <div class="clearfix">                               
                        <div class="text-center">                                
                            <?php
                                echo $this->Form->button(__('Save'), array(
                                    'class' => 'btn btn-lg btn-primary',
                                    'type' => 'submit'
                                ));
                            ?>
                        </div>
                    </div> 
                </div>     
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</section>