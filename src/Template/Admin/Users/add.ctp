<style>
.pd-rt{
    padding-right:12px;
}

</style>

<section>
    <div class="col-sm-12">
        <h3><?= $pageTitle; ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Subadmin'), [
                        'controller' => 'users',
                        'action' => 'index'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Add'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="box">
    <div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
        <?= $this->Form->create($user,['id'  => 'add-admin-form']) ?>
          <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.first_name', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.last_name', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>  
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.phone_number', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>12]);
                    ?>
            </div>                        
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('email', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>
           
            <div class="form-group right-inner-addon">
                    <?php 
                         echo $this->Form->control('password', ['type' => 'password', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>
            <div class="form-group right-inner-addon">
                    <?php 
                         echo $this->Form->control('confirm_password', ['type' => 'password', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('role_id', [
                            'options' => $conditionArrRole, 
                            'empty' => 'Please select role',
                            'class' => 'form-control',
                            'default' => '2',
                            ]
                        );
                    ?>
            </div>
             <div class="form-group right-inner-addon">
                <?php 
                    echo $this->Form->control('user_profile.gender', [
                        'type' => 'radio',
                        'options' => [
                            ['text' => __('Male'), 'value' => 'male'], 
                            ['text' => __('Female'), 'value' => 'female'] 
                        ],
                        'default' => 'male',
                        'label' => false, 
                        'class' => '', 
                        'templates'=>[
                            'nestingLabel' => '{{hidden}}<label{{attrs}} class="pd-rt">{{input}}{{text}}</label>',
                        ],
                        'maxlength'=>100
                    ]);
                    
                ?>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label><?php echo __('Zipcode'); ?></label>
                
                    <?php 
                        echo $this->Form->control('user_profile.zipcode', [
                                    'type' => 'number',
                                    'default' => '0000',
                                    "oninput" => "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);",
                                    'class' => 'form-control',
                                    'id' => 'zipcode',
                                    "maxlength" => "4",
                                    'templateVars' => [
                                        'text' => __('Zipcode'),
                                    ],
                                    'placeholder' => 'zipcode',
                                    'label' => false,
                                    'error' => false
                                ]);
                    
                    ?>

                </div>
            </div>

            <div class="form-group">
                <label><?php echo __('Date of birth'); ?></label>
                <div class="row">
                    <?php
                        $dobTemplates = [
                            'inputContainer' => '<div class="col-sm-4">{{content}}</div><div class="error-message">{{errormsg}}</div>',
                        ];
                        $this->Form->templates($dobTemplates);

                        echo $this->Form->control('user_profile.dob', [
                            'type' => 'month',
                            'empty' => 'Month',
                            'class' => 'form-control',
                            'templateVars' => [
                                'text' => '',
                                'haserror' => ($this->Form->isFieldError('user_profile.dob.month')) ? 'has-error' : '',
                                'errormsg' => ($this->Form->isFieldError('user_profile.dob.month')) ? $this->Form->error('user_profile.dob.month') : ''
                            ],
                            'default' => '01',
                            'label' => false,
                            'error' => false
                        ]);

                        echo $this->Form->control('user_profile.dob', [
                            'type' => 'day',
                            'empty' => 'Date',
                            'class' => 'form-control',
                            'templateVars' => [
                                'text' => '',
                                'haserror' => ($this->Form->isFieldError('user_profile.dob.day')) ? 'has-error' : '',
                                'errormsg' => ($this->Form->isFieldError('user_profile.dob.day')) ? $this->Form->error('user_profile.dob.day') : ''
                            ],
                            'default' => '01',
                            'label' => false,
                            'error' => false
                        ]);

                        echo $this->Form->control('user_profile.dob', [
                            'type' => 'year',
                            'empty' => 'Year',
                            'minYear' => 1950,
                            'maxYear' => date('Y'),
                            'class' => 'form-control',
                            'templateVars' => [
                                'text' => '',
                                'haserror' => ($this->Form->isFieldError('user_profile.dob.year')) ? 'has-error' : '',
                                'errormsg' => ($this->Form->isFieldError('user_profile.dob.year')) ? $this->Form->error('user_profile.dob.year') : ''
                            ],
                            'default' => '1950',
                            'label' => false,
                            'error' => false
                        ]);
                    ?>
                </div>
            </div>
          
            <div class="form-group btn-margin-bottom right-inner-addon text-center">
                    <?=
                        $this->Form->button(__('Submit'),[
                            'type' => 'submit',
                            'class' => 'btn btn-sm btn-primary'
                        ]);
                    ?>
                    <?=
                    $this->Html->link(__('Back'), [
                        'action' => 'index'
                    ], [
                        'class' => 'btn btn-sm btn-primary'
                    ]);
                    ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?= 
    $this->Html->script(
        [
          'backend/common/jquery.validate',
          'backend/users/user'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>

