<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?= 
    $this->element('common/change_password_common');
    $this->Html->script(
        [
          'backend/common/jquery.validate',
          'backend/users/change.password'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>

