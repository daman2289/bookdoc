<section>
    <div class="col-sm-12">
        <h3><?= $user->user_profile->first_name; ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Edit'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="box">
    <div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
        <?= $this->Form->create($user,['id'  => 'add-admin-form']) ?>    

        <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.first_name', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.last_name', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div> 
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('user_profile.phone_number', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>12]);
                    ?>
            </div>          
            <div class="form-group right-inner-addon">
                    <?php 
                        echo $this->Form->control('email', ['type' => 'text', 'class' => 'form-control', 'maxlength'=>100]);
                    ?>
            </div>                        
                      
            <div class="form-group btn-margin-bottom right-inner-addon text-center">
                    <?=
                        $this->Form->button(__('Submit'),[
                            'type' => 'submit',
                            'class' => 'btn btn-lg btn-primary'
                        ]);
                    ?>
                    <?=
                    $this->Html->link(__('Back'), [
                        'action' => 'patients'
                    ], [
                        'class' => 'btn btn-lg btn-primary'
                    ]);
                    ?>
            </div>
        <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<?= 
    $this->Html->script(
        [
          'backend/common/jquery.validate',
          'backend/users/user'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
