<section>
    <div class="col-lg-12 heading-top">
        <h1 class="heading-text-color pull-left"><?= __("Dashboard"); ?></h1>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
        <div class="dashboard-top clearfix">
            <div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
                <div class="outer-box-red clearfix">
                    <div class="col-xs-8 green-box">
                        <b><?= __("Total Doctors"); ?> </b>
                         <div><?php echo $totalDoctors;?></div>
                    </div>
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
                <div class="outer-box-blue clearfix">
                    <div class="col-xs-8 green-box">
                        <b><?= __("Total Patients"); ?></b>
                        <div><?php echo $totalPatients ?></div>
                    </div>
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
                <div class="outer-box-blue clearfix">
                    <div class="col-xs-8 green-box">
                        <b> <?= __("Appointments"); ?> </b>
                        <div><?php echo $appointments?></div>
                    </div>
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3 boxes-margin-bottom">
                <div class="outer-box-blue clearfix">
                    <div class="col-xs-8 green-box">
                        <b><?= __("Sponsored Doctor"); ?></b>
                        <div><?php echo $sponsered_doctors ?></div>
                    </div>
                    <div class="col-xs-4 text-center">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="dash_board">
    <div class="container-fluid">
        <h4 class="col-sm-12 pull-left">
                <?= __("Pending for Approval"); ?>
        </h4>
        <div class="col-sm-12">

            
            <div class="filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-1 filter-txt">
                            <h4><i><?= __('Filter :') ?></i></h4>
                        </div>
                        <div class="col-md-11">
                            <?php
                                echo $this->Form->create(null, [                                        
                                    'type' => 'get',
                                    'class' => 'row'
                                ]);
                                
                                echo $this->Form->control('search', [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'value' => $this->request->getQuery('search'),
                                    'placeholder' => __('Search by: First Name, Email'),
                                    'templates' => [
                                        'inputContainer' => '<div class="col-md-3">
                                                                <div class="form-group">
                                                                    {{content}}
                                                                </div>
                                                            </div>'
                                    ]
                                ]);
                            ?>
                            <div class="col-md-3">
                                <?php
                                    echo $this->Form->button(__('Filter'), [
                                        'class' => 'btn btn-primary',
                                        'type' => 'submit'
                                    ]);
                                ?>
                            </div>                            
                            <?php  
                                $this->Form->end();
                            ?>
                                                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.first_name', 'Name'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.address', 'Address') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.email', 'Email') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserSpecialities.speciality_id', 'Specialities') ?></th>                
                        <th scope="col"><?php echo $this->Paginator->sort('UserEducations.degree', 'Education') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.is_approved', 'Approve Doctor') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.is_delete', 'Delete Doctor') ?></th>
                        <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
                    </thead>
                    <tbody>
                    <?php foreach ($users as $key=>$user): ?>
                    <tr>
                        <td><?php echo $this->Html->image($this->User->checkAndGetImage($user->user_profile->profile_pic),
                        ['class' => 'profile-left-img']);?></td>
                        <td><?= ucfirst(h($user->user_profile->first_name)).' '.h($user->user_profile->last_name).'<br>'.$this->User->getUserAge($user->user_profile->dob).' yrs'; ?></td>
                        <td><?php echo $user->user_profile->address.','.'<br>'.'Mobile No:'.$user->user_profile->phone_number ?></td>
                        <td><?php echo $user->email?></td>
                        <td><?= h($this->User->getCommaSeparatedUserSpecialities($user->user_specialities)) ?></td>
                        <td><?php echo $this->User->getUserEducationsToShowInStringFormat($user->user_educations)?></td>
                        <td>
                            <?php echo $this->Html->link('Approve',
                                ['controller' => 'Users','action' => 'approveDoctor',base64_encode($user->id)],
                                ['class' => 'btn btn-primary']
                            );?>                                         
                        </td>
                        <td>
                            <?php echo $this->Html->link('Delete',
                                ['controller' => 'Users','action' => 'delete',base64_encode($user->id)],
                                ['class' => 'btn btn-primary']
                            );?>
                        </td>
                        <!-- <?php if ($this->request->getSession()->read('Auth.User.role_id') === ROLE_ADMIN): ?>                
                        <td class="actions">
                            <?php if ($user->is_approved === 1): ?>

                            <?= '<span class="fa fa-check icon-setting" title="Approved" aria-hidden="true"></span>' ?>
                            <?php endif;?>

                            <?php if ($user->is_approved === 0): ?>
                            <?= $this->Html->link(__('<span class="fa  fa-times icon-setting" title="Click here to approve" aria-hidden="true"></span>'), ['action' => 'approve', $user->id], ['escape' => false]) ?>
                            <?php endif;?>

                            <?= $this->Html->link(__('<span class="fa fa-pencil-square-o icon-setting"></span>'), ['action' => 'edit', $user->id], ['escape' => false]) ?>
                                                
                            <a class="delete_user" data-id="<?php echo $user->id; ?>" href="javascript:void(0)">
                               <i class="fa fa-trash-o icon-setting"></i>
                            </a>                    
                        </td>
                        <?php endif;?> -->
                    </tr>
                    <?php endforeach; ?>
                    <?php if ($users->isEmpty()) { ?>
                        <tr>
                            
                            <td colspan="8" class="text-center">
                                <span> <?= __("No data found")?> </span>
                            </td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td colspan="12" class="text-right">
                            <?php echo $this->element('Admin/pagination'); ?>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>  
        </div>
    </div>
</section>
<?php
    echo $this->Html->css('backend/dashboard', [
            'block' => 'meta'
        ]);
    
?>
<?= 
    $this->Html->script(
        [
            'backend/users/index'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>