<div class="col-md-5 login-left-bg  text-center">    
    <div class="call-us">
          <?=
        $this->Html->link($this->Html->image('logo.png'),[
        ],[
            'escape' => false,
            'class' => 'logo'
        ]);
    ?>
        <div class="call-us-heading">
        
            <p class="call-phone"><?php echo __('Welcome to Book Doc'); ?></p>
        </div>
    </div>
</div>
<div class="col-md-7 login-right-bg">
    <div class="text-center top-right">
        <div class="user-login"><?php echo __('Reset Password'); ?></div>
    </div>
    <?php echo $this->element('common/reset_password_common'); ?>
</div>
<?= 
    $this->Html->script(
        [
          'jquery.validate',
          'backend/reset_pwd'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>