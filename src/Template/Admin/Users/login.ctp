<div class="col-md-5 login-left-bg  text-center">    
    <div class="call-us">
          <?=
        $this->Html->link($this->Html->image('logo.jpg'),[
        ],[
            'escape' => false,
            'class' => 'logo'
        ]);
    ?>
        <div class="call-us-heading">
        
            <p class="call-phone"><?php echo __('Welcome to BookDoc Administration'); ?></p>
        </div>
    </div>
</div>
<div class="col-md-7 login-right-bg">
    <div class="text-center top-right">
        <div class="user-login"><?php echo __(' Admin Login'); ?></div>
    </div>
    <?php echo $this->element('common/login_common'); ?>
</div>
<?= 
    $this->Html->script(
        [
            'backend/common/jquery.validate',
            'backend/users/login'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>