<section>
    <div class="col-sm-12">
        <h3><?= __('Doctors') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Doctors'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="filter-panel">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-1 filter-txt">
                            <h4><i><?= __('Filter :') ?></i></h4>
                        </div>
                        <div class="col-md-11">
                            <?php
                                echo $this->Form->create(null, [                                        
                                    'type' => 'get',
                                    'class' => 'row'
                                ]);
                                
                                echo $this->Form->control('search', [
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => false,
                                    'value' => $this->request->getQuery('search'),
                                    'placeholder' => __('Search by: First Name, Email'),
                                    'templates' => [
                                        'inputContainer' => '<div class="col-md-3">
                                                                <div class="form-group">
                                                                    {{content}}
                                                                </div>
                                                            </div>'
                                    ]
                                ]);
                            ?>
                            <div class="col-md-3">
                                <?php
                                    echo $this->Form->button(__('Filter'), [
                                        'class' => 'btn btn-primary',
                                        'type' => 'submit'
                                    ]);
                                ?>
                            </div>                            
                            <?php  
                                $this->Form->end();
                            ?>
                                                      
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.first_name', 'Name'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.role_id', 'Type'); ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserProfiles.address', 'Address') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.email', 'Email') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('UserSpecialities.speciality_id', 'Specialities') ?></th>                
                        <th scope="col"><?php echo $this->Paginator->sort('UserEducations.degree', 'Education') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.is_sponsored', 'Sponsored') ?></th>
                        <!-- <th scope="col"><?php echo $this->Paginator->sort('Users.is_approved', 'Approve Doctor/Hospital') ?></th> -->
                        <th scope="col"><?php echo $this->Paginator->sort('Users.is_approved', 'Deactivate Doctor/Hospital') ?></th>
                        <th scope="col"><?php echo $this->Paginator->sort('Users.is_delete', 'Delete Doctor/Hospital') ?></th>
                        <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
                    </thead>
                    <tbody>
                    <?php foreach ($users as $key=>$user):?>
                    <tr>
                        <?php if($user->role_id == 5) { ?>
                        <td><?php echo $this->Html->image('default-building-image.png',
                        ['class' => 'profile-left-img'])?></td>
                        <?php } else { ?>
                        <td><?php echo $this->Html->image($this->User->checkAndGetImage($user->user_profile->profile_pic),
                        ['class' => 'profile-left-img']);?></td>
                        <?php } ?>
                        <?php if($user->role_id == 5) { ?>
                            <td><?php echo $user->hospital_profile->hospital_name.'<br> Contact Person:'.$user->hospital_profile->contact_name?></td>
                        <?php } else { ?>
                            <td><?= ucfirst(h($user->user_profile->first_name)).' '.h($user->user_profile->last_name).'<br>'.$this->User->getUserAge($user->user_profile->dob).' yrs'; ?></td>
                        <?php } ?>
                        <td><?php echo ($user->role_id == 5) ? "Hospital" : "Doctor"?></td>
                        <?php if($user->role_id == 5) { ?>
                            <td><?php echo $user->hospital_profile->address.','.'<br>'.'Mobile No:'.$user->hospital_profile->phone_number ?></td>
                        <?php } else { ?>
                        <td><?php echo $user->user_profile->address.','.'<br>'.'Mobile No:'.$user->user_profile->phone_number ?></td>
                        <?php } ?>
                        <td><?php echo $user->email?></td>
                        <?php if($user->role_id == 5) { ?>
                            <td><?php echo '-'?></td>
                        <?php } else { ?>
                            <td><?= h($this->User->getCommaSeparatedUserSpecialities($user->user_specialities)) ?></td>
                        <?php } ?>
                        <?php if($user->role_id == 5) { ?>
                            <td><?php echo '-'?></td>
                        <?php } else { ?>
                        <td><?php echo $this->User->getUserEducationsToShowInStringFormat($user->user_educations)?></td>
                        <?php } ?>
                        <td>
                            <span class="switch-toggle custom-switch">
                               <?php echo $this->Form->control('',array(
                                   "data-url" => $this->Url->build(array('controller' => 'Users','action' => 'status',base64_encode($user->id)),true),
                                   "id"=> "test".$user->id,
                                   "class" =>  "switch_custom",
                                   "type" => "checkbox",
                                   "hidden" => "hidden",
                                   "label" => false,
                                   'templates' => [
                                      'inputContainer' => '{{content}}',
                                      'inputContainerError' => '{{content}}{{error}}'
                                    ],
                                   "value"=>$user->id,
                                   ($user->is_sponsored == 0) ? 'checked' : ''
                               ))
                             ?>
                            <label for="test<?php echo $user->id?>" class="switch"></label>
                                                                      
                        </td>
                        <!-- <td>
                            <span class="switch-toggle custom-switch1">
                               <?php echo $this->Form->control('',array(
                                   "data-url" => $this->Url->build(array('controller' => 'Users','action' => 'approve',base64_encode($user->id)),true),
                                   "id"=> "testing".$user->id,
                                   "class" =>  "switch_custom1",
                                   "type" => "checkbox",
                                   "hidden" => "hidden",
                                   "label" => false,
                                   'templates' => [
                                      'inputContainer' => '{{content}}',
                                      'inputContainerError' => '{{content}}{{error}}'
                                    ],
                                   "value"=>$user->id,
                                   ($user->is_approved == 0) ? 'checked' : ''
                               ))
                             ?>
                            <label for="testing<?php echo $user->id?>" class="switch"></label>
                        </td> -->
                        <td>
                            <span class="switch-toggle custom-switch2">
                               <?php echo $this->Form->control('',array(
                                   "data-url" => $this->Url->build(array('controller' => 'Users','action' => 'deactivate',base64_encode($user->id)),true),
                                   "id"=> "testing1".$user->id,
                                   "class" =>  "switch_custom2",
                                   "type" => "checkbox",
                                   "hidden" => "hidden",
                                   "label" => false,
                                   "data-roleid" => $user->role_id,
                                   'templates' => [
                                      'inputContainer' => '{{content}}',
                                      'inputContainerError' => '{{content}}{{error}}'
                                    ],
                                   "value"=>$user->id,
                                   ($user->is_active == 0) ? 'checked' : ''
                               ))
                             ?>
                            <label for="testing1<?php echo $user->id?>" class="switch"></label>
                        </td>
                        <td>
                            <span class="switch-toggle custom-switch3">
                               <?php echo $this->Form->control('',array(
                                   "data-url" => $this->Url->build(array('controller' => 'Users','action' => 'deleteDoctor',base64_encode($user->id)),true),
                                   "id"=> "testing2".$user->id,
                                   "class" =>  "switch_custom3",
                                   "type" => "checkbox",
                                   "hidden" => "hidden",
                                   "label" => false,
                                   "data-roleid" => $user->role_id,
                                   'templates' => [
                                      'inputContainer' => '{{content}}',
                                      'inputContainerError' => '{{content}}{{error}}'
                                    ],
                                   "value"=>$user->id,
                                   ($user->is_delete == 0) ? 'checked' : ''
                               ))
                             ?>
                            <label for="testing2<?php echo $user->id?>" class="switch"></label>
                        </td>
                        <!-- <?php if ($this->request->getSession()->read('Auth.User.role_id') === ROLE_ADMIN): ?>                
                        <td class="actions">
                            <?php if ($user->is_approved === 1): ?>

                            <?= '<span class="fa fa-check icon-setting" title="Approved" aria-hidden="true"></span>' ?>
                            <?php endif;?>

                            <?php if ($user->is_approved === 0): ?>
                            <?= $this->Html->link(__('<span class="fa  fa-times icon-setting" title="Click here to approve" aria-hidden="true"></span>'), ['action' => 'approve', $user->id], ['escape' => false]) ?>
                            <?php endif;?>

                            <?= $this->Html->link(__('<span class="fa fa-pencil-square-o icon-setting"></span>'), ['action' => 'edit', $user->id], ['escape' => false]) ?>
                                                
                            <a class="delete_user" data-id="<?php echo $user->id; ?>" href="javascript:void(0)">
                               <i class="fa fa-trash-o icon-setting"></i>
                            </a>                    
                        </td>
                        <?php endif;?> -->
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="12" class="text-right">
                            <?php echo $this->element('Admin/pagination'); ?>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>  
        </div>
    </div>
</section>
<script type="text/javascript">
    window.url          = '<?php echo  $this->Url->build('/',true); ?>';
</script>
<?= 
    $this->Html->script(
        [
            'bootbox.min',
            'backend/users/index'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
