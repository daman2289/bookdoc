<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cms Pages'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="cmsPages form large-9 medium-8 columns content">
    <?= $this->Form->create($cmsPage) ?>
    <fieldset>
        <legend><?= __('Add Cms Page') ?></legend>
        <?php
            echo $this->Form->control('meta_title');
            echo $this->Form->control('meta_description');
            echo $this->Form->control('meta_keyword');
            echo $this->Form->control('page_title');
            echo $this->Form->control('page_name');
            echo $this->Form->control('is_activated');
            echo $this->Form->control('is_header');
            echo $this->Form->control('page_order');
            echo $this->Form->control('additional_key');
            echo $this->Form->control('is_deleted');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
