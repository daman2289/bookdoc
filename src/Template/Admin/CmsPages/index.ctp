<section>
    <div class="col-sm-12">
        <h3><?= __('Cms Pages') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'Users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Cms Pages'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="dash_board">
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('meta_title') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('meta_keyword') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('page_title') ?></th>
                        <th scope="col"><?= __('Actions') ?></th>
                    </thead>
                    <tbody>
                    <?php foreach ($cmsPages as $cmsPage): ?>
                    <tr>
                        <td><?= $this->Number->format($cmsPage->id) ?></td>
                        <td><?= h($cmsPage->meta_title) ?></td>
                        <td><?= h($cmsPage->meta_keyword) ?></td>
                        <td><?= h($cmsPage->page_title) ?></td>
                        <td class="actions">                        
                            <?= $this->Html->Link("<span class='fa fa-pencil-square-o icon-setting'></span>", 
                                    ['action' =>'edit', base64_encode($cmsPage->id)],
                                    [
                                        'escape'   => false, 
                                    ]
                                )
                            ?>                   
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>        
        </div>
    </div>
</section>
