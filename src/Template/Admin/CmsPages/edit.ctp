<section>
    <div class="col-sm-12">
        <h3><?= __('CMS Pages') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Cms Pages'), [
                        'controller' => 'CmsPages',
                        'action' => 'index'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Edit'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="content">
        <div class="box signup-page clearfix">
            <div class="col-sm-11 ">
                <?php
                echo $this->Form->create(
                        $cmsPage,
                        array(                            
                            'type' => 'post',
                            'class' => 'form-horizontal',
                            'id' => 'EditCms',
                            'novalidate' => true
                            )
                    );
            ?>
                <div class="box-body">
                        <div class="form-group">
                            <label><?php echo __('Page Title'); ?><span>*</span></label>
                        
                            <?php
                                echo $this->Form->control('page_title', array(
                                    'div' => false,
                                    'label' => false,
                                    'class' => 'form-control required',
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Page Name'); ?><span>*</span></label>
                          
                            <?php
                                echo $this->Form->control('page_name', array(
                                    'div' => false,
                                    'label' => false,
                                    'class' => 'form-control required',
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Meta Title'); ?><span>*</span></label>
                   
                            <?php
                                echo $this->Form->control('meta_title', array(
                                    'div' => false,
                                    'label' => false,
                                    'class' => 'form-control required',
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Meta Keywords'); ?><span>*</span></label>
                            
                                <?php
                                    echo $this->Form->control('meta_keyword', array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control required',
                                        )
                                    );
                                ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Description'); ?><span>*</span></label>
                            <?php
                                echo $this->Form->control('meta_description', 
                                    array(
                                        'type' => 'textarea',
                                        'rows' => 10,
                                        'cols' => 150,
                                        'label' => false,
                                        'div' => false,
                                        'class' => '',
                                        'id' => 'text-div',
                                    )
                                );
                            ?>
                        </div>
                        <div class="form-actions text-center">
                            <div class="form-group btn-margin-bottom right-inner-addon text-center">                    
                                    <?=
                                        $this->Form->button(__('Submit'),[
                                            'type' => 'submit',
                                            'class' => 'btn btn-lg btn-primary'
                                        ]);
                                    ?>
                                    <?=
                                    $this->Html->link(__('Back'), [
                                        'action' => 'index'
                                    ], [
                                        'class' => 'btn btn-lg btn-primary'
                                    ]);
                                    ?>
                            </div>
                        </div>
                    </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </section>
<?= 
    $this->Html->script(
        [
          'https://cloud.tinymce.com/stable/tinymce.min.js',
          'backend/tinymce/edit.js'
        ],
        [
          'block' => 'scriptBottom'
        ]
    );
?>
