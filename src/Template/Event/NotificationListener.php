<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\InternalErrorException;
use Cake\Core\Configure;


class NotificationListener implements EventListenerInterface
{   

    public function implementedEvents(){

        return [
            'Model.Bookings.status' => 'bookingStatusChange',
            'Model.Bookings.new' => 'newBookingReceived',
            'Model.DoctorTimeOffs.new' => 'newDoctorTime',
            'Model.Bookings.rescheduled' => 'rescheduleAppointment',
        ];
    }

    private function save_notification($data){
        $notificationsTable = TableRegistry::get('Notifications');
        $notificationEntity = $notificationsTable->newEntity();
        $notificationText = "";
       
        switch ($data['template']) {
            
            case 'temp_new_appointment_booked':
                $notificationTemplate = Configure::read('temp_new_appointment_booked');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                    [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'temp_reschedule_appointment':
                $notificationTemplate = Configure::read('temp_reschedule_appointment');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                    [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'temp_cancel_appointment':
                $notificationTemplate = Configure::read('temp_cancel_appointment');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                    [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'temp_patient_cancel_appointment':
                $notificationTemplate = Configure::read('temp_patient_cancel_appointment');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                    [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;

            case 'temp_confirm_appointment':
                $notificationTemplate = Configure::read('temp_confirm_appointment');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                   [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                   $notificationTemplate['message']
                );
                break;

            case 'push_confirm_appointment':
                $notificationTemplate = Configure::read('push_confirm_appointment');
                $notificationText = str_replace(
                    ['#DOCTORNAME'], 
                   [$data['vars']['patient_name'], $data['vars']['booking_date']], 
                   $notificationTemplate['message']
                );
                break;

            case 'temp_unavailable_time':
                $notificationTemplate = Configure::read('temp_unavailable_time');
                $notificationText = str_replace(
                    ['#FROMTIMEOFF', '#TOTIMEOFF', '#ONDATE'], 
                    [$data['vars']['from_time_off'], $data['vars']['to_time_off'], $data['vars']['on_date']], 
                    $notificationTemplate['message']
                );
                break;
            
            case 'temp_decline_appointment':
                $notificationTemplate = Configure::read('temp_decline_appointment');
                $notificationText = str_replace(
                    ['#PATIENTNAME', '#BOOKINGDATE'], 
                    [ $data['vars']['patient_name'], $data['vars']['booking_date']], 
                    $notificationTemplate['message']
                );
                break;
            
            default:
                
                break;
        }
        $newData = [
            'user_id' => isset($data['doctor_id']) ? $data['doctor_id'] : NULL,
            'text' => $notificationText,
            'template' => $data['template'],
            'unread' => 1
        ];
                // pr($notificationText); die;

        $notificationEntity = $notificationsTable->patchEntity($notificationEntity, $newData);
        // pr($notificationEntity); die;
        if(!$notificationsTable->save($notificationEntity)) {
            throw new InternalErrorException(__('Notification not saved.'));
        }    
        return true;
    }

    public function bookingStatusChange($event, $options){
        // PENDING -> APPROVED
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.approved')){
            $options['template'] =   'temp_confirm_appointment';
        }

        // PENDING -> DECLINED  by Doctor
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.doctorDecline')){
            $options['template'] =   'temp_decline_appointment';
        }

        // APPROVED -> DECLINED by Doctor
        if($options['status']['from'] == Configure::read('booking.status.approved') && $options['status']['to'] == Configure::read('booking.status.doctorDecline')){
            $options['template'] =   'temp_cancel_appointment';

        }

        // APPROVED -> DECLINED by Patient
        if($options['status']['from'] == Configure::read('booking.status.approved') && $options['status']['to'] == Configure::read('booking.status.patientDecline')){
            $options['template'] =   'temp_patient_cancel_appointment';

        }

        // PENDING -> DECLINED by Patient
        if($options['status']['from'] == Configure::read('booking.status.pending') && $options['status']['to'] == Configure::read('booking.status.patientDecline')){
            $options['template'] =   'temp_patient_cancel_appointment';

        }
        $this->save_notification($options);
    }

    public function newBookingReceived($event, $options){
        // NEW APPOINTMENT
        $options['template'] = 'temp_new_appointment_booked';

        $this->save_notification($options);
    }

    public function newDoctorTime($event, $options){
        // NEW APPOINTMENT
        $options['template'] = 'temp_unavailable_time';

        $this->save_notification($options);
    }

    public function rescheduleAppointment($event, $options){
        // Reschedule APPOINTMENT
        $options['template'] = 'temp_reschedule_appointment';
        $this->save_notification($options);
    }

   
}