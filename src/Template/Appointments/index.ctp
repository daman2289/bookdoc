<?php use Cake\Core\Configure; ?>
<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('My Appointments') ?></h2>
        </div>
    </div>
    <div class="edit-form manage-calender">
        <div class="row mb-10">
            <div class="row">
        	<div class="col-sm-12 mb-10">
                <div class="row">
        		<div class="col-sm-6">
	            	<div class="col-sm-3">
	                	<div class="box yellow"></div>
	            	</div>
	            	<div class="col-sm-6">
	            		<?= __("Pending for Approval"); ?>
	            	</div>
	            </div>
	            <div class="col-sm-6">
	            	<div class="col-sm-3">
	                	<div class="box green"></div>
	            	</div>
	            	<div class="col-sm-3">
	            		<?= __("Confirmed"); ?>
	            	</div>
	            </div>
            </div>
        </div>
        	</div>
            <div class="row">
        	<div class="col-sm-12 ">
                  <div class="row">
        		<div class="col-sm-6">
	            	<div class="col-sm-3">
	                	<div class="box blue"></div>
	            	</div>
	            	<div class="col-sm-6">
                        <?= __("Cancelled by Patient"); ?>
	            		
	            	</div>
	            </div>
	            <div class="col-sm-6">
	            	<div class="col-sm-3">
	                	<div class="box red"></div>
	            	</div>
	            	<div class="col-sm-6">
	            		<?= __("Decline by Doctor"); ?>
	            	</div>
	            </div>
            </div>
        	</div>
        </div>
        </div>
        <div class="clearfix">
		    <div id='admin-calendar' data-href="<?php echo $url.'.json'; ?>"></div>	
        </div>
    </div>
</div>
<style type="text/css">
.fc-button-group, .fc button {
    display: block;
}
</style>
<div id="patient-info" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= __("Patient Information"); ?></h4>
      </div>
      <div class="modal-body">
        <p><?= __("Current Status ") ?>: <span class="status"></span></p>
        <p><?= __("Who will seeing") ?> : <span class="seeing"></span></p>
        <p class="name_patient"><?= __("Patient Name")?>: <span class="name"></span></p>
        <p class="primary_email"><?= __("Email"); ?> : <span class="email"></span></p>
        <p class="primary_gender"><?= __("Gender"); ?> : <span class="gender"></span></p>
        <p class="primary_number"><?= __("Phone Number "); ?>: <span class="phone"></span></p>
        <p><?= __("Illness Reason"); ?> : <span class="illness"></span></p>
        <p><?= __("Doctor Note"); ?>  : <span class="doctor"></span></p>
        <p class="else-name hide"><?= __("Patient Name"); ?>  : <span class="else-patient"></span></p>
        <p class ="else-patient-email hide"><?= __("Patient Email"); ?> : <span class="else-email"></span></p>
        <p class="else-patient-gender hide"><?= __("Patient Gender"); ?> : <span class="else-gender"></span></p>
        <p class="booked hide"><?= __("Booked By"); ?> : <span class="else-booked"></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close"); ?> </button>
      </div>
    </div>

  </div>
</div>

<?php echo $this->Html->script('frontend/calender.js', ['block' => 'script']); ?>