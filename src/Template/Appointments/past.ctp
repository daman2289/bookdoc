<style>
.p-font, .fc-title{
    color: blue;
    font-size: 15px;
}
.dashbroad-content .table {
    margin-top: 0;
}
.checkbox-inline, .radio-inline {
   padding-right: 10PX;
}
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
   margin-top: 0;
   margin-left: 0px;
}
.doctor-pic-custom-dimensions{
    width:70px;
    height:70px;
    border-radius:50%;
}
.avalableLabel{
    display: none;
}
</style>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>



<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Past Appointments")?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-form dashbroad-content">
        
        <div class="row">
            <div class="col-sm-12 mobile-res-tb">
                <table class="table table-striped past-appointment  ">
                
                    <thead>
                        <tr>
                            <th><?= __("Patient Image"); ?></th>
                            <th><?= __("Patient Details");?></th>
                            <th><?= __("Booking Date"); ?></th>
                            <th><?= __("Booked By"); ?></th>
                            <th><?= __("Reason"); ?></th>
                            <th><?= __("Action"); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($doctorBookings->isEmpty()) { ?>
                        <tr><td colspan="5" class="text-center"><?= __("No Bookings Found"); ?> </td></tr>
                        <?php }?>
                        <?php foreach ($doctorBookings as $booking) { ?>
                            <tr>
                                <?php $bookingDateTime = $this->General->getBookingDateAndTime($booking->booking_date, $booking->booking_time); ?>
                                <td>
                                    <?php 
                                        $isFemale = false;
                                        $exists= ($booking->patient) ? $booking->patient->user_profile->profile_pic:false;
                                        if ($booking->patient && $booking->patient->user_profile->gender== 'Female') {
                                            $isFemale = true;
                                        }
                                    ?>
                                        <?php echo $this->Html->image($this->User->checkAndGetImage($exists, $isFemale), [
                                            'class' => 'doctor-pic-custom-dimensions'
                                        ]);
                                    ?>
                            
                                    
                                
                                </td>

                                <td>
                                        <div class="paitent-name"><?php echo $booking->patient_first_name;  echo ' '.$booking->patient_last_name ?>
                                        </div>
                                        <p class="text-muted">
                                        <?php if ($booking->who_will_seeing == 1) {
                                            echo ($booking->patient)?$booking->patient->email:" ";
                                        }else{
                                            echo $booking->patient_email;
                                        } 
                                        ?>
                                    </p>
                                    <p class="text-muted">
                                        <?php echo $booking->patient_sex; ?>
                                    </p>
                                </td>
                                <td><?= $bookingDateTime; ?> </td>
                                <td><?php echo ($booking->patient)? $booking->patient->user_profile->full_name: ''; ?></td>
                            
                                <td><?= $booking->illness_reason? $booking->illness_reason->title_eng : ''; ?></td>
                                <td>
                                    <?php if (empty($booking->patient->spam_patient)) { 
                                        if ($booking->booking_type != 1 && !empty($booking->patient_id)) {
                                            $key = __("Spam User");
                                            echo $this->Html->link(
                                                "<i class='fa fa-ban' aria-hidden='true'></i> $key",
                                                "http://javascript:void(0)",
                                                [
                                                'escape' => false,
                                                'class' =>'btn btn-danger',
                                                'title' => __('Mark as Spammer'), 
                                                "data-toggle" => "modal" , 
                                                "data-target"=> "#myConfirmModel",
                                                "data-patient-id" => $booking->patient_id,
                                                "data-bookedby" => ($booking->patient)?$booking->patient->user_profile->full_name: ''
                                                ]
                                            );
                                        }
                                        
                                        ?>
                                        
                                        <?php 
                                        $key = __(" New Appointment");
                                        echo $this->Html->link(
                                            "<i class='fa fa-calendar-check-o' aria-hidden='true'></i>$key",
                                            "javascript:void(0)",
                                            [
                                            'escape' => false,
                                            'class' =>'btn btn-success NewAppointment', 
                                            "data-toggle" => "modal" , 
                                            // "data-target"=> "#createEventModal",
                                            "data-booking-id" => $booking->id 
                                            ]
                                        );?>
                                    <?php } else { 
                                        
                                        echo __("Marked as Spam User");

                                    } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    
                </table>
            </div>
            <div class="col-sm-12 pagination-inner">
                <?= $this->element('common/paginator') ?>
            </div>
        </div>
      
        



    </div> 
</div>

<div class="modal fade myconfirm-modal" id="myConfirmModel" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
          <h4 class="modal-title text-center"><?= __("Confirm") ?> </h4>
        </div>
        <div class="modal-body">
          <p><?= __("Are you sure to mark "); ?><strong><span class="spammer"></span></strong> <?= __("as spammer ?")?></p>
          <p class="text-muted"><?= __("This will prevent all further bookings from"); ?> <span class="spammer"></span></p>
        </div>
        <div class="modal-footer" style="text-align: center">
            <form action="#" method="post">
                <input name="user_id" id="patientId" type="hidden" value="">
                <button type="submit" class="btn btn-model" ><?= __("Confirm") ?></button>
            
            </form>
        </div>
      </div>
      
    </div>
</div>


<?= $this->Element('Modals/calender/book_appointment') ?>




<?php
    echo $this->Html->script([
        "past-bookings.js",
        "backend/common/table-responsive"
    ],['block' => true]);
?>
<?php
    echo $this->Html->css([
        'calender/datepicker.css',
        'calender/style.css'
    ],['block' => true]);

    echo $this->Html->script([
        'calender/datepicker.js',
    ], ['block' => true]);
?>
<script>
    // $(function(){
    //     $('#myConfirmModel').on('show.bs.modal', function(e) {
    //         console.log("Model Execure");
            
    //         //get data-id attribute of the clicked element
    //         var bookId = $(e.relatedTarget).data('book-id');

    //         //populate the textbox
    //         $(e.currentTarget).find('input[name="bookId"]').val(bookId);
    //     });
    // })
    // function deleteIt(){
    //     swal({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //         }).then((result) => {
    //         if (result.value) {
    //             swal(
    //             'Deleted!',
    //             'Your file has been deleted.',
    //             'success'
    //             )
    //         }
    //         })
    // }
</script>