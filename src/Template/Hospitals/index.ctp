
<?php
use Cake\Core\Configure;


echo $this->Html->css([
        'jquery.timepicker',
    ],['block' => true]);

?>
<style>
.activePlan{
    background: #19ca68 !important;
}

.activePricingBorder{
    border: 3px solid #19ca68 !important;
}

</style>

<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Dashboard")?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-form dashbroad-content">
        <div class="row">
            <div class="col-sm-4 m-b-15">
                <div class="event event-dark-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Total")?> <br/><?= __("Doctors")?> </h3></li>
                        <li><span><?= 100 ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 m-b-15">
                <div class="event event-sky-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Total")?> <br/><?= __("Doctor added")?> </h3></li>
                        <li><span><?= 50 ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
        
          <!-- Booking Calender -->

     



        <!-- choose palan -->
        <div class="row">
            <div class="col-sm-12">
                <div class="plan-heading">
                    <h2 id="choosePlan"><?= __("Choose Plan"); ?></h2>
                </div>
            </div>
        </div>
        <div class="row pricing-inner">
            
            <?php foreach ($allPlans as $plan) { ?>
            <?php $myPlan = $this->General->isPlanActive($activePlanId, $plan->stripe_id); ?>
                <div class="col-md-4">
                    <div class="pricing hover-effect <?= ($myPlan ? "activePricingBorder":"")?>">
                        <div class="pricing-head">
                            <?php
                                switch ($plan->plan_type) {
                                    case '1':
                                        $price = $plan->price;
                                        $month = __("1 Month");
                                        break;

                                    case '2':
                                        $price = $plan->price/6;
                                        $month = __("6 Month");
                                        break;

                                    case '3':
                                        $price = $plan->price/12;
                                        $month = __("12 Month");
                                        break;
                                    
                                    default:
                                        $price = "";
                                        $month = "";

                                        break;
                                }
                            ?>
                            <?php if ($myPlan) { ?>
                                <h3 class="activePlan"><?php echo $month; echo __(" (Active)"); ?></h3>
                            <?php }else { ?>
                                <h3><?= $month ?> </h3>
                            <?php } ?>

                            <h4><i>€</i><?= $price ?><span>/ <?= __(' Month'); ?></span></h4>
                        </div>
                        <ul class="pricing-content list-unstyled">
                            <li><?= __("Top Five in search"); ?></li>
                            <li><?= __("Send SMS to Patient"); ?></li>
                            <li><?= __("communicate with patient thought internet messging"); ?></li>
                            <li><?= __("support"); ?></li>
                        </ul>
                        <div class="pricing-footer">
                            <?php if ($myPlan) { ?>
                                <a href="<?= $this->Url->build(['controller' => 'payments', 'action' => 'mySubscriptions']) ?>" class="btn yellow-crusta"><?= __("Manage Subscription"); ?></a>
                            <?php    
                            }else { ?>
                                <a href="<?= $this->Url->build(['controller' => 'payments', 'action' => 'index', $plan->stripe_id]) ?>" class="btn yellow-crusta"><?= __("Choose Plan");?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php }?>

            <!--//End Pricing -->
        </div>
    </div> 
</div>



<?php $this->Html->script([
    "my-model.js",
    "datepick"
],['block' => true])?>
