<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('Hospitals/Add Doctor') ?></h2>
        </div>
    </div>
    <div class="edit-form manage-calender">
    	        <?php 
                echo $this->Form->create('', [
                    'url'   => ['controller' => 'Hospitals', 'action' => 'addDoctor'],
                    'type'  => 'post',
                    'id'    => 'add_hospital_doctor_form'
                    ]
                );
        ?>  
        <div class="form-group">
        <label class="control-label"><?php echo __('First Name'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('user_profile.first_name', array(
                    'class' => 'form-control',
                    'label' => false,
                    'Placeholder' => __('Doctor First Name')
                    )
                );
            ?>
        </div> 
        <div class="form-group">
        <label class="control-label"><?php echo __('Last name'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('user_profile.last_name', array(
                    'class' => 'form-control', 
                    'label' => false,
                    'Placeholder' => __('Doctor Last Name')
                    )
                );
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Phone Number'); ?><span class="red-star">*</span></label>
            <?php
                 echo $this->Form->control('user_profile.phone_number', [
                        'class' => 'form-control',
                        'data-id' => 'phone-codes',
                        'label' => false,
                        'error' => false
                    ]);
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Email'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('email', array(
                    'class' => 'form-control',
                    'label' => false
                    )
                );
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Password'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('password', array(
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'passwd-doc'
                    )
                );
            ?>
        </div>
        <div class="form-group">
            <div class="">
                <label class="control-label"><?php echo __('Date Of Birth'); ?><span class="red-star">*</span></label>
            </div>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->control('user_profile.dob.day', array(
                            'class' => 'form-control',
                            'label' => false,
                            'type' => 'month',
                            'empty' => __('Month')
                            )
                        );
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->control('user_profile.dob.month', array(
                            'class' => 'form-control',
                            'label' => false,
                            'type' => 'day',
                            'empty' => __('Day')
                            )
                        );
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                        echo $this->Form->control('user_profile.dob.year', array(
                            'class' => 'form-control',
                            'label' => false,
                            'type' => 'year',
                            'empty' => __('Year'),
                            'minYear' => 1950,
                            'maxYear' => date('Y'),
                            'empty' => __('Year')
                            )
                        );
                    ?>
                </div>
        </div>
        <div class="form-group">
            <div>
                <label class="control-label"><?php echo __('Gender'); ?><span class="red-star">*</span></label>
            </div>
            <label><?php echo __('Male'); ?>
                <input type="radio" checked="checked" name="user_profile[gender]" value='male'>
                <span class="checkmark"></span>
            </label>
            <label><?php echo __('Female'); ?>
                <input type="radio" name="user_profile[gender]" value='female'>
                <span class="checkmark"></span>
            </label>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Address'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('user_profile.address', array(
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'geocomplete',
                    )
                );
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Zipcode'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('user_profile.zipcode', array(
                    'class' => 'form-control',
                    'label' => false
                    )
                );
            ?>
        </div>
        <?php echo $this->Form->hidden('user_profile.lat', [
            'data-id' => 'latitude'
        ]);
        echo $this->Form->hidden('user_profile.lng', [
            'data-id' => 'longitude'
        ]); ?>
        <div class="form-group">
        <label class="control-label"><?php echo __('Country'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->select('user_profile.country_id', $countryList, [
                                        'class' => 'form-control',
                                        'default' => 14,
                                        'data-id' => 'change-country',
                                        'data-url' => $this->Url->build([
                                                                    'controller' => 'States',
                                                                    'action' => 'webView',
                                                                    'prefix' => 'api'
                                                                ],true),
                                    ]);
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('States'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->select('user_profile.state_id', $stateList, [
                                        'class' => 'form-control',
                                        'data-id' => 'states'
                                    ]);
            ?>
        </div>
        <div class="form-group">
            <div class="clearfix">                               
                <div class="text-center save-btn">                                
                    <?php
                        echo $this->Form->button('Save', array(
                            'class' => 'btn submit',
                            'type' => 'submit'
                        ));
                    ?>
                </div>
            </div> 
        </div> 
        </form>
    </div>
</div>
<?php echo $this->element('states');
    echo $this->Html->css('intlTelInput', [
        'block' => 'css'
    ]);
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>
<?php echo $this->Html->script([
	'intlTelInput',
	'google_geocode',
	'state',
	'backend/hospital/doctor',
	'backend/common/table-responsive'], ['block' => 'script']);?>