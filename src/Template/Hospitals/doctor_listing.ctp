<?php use Cake\Utility\Text;?>
<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('Hospitals/Doctor Listing') ?></h2>
        </div>
    </div>
    <div class="text-right add-buuton">
    	<?php echo $this->Html->link(__('Add Doctor'),
        	['controller' => 'Hospitals','action' => 'addDoctor'],
        	[
        		'class' => 'btn btn-default'
        	] 
        );?>
    </div>
    <div class="edit-form manage-calender">
        <div class="row mb-10">
			<div class="row">
			    <div class="col-sm-12 mobile-res-tb">
			        <table class="table table-striped ">
			            <thead><tr>
			                <th><?= __("Doctor Name"); ?></th>
			                <th><?= __("Email"); ?></th>
			                <th><?= __("Phone number"); ?></th>
			                <th><?= __("Gender"); ?></th>
			                <th><?= __("Action"); ?></th>
						</tr>
					</thaed>
					<tbody>
			            <?php if ($doctors->isEmpty()) { ?>
			               <tr><td colspan="5" class="text-center"><?= __("No Doctors Found"); ?> </td></tr>
			            <?php }?>
			             <?php foreach ($doctors as $doctor) { ?>
			                <tr>
			                	<td><?php echo $doctor['user']['user_profile']->first_name.' '.$doctor['user']['user_profile']->last_name?></td>
			                    <td><?php echo $doctor['user']->email?></td>
			                    <td><?php echo $doctor['user']['user_profile']->phone_number?></td>
			                    <td><?php echo $doctor['user']['user_profile']->gender?></td>
			                    <td><?php echo $this->Html->link('View Dashboard',
			                    	['controller' => 'Hospitals','action' => 'viewDashboard',$doctor['user']->uuid],
			                    	['class' => 'btn btn-default view_button']
			                    );?></td>
			                </tr>
			            <?php } ?>
							</tbody>
			        </table>
			    </div>
			    <div class="col-sm-12 pagination-inner">
			        <?= $this->element('common/paginator') ?>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('states');
    echo $this->Html->css('intlTelInput', [
        'block' => 'css'
    ]);
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>
<?php echo $this->Html->script([
	'intlTelInput',
	'google_geocode',
	'state',
	'backend/hospital/doctor',
	'backend/common/table-responsive'], ['block' => 'script']);?>
<style type="text/css">
	.patient-image {
		width: 45px;
    	height: 45px;
    	border-radius: 50%;
	}
	.view_button {
		background-color: #19ca68;
	    border: 1px #19ca68;
	    color: #fff;
	}
	.view_button:hover {
	    background-color: #19ca68;
	    border: 1px #19ca68;
	    color: #fff;
	}
</style>
