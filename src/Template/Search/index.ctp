<?php 
use Cake\Utility\Hash; 
use Cake\I18n\Time;
use Cake\Chronos\Chronos;
use Cake\I18n\FrozenTime;
?>
<?=
    $this->Html->css([
        'search',
        'rating-star'
    ]);
?>
<div class="finddoctor health-solution">
    <div class="wrapper">
        <h1 class="find_text"><?= __("Find your Best Health solution near you"); ?></h1>
        <div class="searchdiv">
            <?php echo $this->Form->create(null,[
                'type' => 'post',
                'url' => ['controller' => 'search','action' => 'index'],
                'autocomplete' => 'off'
            ]); ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="searchbox autocomplete">
                        <?= $this->Form->control('term',[
                            'type' => 'text',
                            'label' => false,
                            'class' => 'search_input typeahead form-control',
                            "id" => "myInput",
                            'value' => $this->request->getData('term'),
                            'placeholder' => __('Speciality/Doctor name')
                        ])
                        ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="searchbox">
                        <?= $this->Form->control('zip',[
                            'type' => 'text',
                            'label' => false,
                            'class' => 'search_input form-control',
                            'value' => $this->request->getData('zip'),
                            'placeholder' => __('Zip Code'),
                            'maxlength' => 4
                        ])
                        ?>
                        <div id="autoc"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="searchbox">
                        <?= $this->Form->control('insurance',[
                            'type' => 'select',
                            'label' => false,
                            'options' => $insurances,
                            'class' => 'search_input blue_color form-control',
                            'value' => $this->request->getData('insurance'),
                            'placeholder' => __('I’ll choose my insurance later'),
                            'empty' => __('I’ll choose my insurance later')
                        ])
                        ?>
                        <?= $this->Form->button('<i class="fa fa-search white_icon"></i>',[
                            'class' => 'submit_btn',
                            'escape' => false,
                            'type' => 'submit',
                        ]); ?>
                        <?= $this->Form->button('Search',[
                            'class' => 'submit_btn-mob',
                            'escape' => false,
                            'type' => 'submit',
                        ]); 
                        ?>
                    </div>
                </div>
            
        </div>
    </div>
</div>


<div class="mainbody">
    <div class="doctor_listing_shadual">
        <div class="container">
                <div class="doc_sheduals">
                    <div class="col-sm-3 pd-mb-0">
                        <?php
                            echo $this->Form->control('illness',[
                                'type' => 'select',
                                'label' => false,
                                'value' => $this->request->getQuery('illness'),
                                'class' => "form-control", 
                                'options' => $specialities,
                                'onchange'=> "this.form.submit()"
                            ]);
                        ?>
                    </div>
                    <div class="col-md-3 col-sm-6 p-0 pd-mb-0 ">
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                            <label class="btn btn-primary <?= $this->General->isActive('gender', 'no'); ?>">
                                <input type="radio" name="gender" value="no" onchange = "this.form.submit()" <?= $this->General->isSelected('gender','no'); ?> > <?= __("Any <br> Gender"); ?>
                            </label>
                            <label class="btn btn-primary <?= $this->General->isActive('gender', 'm'); ?>">
                                <input type="radio" name="gender" value="m" onchange = "this.form.submit()" <?= $this->General->isSelected('gender','m'); ?> > <?= __("Male"); ?>
                            </label>
                            <label class="btn btn-primary <?= $this->General->isActive('gender', 'f'); ?>">
                                <input type="radio" name="gender" value="f" onchange = "this.form.submit()" <?= $this->General->isSelected('gender','f'); ?>> <?= __("Female")?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4 pd-mb-0">
                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                            <label class="btn btn-primary <?= $this->General->isActive('day', 'no'); ?>">
                                <input type="radio" name="day" value="no" onchange = "this.form.submit()"  <?= $this->General->isSelected('day','no'); ?> > <?= __("Any Day"); ?>
                            </label>
                            <label class="btn btn-primary <?= $this->General->isActive('day', 'today'); ?>">
                                <input type="radio" name="day" value="today" onchange = "this.form.submit()" <?= $this->General->isSelected('day','today'); ?>> <?= __("Today"); ?>
                            </label>
                            <label class="btn btn-primary <?= $this->General->isActive('day', 'next'); ?>">
                                <input type="radio" name="day" value="next"  onchange = "this.form.submit()" <?= $this->General->isSelected('day','next'); ?> > <?= __("Next 3 Days")?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 p-0 pd-mb-0">
                        <?=
                            $this->Form->control('time',[
                                'type' => 'select',
                                'class' => 'form-control',
                                'label' => false,
                                'empty' => __('Select Time'),
                                'options' => $this->General->getTimimgSlots(),
                                'onchange' => "this.form.submit()",
                                'value' => $this->request->getQuery('time')
                            ]);
                        ?>
                    </div>
                </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="doctor_listing_block">
    <div class="container">
        <div class="listing">
             <?php if ( !$doctors->isEmpty() || empty($this->request->getQuery()) ) {?>
                <div class="col-sm-4 pull-right time-top-slider-header">
                    <div class="time_table_calender">
                        <div class="calender">
                            <?php 
                                $now = new DateTime('NOW');
                                

                                // $timeObj = new DateTime('NOW');
                                $currentDay = $now->format('N');

                                $ulcolsed = true; 
                                for($i=0; $i < 30; $i++) {                                    
                                    if($i % 3 == 0) {
                                        echo '<ul class="dates hidden-xs">';
                                        $ulcolsed = false;
                                    }                                    
                            ?>
                                    <li class="text-center">
                                        <?= __($now->format('D')) ?>
                                        <br> <?= __($now->format('F')) ?> <?= __($now->format('j')) ?>
                                    </li>
                                    <?php
                                        $timeObj = Time::now();

                                        $next15Days[$i] = [
                                            "weekday" => $now->format('N') , 
                                            "timeObj" => $timeObj->modify("+{$i} days")
                                        ]; 
                                        
                                        $arr = $now->format('N'); 
                                        $currentDay = $now->format('N');
                                        if($i != 0 && ($i+1)%3 == 0) {
                                            echo '</ul>';
                                            $ulcolsed = true;
                                        }
                                        $now->modify("1 day");

                                }
                                    if(!$ulcolsed) {
                                        echo '</ul>';
                                    }
                                    ?>
                                <a href="javascript:void(0)" onclick="next(this)" class='calender-arrow next-arrow' data-current='0'><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" onclick="prev(this)" class='calender-arrow prev-arrow' data-current='0'><i class="fa fa-chevron-left" aria-hidden="true" style="opacity:0.5"></i></a>
                            
                        </div>
                    </div>
                </div>
             <?php } ?>



            <!-- GENERAL RESULTS -->
            <?php if ($doctors->isEmpty()) { ?>
                <div> <?= __("No Data Found"); ?> </div>       
            
            <?php } ?>
            <?php
                foreach ($doctors as $doctor) { 
                $rating = Hash::extract($doctor->doctor_ratings, '{n}.overall_rating');
                if(empty($rating)) {
                    $overall_rating = 0;
                } else {   
                    $overall_rating = array_sum($rating) / count($rating);
                }
                ?>
                <?php $currentEnd = $doctor->user_stripe_detail->current_period_end; $planExpiry = new Chronos($currentEnd->i18nFormat('yyyy-MM-dd HH:mm:ss'));
                    if(!$planExpiry->isPast()) {
                ?>
                <div class="list_box <?= ($doctor->is_sponsored) ? "active" : "" ;?>">
                    <div class="col-md-8 col-sm-8 col-xs-12 p-0">

                        <div class="doctor_detail_part">

                                <?php if ($this->request->getSession()->read('Auth.User')) {

                                    if(!empty($doctor->doctor_favourite)) {
                                        echo $this->Html->link('<i class="fa fa-heart" aria-hidden="true"></i>',
                                            'javascript:void(0)',
                                            [
                                                'escape' => false,
                                                'class' => '',
                                                'title' => __('Remove favourite'),
                                                'onclick' => 'removeFav()',
                                                'id' => 'alreadFav',
                                                'data-id' => $doctor->id,
                                            ]
                                        );
                                    } else {
                                        echo $this->Html->link('<i class="fa fa-heart-o" aria-hidden="true"></i>',
                                            'javascript:void(0)',
                                            [
                                                'escape' => false,
                                                'class' => 'favt-doctor',
                                                'title' => __('Make it favourite'),
                                                'data-id' => $doctor->id
                                            ]
                                        );
                                    } 
                                
                                } else {
                                    echo $this->Html->link('<i class="fa fa-heart-o" aria-hidden="true"></i>',
                                            'javascript:void(0)',
                                            [
                                                'escape' => false,
                                                'class' => 'favt-doctor',
                                                'data-toggle' => 'modal',
                                                'data-target' => '#login',
                                                'title' => __('Make it favourite')
                                            ]
                                        );
                                }
                                ?>
                            <div class="doctor_img <?= ($doctor->is_sponsored) ? "sponsored-img" : "" ;?>">
                                <?php 
                                    $isFemale = false;
                                    if (!empty($doctor->user_profile)) {
                                        if ($doctor->user_profile->gender == 'Female') {
                                            $isFemale = true;
                                        }
                                        
                                        
                                        echo $this->Html->image($this->User->checkAndGetImage($doctor->user_profile->profile_pic, $isFemale), [
                                            'class' => 'doctor-pic-custom-dimensions'
                                            ]);
                                    }else{
                                        echo $this->Html->image($this->User->checkAndGetImage(null), [
                                            'class' => 'doctor-pic-custom-dimensions'
                                        ]);
                                    }
                                ?>
                            </div>
                            <div class="doctor_details">
                                <?php if($doctor->is_sponsored){ ?>
                                    <h3 class="sponsers_tag"><?= __("")?><?= __("Sponsored Result"); ?></h3>
                                <?php }?>
                                <a href="<?php echo $this->Url->build(['controller' => 'Doctors', 'action' => 'view', base64_encode($doctor->id)] ); ?>" > 
                                    <?php if (!empty($doctor->user_profile)) { ?>
                                        <h2 class="doctorname"> <?= __("Dr."); ?> <?= $doctor->user_profile->full_name ?></h2>
                                    <?php }else{ ?>
                                        <h2 class="doctorname"> <?= __("No Name"); ?></h2>
                                    <?php } ?>
                                </a>
                                <h5 class="doctor-details-age">
                                    <?php if (!empty($doctor->user_profile)) { 
                                        echo $this->User->getUserAge($doctor->user_profile->dob) . ' ' . __('yrs. old'); 
                                        }else{
                                            echo __("No Age");
                                        }
                                    ?>
                                </h5>                                
                                <div class="groupstar">
                                    <input type="text" class="rating rate_overall" name="overall_rating" value="<?= $overall_rating?>" data-disabled="true">
                                </div>
                                <div class="deatils">
                                    <?php if (!empty($doctor->user_profile)) { 
                                        echo $this->Text->truncate(
                                           h($doctor->user_profile->professional_statement),
                                            70,
                                            [
                                                'ellipsis' => '...',
                                                'exact' => true
                                            ]
                                        );
                                    }else{
                                        echo __("No Professional Statement");
                                    }
                                    ?>
                                </div>
                                <div class="add_map"><i class="icon fa fa-map-marker"></i> 
                                    <?php if (!empty($doctor->user_profile)) { 
                                        echo $doctor->user_profile->address; 
                                        echo ' '.$doctor->user_profile->zipcode; 
                                    }else{
                                        echo __("No Address added");
                                    }                                        
                                    ?>
                                </div>
                                <div>
                                    <?php
                                        $mySpecilities = []; 
                                        foreach ($doctor->user_specialities as $user_speciality) {
                                            array_push($mySpecilities, $user_speciality->speciality->title_eng);
                                        }
                                        if (!empty($mySpecilities)) {
                                            echo "<strong>Specialist In : </strong>";
                                            echo $this->Text->toList($mySpecilities); 
                                        }
                                    ?>
                                </div>
                                <div class="location_address">
                                    <?php if (!empty($doctor->user_profile)) { ?>
                                    <a href="javascript:void(0)" id="view<?= $doctor->id ?>" onclick= "showMap(<?= $doctor->id ?>, <?= $doctor->user_profile->lat ?>, <?= $doctor->user_profile->lng ?>)">
                                        <?= __("View my Location"); ?> <i class="fa fa-angle-down"></i>
                                    </a>
                                    <?php }else{
                                        echo __("No Location added");
                                     } ?>
                                </div>
                                


                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    <?php
                        $days = $this->General->getDoctorTimings($doctor->doctor_availability);
                    ?>
                    <div class="col-md-4 col-sm-4 col-xs12 p-0">
                        <div class="doctor_time_Table">
                            <div class="time_table mob_time_table visible-xs">
                                <ul>
                                <?php //pr($next15Days);die;
                                        $noSlotsAvailable = true;
                                        foreach ($next15Days as $key => $value) { ?>
                                  


                                        <?php
                                            $times = (!empty($days[$value['weekday']])) ? $days[$value['weekday']] :[];
                                            $timeDay  = $value;
                                            $unavailableTime = !empty($doctor->all_unavailable_slots) ? $doctor->all_unavailable_slots : array();
                                            $isSignedIn = $this->request->getSession()->check('Auth.User');
                                            
                                            if (empty($doctor)) {
                                                $doctorId = "";
                                            }else{
                                                $doctorId = base64_encode($doctor->id);
                                            }

                                            if ($isSignedIn) {
                                                $isSignedIn = "True";
                                            }else{
                                                $isSignedIn = "False";
                                            }

                                        ?>

                                        <?php if(!empty($times)): 
                                            $noSlotsAvailable = false;
                                        ?>
                                                <?php 
                                                    foreach($times as $key => $value): 
                                                ?>  
                                                            <?php if(!empty($value)) { 
                                                                    $dayAvailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
                                                                    if($dayAvailable) { 
                                                                        ?>
                                                                    <li>
                                                                
                                                                        <a href="javascript:void(0)" onclick='setTime(this, "<?= date('H:i',strtotime($value));  ?>", "<?= $timeDay['weekday']; ?>", "<?= $timeDay['timeObj']->format('Y-m-d'); ?>" ); 
                                                                            showBookingModal("<?= $isSignedIn; ?>", "<?= $doctorId ?>", "<?php echo $doctor->user_profile->my_patient_only ?>");' data-day=" <?= $timeDay['weekday'] ?> "> 
                                                                            <?= date('H:i',strtotime($value)); ?> 
                                                                        </a>
                                                                    
                                                                    </li>
                                                            <?php } }?>

                                                <?php
                                                    endforeach; 
                                                ?>
                                        <?php endif;?>
                                   <?php     }
                                    ?>
                                </ul>

                                <?php  if ($noSlotsAvailable): 
                                ?>
                                    <ul>
                                        <li><?php echo "No Slots Available"; ?></li>
                                    </ul>
                                <?php endif; ?>
                                <!-- <ul class="list-inline"> -->
                                   <!--  <?php for ($i=0; $i < 8; $i++) { ?>
                                        <li>
                                            <a href="javascript:void(0)" onclick="setTime(this, &quot;13:30&quot;, &quot;4&quot;, &quot;2018-08-23&quot;); 
                                                showBookingModal(&quot;False&quot;, &quot;OTM=&quot;);" data-day=" 4 "> 
                                                13:30
                                            </a>
                                        </li>
                                    <?php } ?> -->
                                <!-- </ul> -->
                            </div>
                            <div class="time_table hidden-xs">
                                <!-- <ul> -->
                                    <?php
                                $days = $this->General->getDoctorTimings($doctor->doctor_availability);
                                $html = $content = "";
                                $hasSlots = false;
                                $uavblSlots = $this->DayAvailability->unavailableSlots($doctor->doctor_unavailability);
                                foreach ($next15Days as $key => $value) {
                                    //pr($value);
                                    if($key % 3 == 0) {
                                            echo '<div class="row times" >';
                                            $divcolsed = false;
                                    }
                                     $content .= $this->element('/AvailabilityTimings/timings',[
                                            'times' => (!empty($days[$value['weekday']])) ? $days[$value['weekday']] :[] ,
                                            'timeDay'  => $value,
                                            'doctor' => $doctor,
                                            'unavailableTime' => $uavblSlots
                                        ]);

                                     if (!$hasSlots && !empty($days[$value["weekday"]])) {
                                        $hasSlots = true;
                                     }

                                    if($key != 0 && ($key+1) % 3 == 0) {
                                        $availableDay = $this->DayAvailability->nextAvailableDay($value, $days);
                                        
                                        if (!$hasSlots && !is_null($availableDay)) {
                                            echo "<h5>Doctor will be available on {$availableDay}</h5>";
                                        } else {
                                            echo $content;
                                        }

                                        $content = "";

                                        $hasSlots = false;
                                        
                                                echo '</div>';
                                                $divcolsed = true;
                                    }
                                }

                                if(!$divcolsed) {
                                    echo '</div>';
                                }
                            ?>
                                <!-- </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div id= "<?= $doctor->id; ?>" class="hide" style="height: 300px;"></div>
                    </div>
                </div>
            <?php } } ?>
            

        </div>
        <?= $this->element('common/paginator')?>
    </div>
</div>
<section class="find">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-heading">
                    <h2><?= __("Find doctors and dentists by"); ?>...</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-location-pin"></i> <?= __("State")?></h3>
                    <ul>
                        <?php
                            foreach ($state_name as $key => $state) { ?>
                                <li>
                                    <!-- <a href="<?= $this->Url->build([
                                        'controller' => 'search', 
                                        'action' => 'index', 
                                        '?' => ['zip' =>  $key]]); ?>">
                                        <?= $state; ?>
                                    </a> -->
                                    <a href="/location/<?= $state; ?>"><?= $state; ?></a>
                                    <?php //echo $this->Form->postLink($state,'/search/'.$key,['data' => ['zip' => $key]]);?>
                                </li>
                        <?php }?>
                        
                    </ul>
                   
                </div>
            </div>
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-user"></i> <?= __("Specialty"); ?></h3>
                    <ul>
                       <?php
                            $output = array_slice($specialities, 0, 8, true);  
                            foreach ($output as $key => $speciality) { ?>
                            <li>
                                    <!-- <a href="<?= $this->Url->build([
                                        'controller' => 'search', 
                                        'action' => 'index', 
                                        '?' => ['term' =>  $speciality]]); ?>">
                                        <?= $speciality; ?>
                                    </a> -->
                                    <?php echo $this->Form->postLink($speciality,'/search/'.$speciality,['data' => ['term' => $speciality]]);?>
                                </li>
                        <?php }?>
                       
                    </ul>
                    <div class="view-all-list">
                        <a href="#" data-toggle="modal" data-target="#mySpecialModel"><?= __("View all"); ?></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-credit-card"></i><?php echo __('Insurance');?></h3>
                    <ul>
                        <?php 
                            $output = array_slice($insurances, 0, 7, true);
                            unset($output[1]);  
                            foreach ($output as $key => $insurance) { ?>
                                <li>
                                <!-- <a href="<?= $this->Url->build([
                                    'controller' => 'search', 
                                    'action' => 'index', 
                                    '?' => ['insurance' =>  $key]]); ?>">
                                    <?= $insurance; ?>
                                </a> -->
                                <a href="/insurance/<?= $key; ?>"><?= $insurance; ?></a>
                                <?php //echo $this->Form->postLink($insurance,'/search/'.$key,['data' => ['insurance' => $key]]);?>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="view-all-list">
                        <a href="#" data-toggle="modal" data-target="#myInsuranceModel"><?= __("View all"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="favourite" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p><?= __("Are you sure you want this doctor your favourite doctor "); ?> ?</p>
      </div>
      <div class="modal-footer">
        <?php
        echo $this->Form->create('', [
            'url' => ['controller' => 'Doctors', 'action' => 'makeFavourite'],
            'type' => 'post',
            'id' => 'make-favourite',
            'novalidate' => true
                ]
        );
        ?> 
        <?php echo $this->Form->control('doctor_id',['value' => '' ,'type' => 'hidden','id' => 'doctor_id_favourite']);?>
        <?php echo $this->Form->control('patient_id',['value' => $this->request->getSession()->read('Auth.User.id') ,'type' => 'hidden']);?>
        <button type="submit" class="btn btn-default"><?= __("Yes"); ?></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("No"); ?></button>
        </form>
      </div>
    </div>

  </div>
</div>



<?= $this->element('Modals/login'); ?>

<script src="js/map.js"></script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWkTTPnQQzOzNsLAirtAWh1v1-pds2yZk">
</script>

<?php 
    echo '<script> var selectedSlot=' . json_encode($this->request->session()->read('Config.SelectedSlot')) . '</script>';
    if($this->request->getQuery('old_patient')) {
        echo '<script>var old_patient=true</script>';
    } else {
        echo '<script>var old_patient=false</script>';
    }
    echo '<script> var booking=' . json_encode($booking) . '</script>';
    
?>

<?= $this->Html->script([
     'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js',
    'search',
],['block'=> true]); ?>

<style type="text/css">
    .time_table ul {
        margin-left: 0;
        padding-left: 0;
    }
    .time_table ul li {
        width: auto;
    }

    .time_table a {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #0b0b46;
        border-radius: 6px;
        display: block;
        font-size: 14px;
        height: 45px;
        line-height: 11px;
        margin: 5px;
        padding: 18px;
        color: #0b0b46;
        font-weight: 600;
        width: 79px;
    }
    .more-link a {
        background: #19CA68;
        border: 1px solid #19CA68;
        color: #fff;
    }
    .checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
        margin-left:0
    }
    @media (max-width: 767px) {
        .mob_time_table ul {
            min-height: 32px;
            overflow-x: auto;
            overflow-y: hidden;
            width: 100%;
            white-space: nowrap;
        }
        .mob_time_table ul > li {
            float: none;
        }
    }
</style>
<?php $this->Html->scriptStart(['block' => true]); ?>
        //<script>
        // Rating Script(Star Rating Custom Attributes)
        $(".rating").rating({
            'showCaption': false,
            'showClear': false,
            'size':'xxs'
        });

    //</script>
    <script>
    $(window).scroll(function() {
    var height = $(window).scrollTop();

    if(height  > 725 ) {
             $(".time_table_calender").addClass("fixed-calender");
    }
    if(height  < 725 ) {
             $(".time_table_calender").removeClass("fixed-calender");
             
    }
});
    </script>
<?php $this->Html->scriptEnd(); ?>
<?= $this->Element('Modals/speciality') ?>
<?= $this->Element('Modals/insurance') ?>
<?= $this->element('Modals/book_appointment_search'); ?>
