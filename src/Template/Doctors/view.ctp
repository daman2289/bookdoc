<?php use Cake\Core\Configure; ?>
<?php use Cake\I18n\Time; ?>
<?=
$this->Html->css([
    'viewbox',
    'rating-star'
]);
?>

<?php use Cake\Utility\Hash; ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<div class="doctor-details-container">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="doctor_detail_part bg-white ">
                    <?php if ($this->request->getSession()->check('Auth.User')) {
                        if(!empty($favourite)) {
                            echo $this->Html->link('<i class="fa fa-heart" aria-hidden="true"></i>',
                                'javascript:void(0)',
                                [
                                    'escape' => false,
                                    'class' => 'favt-doctor',
                                    'title' => __('Remove favourite'),
                                    'onclick' => 'removeFav()',
                                    'id' => 'alreadFav',
                                    'data-id' => base64_decode($this->request->params['pass'][0])

                                ]
                            );
                        } else {
                            echo $this->Html->link('<i class="fa fa-heart-o" aria-hidden="true"></i>',
                                'javascript:void(0)',
                                [
                                    'escape' => false,
                                    'class' => 'favt-doctor',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#favourite',
                                    'title' => __('Make it favourite')
                                ]
                            );
                        } 
                    
                    } else {
                        echo $this->Html->link('<i class="fa fa-heart-o" aria-hidden="true"></i>',
                                'javascript:void(0)',
                                [
                                    'escape' => false,
                                    'class' => 'favt-doctor',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#loginModel',
                                    'title' => __('Make it favourite')
                                ]
                            );
                    }
                    $addClass = "";
                    if ($doctor->is_sponsored) {
                        $addClass = 'sponsored-img'; 
                    }
                    ?>
                    <div class="doctor_img <?= $addClass ?>">
                        <?php 
                            $isFemale = false;
                            if ($doctor->user_profile->gender == 'Female') {
                                $isFemale = true;
                            }
                        ?>
                        <?php echo $this->Html->image($this->User->checkAndGetImage($doctor->user_profile->profile_pic, $isFemale), [
                            'class' => 'doctor-pic-custom-dimensions'
                        ]);
                        ?>
                        
                    </div>
                    <div class="doctor_details">
                        <?php if ($doctor->is_sponsored) { ?>
                            <h3 class="sponsers_tag"><?= __("Sponsored Result"); ?></h3>
                        <?php }?>
                        <h2 class="doctorname">
                            <?php echo __('Dr.') . ' ' . $doctor->user_profile->full_name; ?>
                        </h2>
                        <div><?php echo $this->User->getCommaSeparatedUserSpecialities($doctor->user_specialities); ?></div>
                        <h5 class="doctor-details-age">
                            <?php echo $this->User->getUserAge($doctor->user_profile->dob) . ' ' . __('yrs. old'); ?>
                        </h5>
                        <div class="groupstar">
                            <?php $rating_doctor = Hash::extract($rating, '{n}.overall_rating');
                            if(empty($rating_doctor)) {
                                $overall_rating_doctor = 0;
                            } else {   
                                $overall_rating_doctor = array_sum($rating_doctor) / count($rating_doctor);
                            }?>
                            <input type="text" class="rate_overall rating myrate" data-disabled="true" name="overall_rating" value="<?= $overall_rating_doctor?>">
                        </div>
                        <div class="deatils"><?php echo $doctor->user_profile->professional_statement; ?></div>
                        <div class="add_map"><i class="icon fa fa-map-marker"></i> <?php echo $doctor->user_profile->address . ' ' . $doctor->user_profile->zipcode; ?></div>
                        <?php
                            if (!$images->isEmpty()) { 
                                echo $this->element('Doctors/gallary',[
                                    'images' => $images,
                                ]);       
                         } ?>
                    
                    </div>
                </div>
                <div class="about-doctor-details bg-white">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('Address'); ?></h3>
                                <p><?php echo $doctor->user_profile->address . ' ' . $doctor->user_profile->zipcode; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('Specializations'); ?></h3>
                                <p><?php echo $this->User->getCommaSeparatedUserSpecialities($doctor->user_specialities); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('Education'); ?></h3>
                                <?php
                                    $userEducations = $this->User->getUserEducationsToShowInStringFormat($doctor->user_educations);
                                    if (!empty($userEducations)):
                                        echo $userEducations;
                                    else:
                                        echo __('N/A');
                                    endif;
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('Language'); ?></h3>
                                <p><?php echo $this->User->getCommaSeparatedUserLanguages($doctor->user_languages); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('Hospital Affiliation'); ?></h3>
                                <?php
                                    $userHospitalAffiliations = $this->User->getUserHohspitalAffiliationsToShowInStringFormat($doctor->user_hospital_affiliations);
                                    if (!empty($userHospitalAffiliations)):
                                        echo $userHospitalAffiliations;
                                    else:
                                        echo __('N/A');
                                    endif;
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="about-doctor-details-inner">
                                <h3><?php echo __('In Network Insurance'); ?></h3>
                                <p><?php echo $this->User->getCommaSeparatedUserInsurances($doctor->user_insurances); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="location_map" id="location_map">
                                <div class="map_frame" id="map" data-spam="<?= $is_spammer; ?>" data-doc="<?= $doctor->user_profile->full_name; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="doctor-detail-right">
                    <div class="time_table_calender">
                        <div class="calender">
                            <?php 
                                $now = new DateTime('NOW');
                                

                                // $timeObj = new DateTime('NOW');
                                $currentDay = $now->format('N');

                                $ulcolsed = true; 
                                for($i=0; $i < 30; $i++) {                                    
                                    if($i % 3 == 0) {
                                        echo '<ul class="dates">';
                                        $ulcolsed = false;
                                    }                                    
                            ?>
                                    <li class="text-center">
                                        <?= __($now->format('D')) ?>
                                        <br> <?= __($now->format('F')) ?> <?= __($now->format('j')) ?>
                                    </li>
                                    <?php
                                        $timeObj = Time::now();

                                        $next15Days[$i] = [
                                            "weekday" => $now->format('N') , 
                                            "timeObj" => $timeObj->modify("+{$i} days")
                                        ]; 
                                        
                                        $arr = $now->format('N'); 
                                        $currentDay = $now->format('N');
                                        if($i != 0 && ($i+1)%3 == 0) {
                                            echo '</ul>';
                                            $ulcolsed = true;
                                        }
                                        $now->modify("1 day");

                                }
                                    if(!$ulcolsed) {
                                        echo '</ul>';
                                    }
                                    ?>
                                <a href="javascript:void(0)" onclick="next(this)" class='calender-arrow next-arrow' data-current='0'><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" onclick="prev(this)" class='calender-arrow prev-arrow' data-current='0'><i class="fa fa-chevron-left" aria-hidden="true" style="opacity:0.5"></i></a>
                            
                        </div>
                    </div>
                    <div class="doctor_time_Table">
                        <div class="time_table">
                            <?php
                                $days = $this->General->getDoctorTimings($doctor->doctor_availability);
                                $html = $content = "";
                                $hasSlots = false;
                                $uavblSlots = $this->DayAvailability->unavailableSlots($doctor->doctor_unavailability);
                                foreach ($next15Days as $key => $value) {
                                    if($key % 3 == 0) {
                                            echo '<div class="row times" >';
                                            $divcolsed = false;
                                    }

                                     $content .= $this->element('/AvailabilityTimings/timings',[
                                            'times' => (!empty($days[$value['weekday']])) ? $days[$value['weekday']] :[] ,
                                            'timeDay'  => $value,
                                            'doctor' => $doctor,
                                            'unavailableTime' => $uavblSlots
                                        ]);
                                     if (!$hasSlots && !empty($days[$value["weekday"]])) {
                                        $hasSlots = true;
                                     }

                                    if($key != 0 && ($key+1) % 3 == 0) {
                                        $availableDay = $this->DayAvailability->nextAvailableDay($value, $days);
                                        
                                        if (!$hasSlots && !is_null($availableDay)) {
                                            echo "<h5>Doctor will be available on {$availableDay}</h5>";
                                        } else {
                                            echo $content;
                                        }

                                        $content = "";

                                        $hasSlots = false;
                                        
                                                echo '</div>';
                                                $divcolsed = true;
                                    }
                                }

                                if(!$divcolsed) {
                                    echo '</div>';
                                }
                            ?>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('Doctors/reviews',[
     'overall_rating_doctor' => $overall_rating_doctor,
]); ?>


   

<!-- Dynamic Data -->
                           
</div>
<?= $this->element('Modals/favorite'); ?>
<?= $this->element('Modals/spammer'); ?>
<?= $this->element('Modals/login'); ?>
<?= $this->element('Modals/book_appointment'); ?>
<?php 
    echo '<script> var selectedSlot=' . json_encode($this->request->session()->read('Config.SelectedSlot')) . '</script>';
    if($this->request->getQuery('old_patient')) {
        echo '<script>var old_patient=true</script>';
    } else {
        echo '<script>var old_patient=false</script>';
    }
    echo '<script> var booking=' . json_encode($booking) . '</script>';
    
?>
<?php
    $this->Html->scriptStart(['block' => 'script']);
?>
        var location_latitude = <?php echo $doctor->user_profile->lat; ?>;
        var location_longitude = <?php echo $doctor->user_profile->lng; ?>;
        var doctorId = <?php echo $doctor->id; ?>;

<?php
    $this->Html->scriptEnd(); 
    echo $this->Html->script([
        'frontend/Doctor/view',
        'https://maps.googleapis.com/maps/api/js?libraries=places&key=' . GOOGLE_MAP_API_KEY . '&callback=initMap',
        'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js',
        'viewbox.min.js'
        ], ['block' => 'script']);
?>
<style type="text/css">
    .time_table ul {
        margin-left: 0;
        padding-left: 0;
    }
    .time_table ul li {
        width: auto;
    }

    .time_table a {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #0b0b46;
        border-radius: 6px;
        display: block;
        font-size: 14px;
        height: 45px;
        line-height: 11px;
        margin: 5px;
        padding: 18px;
            color: #0b0b46;
            font-weight: 600;
            width: 79px;
    }
    .more-link a {
      background: #19CA68;
    border: 1px solid #19CA68;
    color: #fff;
    }
    .checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
    margin-left:0
}
</style>

   
<?php $this->Html->scriptStart(['block' => true]); ?>
        //<script>
        // Rating Script(Star Rating Custom Attributes)
        $(".myrate").rating({
            'showCaption': false,
            'showClear': false,
            'size':'xxs'
        });
    //</script>
<?php $this->Html->scriptEnd(); ?>
