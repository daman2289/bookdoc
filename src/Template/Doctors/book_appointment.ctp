<div class="container conformation-page">
    <div class="col-sm-6">
        <h1> <?= __("Your appointment is booked!"); ?> </h1>
        <p class="schedule-message"><?= __("You are scheduled with"); ?> 
            <b><?= __("Dr"); ?>. <?= $bookingData->user->user_profile->full_name; ?></b><?= __(" for a ")?>  
            <b><?= $bookingData->illness_reason->title_eng; ?></b> on </br>
            <b> <?= $bookingData->booking_date->i18nFormat('yyyy-MM-dd'); ?> 
                <?= $bookingData->booking_time->i18nFormat('HH:mm'); ?> <?= __("CEST"); ?></i>
            </b>
        </p>
    </div>
    <div class="col-sm-5 col-sm-offset-1 bg-gray-conformation">
        <div class="col-sm-6">
            <?php 
                $isFemale = false;
                if ($bookingData->user->user_profile->gender == 'Female') {
                    $isFemale = true;
                }
                echo $this->Html->image($this->User->checkAndGetImage($bookingData->user->user_profile->profile_pic, $isFemale), [
                    'class' => 'doctor-pic-custom-dimensions'
                ]);
            ?>

            <div class="doctor-name"><?= __("Dr")?>. <?= $bookingData->user->user_profile->full_name; ?></div>
            <div class="specialtys">
                <?php
                    $special = [];
                    foreach ($bookingData->user->user_specialities as $eachSpeciality) {
                        array_push($special, $eachSpeciality->speciality->title_eng);
                    }
                    echo $this->Text->toList($special);

                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="appointment-detail">
                <div class="small-label"><?= __('Appointment') ?></div>
                <div class="apointemnt-time"><?= $bookingData->booking_date->i18nFormat('yyyy-MM-dd'); ?> <?= $bookingData->booking_time->i18nFormat('HH:mm'); ?> <?= __("CEST")?></div>
                <div class="small-label"><?= __('Patient') ?></div>
                <div class="patient-name"><?php echo $bookingData->patient_first_name;  echo ' '.$bookingData->patient_last_name ?></div>
                <div class="small-label"><?= __('Visit reason') ?></div>
                <div>
                    <?= $bookingData->illness_reason->title_eng; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?=  $this->element('common/push_notification'); ?>
