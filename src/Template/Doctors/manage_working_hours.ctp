<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('Doctor/Add Working Hours') ?></h2>
        </div>
    </div>
    <div class="edit-form manage-calender">
    	        <?php 
                echo $this->Form->create('', [
                    'url'   => ['controller' => 'Doctors', 'action' => 'addWorkingHours'],
                    'type'  => 'post',
                    'id'    => 'add_working_hour_form'
                    ]
                );
                $timingOptions = $this->General->getTimings();
        ?> 
        <div class="form-group">
            <div class="col-md-4">
                <label class="control-label"><?php echo __('Select Day'); ?><span class="red-star">*</span></label>
                    <?php
                        echo $this->Form->control('weekday', array(
                            'class' => 'form-control',
                            'label' => false,
                            'options' => ['1' => 'Monday', '2' => 'Tuesday','3' => 'Wednesday','4' => 'Thursday','5' => 'Friday','6' => 'Saturday','7' => 'Sunday']
                            )
                        );
                    ?>
            </div>
            <div class="col-md-4">
                <label class="control-label"><?php echo __('Available From'); ?><span class="red-star">*</span></label>
                    <?php
                        echo $this->Form->control('available_from', array(
                            'class' => 'form-control',
                            'label' => false,
                            'options' => $timingOptions
                            )
                        );
                    ?>
            </div>
            <div class="col-md-4">
                <label class="control-label"><?php echo __('Available To'); ?><span class="red-star">*</span></label>
                    <?php
                        echo $this->Form->control('available_upto', array(
                            'class' => 'form-control',
                            'label' => false,
                            'options' => $timingOptions
                            )
                        );
                    ?>
            </div>
        </div> 
        <div class="form-group save-btn col-xs-12">
            <button type="submit" class="btn"><?= __("Save");?></button>
        </div>
        </form>
    </div>
    <div class="edit-form manage-calender">
                <?php 
                if(!empty($duration)) {
                    $timeDuration = $duration;
                } else {
                    $timeDuration = '';
                }
                echo $this->Form->create($timeDuration, [
                    'url'   => ['controller' => 'Doctors', 'action' => 'addDuration'],
                    'type'  => 'post',
                    'id'    => 'add_duration_form'
                    ]
                );

                if(!empty($duration)) {
                    echo $this->Form->control('id',['type' => 'hidden','value' => $timeDuration->id]);
                } 
               
        ?> 
        <div class="form-group">
            <div class="col-md-4">
                <label class="control-label"><?php echo __('Duration'); ?><span class="red-star">*</span></label>
                    <?php
                        echo $this->Form->control('duration', array(
                            'class' => 'form-control',
                            'label' => false,
                            'options' => $this->General->getDuration()
                            )
                        );
                    ?>
            </div>
        </div> 
        <div class="form-group save-btn col-xs-12">
            <button type="submit" class="btn"><?= __("Save");?></button>
        </div>
    </form>
    </div>
    <div class="right-sidebar bg-white">
        <div class="row">
            <div class="col-sm-12">
                <div class="top-tilte-bar">
                    <div class="heading-tilte">
                        <h2><?= __("Availability Timings")?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="edit-form dashbroad-content">
            <div class="row">
                <div class="col-sm-12 mobile-res-tb">
                    <table class="table table-striped past-appointment  ">
                        <thead>
                            <tr>
                                <th><?= __("Weekday"); ?></th>
                                <th><?= __("Available From");?></th>
                                <th><?= __("Available Upto"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($timings)) {
                                foreach ($timings as $key => $time) { 
                                    if($time->weekday == 1) {
                                        $day = 'Monday';
                                    } elseif ($time->weekday == 2) {
                                        $day = 'Tuesday';
                                    } elseif ($time->weekday == 3) {
                                        $day = 'Wednesday';
                                    } elseif ($time->weekday == 4) {
                                        $day = 'Thursday';
                                    } elseif ($time->weekday == 5) {
                                        $day = 'Friday';
                                    } elseif ($time->weekday == 6) {
                                        $day = 'Saturday';
                                    } else {
                                        $time = 'Sunday';
                                    }
                                ?>
                              <tr>
                                  <td><?php echo $day ?></td>
                                  <td><?php echo $time->available_from ?></td>
                                  <td><?php echo $time->available_upto ?></td>
                              </tr>      
                            <?php 
                        } }?>
                        </tbody> 
                    </table>
                </div>
                <div class="col-sm-12 pagination-inner">
                    <?= $this->element('common/paginator') ?>
                </div>
            </div>
        </div> 
    </div>
</div>
<?php $this->Html->script([
    "calendar/manage_hours"
],['block' => true])?>