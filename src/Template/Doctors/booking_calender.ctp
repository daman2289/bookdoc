<div class="right-sidebar bg-white">
    <?= $this->element('Doctors/calender'); ?>
    <?php echo $this->Html->css([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css' 
    ]);

    ?>
</div>
    
    <script type="text/javascript">
        var duration = "<?php echo $duration->duration; ?>";
        var notification = {
            'data' :  {
                title: false,
                width: "100%",
                hide: false,
                stack: {
                    "dir1": "down", 
                    "dir2": "left", 
                    "push": "top", 
                    "spacing1": 0, 
                    "spacing2": 0, 
                    "firstpos1": 0, 
                    "firstpos2": 0
                },
                styling: "bootstrap3",
                // addclass: "ui-pnotify-inline",
                buttons: {
                    sticker: false
                }
                
            }
        };
        
      var base_url = "<?= $this->Url->build('/', true); ?>";
      // Client ID and API key from the Developer Console
      var CLIENT_ID = '120958547845-upspc5bagtiv0hbuc8ouke041mpjk7bf.apps.googleusercontent.com';
      var API_KEY = 'AIzaSyBEfWq8znBLidHMpz81bv4Ckz2jmtRVucs';
      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

      // Authorization scopes required by the API; multiple scopes can be
      // included, separated by spaces.
      var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

      var authorizeButton = document.getElementById('authorize_button');
      var signoutButton = document.getElementById('signout_button');
      /**
       *  On load, called to load the auth2 library and API client library.
       */
      function handleClientLoad() {
        gapi.load('client:auth2', initClient);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      function initClient() {
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(function () {
            console.log('daman');
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
          authorizeButton.onclick = handleAuthClick;
          signoutButton.onclick = handleSignoutClick;
        });
      }

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
          authorizeButton.style.display = 'none';
          signoutButton.style.display = 'block';
          listUpcomingEvents();
//location.reload();
        } else {
          authorizeButton.style.display = 'block';
          signoutButton.style.display = 'none';
        }
      }

      /**
       *  Sign in the user upon button click.
       */
      function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
      }

      /**
       *  Sign out the user upon button click.
       */
      function handleSignoutClick(event) {
        gapi.auth2.getAuthInstance().signOut();
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
      function appendPre(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      }

      /**
       * Print the summary and start datetime/date of the next ten events in
       * the authorized user's calendar. If no events are found an
       * appropriate message is printed.
       */
      function listUpcomingEvents() {
        gapi.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 10,
          'orderBy': 'startTime'
        }).then(function(response) {
          var events = response.result.items;
          console.log(events);          
          if (events.length > 0) {
            
             
        var url = "/api/doctors/addCalendar.json";    
          $.ajax({
              type: "POST",
              url: url,
              dataType : 'json',
              data : {'event' : events},             
              success: function (response) {
                     notification.data.text  = '<div class="text-center calmsg">' + response.response.message + '</div>';
                    new PNotify(notification.data);
               gapi.auth2.getAuthInstance().signOut();
               location.reload();
             },
        });   

           
            
          } else {
            //appendPre('No upcoming events found.');
          }
          // process the form
            
      });
      }


    </script>

    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
<style>
.calmsg.alert-warning {
    color: #fff;
    background-color: #AFF29A;
    border-color: #35DB00;
}
</style>