<div class="container">
	<h2>Kontaktieren Sie uns</h2>

	<address>
		Bookdoc GmbH<br>
		Margaretengürtel 18/1/B4<br>
		1050 Wien<br>
		Tel: 0800 66 55 91<br>
		Email : <a href="mailto:Office@bookdoc.at">Office@bookdoc.at</a>
	</address>
	<div class="col-xs-12">
		<?php echo $this->Form->create('',['type' => 'post','class' => 'form-horizontal','id' => 'contact_form']); ?>
			<div class="form-group form-group-lg">
				<label class="control-label" for="name">Name
					<span class="text-danger">*</span>
				</label>
				<input type="text" name= "name" class="form-control" id="name">
			</div>
			<div class="form-group form-group-lg">
				<label class="control-label" for="email">Email
					<span class="text-danger">*</span>
				</label>
				<input type="email" name="email" class="form-control" id="email">
			</div>
			<div class="form-group form-group-lg">
				<label class="control-label" for="telephone">Telefon:</label>
				<input type="tel" name="telephone" class="form-control" id="telephone">
			</div>
			<div class="form-group form-group-lg">
				<label class="control-label" for="comment">Kommentar
					<span class="text-danger">*</span>
				</label>
				<textarea class="form-control" name="message" rows="4"></textarea>
			</div>
			<hr/>
			<div class="form-group">
				<button type="submit" class="btn btn-lg btn-danger">ABSENDEN</button>
			</div>
		</form>
	</div>
</div>
<?php echo $this->Html->script([
    'frontend/User/contact'
        ], ['block' => 'script']);
?>

