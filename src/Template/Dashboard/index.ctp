<?=
    $this->Html->css('search');
    
?>
<div class="header_section">
    <!-- top header -->
    <?php echo $this->element('front/top_header');?>
    <div class="headinglogin">
    </div>
    <div class="finddoctor">
        <div class="container">
            <div class="row bg-midle">
                <h2><?= __("Find your Best Health solution near you"); ?></h2>
                <div class="searchdiv">
                   <?php echo $this->Form->create(null,[
                        'type' => 'post',
                        'url' => ['controller' => 'search','action' => 'index'],
                        'autocomplete' => 'off'
                    ]); ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="searchbox autocomplete">
                                <?= $this->Form->control('term',[
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    "id" => "myInput",
                                    'value' => $this->request->getData('term'),
                                    'placeholder' => __('Speciality/Doctor name')
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="searchbox">
                                <?= $this->Form->control('zip',[
                                    'type' => 'text',
                                    'label' => false,
                                    'maxlength' => 4,
                                    'class' => 'form-control',
                                    'value' => $this->request->getData('zip'),
                                    'placeholder' => __('Zip Code')
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="searchbox">
                                <?= $this->Form->control('insurance',[
                                    'type' => 'select',
                                    'label' => false,
                                    'class' => 'form-control blue_color',
                                    'value' => $this->request->getData('insurance'),
                                    'option' => $insurances,
                                    'empty' => __('I’ll choose my insurance later')
                                ])
                                ?>
                                <?= $this->Form->button('<i class="ti-search white_icon"></i>',[
                                    'class' => 'submit_btn',
                                    'escape' => false,
                                    'type' => 'submit',
                                ]); 
                                ?>
                                <?= $this->Form->button('Search',[
                                    'class' => 'submit_btn-mob',
                                    'escape' => false,
                                    'type' => 'submit',
                                ]); 
                                ?>
                            </div>
                        </div>
                    <?= $this->Form->end(); ?>
                </div>

                <div class="one-page-bottom-section hidden-lg hidden-md hidden-sm">

                <div class="col-sm-12 links-mobile">
                      <ul class="list-inline">
                                <li><a href="/terms-conditions"><?php echo __('Terms & conditions'); ?></a></li>
                                <li><a href="/trust-safety"><?php echo __('Trust & Safety'); ?></a></li>
                                <li><a href="/privacy-policy"><?php echo __('Privacy Policy'); ?></a></li>
                                <li><a href="/careers"><?php echo __('Careers'); ?></a></li>
                                <li><a href="/press"><?php echo __('Press'); ?></a>
                                <li><a href="/about"><?php echo __('About Us'); ?></a>
                                </li>
                            </ul>
                </div>
                 <div class="">

                <div class="copyright">
                    © 2018 <?php echo  __('Bookdoc. All Rights Reserved.'); ?>
                </div>

            </div>
        </div>


            </div>
        </div>
    </div>
</div>
<div class="app-download blue-bg hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="section_heading">
                    <h1><?= __("Get the Bookdoc app"); ?></h1>
                    <h2><?= __("We've got you covered"); ?></h2>
                </div>
                <div class="section_body pr-130 pr-sm-0 pr-xs-0">
                    <div class="features">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feat_box">
                                <div class="fea_icon"><img src="img/frontend/icon01.png"></div>
                                <div class="fea_content"><?= __("We cover different")?></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feat_box">
                                <div class="fea_icon"><img src="img/frontend/icon02.png"></div>
                                <div class="fea_content"><?= __("Verfied Professional"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="feat_box">
                                <div class="fea_icon"><img src="img/frontend/icon03.png"></div>
                                <div class="fea_content"><?= __("Safe and Secure"); ?>
                                    <br> <?= __("Appointments "); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="playstores">
                        <a href="" target="_blank"><img src="img/frontend/googleplay.png"></a>
                        <a href="" target="_blank"><img src="img/frontend/appstore.png"></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 d-none-mob">
                <div class="app-mobile-screen">
                    <?php echo $this->Html->image('frontend/mockup.png'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- find doctor -->
<section class="find  hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-heading">
                    <h2><?= __("Find doctors and dentists by"); ?>...</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-location-pin"></i> <?= __("State")?></h3>
                    <ul>
                        <?php
                            foreach ($state_name as $key => $state) { ?>
                                <li>
                                    <!-- <a href="<?= $this->Url->build([
                                        'controller' => 'search', 
                                        'action' => 'index', 
                                        '?' => ['zip' =>  $key]]); ?>">
                                        <?= $state; ?>
                                    </a> -->
                                    <a href="/location/<?= $state; ?>"><?= $state; ?></a>
                                    <?php //echo $this->Form->postLink($state,'/search/'.$key,['data' => ['zip' => $key]]);?>
                                </li>
                        <?php }?>
                        
                    </ul>
                    <!-- <div class="view-all-list">
                        <a href="#">View all</a>
                    </div> -->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-user"></i> <?= __("Specialty"); ?></h3>
                    <ul>
                        <?php
                            $output = array_slice($specialities, 0, 8, true);  
                            foreach ($output as $key => $speciality) { ?>
                                <li>
                                    <a href="/speciality/<?= $key; ?>"><?= $speciality; ?></a>
                                    <!-- <?php echo $this->Form->postLink($speciality,'/search/'.$speciality,['data' => ['term' => $speciality]]);?> -->
                                </li>
                        <?php }?>
                    </ul>
                    <div class="view-all-list">
                        <a href="#" data-toggle="modal" data-target="#mySpecialModel">
                            <?= __("View all"); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="find-list-container">
                    <h3><i class="ti-credit-card"></i><?= __("Insurance"); ?> </h3>
                    <ul>
                        <?php
                            $output = array_slice($insurances, 0, 16, true);
                            unset($output[1]);  
                            foreach ($output as $key => $insurance) { ?>
                            <li>
                                <!-- <a href="<?= $this->Url->build([
                                    'controller' => 'search', 
                                    'action' => 'index', 
                                    '?' => ['insurance' =>  $key]]); ?>">
                                    <?= $insurance; ?>
                                </a> -->
                                <a href="/insurance/<?= $key; ?>"><?= $insurance; ?></a>
                                <?php //echo $this->Form->postLink($insurance,'/search/'.$key,['data' => ['insurance' => $key]]);?>
                            </li>
                        <?php }?>
                    </ul>
                    <div class="view-all-list">
                       <a href="#" data-toggle="modal" data-target="#myInsuranceModel"><?= __("View all"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- five star doctor -->
<section class="five-star-dr blue-bg  hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="page-heading">
                    <h2><?= __("Are you a five-star doctor"); ?>?</h2>
                    <p> <?= __("List your practice to reach millions of patients"); ?>.</p>
                </div>
                <div class="benefits">
                    <ul>
                        <li><i class="ti-check"></i> <?= __("Attract and engage new patients"); ?></li>
                        <li><i class="ti-check"></i> <?= __("Build and strengthen your online reputation"); ?></li>
                        <li><i class="ti-check"></i> <?= __("Deliver a premium experience patients love"); ?></li>
                    </ul>
                </div>
                <div class="list-of-practice">
                    <?php
                    echo $this->Html->link( 
                        __('List your practice on Bookdoc').'<i class="ti-angle-right"></i> ', [
                            'controller' => 'Users', 
                            'action' => 'signUp'
                        ], ['escape' => false]
                    );
                    ?>
                </div>
            </div>
            <div class="col-sm-4 d-none-mob">
                <div class="dr-img">
                    <?php echo $this->Html->image('frontend/five-star-dr.png'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonials -->
<section class="testimonials  hidden-xs">
    <div class="container-fluid">
        <!--  <div class="col-md-5 col-sm-6">
            <div class="join" style="background-image:url(img/frontend/joingroup_bg.jpg);">
                <h2>Best Health services at <br/>Resonable Prices</h2>
                <p>Need help? <br/>Visit our Knowledge base</p>
                <div class="join-us">

                    <?php
                    echo $this->Html->link(__('Join us for care'), [
                        'controller' => 'Users', 
                        'action' => 'signUp'], 
                        ['escape' => false]
                    );
                    ?>
                </div>
            </div>
        </div> -->
        <div class="col-md-6 col-sm-6 p-0">
            <div class="joingroup">
                <header class="heading"><?= __("BEST"); ?> 
                    <span class="bold_font">
                        <?= __("HEALTH SERVICES"); ?>
                    </span> 
                        <?= __("AT RESONABLE"); ?> 
                    <span class="bold_font">
                        <?= __("PRICES"); ?>
                    </span>
                </header>
                <h2 class="txt2">
                    <?= __("Need help"); ?>?  <br> <?= __("Visit our Knowledge base"); ?>
                </h2>
                <div class="buttons join-us-btn">
                    <a href="<?= $this->Url->build([
                        'controller' => 'Users', 
                        'action' => 'signUp'])?>">
                        <?= "Join Us for Care"; ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 p-0">
            <div class="testimonials-slider testimonialss">
                <div class="page-heading">
                    <h2><?= __("Patient Speak’s"); ?></h2>
                </div>
                <div class="qoute">
                    <i class="ti-quote-left"></i>
                </div>
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <p><?= __("I initially visited to try and resolove a longstanding calf problem that keps reoccurring after running. By working"); ?>...</p>
                        <div class="author-info">
                            <h3><?= __("Mr David Landon"); ?></h3>
                            <span><?= __("Osteopathy"); ?></span>
                        </div>
                    </div>
                    <div class="item">
                        <p><?= __("I initially visited to try and resolove a longstanding calf problem that keps reoccurring after running. By working");?>...</p>
                        <div class="author-info">
                            <h3><?= __("Mr David Landon"); ?></h3>
                            <span><?= __("Osteopathy")?></span>
                        </div>
                    </div>
                    <div class="item">
                        <p><?= __("I initially visited to try and resolove a longstanding calf problem that keps reoccurring after running. By working"); ?>...</p>
                        <div class="author-info">
                            <h3><?= __("Mr David Landon"); ?></h3>
                            <span><?= __("Osteopathy"); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->Element('Modals/speciality') ?>
<?= $this->Element('Modals/insurance') ?>
<script type="text/javascript">    
    var lng = '<?php echo  ($this->request->getSession()->read("Config.locale")== "en_DE" )? "de":"en" ?>';
</script>
<?php
    echo $this->Html->script([
        'frontend/home',
        'search'
    ], ['block' => true, 'async']);
?>
<style type="text/css">
    #myInput {
        text-transform: none !important;
    }
</style>