<?php //pr($user->user_profile); exit; ?>
<section class="dashbroad-main">
    <div class="container-fluid">
        <div class="d-flex-container">
            <!-- left sidebar -->
            <div class="dashbroad-left-sidebar">
               <?php echo $this->element('front/my_profile_left_bar'); ?>
               <div class="darshboard-nav">
                  <?php echo $this->element('front/my_side_bar'); ?>
               </div> 
            </div>
            <div class="right-sidebar">
                <div class="bg-white">
                  <div class="top-tilte-bar">
                    <div class="heading-tilte">
                      <h2><?= __('My bio');?> </h2>
                    </div>
                    <div class="heading-title">
                      <i class="ti-pencil-alt"></i>
                      <span><?= __('Edit');?></span>
                    </div>
                  </div>

                   <div class="edit-form">
                       <?php echo $this->Form->create('Users', array('id' => 'edit_bio') ); ?>
                       <div class="row">
                         <div class="col-sm-12">
                            <div class="form-group custom-select-dropdown">
                              <label><?= __('Specialty');?></label>    
                               <div class="select-group">
                                    <i class="ti-angle-down"></i>
                                     <?php
                                           echo $this->Form->select(
                                          'specialty',$specialtyList,
                                            [
                                            	'empty' => 'Select', 
                                            	'class' =>'js-example-basic-multiple form-control',
                                            	'required' => true, 
                                            	'multiple' => true,
                                            	'default' => array_values(json_decode($user->user_profile->specialty))
                                            ]
                                        ); 
                                    ?>
                                </div>
                              </div>
                         </div>
                       </div>
                       <div class="row">
                         <div class="col-sm-12">
                            <div class="form-group">
                              <label><?= __('Education');?></label>
                              <div class="control-group">
                                  <?php
                                  if(!empty($user->user_profile->degree)){
                                  $degree = json_decode($user->user_profile->degree);
	                                  foreach ($degree as $d => $value) {
                                      echo $this->Form->control('degree[]', [
	                                            'class' => 'form-control', 'placeholder' => 'Degree','required' => true , 'label' => false, 'value' =>$value 
	                                        ]);
	                                  }
                              		}else{
	                          			echo $this->Form->control('degree[]', [
	                                            'class' => 'form-control', 'placeholder' => 'Degree','required' => true , 'label' => false
	                                        ]);
                              		}
                                  ?>           
                              </div>
                            </div>
                         </div>
                       </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <div class="input-group control-group after-add-more">
                              <?php
                              if(!empty($user->user_profile->university)){
                                  $university = json_decode($user->user_profile->university);
	                                  foreach ($university as $d => $value) {
	                                    	echo $this->Form->control('university[]', [
	                                            'class' => 'form-control', 'placeholder' => __('University'),'required' => true , 'label'=> false, 'value' => $value
	                                        ]);
                                		}
                                    }else{
                                    	echo $this->Form->control('university[]', [
                                            'class' => 'form-control', 'placeholder' => __('University'),'required' => true , 'label'=> false
                                        ]);
                                    }
                                  ?>
                              <div class="input-group-btn"> 
                                    <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i> <?= __('Add');?></button>
                               </div>            
                            </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group custom-select-dropdown">
                            <label><?= __('In network insurance');?></label>    
                           <div class="select-group">
                                <i class="ti-angle-down"></i>
                               <?php
                                         echo $this->Form->select(
                                        'network_insurance',
                                        ['AL' => 'Aetna', 'WY'=> 'Anthem Blue Cross Blue Shield', 'WY' => 'Blue Cross Blue Shield', 'Cigna' => 'Cigna','UnitedHealthcare Oxford' => 'UnitedHealthcare Oxford' , 'UnitedHealthcare' => 'UnitedHealthcare'],
                                          ['empty' => __('Select'), 'class' =>'js-example-basic-multiple form-control','required' => true, 'multiple' => true, 'default' => array_values(json_decode($user->user_profile->network_insurance))]
                                      ); 
                                    ?>
                            </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group custom-select-dropdown">
                            <label><?= __('language spoken');?></label>    
                           <div class="select-group">
                                <i class="ti-angle-down"></i>
                               <?php
                                        echo $this->Form->select(
                                        'languages_spoken',
                                        ['AL' => 'Englist', 'WY'=> 'Spanish', 'WY' => 'French'],
                                          ['empty' => 'Select', 'class' =>'js-example-basic-multiple form-control','required' => true, 'multiple' => true, 'default' => array_values(json_decode($user->user_profile->languages_spoken))]
                                      ); 
                                    ?>
                            </div>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <?php echo $this->Form->control('professional_statement', ['class' => 'form-control' ,'type' => 'textarea', 'escape' => false, 'value'=>$user->user_profile->professional_statement]); 
                            ?>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label><?= __('Hospital affiliation');?> </label>  
                            <div class="input-group control-group after-add-more-2">
                            <?php
                                if(!empty($user->user_profile->hospital_affiliation)){
                                  $hospital_affiliation = json_decode($user->user_profile->hospital_affiliation);
                                    foreach ($hospital_affiliation as $d => $value) {
                                        echo $this->Form->control('hospital_affiliation[]', [
                                              'class' => 'form-control', 'required' => true , 'label'=> false, 'value' => $value
                                          ]);
                                    }
                                    }else{
                                      echo $this->Form->control('hospital_affiliation[]', [
                                              'class' => 'form-control','required' => true , 'label'=> false
                                          ]);
                                    }
                                ?>
                              <div class="input-group-btn"> 
                                    <button class="btn add-btn add-more-2" type="button"><i class="ti-plus"></i><?= __('Add');?> </button>
                               </div>            
                            </div>
                        </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label><?= __('Award');?></label>  
                            <div class="input-group control-group after-add-more-3">
                            <?php
                                if(!empty($user->user_profile->award)){
                                  $award = json_decode($user->user_profile->award);
                                    foreach ($award as $d => $value) {
                                        echo $this->Form->control('award[]', [
                                              'class' => 'form-control', 'required' => true , 'label'=> false, 'value' => $value
                                          ]);
                                    }
                                    }else{
                                      echo $this->Form->control('award[]', [
                                              'class' => 'form-control','required' => true , 'label'=> false
                                          ]);
                                    }
                                ?>
                              <div class="input-group-btn"> 
                                    <button class="btn add-btn add-more-3" type="button"><i class="ti-plus"></i><?= __('Add');?> </button>
                               </div>            
                            </div>
                        </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label><?= __('Professional membership');?></label>  
                             <?php
                                  echo $this->Form->control('professional_membership', [
                                          'class' => 'form-control','required' => true , 'label'=> false, 'value'=> $user->user_profile->professional_membership
                                      ]);
                                ?>
                        </div> 
                        </div>
                      </div>  
                       <div class="row">
                         <div class="col-sm-12">
                           <div class="save-btn">
                           <?php
                                echo $this->Form->button(__('Save Bio'),
                                    [
                                        'type' => 'submit',
                                        'class' => 'btn'
                                    ]
                                ); ?>
                        </div>
                         </div>
                       </div> 
                       <?php echo $this->Form->end();  ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Copy Fields-These are the fields which we get through jquery and then add after the above input,-->
        <!-- education -->
        <div class="copy-fields hide">
          <div class="control-group input-group">
           <?php
                echo $this->Form->control('degree[]', [
                        'class' => 'form-control add-input', 'placeholder' => 'Degree','required' => true , 'label'=> false
                    ]);
              ?>
            <div class="input-group-btn"> 
              <button class="btn remove" type="button"><i class="ti-close"></i><?= __('Remove');?> </button>
            </div>
          </div>
          <div class="control-group input-group">
            <!-- <input type="text" name="addmore[]" class="form-control add-input" placeholder="University"> -->
            <?php
                echo $this->Form->control('university[]', [
                        'class' => 'form-control add-input', 'placeholder' => 'University','required' => true , 'label'=> false
                    ]);
               ?>
            <div class="input-group-btn"> 
              <button class="btn  remove" type="button"><i class="ti-close"></i><?= __('Remove');?> </button>
            </div>
          </div>
        </div>
        <!-- Hospital affiliation -->
        <div class="copy-fields-2 hide">
          <div class="control-group input-group">
            <!-- <input type="text" name="addmore[]" class="form-control add-input" placeholder="Hospital affiliation"> -->
            <?php
                echo $this->Form->control('hospital_affiliation[]', [
                        'class' => 'form-control add-input', 'placeholder' => 'Hospital affiliation','required' => true , 'label'=> false
                    ]);
               ?>
            <div class="input-group-btn"> 
              <button class="btn remove" type="button"><i class="ti-close"></i><?= __('Remove');?> </button>
            </div>
          </div>
        </div>
         <!-- Awards -->
        <div class="copy-fields-3 hide">
          <div class="control-group input-group">
            <!-- <input type="text" name="addmore[]" class="form-control add-input" placeholder="Awards"> -->
            <?php
                echo $this->Form->control('award[]', [
                        'class' => 'form-control add-input', 'placeholder' => 'Awards','required' => true , 'label'=> false
                    ]);
               ?>
            <div class="input-group-btn"> 
              <button class="btn remove" type="button"><i class="ti-close"></i><?= __('Remove');?> </button>
            </div>
          </div>
        </div>
<?php echo $this->Html->script('backend/common/jquery.validate'); ?> 
<script>
  $(document).ready(function(){
    $("#edit_bio").validate({
      rules:{
        
      },
      messages:{
        
      }
    });
  });
</script>