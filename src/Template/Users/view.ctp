<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Insurances'), ['controller' => 'Insurances', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Insurance'), ['controller' => 'Insurances', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Specialities'), ['controller' => 'Specialities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Speciality'), ['controller' => 'Specialities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Profiles'), ['controller' => 'UserProfiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Profile'), ['controller' => 'UserProfiles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->id, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Persist Code') ?></th>
            <td><?= h($user->persist_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password Reset Token') ?></th>
            <td><?= h($user->password_reset_token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hashval') ?></th>
            <td><?= h($user->hashval) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($user->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Approved') ?></th>
            <td><?= $this->Number->format($user->is_approved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Email Verified') ?></th>
            <td><?= $this->Number->format($user->is_email_verified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($user->is_deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Insurances') ?></h4>
        <?php if (!empty($user->insurances)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title French') ?></th>
                <th scope="col"><?= __('Title Eng') ?></th>
                <th scope="col"><?= __('Title Italic') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Is Deleted') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->insurances as $insurances): ?>
            <tr>
                <td><?= h($insurances->id) ?></td>
                <td><?= h($insurances->title_french) ?></td>
                <td><?= h($insurances->title_eng) ?></td>
                <td><?= h($insurances->title_italic) ?></td>
                <td><?= h($insurances->user_id) ?></td>
                <td><?= h($insurances->status) ?></td>
                <td><?= h($insurances->is_deleted) ?></td>
                <td><?= h($insurances->created) ?></td>
                <td><?= h($insurances->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Insurances', 'action' => 'view', $insurances->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Insurances', 'action' => 'edit', $insurances->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Insurances', 'action' => 'delete', $insurances->id], ['confirm' => __('Are you sure you want to delete # {0}?', $insurances->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Specialities') ?></h4>
        <?php if (!empty($user->specialities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title Eng') ?></th>
                <th scope="col"><?= __('Title French') ?></th>
                <th scope="col"><?= __('Title Italic') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Is Deleted') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->specialities as $specialities): ?>
            <tr>
                <td><?= h($specialities->id) ?></td>
                <td><?= h($specialities->title_eng) ?></td>
                <td><?= h($specialities->title_french) ?></td>
                <td><?= h($specialities->title_italic) ?></td>
                <td><?= h($specialities->status) ?></td>
                <td><?= h($specialities->is_deleted) ?></td>
                <td><?= h($specialities->user_id) ?></td>
                <td><?= h($specialities->created) ?></td>
                <td><?= h($specialities->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Specialities', 'action' => 'view', $specialities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Specialities', 'action' => 'edit', $specialities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Specialities', 'action' => 'delete', $specialities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $specialities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Profiles') ?></h4>
        <?php if (!empty($user->user_profiles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Profile Pic') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->user_profiles as $userProfiles): ?>
            <tr>
                <td><?= h($userProfiles->id) ?></td>
                <td><?= h($userProfiles->user_id) ?></td>
                <td><?= h($userProfiles->first_name) ?></td>
                <td><?= h($userProfiles->last_name) ?></td>
                <td><?= h($userProfiles->phone_number) ?></td>
                <td><?= h($userProfiles->address) ?></td>
                <td><?= h($userProfiles->profile_pic) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserProfiles', 'action' => 'view', $userProfiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserProfiles', 'action' => 'edit', $userProfiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserProfiles', 'action' => 'delete', $userProfiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userProfiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
