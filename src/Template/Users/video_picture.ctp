<?php

use Cake\View\Helper\SessionHelper;

$session = $this->request->getSession();
?>

<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('Picture / Video Upload'); ?></h2>
        </div>
    </div>

    <div class="edit-form upload-sec">
        <div class="row">
        
            <div class="image-sec clearfix">
                <div class="col-sm-12">
                    <h3><?php echo __('Upload Images') ?> </h3>
                </div>
                <div class="col-sm-12">
                    <?php
                    echo $this->Form->create('', [
                        'url' => ['controller' => 'Users', 'action' => 'save-images'],
                        'type' => 'post',
                        'id' => 'save-images',
                        'enctype' => 'multipart/form-data',
                        'novalidate' => true
                        ]
                    );
                    ?>
                    <ul>

                        <?php
                        if ($images->count() > 0) {
                            foreach ($images as $image) {
                                ?> 
                                <li> <i class="ti-close" id="<?= $image->id; ?>"></i>
                                    <?= $this->Html->image($this->User->checkAndGetDoctorImages($image->images)); ?>
                                </li>
                                <?php
                            }
                        } else {

                            $pImg = 'frontend/Dummy-image.jpg';
                            echo "<li>" . $this->Html->image($pImg) . "</li>";
                        }
                        ?>

                    </ul>
                    <div class="file-upload">
                        <label for="upload" class="file-upload__label"> <i class="ti-upload"></i> <?= __("Upload Images"); ?>  </label>
                        <input id="upload"  class="file-upload__input" type="file" name="upload_image[]" multiple>
                        <?php
                        // echo $this->Form->control('upload_image[]', [
                        //         'class' => 'file-upload__input','label'=> false , 'multiple' => true , 'type' => 'file'
                        //     ]);
                        ?>
                    </div>
                    <?php
                    echo $this->Form->button(__('Save Images'), [
                        'type' => 'submit',
                        'class' => 'btn btn-signup'
                            ]
                    );
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>


            <div class="video-sec">
                <div class="col-sm-12">
                    <h3><?php echo __('Upload Video'); ?> </h3>
                </div>
                <div class="col-sm-12">
                    <?php
                        echo $this->Form->create($video, [
                            'url' => ['controller' => 'Users', 'action' => 'video-picture'],
                            'type' => 'post',
                            'id' => 'videoPic',
                            'novalidate' => true
                                ]
                        );
                    ?> 
                    <ul>
                        <li>
                            <?php
                            echo $this->Form->control('video_link_one', [
                                'class' => 'form-control video-group', 'label' => __('Video Link 1')
                            ]);
                            ?>
                        </li>
                        <li>
                            <?php
                            echo $this->Form->control('video_link_two', [
                                'class' => 'form-control video-group', 'label' => __('Video Link 2')
                            ]);
                            ?>
                        </li>
                        <li>
                            <?php
                            echo $this->Form->control('video_link_three', [
                                'class' => 'form-control video-group', 'label' => __('Video Link 3')
                            ]);
                            ?>
                        </li>
                        <li>
                            <?php
                            echo $this->Form->control('video_link_four', [
                                'class' => 'form-control video-group', 'label' => __('Video Link 4')
                            ]);
                            ?>
                        </li>
                        <li>
                            <?php
                            echo $this->Form->control('video_link_five', [
                                'class' => 'form-control video-group', 'label' => __('Video Link 5')
                            ]);
                            ?>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="save-btn">
                                <?php
                                echo $this->Form->button(__('Save Video'), [
                                    'type' => 'submit',
                                    'class' => 'btn'
                                        ]
                                );
                                ?>
                            </div>
                        </div>
                    </div>


                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('video-pictures',['block' => true]); ?>
