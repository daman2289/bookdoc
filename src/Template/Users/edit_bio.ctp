<?php use Cake\Utility\Hash; ?>
<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?= __('My bio'); ?> </h2>
        </div>
        <div class="heading-title">
            <i class="ti-pencil-alt"></i>
          <!--   <span><?= __('Edit'); ?></span> -->
        </div>
    </div>

    <div class="edit-form">
        <?php echo $this->Form->create('Users', array('id' => 'edit_bio')); ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group custom-select-dropdown">
                    <label><?= __('Specialty'); ?></label>    
                    <div class="select-group">
                        <i class="ti-angle-down"></i>
                        <?php
                            $savedSpecialities = Hash::extract($user->user_specialities, '{n}.speciality_id');
                            echo $this->Form->select('user_specialities.speciality_id', $specialtyList, [
                                    'class' => 'js-example-basic-multiple form-control',
                                    'required' => true,
                                    'multiple' => true,
                                    'value' => $savedSpecialities
                                ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                <label><?= __('Education'); ?></label>
                <?php
                    if (!empty($user->user_educations)):
                        $i = 1;
                        foreach ($user->user_educations as $userEducation):
                            ?>
                            <div class="form-group">
                                <div class="copy-fields add-ed">
                                    <div class="control-group two-field ">
                                        <?php
                                            echo $this->Form->control('degree[]', [
                                                'class' => 'form-control deg', 'placeholder' => 'Degree', 'required' => true, 'label' => false, 'value' => $userEducation->degree
                                            ]);
                                        ?>
                                    </div>
                                    <br/>
                                    <div class="input-group control-group two-field ">
                                        <?php
                                            echo $this->Form->control('university[]', [
                                                'class' => 'form-control uni', 'placeholder' => 'University', 'required' => true, 'label' => false, 'value' => $userEducation->university
                                            ]);
                                        ?>
                                        <div class="input-group-btn control-group two-field ">
                                            <div class="remove-btn"> 
                                                    <button class="btn add-more-education add-btn" type="button"><i class="ti-plus"></i></button>
                                                    <button class="btn remove-education add-btn" type="button"><i class="ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                <?php
                            $i++;
                        endforeach;
                    else:
                ?>
                        <div class="form-group">
                            <div class="copy-fields add-ed">
                                <div class="control-group two-field ">

                                    <?php
                                    echo $this->Form->control('degree[] ', [
                                        'class' => 'form-control', 'placeholder' => 'Degree', 'required' => true, 'label' => false
                                    ]);
                                    ?>
                                </div>
                                <br/>
                                <div class="input-group control-group two-field ">
                                    <?php
                                        echo $this->Form->control('university[ ] ', [
                                            'class' => 'form-control', 'placeholder' => 'University', 'required' => true, 'label' => false
                                        ]);
                                    ?>
                                    <div class="input-group-btn control-group two-field ">
                                        <div class="remove-btn"> 
                                            <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i><?php echo __(' Add'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group custom-select-dropdown">
                    <label><?= __('In network insurance'); ?></label>    
                    <div class="select-group">
                        <i class="ti-angle-down"></i>
                        <?php
                            $savedInsurances = Hash::extract($user->user_insurances, '{n}.insurance_id');
                            echo $this->Form->select('user_insurances.insurance_id', $insuranceList, [
                                    'class' => 'js-example-basic-multiple form-control',
                                    'required' => true,
                                    'multiple' => true,
                                    'value' => $savedInsurances
                                ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group custom-select-dropdown">
                    <label><?= __('language spoken'); ?></label>    
                    <div class="select-group">
                        <i class="ti-angle-down"></i>
                        <?php
                            $savedLanguages = Hash::extract($user->user_languages, '{n}.language_id');
                            echo $this->Form->select('user_languages.language_id', $languageList, [
                                    'class' => 'js-example-basic-multiple form-control',
                                    'required' => true,
                                    'multiple' => true,
                                    'value' => $savedLanguages
                                ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <?php echo $this->Form->control('user_profile.professional_statement', ['class' => 'form-control', 'type' => 'textarea', 'escape' => false, 'value' => $user->user_profile->professional_statement]);
                    ?>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">

                <label><?= __('Hospital affiliation'); ?></label>
                <?php
                    if (!empty($user->user_hospital_affiliations)):
                        $i = 1;
                        foreach ($user->user_hospital_affiliations as $userHospital):
                            ?>
                            <div class="form-group">
                                <div class="copy-fields add-ed">
                                    <div class="input-group control-group two-field ">
                                        <?php
                                            echo $this->Form->control('hospital_affiliation[]', [
                                                'class' => 'form-control', 'placeholder' => 'Hospital Affiliation', 'label' => false, 'value' => $userHospital->name
                                            ]);
                                        ?>

                                        <div class="input-group-btn control-group two-field ">
                                            <div class="remove-btn">
                                                <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i></button>
                                                <button class="btn remove add-btn" type="button"><i class="ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                <?php
                            $i++;
                        endforeach;
                    else:
                ?>
                        <div class="form-group">
                            <div class="copy-fields add-ed">
                                <div class=" input-group control-group two-field ">
                                    <?php
                                    echo $this->Form->control('hospital_affiliation[]', [
                                        'class' => 'form-control', 'placeholder' => 'Hospital Affiliation', 'label' => false
                                    ]);
                                    ?>
                                    <div class="remove-btn"> 
                                        <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <label><?= __('Award'); ?></label>
                <?php
                    if (!empty($user->user_awards)):
                        $i = 1;
                        foreach ($user->user_awards as $userAward):
                ?>
                            <div class="form-group">
                                <div class="copy-fields add-ed">
                                    <div class="input-group control-group two-field ">
                                        <?php
                                            echo $this->Form->control('award[]', [
                                                'class' => 'form-control', 'placeholder' => 'Award', 'label' => false, 'value' => $userAward->name
                                            ]);
                                        ?>
                                        <div class="input-group-btn control-group two-field ">
                                            <div class="remove-btn">
                                                    <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i></button>
                                                    <button class="btn remove add-btn" type="button"><i class="ti-close"></i></button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                <?php
                            $i++;
                        endforeach;
                    else:
                ?>
                        <div class="form-group">
                            <div class="copy-fields add-ed">
                                <div class="input-group control-group two-field ">
                                    <?php
                                        echo $this->Form->control('award[]', [
                                            'class' => 'form-control', 'placeholder' => 'Award', 'label' => false
                                        ]);
                                    ?>
                                    <div class="input-group-btn control-group two-field ">
                                        <div class="remove-btn"> 
                                            <button class="btn add-more add-btn" type="button"><i class="ti-plus"></i> Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Professional membership'); ?></label>  
                    <?php
                    echo $this->Form->control('user_profile.professional_membership', [
                        'class' => 'form-control', 'required' => true, 'label' => false, 'value' => $user->user_profile->professional_membership
                    ]);
                    ?>
                </div> 
            </div>
        </div>  
        <div class="row">
            <div class="col-sm-12">
                <div class="save-btn">
                    <?php
                    echo $this->Form->button(__('Save Bio'), [
                        'type' => 'submit',
                        'class' => 'btn'
                            ]
                    );
                    ?>
                </div>
            </div>
        </div> 
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<?php echo $this->Html->script('frontend/User/edit_bio', ['block' => 'script']); ?>