<div class="container">
    <div class="col-sm-6 signin-wraper col-sm-offset-3">
        <h4><?php echo __('Reset Password'); ?></h4>
        <?= $this->Form->create('', ['class' => 'login-section', 'id' => 'reset-password']) ?>

        <div class="form-group">

            <?php
            echo $this->Form->control('new_password', [
                'class' => 'form-control',
                'placeholder' => __('New Password'),
                'type' => 'password',
                'required' => 'required',
                'autocomplete' => 'off',
                'maxlength' => 100,
                'label' => false
            ]);
            ?>
        </div> 
        <div class="form-group">
            <label><?php echo __('Confirm Password'); ?></label>
            <?php
            echo $this->Form->control('confirm_password', [
                'class' => 'form-control',
                'placeholder' => __('New Password'),
                'type' => 'password',
                'required' => 'required',
                'autocomplete' => 'off',
                'maxlength' => 100,
                'label' => false
            ]);
            ?>
        </div>   
        <div class="form-group ">
            <?php
            echo $this->Form->button(__('Submit'), [
                'type' => 'submit',
                'class' => 'btn btn-black btn-signup'
                    ]
            );
            ?>
            <?= $this->Form->end() ?>
        </div> 

        <div>
            <h5><?= __("Please Note") ?>: </h5>
            <h5>
            <ul>
                <li><?= __("Password must be atleast 8 characters long"); ?> </li>
                <li><?= __("Password must not be easy guess"); ?>.</li>
                <li><?= __("New Password and Confirm Password should be same"); ?>.</li>
            </ul>
            </h5>
        </div>
    </div>
</div>
<?=
$this->Html->script(
        [
    'jquery.validate',
    'backend/reset_pwd'
        ], [
    'block' => 'scriptBottom'
        ]
);
?>