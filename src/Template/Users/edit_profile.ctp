<?php

use Cake\I18n\Time;
use Cake\Core\Configure;
?>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<!-- <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->

<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?= __('Edit Profile') ?></h2>
        </div>
        <div class="heading-title">
            <i class="ti-pencil-alt"></i>
        </div>
    </div>

    <div class="edit-form">
        <?php
        $arr = [];
        for ($i = 1; $i <= 31; $i++) {
            if ($i < 10) {
                $i = sprintf("%02d", $i);
            }
            $arr[$i] = $i;
        }
        $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
        $dob = explode('-', date('Y-m-d', strtotime($user->user_profile->dob)));
        $dobObj = Time::parse($user->user_profile->dob);
        $dob = [
            $dobObj->i18nFormat('yyyy'),
            $dobObj->i18nFormat('dd'),
            $dobObj->i18nFormat('MM')
        ];
        ?>
        <?php
        echo $this->Form->create($user, [
            'url' => ['controller' => 'Users', 'action' => 'edit-profile'],
            'type' => 'post',
            'id' => 'edit-profile',
            'enctype' => 'multipart/form-data',
            'novalidate' => true
                ]
        );
        ?> 
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Title') ?></label>
                    <?php
                    echo $this->Form->control('user_profile.title', ['label' => false, 'type' => 'text', 'class' => 'form-control', 'maxlength' => 100]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('First name') ?></label>
                    <?php
                    echo $this->Form->control('user_profile.first_name', ['label' => false, 'type' => 'text', 'class' => 'form-control', 'maxlength' => 100]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('last Name') ?></label>
                    <?php
                    echo $this->Form->control('user_profile.last_name', ['label' => false, 'type' => 'text', 'class' => 'form-control', 'maxlength' => 100]);
                    ?>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Phone Number') ?></label>
                    <?php
                        echo $this->Form->input('user_profile.phone_number', [
                                'type' => 'tel',
                                'data-id' => 'phone-codes',
                                'label' => false,
                                'type' => 'text',
                                'class' => 'form-control'
                            ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Email id') ?></label>
                    <?php
                    echo $this->Form->control('email', ['label' => false, 'type' => 'text', 'class' => 'form-control', 'maxlength' => 100]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-8">
                <label><?= __('Date of birth') ?></label>
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->select('user_profile.month', 
                        ['01' => __('Jan'), '02' => __('Feb'), '03' => __('March'), '04' => __('April'), 
                        '05' => __('May'), '06' => __('June'), '07' => __('July'), '08' => __('August'), 
                        '09' => __('September'), '10' => __('October'), '11' => __('November'), '12' => __('December')], [
                            'empty' => __('Month'),
                            'class' => 'form-control',
                            'default' => $dob[2]
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->select('user_profile.date', $arr, [
                            'empty' => __('Date'),
                            'class' => 'form-control',
                            'default' => $dob[1]
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        echo $this->Form->select('user_profile.year', $years, [
                            'empty' => __('Year'),
                            'class' => 'form-control',
                            'default' => $dob[0]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <label><?= __('Gender') ?></label>
                <div class="row m-top">
                    <label class="custom-radio"><?= __('Male') ?>
                        <?php if ($user->user_profile->gender == 'Male') { ?>
                            <input type="radio" checked="checked" value="male" name="gender">
                        <?php } else { ?>
                            <input type="radio"  value="male" name="gender">
<?php } ?>
                        <span class="checkmark"></span>
                    </label>
                    <label class="custom-radio"><?= __('Female') ?>
                        <?php if ($user->user_profile->gender == 'Female') { ?>
                            <input type="radio" checked="checked" value="fmale" name="gender">
                        <?php } else { ?>
                            <input type="radio"  value="fmale" name="gender">
<?php } ?>
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 15px;">
            <?php
            $addressTemplates = [
                'inputContainer' => '<div class="{{addressclass}}"><label>{{text}}</label>{{content}}</div>',
            ];
            $this->Form->templates($addressTemplates);
            echo $this->Form->control('user_profile.address', [
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'geocomplete',
                'templateVars' => [
                    'text' => __('Address'),
                    'addressclass' => 'col-sm-8'
                ],
                'label' => false,
                'error' => false,
                'value' => $user->user_profile->address
            ]);
            echo $this->Form->hidden('user_profile.lat', [
                                'data-id' => 'latitude',
                                'value' => $user->user_profile->lat
            ]);

            echo $this->Form->hidden('user_profile.lng', [
                'data-id' => 'longitude',
                'value' => $user->user_profile->lng

            ]);

            echo $this->Form->control('user_profile.zipcode', [
                'type' => 'number',
                'class' => 'form-control',
                'id' => 'zipcode',
                "pattern" => ".{4,4}",
                'maxlength' => 4,
                "oninput" => "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);",
                "title" => __("Must be 4 charaters long"),
                'templateVars' => [
                    'text' => __('Zipcode'),
                    'addressclass' => 'col-sm-4'
                ],
                'label' => false,
                'error' => false,
                'value' => $user->user_profile->zipcode
            ]);
            ?>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label><?= __('Country') ?></label>
                    <?php
                    echo $this->Form->select(
                            'user_profile.country_id', $countryList, [
                        'empty' => __('Please Select'),
                        'class' => 'form-control',
                        'data-id' => 'change-country',
                        'value' => $user->user_profile->country_id,
                        
                        // 'value' => $user->user_profile,
                        'data-url' => $this->Url->build([
                            'controller' => 'States',
                            'action' => 'webView',
                            'prefix' => 'api'
                                ], true),
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label><?= __('State') ?></label>
                    <?php
                    echo $this->Form->select('user_profile.state_id', $stateList, [
                        'empty' => __('Please Select'),
                        'class' => 'form-control',
                        'data-id' => 'states',
                        'value' => $user->user_profile->state_id,
                        
                    ]);
                    ?>
                </div>
            </div>
            
            
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                     <label><?php echo __('Approval Mode') ?></label>
                    <?php
                    echo $this->Form->control('user_profile.mode', [
                        'type' => 'checkbox',
                        'label' =>false,
                        'checked' => $user->user_profile->mode,
                        'id' => 'modeToggle',
                        "data-onstyle" => "success",
                        "data-offstyle"=>"info",
                        "data-width" => "100"
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                     <label><?= __('I just want to see my existing patients') ?></label>
                    <?php
                    echo $this->Form->control('user_profile.my_patient_only', [
                        'type' => 'checkbox',
                        'label' =>false,
                        'checked' => $user->user_profile->my_patient_only,
                        'id' => 'modeToggle1',
                        "data-onstyle" => "success",
                        "data-offstyle"=>"info",
                        "data-width" => "100"
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label><?= __('Profile Images') ?> </label>
                    <?php
                    echo $this->Form->control('upload_image', [
                        'class' => '', 'label' => false, 'div' =>false,
                        'type' => 'file', 'id' => 'upload'
                    ]);
                    ?>
                </div>
            </div>
        </div>

        

        <div class="row">
            <div class="col-sm-12">
                <div class="save-btn">
                    <button class="btn" type="submit"><?php echo __('Save'); ?></button>
                </div>
            </div>
        </div>
<?= $this->Form->end() ?>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>" ></script>
<?php 
    $this->Html->script([
        'https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js',
    ], ['block' => true]);
?>

<?php
echo $this->element('states');
echo $this->Html->css('intlTelInput', [
    'block' => 'css'
]);
echo $this->Html->script([
    'intlTelInput',
    'frontend/User/edit',
    'google_geocode',
    'state',
    //'frontend/Doctor/doctor-profile'
        ], ['block' => 'script']);
?>
<?php $this->Html->scriptStart(['block' => true]); ?>
var lng = '<?php echo  ($this->request->getSession()->read("Config.locale")== "en_DE" )? "de":"en" ?>';
var ton = 'Auto';
	var toff= 'Manual';
	var t1on = 'Yes';
	var t1off= 'No';
	console.log(lng);
	if(lng == 'de') {
		ton = 'automatisch';
		toff= 'manuell';
		t1on = 'Ja';
		t1off= 'Nein';
	}
	
    $('#modeToggle').bootstrapToggle({
        on: ton,
        off: toff,
        height: 0,
    });

    $('#modeToggle1').bootstrapToggle({
        on: t1on,
        off: t1off,
        height: 0,
    });
<?php $this->Html->scriptEnd(); ?>