<div class="container">
    <div class="col-sm-6 signin-wraper col-sm-offset-3">
        <h4><?php echo __('Sign in or create an account'); ?></h4>
        <?php echo $this->Form->create(''); ?>
                <div class="form-group">
                    <label><?php echo __('Email Address'); ?></label>
                    <?php echo $this->Form->control('email', [
                        'class' => 'form-control',
                        'placeholder' => __('Email Address'),
                        'autocomplete' => 'on',
                        'label' => false
                    ]); ?>
                </div>
                <div class="form-group">
                    <label><?php echo __('Password'); ?></label>
                   <?php
                        echo $this->Form->password('password', [
                                'class' => 'form-control',
                                'placeholder' => __('Password'),
                                 'autocomplete' => 'on'
                            ]);
                    ?>
                    <span id="result"></span>
                </div>
                <div class="form-group forget-pass">
        			<?php echo $this->Html->link(__('Forget Your Password'),['controller'=>'Users','action'=>'forgetPassword'], ['class' => '']); ?>
                </div>
                <div class="form-group ">
                   <?php
                        echo $this->Form->button(__('Login'),
                            [
                                'type' => 'submit',
                                'class' => 'btn btn-block btn-signup'
                            ]
                        );
                        /*echo $this->Html->link("Forgot Password ?",['controller'=>'Users','action'=>'forgotPassword'], ['class' => 'pull-right']);*/
                    ?>
                </div>
        <?php echo $this->Form->end(); ?>
        <div class="form-group text-center or-text"><?= __('or') ?></div>
        <div class="form-group">
            <?php echo $this->Html->link(__('Create an Account') ,array('controller' => 'Users', 'action' => 'signUp',$this->request->pass[0]), array('escape' => false , 'class' => 'btn btn-block btn-borderd')); ?>
        </div>
    </div>
</div>