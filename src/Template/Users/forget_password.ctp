<div class="container">
<div class="col-sm-6 signin-wraper col-sm-offset-3">
    <h4><?= __("Forget password"); ?></h4>
    <?php echo $this->Form->create('', ['id' => 'signupForm']); ?>
        <div class="form-group">
            <label><?php echo __('Email Address'); ?></label>
            <?php echo $this->Form->control('email', [
                'class' => 'form-control',
                'placeholder' => __('Email Address'),
                'autocomplete' => 'on',
                'label' => false
            ]); ?>
        </div>
      
       
        <div class="form-group ">
           <?php
            echo $this->Form->button(__('Submit'),
                [
                    'type' => 'submit',
                    'class' => 'btn btn-block btn-signup'
                ]
            );

            echo $this->Form->end(); 
            ?>
</div>
       
</div>
</div>