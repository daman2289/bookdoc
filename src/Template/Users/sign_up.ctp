<?php use Cake\Core\Configure; ?>
<div class="container">
    <div class=" signin-wraper  col-center login-box ">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?= $this->General->getActiveTab('patient'); ?>">
                <?php
                    echo $this->Html->link(__('I am patient'),
                        ['controller' => 'Users','action' => 'signUp','patient'],
                        [
                            // 'aria-controls' => 'home',
                            // 'role' => 'tab',
                            // 'data-toggle' => 'tab'
                        ]
                    );
                ?>
            </li>
            <li role="presentation"  class="<?= $this->General->getActiveTab('doctor'); ?>">
                <?php
                    echo $this->Html->link(__('I am a doctor'),
                        ['controller' => 'Users','action' => 'signUp','doctor'],
                        [
                            // 'aria-controls' => 'profile',
                            // 'role' => 'tab',
                            // 'data-toggle' => 'tab'
                        ]
                    );
                ?>
            </li>
            <li role="presentation"  class="<?= $this->General->getActiveTab('hospital'); ?>"> 
                <?php
                    echo $this->Html->link(__('Register as Hospital'),
                        ['controller' => 'Users','action' => 'signUp','hospital'],
                        [
                            // 'aria-controls' => 'hospital',
                            // 'role' => 'tab',
                            // 'data-toggle' => 'tab'
                        ]
                    );
                ?> 
             </li> 
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?= $this->General->getActiveTab('patient'); ?>" id="home">
                <h4><?php echo __('Create an account'); ?></h4>
                <?php
                    echo $this->Form->create($user, array('id' => 'signupForm'));

                    echo $this->Form->hidden('User.role_id', [
                        'value' => Configure::read('UserRoles.Patient')
                    ]);

                    $nameProfileTemplates = [
                        'inputContainer' => '<div class="form-group {{haserror}}"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($nameProfileTemplates);

                    echo $this->Form->control('User.user_profile.first_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('First Name'),
                            'haserror' => ($this->Form->isFieldError('User.user_profile.first_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.first_name')) ? $this->Form->error('User.user_profile.first_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    echo $this->Form->control('User.user_profile.last_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('Last Name'),
                            'haserror' => ($this->Form->isFieldError('User.user_profile.last_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.last_name')) ? $this->Form->error('User.user_profile.last_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    $bodyProfileTemplates = [
                        'inputContainer' => '<div class="{{cuswidth}}"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($bodyProfileTemplates);
                ?>
                    
                <?php
                    $phoneProfileTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div><div>{{phonenotice}}</div></div>',
                    ];
                    $this->Form->templates($phoneProfileTemplates);
                    echo $this->Form->control('User.user_profile.phone_number', [
                        'type' => 'number',
                        'class' => 'form-control',
                        'data-id' => 'phone-codes',
                        'templateVars' => [
                            'text' => '',
                            'haserror' => ($this->Form->isFieldError('User.user_profile.phone_number')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.phone_number')) ? $this->Form->error('User.user_profile.phone_number') : '',
                            'phonenotice' => __('Mobile no. will not share with anyone it is for verfication only.')
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    $userTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($userTemplates);

                    echo $this->Form->control('User.email', [
                        'type' => 'email',
                        'class' => 'form-control',
                        'templateVars' => [
                            'text' => __('Email'),
                            'haserror' => ($this->Form->isFieldError('User.email')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.email')) ? $this->Form->error('User.email') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                    echo $this->Form->control('User.password', [
                        'type' => 'password',
                        'class' => 'form-control',
                        'id' => 'passwd-patient',
                        'required' => true,
                        "pattern" => ".{8,50}", 
                        'templateVars' => [
                            'text' => __('Password'),
                            'haserror' => ($this->Form->isFieldError('User.password')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.password')) ? $this->Form->error('User.password') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                ?>
                <div class="form-group">
                    <ul class="password-text">
                        <li>
                            <?php echo __('Has at least 8 characters'); ?>
                        </li>
                        <!-- <li><?php echo __('Has letters, numbers, and special characters'); ?></li> -->
                        <li><?php echo __('Not easy to guess'); ?></li>
                    </ul>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label><?php echo __('Date of birth'); ?></label>
                            <div class="row">
                                <?php
                                    $dobTemplates = [
                                        'inputContainer' => '<div class="col-sm-4">{{content}}</div><div class="error-message">{{errormsg}}</div>',
                                    ];
                                    $this->Form->templates($dobTemplates);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'month',
                                        'empty' => __('Month'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.month')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.month')) ? $this->Form->error('User.user_profile.dob.month') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'day',
                                        'empty' => __('Date'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.day')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.day')) ? $this->Form->error('User.user_profile.dob.day') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'year',
                                        'empty' => __('Year'),
                                        'minYear' => 1950,
                                        'maxYear' => date('Y'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.year')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.year')) ? $this->Form->error('User.user_profile.dob.year') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo __('Gender'); ?></label>
                            <div class="row m-top">
                                <label class="custom-radio"><?php echo __('Male'); ?>
                                    <input type="radio" checked="checked" name="User[user_profile][gender]" value='male'>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"><?php echo __('Female'); ?>
                                    <input type="radio" name="User[user_profile][gender]" value='female'>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <?php
                            $addressTemplates = [
                                'inputContainer' => '<div class="{{addressclass}}"><label>{{text}}</label>{{content}}</div>',
                            ];
                            $this->Form->templates($addressTemplates);
                            // echo $this->Form->control('User[user_profile][address]', [
                            //     'type' => 'text',
                            //     'class' => 'form-control',
                            //     'id' => 'geocomplete',
                            //     'templateVars' => [
                            //         'text' => __('Address'),
                            //         'addressclass' => 'col-sm-8'
                            //     ],
                            //     'label' => false,
                            //     'error' => false
                            // ]);
                            // echo $this->Form->hidden('User[user_profile][lat]', [
                            //     'data-id' => 'latitude'
                            // ]);
                            // echo $this->Form->hidden('User[user_profile][lng]', [
                            //     'data-id' => 'longitude'
                            // ]);
                            echo $this->Form->control('User[user_profile][zipcode]', [
                                'type' => 'number',
                                "oninput" => "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);",
                                'class' => 'form-control',
                                'id' => 'zipcode',
                                "maxlength" => "4",
                                'templateVars' => [
                                    'text' => __('Zipcode'),
                                    'addressclass' => 'col-sm-12'
                                ],
                                'label' => false,
                                'error' => false,
                                'placeholder' => 'Zipcode'
                            ]);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label><?= __('Country'); ?></label>
                            <?php
                                echo $this->Form->select('User[user_profile][country_id]', $countryList, [
                                        'class' => 'form-control',
                                        'default' => 14,
                                        'data-id' => 'change-country',
                                        'data-url' => $this->Url->build([
                                                                    'controller' => 'States',
                                                                    'action' => 'webView',
                                                                    'prefix' => 'api'
                                                                ],true),
                                    ]);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <label><?= __('State'); ?></label>
                            <?php
                                echo $this->Form->select('User[user_profile][state_id]', $stateList, [
                                        'class' => 'form-control',
                                        'data-id' => 'states'
                                    ]);
                            ?>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="term" name="terms" value=""><?= __('I have read and accept Bookdoc'); ?> <a href="/careers"><?= __('Term of use'); ?> </a> & <a href="/privacy-policy"><?= __('Privacy Policy.'); ?> </a></label>
                    </div>
                </div>
                <div class="form-group ">
                    <?php
                    echo $this->Form->button(__('Continue'), [
                        'type' => 'submit',
                        'class' => 'btn btn-block btn-signup',
                        'name' => 'patient'
                            ]
                    );
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div role="tabpanel" class="tab-pane <?= $this->General->getActiveTab('doctor'); ?>" id="profile">
                <h4><?= __('Create an account'); ?> </h4>
                <?php
                    echo $this->Form->create($user, array('id' => 'signupForm_doctor'));

                    echo $this->Form->hidden('User.role_id', [
                        'value' => Configure::read('UserRoles.Doctor')
                    ]);

                    $nameProfileTemplates = [
                        'inputContainer' => '<div class="form-group {{haserror}}"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($nameProfileTemplates);

                    echo $this->Form->control('User.user_profile.first_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('First Name'),
                            'haserror' => ($this->Form->isFieldError('User.user_profile.first_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.first_name')) ? $this->Form->error('User.user_profile.first_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    echo $this->Form->control('User.user_profile.last_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('Last Name'),
                            'haserror' => ($this->Form->isFieldError('User.user_profile.last_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.last_name')) ? $this->Form->error('User.user_profile.last_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                    $phoneProfileTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div><div>{{phonenotice}}</div></div>',
                    ];
                    $this->Form->templates($phoneProfileTemplates);
                    echo $this->Form->control('User.user_profile.phone_number', [
                        'type' => 'number',
                        'class' => 'form-control',
                        'data-id' => 'phone-codes',
                        'templateVars' => [
                            'text' => '',
                            'haserror' => ($this->Form->isFieldError('User.user_profile.phone_number')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.user_profile.phone_number')) ? $this->Form->error('User.user_profile.phone_number') : '',
                            'phonenotice' => __('Mobile no. will not share with anyone it is for verfication only.')
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    $userTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($userTemplates);

                    echo $this->Form->control('User.email', [
                        'type' => 'email',
                        'class' => 'form-control',
                        'templateVars' => [
                            'text' => __('Email'),
                            'haserror' => ($this->Form->isFieldError('User.email')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.email')) ? $this->Form->error('User.email') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                    echo $this->Form->control('User.password', [
                        'type' => 'password',
                        'class' => 'form-control',
                        'id' => 'passwd-doc',
                        'minlength' => 8,
                        "pattern" => ".{8,50}", 
                        'required' => true,
                        'templateVars' => [
                            'text' => __('Password'),
                            'haserror' => ($this->Form->isFieldError('User.password')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.password')) ? $this->Form->error('User.password') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                ?>
                <div class="form-group">
                    <ul class="password-text">
                        <li>
                            <?php echo __('Has at least 8 characters'); ?>
                        </li>
                        <!-- <li><?php echo __('Has letters, numbers, and special characters'); ?></li> -->
                        <li><?php echo __('Not easy to guess'); ?></li>
                    </ul>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label><?php echo __('Date of birth'); ?></label>
                            <div class="row">
                                <?php
                                    $dobTemplates = [
                                        'inputContainer' => '<div class="col-sm-4">{{content}}</div><div class="error-message">{{errormsg}}</div>',
                                    ];
                                    $this->Form->templates($dobTemplates);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'month',
                                        'empty' => __('Month'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.month')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.month')) ? $this->Form->error('User.user_profile.dob.month') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'day',
                                        'empty' => __('Date'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.day')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.day')) ? $this->Form->error('User.user_profile.dob.day') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);

                                    echo $this->Form->control('User.user_profile.dob', [
                                        'type' => 'year',
                                        'empty' => __('Year'),
                                        'minYear' => 1950,
                                        'maxYear' => date('Y'),
                                        'class' => 'form-control',
                                        'templateVars' => [
                                            'text' => '',
                                            'haserror' => ($this->Form->isFieldError('User.user_profile.dob.year')) ? 'has-error' : '',
                                            'errormsg' => ($this->Form->isFieldError('User.user_profile.dob.year')) ? $this->Form->error('User.user_profile.dob.year') : ''
                                        ],
                                        'label' => false,
                                        'error' => false
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label><?php echo __('Gender'); ?></label>
                            <div class="row m-top">
                                <label class="custom-radio"><?php echo __('Male'); ?>
                                    <input type="radio" checked="checked" name="User[user_profile][gender]" value='male'>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="custom-radio"><?php echo __('Female'); ?>
                                    <input type="radio" name="User[user_profile][gender]" value='female'>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="row">
                        <?php
                            $addressTemplates = [
                                'inputContainer' => '<div class="{{addressclass}}"><label>{{text}}</label>{{content}}</div>',
                            ];
                            $this->Form->templates($addressTemplates);
                            echo $this->Form->control('User[user_profile][address]', [
                                'type' => 'text',
                                'class' => 'form-control',
                                'id' => 'doctor-geocomplete',
                                'templateVars' => [
                                    'text' => __('Address'),
                                    'addressclass' => 'col-sm-8'
                                ],
                                'label' => false,
                                'error' => false
                            ]);
                            echo $this->Form->hidden('User[user_profile][lat]', [
                                'data-id' => 'doc-latitude'
                            ]);
                            echo $this->Form->hidden('User[user_profile][lng]', [
                                'data-id' => 'doc-longitude'
                            ]);
                            echo $this->Form->control('User[user_profile][zipcode]', [
                                'type' => 'number',
                                "oninput" => "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);",
                                'class' => 'form-control',
                                'id' => 'zipcode',
                                "maxlength" => "4",
                                'templateVars' => [
                                    'text' => __('Zipcode'),
                                    'addressclass' => 'col-sm-4'
                                ],
                                'label' => false,
                                'error' => false
                            ]);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label><?= __('Country'); ?></label>
                            <?php
                                echo $this->Form->select('User[user_profile][country_id]', $countryList, [
                                        'class' => 'form-control',
                                        'default' => 14,
                                        'data-id' => 'change-country',
                                        'data-url' => $this->Url->build([
                                                                    'controller' => 'States',
                                                                    'action' => 'webView',
                                                                    'prefix' => 'api'
                                                                ],true),
                                    ]);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <label><?= __('State'); ?></label>
                            <?php
                                echo $this->Form->select('User[user_profile][state_id]', $stateList, [
                                        'class' => 'form-control',
                                        'data-id' => 'states'
                                    ]);
                            ?>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="term" name="terms" value=""><?= __('I have read and accept Bookdoc'); ?> <a href="/terms-conditions"><?= __('Term of use'); ?> </a> & <a href="/privacy-policy"><?= __('Privacy Policy.'); ?> </a></label>
                    </div>
                </div>
                <div class="form-group ">
                    <?php
                    echo $this->Form->button(__('Continue'), [
                        'type' => 'submit',
                        'class' => 'btn btn-block btn-signup',
                        'name' => 'patient'
                            ]
                    );
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div role="tabpanel" class="tab-pane <?= $this->General->getActiveTab('hospital'); ?>" id="hospital">
                <h4><?php echo __('Create an account'); ?></h4>
                <?php
                    echo $this->Form->create('', array('url' => ['controller' => 'Users', 'action' => 'hospitalSignup'],'id' => 'hospitalsignupForm'));

                    echo $this->Form->hidden('role_id', [
                        'value' => '5'
                    ]);

                $nameProfileTemplates = [
                        'inputContainer' => '<div class="form-group {{haserror}}"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($nameProfileTemplates);

                    echo $this->Form->control('hospital_profile.hospital_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('Hospital Name'),
                            'haserror' => ($this->Form->isFieldError('User.hospital_profile.hospital_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.hospital_profile.hospital_name')) ? $this->Form->error('User.hospital_profile.hospital_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    echo $this->Form->control('hospital_profile.contact_name', [
                        'type' => 'text',
                        'class' => 'form-control',
                        "maxlenght" => "15",
                        "onkeypress" => 'return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))',
                        'templateVars' => [
                            'text' => __('Contact Person Name'),
                            'haserror' => ($this->Form->isFieldError('User.hospital_profile.contact_name')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.hospital_profile.contact_name')) ? $this->Form->error('User.hospital_profile.contact_name') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                    $phoneProfileTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div><div>{{phonenotice}}</div></div>',
                    ];
                    $this->Form->templates($phoneProfileTemplates);
                    echo $this->Form->control('hospital_profile.phone_number', [
                        'type' => 'number',
                        'class' => 'form-control',
                        'data-id' => 'phone-codes',
                        'templateVars' => [
                            'text' => '',
                            'haserror' => ($this->Form->isFieldError('User.hospital_profile.phone_number')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.hospital_profile.phone_number')) ? $this->Form->error('User.hospital_profile.phone_number') : '',
                            'phonenotice' => __('Mobile no. will not share with anyone it is for verfication only.')
                        ],
                        'label' => false,
                        'error' => false
                    ]);

                    $userTemplates = [
                        'inputContainer' => '<div class="form-group"><label>{{text}}</label>{{content}}<div class="error-message">{{errormsg}}</div></div>',
                    ];
                    $this->Form->templates($userTemplates);

                    echo $this->Form->control('email', [
                        'type' => 'email',
                        'class' => 'form-control',
                        'templateVars' => [
                            'text' => __('Email'),
                            'haserror' => ($this->Form->isFieldError('User.email')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.email')) ? $this->Form->error('User.email') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                    echo $this->Form->control('password', [
                        'type' => 'password',
                        'class' => 'form-control',
                        'id' => 'passwd-hospital',
                        'minlength' => 8,
                        "pattern" => ".{8,50}", 
                        'required' => true,
                        'templateVars' => [
                            'text' => __('Password'),
                            'haserror' => ($this->Form->isFieldError('User.password')) ? 'has-error' : '',
                            'errormsg' => ($this->Form->isFieldError('User.password')) ? $this->Form->error('User.password') : ''
                        ],
                        'label' => false,
                        'error' => false
                    ]);
                ?>
                <div class="form-group">
                    <ul class="password-text">
                        <li>
                            <?php echo __('Has at least 8 characters'); ?>
                        </li>
                        <!-- <li><?php echo __('Has letters, numbers, and special characters'); ?></li> -->
                        <li><?php echo __('Not easy to guess'); ?></li>
                    </ul>
                </div>
                <div class="form-group">
                    <div class="row">
                        <?php
                            $addressTemplates = [
                                'inputContainer' => '<div class="{{addressclass}}"><label>{{text}}</label>{{content}}</div>',
                            ];
                            $this->Form->templates($addressTemplates);
                            echo $this->Form->control('hospital_profile.address', [
                                'type' => 'text',
                                'class' => 'form-control',
                                'id' => 'hospital_geocomplete',
                                'templateVars' => [
                                    'text' => __('Address'),
                                    'addressclass' => 'col-sm-8'
                                ],
                                'label' => false,
                                'error' => false
                            ]);
                            echo $this->Form->hidden('hospital_profile.lat', [
                                'data-id' => 'hos-latitude'
                            ]);
                            echo $this->Form->hidden('hospital_profile.lng', [
                                'data-id' => 'hos-longitude'
                            ]);
                            echo $this->Form->control('hospital_profile.zipcode', [
                                'type' => 'number',
                                "oninput" => "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);",
                                'class' => 'form-control',
                                'id' => 'zipcode_hospital',
                                "maxlength" => "4",
                                'templateVars' => [
                                    'text' => __('Zipcode'),
                                    'addressclass' => 'col-sm-4'
                                ],
                                'label' => false,
                                'error' => false
                            ]);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label><?= __('Country'); ?></label>
                            <?php
                                echo $this->Form->select('hospital_profile.country_id', $countryList, [
                                        'class' => 'form-control',
                                        'default' => 14,
                                        'data-id' => 'change-country',
                                        'data-url' => $this->Url->build([
                                                                    'controller' => 'States',
                                                                    'action' => 'webView',
                                                                    'prefix' => 'api'
                                                                ],true),
                                    ]);
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <label><?= __('State'); ?></label>
                            <?php
                                echo $this->Form->select('hospital_profile.state_id', $stateList, [
                                        'class' => 'form-control',
                                        'data-id' => 'states'
                                    ]);
                            ?>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="term" name="terms" value=""><?= __('I have read and accept Bookdoc'); ?> <a href="/careers"><?= __('Term of use'); ?> </a> & <a href="/privacy-policy"><?= __('Privacy Policy.'); ?> </a></label>
                    </div>
                </div>
                <div class="form-group ">
                    <?php
                    echo $this->Form->button(__('Continue'), [
                        'type' => 'submit',
                        'class' => 'btn btn-block btn-signup',
                        'name' => 'patient'
                            ]
                    );
                    ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>
<?php
    echo $this->element('states');
    echo $this->Html->css('intlTelInput', [
        'block' => 'css'
    ]);
    echo $this->Html->script([
        'intlTelInput',
        'frontend/signup',
        'google_geocode',
        'state',
    ], ['block' => 'script']);
?>