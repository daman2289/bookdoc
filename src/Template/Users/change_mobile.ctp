
<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?= __('Change Mobile No') ?></h2>
        </div>
        <div class="heading-title">
            <i class="ti-pencil-alt"></i>
            <span><?= __('Edit') ?></span>
        </div>
    </div>

    <div class="edit-form change-mobile">
        <?php
            echo $this->Form->create('', [
                    'url' => ['controller' => 'Users', 'action' => 'change-mobile'],
                    'type' => 'post',
                    'id' => 'change-mobile',
                    'novalidate' => true
                ]);
        ?> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label><?= __('Current Mobile No.') ?></label>
                            <!-- <input type="text" name="current-no" class="form-control"> -->
                            <?php
                            echo $this->Form->control('old_mobile', [
                                'class' => 'form-control',
                                'autocomplete' => 'on', 'label' => false
                            ]);
                            ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label><?= __('New Mobile No.') ?></label>
                            <!-- <input type="Password" name="change-password" class="form-control"> -->
                            <?php
                            echo $this->Form->control('new_mobile', [
                                'class' => 'form-control',
                                'autocomplete' => 'on', 'label' => false
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="save-btn">
                            <!-- <button class="btn">Change mobile no</button> -->
                            <?php
                            echo $this->Form->button(__('Change mobile no'), [
                                'type' => 'submit',
                                'class' => 'btn'
                                    ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<?php echo $this->Html->script('backend/common/jquery.validate'); ?> 
<script>
    (function () {
        // validate signup form on keyup and submit
        var validatechangemobile = function (element) {

            $(element).validate({
                rules: {
                    'old_mobile': {
                        required: true,
                        noSpace: true,
                        digits: true,
                    },
                    'new_mobile': {
                        required: true,
                        noSpace: true,
                        digits: true,
                    }
                }
            });
        };
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "No Space allowed");
        $(document).ready(function () {
            validatechangemobile("#change-mobile");
        });
    })();
</script>