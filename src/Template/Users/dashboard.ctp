<?php
use Cake\Core\Configure;


echo $this->Html->css([
        'jquery.timepicker',
    ],['block' => true]);

?>
<style>
.activePlan{
    background: #19ca68 !important;
}

.activePricingBorder{
    border: 3px solid #19ca68 !important;
}

</style>
<?php if(empty($availability)) { 
    $message = "Your have not mentioned your working days, please fill your availability/slots so that it can be searched by patients.";
}
?>
<?php echo $this->Html->css([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css' 
    ]);

?>
<script type="text/javascript">
    var notification = {
            'data' :  {
                title: false,
                width: "100%",
                hide: false,
                stack: {
                    "dir1": "down", 
                    "dir2": "left", 
                    "push": "top", 
                    "spacing1": 0, 
                    "spacing2": 0, 
                    "firstpos1": 0, 
                    "firstpos2": 0
                },
                styling: "bootstrap3",
                // addclass: "ui-pnotify-inline",
                buttons: {
                    sticker: false
                }
                
            }
        };
        var msg = '<?php $message ?>';
        notification.data.text  = '<div class="text-center calmsg">' + msg + '</div>';
        new PNotify(notification.data);
</script>
<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Dashboard")?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-form dashbroad-content">
        <div class="row">
            <div class="col-sm-4 m-b-15">
                <div class="event event-dark-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Total ")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $totalBookings; ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 m-b-15">
                <div class="event event-sky-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Upcoming ")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $upcomingBookings; ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 m-b-15">
                <div class="event event-aqua-dark">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3> <?= __("Previous")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $previousBooking; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table  ">
                    <tr class="table-top-heading">
                        <th><?= __("Upcoming Appointment"); ?> </th>
                        <th colspan="4"></th>
                    </tr>
                    <tr>
                        <th><?= __("Date"); ?></th>
                        <th><?= __("Patient Name"); ?></th>
                        <th><?= __("Reason"); ?></th>
                        <th><?= __("Action"); ?></th>

                    </tr>
                    <?php if ($doctorBookings->isEmpty()) { ?>
                       <tr><td colspan="5" class="text-center"><?= __("No Bookings Found"); ?> </td></tr>
                    <?php }?>
                     <?php foreach ($doctorBookings as $booking) { ?>
                        <tr>
                            <?php $myBookingdate = $this->General->formatTime($booking->booking_date, $booking->booking_time); ?>

                            <td><?= $myBookingdate ?> </td>
                            <td><?php echo $booking->patient_first_name;  echo ' '.$booking->patient_last_name ?></td>
                            <td><?= $booking->illness_reason? $booking->illness_reason->title_eng : ''; ?></td>
                            
                            <?php 
                                switch ($booking->status) {
                                
                                    case 1: ?>
                                        <td>
                                            <a href="#myConfirmModel" class="btn btn-md btn-success" data-action-id="1" data-booking-id="<?= $booking->id; ?>" data-toggle="modal" data-target="#myConfirmModel"><?= __("Accept"); ?></a>                                            
                                            <a href="#myRejectModel" class="btn btn-md btn-danger" data-action-id="2" data-booking-id="<?= $booking->id; ?>" data-toggle="modal" data-target="#myRejectModel"><?= __("Decline"); ?></a>
                                        </td>
                                        <?php break;

                                    case 2: ?>
                                        <td><a class="btn btn-md btn-success"><?= __("Approved"); ?></a></td>    
                                        <?php break;
                                    
                                    case 3: ?>
                                        <td><a class="btn btn-md btn-danger"><?= __("Declined"); ?></a></td>    
                                        <?php break;
                                    
                                    default: ?>
                                        <td><a class="btn btn-md decline-btn"><?= __("Canceled By Patient"); ?> </a></th>                                    
                                    <?php break;
                                }
                            ?>
                        </tr>
                    <?php } ?>
                    
                </table>
            </div>
            <div class="col-sm-12 pagination-inner">
                <?= $this->element('common/paginator') ?>
            </div>
        </div>
      
        
          <!-- Booking Calender -->

     



        <!-- choose palan -->
        <div class="row">
            <div class="col-sm-12">
                <div class="plan-heading">
                    <h2 id="choosePlan"><?= __("Choose Plan"); ?></h2>
                </div>
            </div>
        </div>
        <div class="row pricing-inner">
            
            <?php foreach ($allPlans as $plan) { ?>
            <?php $myPlan = $this->General->isPlanActive($activePlanId, $plan->stripe_id); ?>
                <div class="col-md-4">
                    <div class="pricing hover-effect <?= ($myPlan ? "activePricingBorder":"")?>">
                        <div class="pricing-head">
                            <?php
                                switch ($plan->plan_type) {
                                    case '1':
                                        $price = $plan->price;
                                        $month = __("1 Month");
                                        break;

                                    case '2':
                                        $price = $plan->price/6;
                                        $month = __("6 Month");
                                        break;

                                    case '3':
                                        $price = $plan->price/12;
                                        $month = __("12 Month");
                                        break;
                                    
                                    default:
                                        $price = "";
                                        $month = "";

                                        break;
                                }
                            ?>
                            <?php if ($myPlan) { ?>
                                <h3 class="activePlan"><?php echo $month; echo __(" (Active)"); ?></h3>
                            <?php }else { ?>
                                <h3><?= $month ?> </h3>
                            <?php } ?>

                            <h4><i>€</i><?= $price ?><span>/ <?= __(' Month'); ?></span></h4>
                        </div>
                        <ul class="pricing-content list-unstyled">
                            <li><?= __("Online und lokal Termine buchen"); ?></li>
                            <li><?= __("Termine bestätigen, ablehnen, stornieren oder verschieben"); ?></li>
                            <li><?= __("Online und lokal Kalender synchronisieren"); ?></li>
                            <li><?= __("Der Kalender ist mir Google oder Outlook synchronisierbar
"); ?></li>
                            <li><?php echo "Bestätigen Sie Ihre Termine manuell oder automatisch"?></li>
                            <li><?php echo "Sie können Ihre Verfügbarkeit jederzeit bearbeiten" ?></li>
                        </ul>
                        <div class="pricing-footer">
                            <?php if ($myPlan) { ?>
                                <a href="<?= $this->Url->build(['controller' => 'payments', 'action' => 'mySubscriptions']) ?>" class="btn yellow-crusta"><?= __("Manage Subscription"); ?></a>
                            <?php    
                            }else { ?>
                                <a href="<?= $this->Url->build(['controller' => 'payments', 'action' => 'index', $plan->stripe_id]) ?>" class="btn yellow-crusta"><?= __("Choose Plan");?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php }?>

            <!--//End Pricing -->
        </div>
    </div> 
</div>
<?= $this->element('Modals/local_book'); ?>
<?= $this->element('Modals/reject_appointment'); ?>
<?= $this->element('Modals/confirm_appointment'); ?>



<?php $this->Html->script([
    "my-model.js",
    "datepick"
],['block' => true])?>
