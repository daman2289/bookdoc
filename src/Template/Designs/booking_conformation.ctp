<div class="container conformation-page">
    <div class="col-sm-6">
        <h1>Your appointment is booked!</h1>
        <p class="schedule-message">You are scheduled with <b>Tara Rao, MD</b> for a <b>Dermatology Consultation</b> on <b><a href="#"> May 31st at 8:00am EST <i class="glyphicon glyphicon-calendar"></i></a></b></p>
    </div>
    <div class="col-sm-5 col-sm-offset-1 bg-gray-conformation">
        <div class="col-sm-4">
            <img class="img-circle img-responsive" src="../img/frontend/five-star-dr.png">
            <div class="doctor-name">Tara Rao, MD</div>
            <div class="specialtys">Dermatologist</div>
        </div>
        <div class="col-sm-12">
            <div class="appointment-detail">
                <div class="small-label">Appointment</div>
                <div class="apointemnt-time">Thu,May 31-8:00 am EST</div>
                <div class="small-label">Patient</div>
                <div class="patient-name">Xyz</div>
                <div class="small-label">Visit reason</div>
                <div>Dermatology Consultation</div>
            </div>
        </div>
    </div>
</div>
