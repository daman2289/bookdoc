<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Favourite Doctors"); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <?php foreach ($doctorFavourite as $favDoc) { 
        $isFemale = false;
        if ($favDoc->user->user_profile->gender == 'Female') {
            $isFemale = true;
        }
        ?>
        <div class="col-sm-8 m-bottom">
            <div class="border-medical clearfix">
                <div class="col-sm-12">
                    <div class="col-sm-2">
                         <?php echo $this->Html->image($this->User->checkAndGetImage($favDoc->user->user_profile->profile_pic, $isFemale), [
                                    'class' => 'img-circle img-responsive'
                                ]);
                        ?>
                    </div>
                    <div class="col-sm-8">
                        <div class="doctor-name"><b>Dr. <?= h($favDoc->user->user_profile->full_name ); ?> MD</b></div>
                        <div class="col-sm-12 specialization"><?= $this->User->getCommaSeparatedUserSpecialities($favDoc->user->user_specialities); ?> </div>
                        <div class="age"><?= $this->User->getUserAge($favDoc->user->user_profile->dob) . ' ' . __('yrs. old'); ?></div>
                        <div><?php echo $favDoc->user->user_profile->address; echo " " ;echo  $favDoc->user->user_profile->zipcode;  ?></div>
                        <!-- <div class="booked-how-many"><small>Booked 5 times</small></div> -->
                    </div>
                    <div class="col-sm-2">
                        <a href="<?= $this->Url->build(['controller' => 'Doctors', 'action' => 'view', base64_encode($favDoc->doctor_id)]); ?>" class="btn btn-blue"><?= __("Book Now"); ?></a>
                        
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if($doctorFavourite->isEmpty()){ ?>
        <div class="col-sm-8 m-bottom">
            <?= __("No Favorites "); ?>
        </div>
    <?php } ?>

</div>
