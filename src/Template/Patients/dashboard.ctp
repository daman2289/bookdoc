

<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Dashboard")?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-form dashbroad-content">
        <div class="row">
            <div class="col-sm-4">
                <div class="event event-dark-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Total")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $totalBookings; ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="event event-sky-blue">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3><?= __("Upcoming")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $upcomingBookings; ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="event event-aqua-dark">
                    <ul>
                        <li><img alt="" src=""></li>
                        <li><h3> <?= __("Previous")?> <br/><?= __("Appointment")?> </h3></li>
                        <li><span><?= $previousBooking; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>


        
        <div class="row">
            <div class="col-sm-12 mobile-res-tb">
                
                <table class="table ">
                    <tr class="table-top-heading">
                        <th><?= __("Upcoming Appointment"); ?> </th>
                        <th colspan="4"></th>
                    </tr>
                </table>
                <?php foreach ($patientBookings as $key => $booking) { ?>
                        <?php $myBookingdate = $this->General->formatTime($booking->booking_date, $booking->booking_time); ?>

                       <div class="col-sm-12 m-bottom">
                            <div class="border-medical clearfix">
                                <div class="col-sm-12">
                                    <div class="col-sm-1">
                                        <?php echo $this->Html->image($this->User->checkAndGetImage($booking->user->user_profile->profile_pic),
                                            ['class' => 'img-circle img-responsive']
                                        );?>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="doctor-name"><b>Dr. <?php echo $booking->user->user_profile->full_name?> <?php echo "" ?></b></div>
                                        <div class="col-sm-12 specialization"><?php echo ""?></div>
                                        <div class="age"><?php echo $this->User->getUserAge($booking->user->user_profile->dob);?> yrs. old</div>
                                        <div>
                                        <?php echo $booking->user->user_profile->address; echo " "; echo $booking->user->user_profile->zipcode; ?></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <strong class="mb-2">Appointment date</strong>
                                        <div>
                                            <?= $myBookingdate ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <strong class="mb-2">Status</strong>
                                        <?php 
                                            switch ($booking->status) {

                                                case '1': //Not Approved and Past ?>
                                                    <div class="label label-warning"><?= __("Pending"); ?></div>
                                                    <?php break;


                                                case '3': //Doctor Declined and Past ?>
                                                    <div class="label label-danger"><?= __("Declined"); ?></div>
                                                    <?php break;

                                                case '4': //Patient Cancel and Past?>
                                                    <div class="label label-warning"><?= __("Cancelled"); ?></div>
                                                    <?php break;
                                                
                                                default: ?>
                                                    <div class="label label-success"><?= __("Complete"); ?></div>
                                                <?php break;
                                            }
                                        ?>
                                    </div>
                                    
                                    <div class="col-sm-4" style="text-align:right">
                                       <?php 
                                            switch ($booking->status) {

                                                case '3': //Declined
                                                echo __("Declined by Doctor");
                                                    break;

                                                case '4': //Declined
                                                echo __("Cancelled by You");
                                                    break;
                                                
                                                default: ?>

                                                    <a class="btn btn-md btn-danger cancelBooking" data-date="<?= $myBookingdate ?>" data-id=<?= $booking->id; ?>  >
                                                        <?= __("Cancel Appointment"); ?> 
                                                    </a>
                                                <?php break;
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php } ?>

                <?php if ($patientBookings->isEmpty()) { ?>
                    <div class="col-sm-12 m-bottom">
                        <div class="border-medical clearfix">
                            <div class="col-sm-12">
                                <?= __("No Upcoming Bookings"); ?>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
            <div class="col-sm-12 pagination-inner">
                <?= $this->element('common/paginator') ?>
            </div>
        </div>  
    </div> 
</div>





<?= $this->element('Modals/cancel_by_patient'); ?>



<?php $this->Html->script([
    'patient-dashboard'
],['block' => true])?>
