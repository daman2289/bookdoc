<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Past Appointments")?></h2>
                </div>
            </div>
        </div>
    </div>
    <?php if(!$appointment->isEmpty()) { 
        foreach ($appointment as $key => $appoint) {
    ?>
    <div class="col-sm-12 m-bottom">
        <div class="border-medical clearfix">
            <div class="col-sm-12">
                
                <div class="col-sm-1">
                    <?php echo $this->Html->image($this->User->checkAndGetImage($appoint->user->user_profile->profile_pic),
                        ['class' => 'img-circle img-responsive']
                    );?>
                </div>
                
                <div class="col-sm-3">
                    <div class="doctor-name"><b>Dr. <?php echo $appoint->user->user_profile->full_name?> <?php echo $this->User->getUserEducationsToShowInStringFormat($appoint->user->user_educations);?></b></div>
                    <div class="col-sm-12 specialization"><?php echo $this->User->getCommaSeparatedUserSpecialities($appoint->user->user_specialities);?></div>
                    <div class="age"><?php echo $this->User->getUserAge($appoint->user->user_profile->dob); echo __("yrs. old"); ?></div>
                    <div><?php echo $appoint->user->user_profile->address?></div>
                </div>

                <div class="col-sm-2">
                    <strong class="mb-2"><?= __("Appointment date"); ?></strong>
                    <div><?php echo date('M d, Y H:i',strtotime($appoint->bookingTime))?></div>
                </div>
                
                <div class="col-sm-2">
                    <strong class="mb-2"><?= __("Status"); ?></strong>
                     <?php 
                        switch ($appoint->status) {

                            case '1': //Not Approved and Past ?>
                                <div class="label label-primary"><?= __("Not Approved"); ?></div>
                                <?php break;


                            case '3': //Doctor Declined and Past ?>
                                <div class="label label-danger"><?= __("Declined by Doctor"); ?></div>
                                <?php break;

                            case '4': //Patient Cancel and Past?>
                                <div class="label label-warning"><?= __("Cancelled by You"); ?></div>
                                <?php break;
                            
                            default: ?>
                                 <div class="label label-success"><?= __("Complete"); ?> </div>
                            <?php break;
                        }
                    ?>
                </div>
                
                
                <div class="col-sm-2" style="">
                    <strong class="mb-2"><?= __("Feedback"); ?></strong>
                    <?php if(empty($appoint->doctor_rating)) { ?>
                        <?php echo $this->Html->link(__('Leave a review'),
                            'javascript:void(0)',
                            ['class' => 'btn btn-md btn-info review' , 'data-booking'=> $appoint->id,'data-id' => $appoint->user->id]
                        ); ?>
                    <?php } else { ?>
                        <?php echo $this->Html->link(__('My Review'),
                            'javascript:void(0)',[
                                'class' => 'btn btn-md btn-info feedback',
                                'data-overall'=> $appoint->doctor_rating->overall_rating,
                                'data-bedside'=> $appoint->doctor_rating->bedside_manner, 
                                'data-waittime'=> $appoint->doctor_rating->wait_time, 
                                'data-review' => $appoint->doctor_rating->review
                            ]
                        ); ?>
                    <?php } ?>
                </div>

                <div class="col-sm-2">
                    <strong class="mb-2"><?= __("Action"); ?></strong>
                    <?php echo $this->Html->link(__('Book Again'),
                            ['controller' => 'Doctors','action' => 'view',base64_encode($appoint->doctor_id)],
                            ['class' => 'btn btn-md btn-danger book_again']
                        ); ?>
                </div>
                
            </div>
        </div>
    </div>
    <?php } } else { ?>
    <div class="col-sm-12 m-bottom">
        <?php echo __('No Past Appointments');?>
    </div>
    <?php } ?>
</div>
 
<?= $this->element('Modals/booking_review'); ?>
<?= $this->element('Modals/my_rating'); ?>

<?php echo $this->Html->script('backend/users/report', ['block' => 'script']); ?>