<div class="right-sidebar bg-white">
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?php echo __('My Reports') ?></h2>
        </div>
    </div>
    <div class="edit-form manage-calender">
        <div class="row mb-10">
			<div class="row">
			    <div class="col-sm-12 mobile-res-tb">
			        <table class="table table-striped ">
			           <thead>
						    <tr>
								<th><?php echo __('Doctor Name')?></th>
								<th><?= __("Title"); ?></th>
								<th><?= __("Descripton"); ?></th>
								<th><?= __("File"); ?></th>
								<th><?= __("Uploaded Date"); ?></th>
								<th><?= __("Download"); ?></th>
							</tr>
						<thead>
					<tbody>		
			            <?php if ($reports->isEmpty()) { ?>
			               <tr><td colspan="5" class="text-center"><?= __("No Reports Found"); ?> </td></tr>
			            <?php }?>
			             <?php foreach ($reports as $report) { ?>
			                <tr>
			                	<td><?php echo $report->user->user_profile->full_name?></td>
			                    <td><?php echo $report->title?></td>
			                    <td><?php echo $this->Text->truncate(
		                            $report->description,
		                            40,
		                            [
		                                'ellipsis' => '...',
		                                'exact' => false
		                            ]
		                        );
		                        ?><?php echo $this->Html->link(__('More'),
		                        	'javascript:void(0)',
		                        	['class' => 'btn btn-dafult more-button' , 'data-desc' => $report->description]
		                        );?></td>
			                    <td><?php echo $report->file?></td>
			                    <td><?php echo date('M d Y', strtotime($report->created))?></td>
			                    <td><?php echo $this->Html->link(__('<i class="fa fa-download" aria-hidden="true"></i>'),
			                    	['controller' => 'Patients','action' => 'download',base64_encode($report->id)],
			                    	['class' => 'btn btn-default','escape' => false,'target' => '__blank']
			                    );?></td>
			                </tr>
			            <?php } ?>
				    </tbody>    
			        </table>
			    </div>
			    <div class="col-sm-12 pagination-inner">
			        <?= $this->element('common/paginator') ?>
			    </div>
			</div>
		</div>
	</div>
</div>
<div id="moredesc" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= __("Description"); ?></h4>
      </div>
      <div class="modal-body">
        <p class="desc"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close"); ?></button>
      </div>
    </div>

  </div>
</div>
<?php echo $this->Html->script(['backend/users/report','backend/common/table-responsive'], ['block' => 'script']);?>

