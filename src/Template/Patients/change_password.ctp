<style>
#err{
    color: red !important;
    font-size: 14px;
    padding-left:12px;
}
</style>
<div class="right-sidebar bg-white">
    <center><div class="err" style="color:red;"></div></center>
    <div class="top-tilte-bar">
        <div class="heading-tilte">
            <h2><?= __('Change Password') ?></h2>
        </div>
        <div class="heading-title">
            <i class="ti-pencil-alt"></i>
            <span><?= __('Edit') ?></span>
        </div>
    </div>

    <div class="edit-form change-password">
        <?php
        echo $this->Form->create('', [
            'url' => ['controller' => 'Patients', 'action' => 'change-password'],
            'type' => 'post',
            'id' => 'change-password',
            'novalidate' => true
                ]
        );
        ?> 
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Current Password') ?></label>
                    <?php
                    echo $this->Form->control('old_password', [
                        'class' => 'form-control',
                        'id' => 'change_password',
                        'type' => 'password',
                        'label' => false,
                        'placeholder' => __('Old Password'),
                        'maxlength' => 100,
                        'required' => true
                            ]
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('New Password') ?></label>
                    <?php
                    echo $this->Form->control('password', [
                        'class' => 'form-control',
                        'type' => 'password',
                        'id' => 'password',
                        'label' => false,
                        'placeholder' => __('New Password'),
                        'data-display' => 'mainPassword',
                        'maxlength' => 100,
                        'required' => true
                            ]
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label><?= __('Confirm Password') ?></label>
                    <?php
                    echo $this->Form->control('confirm_password', [
                        'class' => 'form-control',
                        'type' => 'password',
                        'id' => 'confirm_password',
                        'label' => false,
                        'placeholder' => __('New Password'),
                        'data-display' => 'mainPassword',
                        'maxlength' => 100,
                        'required' => true
                            ]
                    );
                    ?>
                </div>
            </div>
            <div id="err">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="save-btn">
                    <?php
                    echo $this->Form->button(__('Save'), [
                        'class' => 'btn submit',
                        'type' => 'submit'])
                    ?>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<?php $this->Html->script([
    'changePassword'
],['block' => true])

?>

