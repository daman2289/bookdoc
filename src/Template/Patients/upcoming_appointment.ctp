<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2>Upcoming Appointments</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="text-upcoming col-sm-12">Your Upcoming Appointments with doctor(s):-</div>
    <div class="col-sm-12 m-bottom">
        <div class="border-medical clearfix">
            <div class="col-sm-12">
                <div class="col-sm-1">
                    <img class="img-circle img-responsive" src="../img/frontend/five-star-dr.png">
                </div>
                <div class="col-sm-4">
                    <div class="doctor-name"><b>Dr. Tara Rao MD</b></div>
                    <div class="col-sm-12 specialization">Dermatologist, cardiologist </div>
                    <div class="age">34 yrs. old</div>
                    <div>Peer muchalla, Zirkpur 14003</div>
                    <div class="booked-how-many"><small>Booked 5 times</small></div>
                </div>
                <div class="col-sm-3">
                    <strong class="mb-2">Appointment date/time</strong>
                    <div>May 25, 04:00</div>
                </div>
                <div class="col-sm-3">
                    <strong class="mb-2">Current Status</strong>
                    <div class="label label-warning ">Pending for confirmation</div>
                    <div class="label label-success">Confirmed</div>
                </div>
                <div class="col-sm-1">
                    <strong class="mb-2">Action</strong>
                    <div>
                        <button class="btn btn-cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 m-bottom ">
        <div class="border-medical clearfix">
            <div class="col-sm-12">
                <div class="col-sm-1">
                    <img class="img-circle img-responsive" src="../img/frontend/five-star-dr.png">
                </div>
                <div class="col-sm-4">
                    <div class="doctor-name"><b>Dr. Tara Rao MD</b></div>
                    <div class="col-sm-12 specialization">Dermatologist, cardiologist </div>
                    <div class="age">34 yrs. old</div>
                    <div>Peer muchalla, Zirkpur 14003</div>
                    <div class="booked-how-many"><small>Booked 5 times</small></div>
                </div>
                <div class="col-sm-3">
                    <strong class="mb-2">Appointment date/time</strong>
                    <div>May 25, 04:00</div>
                </div>
                <div class="col-sm-3">
                    <strong class="mb-2">Current Status</strong>
                    <div class="label label-warning ">Pending for confirmation</div>
                    <div class="label label-success">Confirmed</div>
                </div>
                <div class="col-sm-1">
                    <strong class="mb-2">Action</strong>
                    <div>
                        <button class="btn btn-cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
