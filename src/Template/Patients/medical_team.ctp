<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Medical Team"); ?></h2>
                </div>
            </div>
        </div>
    </div>

    <?php if ($patientBookings->isEmpty()) { ?>
        <div class="border-medical clearfix">
            <div class="col-sm-12">
                <?= __("No Past Bookings"); ?>
            </div>
        </div>

    <?php } ?>
    <?php foreach ($patientBookings as $key => $patientBooking) { 
            $isFemale = false;
            if ($patientBooking->user->user_profile->gender == 'Female') {
                $isFemale = true;
            }
        ?>
        <div class="col-sm-8 m-bottom">
            <div class="border-medical clearfix">
                <div class="col-sm-12">
                    <div class="col-sm-2">
                        <?php echo $this->Html->image($this->User->checkAndGetImage($patientBooking->user->user_profile->profile_pic, $isFemale), [
                                    'class' => 'img-circle img-responsive'
                                ]);
                        ?>
                    </div>
                    <div class="col-sm-8">
                        <div class="doctor-name"><b><?= __("Dr")?>. <?= $patientBooking->user->user_profile->full_name; ?></b></div>
                        <div class="col-sm-12 specialization"><?= $this->User->getCommaSeparatedUserSpecialities($patientBooking->user->user_specialities); ?> </div>
                        <div class="age"><?= $this->User->getUserAge($patientBooking->user->user_profile->dob) . ' ' . __('yrs. old'); ?></div>
                        <div class="addresss"><?php echo $patientBooking->user->user_profile->address; echo " " ;echo  $patientBooking->user->user_profile->zipcode;  ?></div>
                        <!-- <div class="booked-how-many"><small>Booked 5 times</small></div> -->
                    </div>
                    <div class="col-sm-2">
                        <a href="<?= $this->Url->build(['controller' => 'Doctors', 'action' => 'view', base64_encode($patientBooking->doctor_id)]); ?>" class="btn btn-blue"><?= __("Book again"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
  
</div>
