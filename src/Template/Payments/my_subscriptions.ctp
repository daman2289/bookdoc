<style>
.dashbroad-content .table {
    margin-top: 0px;
}
.transactionTable {
    margin-top: 80px;
}
</style>
<div class="right-sidebar bg-white">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-tilte-bar">
                <div class="heading-tilte">
                    <h2><?= __("Manage Subscriptions")?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="edit-form dashbroad-content">
        <div class="row">
            <div class="col-sm-12 mobile-res-tb">
                    <h4 class="table-top-heading">
                        <?= __("My Active Plan"); ?>
                    </h4>
                    <table class="table">
                        <thead>  
                            <tr>
                                <th><?= __("Plan"); ?></th>
                                <th><?= __("Active from"); ?></th>
                                <th><?= __("Active upto"); ?></th>
                                <th><?= __("Type"); ?></th>
                                <th><?= __("Subscription"); ?></th>
                                <th><?= __("Action"); ?></th>                   
                            </tr>
                        </thead>
                        <tbody>
                        <?php if (empty($userPlan)) { ?> 
                            <tr>
                                <td colspan="6" class="text-center"><?= __("No Plan Selected"); ?>.</td>
                            </tr>
                        <?php }else{ ?>
                            <tr>
                                <td><?= __($userPlan->user->plan->name); ?> </td>
                                <td><?= __($userPlan->current_period_start->format('M d, Y H:i')); ?></td>
                                <td><?= __($userPlan->current_period_end->format('M d, Y H:i')); ?></td>
                                <?php
                                    switch ($userPlan->is_paid_plan_subscribed) {
                                        case '1': ?>
                                            <td><?= __("Paid"); ?></td>
                                            <?php break;
                                        
                                        default: ?>
                                            <td><?= __("Free"); ?></td>
                                        <?php break;
                                    }
                                ?>
                                 <td>
                                     <?php if ($userPlan->auto_renew) { ?>
                                        <a class="btn btn-md btn-info" href="#" data-toggle="modal" data-target="#myRenewModel">
                                            <?= __("ON"); ?>
                                        </a>
                                    <?php }else{ ?>
                                        <a class="btn btn-md btn-default" href="javascript:void(0);">
                                            <?= __("OFF"); ?>
                                        </a>
                                    <?php } ?>
                                </td> 
                                <td>
                                    <a class="btn btn-md btn-success" href="<?= $this->Url->build(
                                        ['controller' => 'Users', 'action' => 'dashboard', '#' => 'choosePlan']); ?>">
                                        <?= __("Upgrade"); ?>
                                    </a>
                                </td>                              
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
           
        </div>
        
        
        <div class="row transactionTable">
            <div class="col-sm-12 mobile-res-tb"> 
            <h4 class="table-top-heading">
                        <?= __("My Transactions"); ?>                       
                    </h4>
                <table class="table  ">
                    <thead>
                        <tr>
                            <th><?= __("Date"); ?></th>
                            <th><?= __("Plan"); ?></th>
                            <th><?= __("Amount"); ?></th>
                            <th><?= __("Status"); ?></th>
                            <th><?= __("Invoice"); ?></th>
                        </tr>
                    </thead>
                    <tbody>  
                        <?php foreach ($userTransactions as $key => $userTransaction) { ?>

                            <tr>
                                <td><?= $userTransaction->created->format('M d, Y H:i'); ?> </td>
                                <td><?= $userTransaction->plan->name; ?></td>
                                <td><?= $this->Number->currency($userTransaction->amount, 'EUR');?></td>
                                <?php
                                    switch ($userTransaction->is_paid) {
                                        case '1': ?>
                                            <td class=""><?= __("Success"); ?></td>
                                            <?php break;
                                        
                                        default: ?>
                                            <td class=""><?= __("Fail"); ?></td>
                                        <?php break;
                                    }
                                ?>
                                
                                <td>
                                    <?php if ($userTransaction->is_paid) { ?>
                                        <a class="btn btn-default" href="<?= $userTransaction->invoice_url; ?>">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    <?php }else{ ?>
                                        <a><?= __("Error"); ?></a>
                                    <?php } ?>
                                </td>

                            </tr>
                        <?php } ?>
                        <?php if ($userTransactions->isEmpty()) { ?>
                                <tr>
                                    <td colspan="5" class="text-center"><?= __("No Transactions found")?>.</td>
                                </tr>
                        <?php } ?>


                </tbody>    
                </table>
            </div>
           
        </div>

      
        
          <!-- Booking Calender -->

    </div> 
</div>


<?= $this->element('Modals/confirm_auto_renew'); ?>

<?php $this->Html->script([
    "my-model.js",
    "datepick",
    "backend/common/table-responsive",
],['block' => true])?>
