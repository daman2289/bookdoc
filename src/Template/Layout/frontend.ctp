<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <?php
            if(empty($meta_title) ) {
                $meta_title = 'Welcome to Bookdoc';
            }
        ?>
        <title><?php  echo __($meta_title); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-signin-client_id" content="764578562728-0j1hdrvmonajciu1fjgbhu9vea915lf5.apps.googleusercontent.com">
        <?php        
            if(!empty($meta_description)) {
                echo $this->Html->meta(
                    'description',
                    $meta_description
                );
            } 
        ?>
        <?= $this->Html->css("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css") ?>
        <!-- <link rel="stylesheet" href="bower_components/themify-icons/themify-icons.css" /> -->
        <?= $this->Html->css('frontend/styles.css') ?>
        <?= $this->Html->css('frontend/select2.css') ?>
        <?= $this->Html->css('frontend/themify-icons.css') ?>

        <!-- flag css -->
        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css") ?>
        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css") ?>
        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css") ?>
        <!-- google fonts Montserrat, Poppins-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">
        <?= $this->Html->css('frontend/owl.carousel.min.css') ?>
        <?= $this->Html->css('frontend/owl.theme.default.css') ?>
         <?php echo $this->Html->css('frontend/star-rating.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <center><?= $this->Flash->render() ?></center>
        <?php
            echo $this->element('front/top_header');
            echo $this->fetch('content');
            echo $this->element('front/footer');

            echo $this->Html->script([
                'frontend/jquery-2.1.4.min.js',
                'frontend/owl.carousel',
                'bootstrap3.3.7.min',
                'bootstrap-select',
                'underscore-min',
                'frontend/jquery.validate.min',
                'frontend/validate',
                'frontend/select2.full',
                'frontend/additional-methods.min',
                'frontend/pwstrength-bootstrap.min',
                'frontend/home',
                'frontend/star-rating'
            ]);

            echo $this->fetch('script');
        ?>
        <script>
            $(document).ready(function() {
                $('.rate_overall').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true
                });

                $('.rate_overall_doctor').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true,
                    showCaption : false
                });
            });
        </script> 
        <script>
            function onSuccess(googleUser) {
              console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
            }
            function onFailure(error) {
              console.log(error);
            }
            function renderButton() {
              gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
              });
            }
          </script>

          <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </body>
</html>