<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= __("Patient Panel"); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstap css -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="bower_components/themify-icons/themify-icons.css" /> -->
        <?= $this->Html->css('frontend/themify-icons.css') ?>
        <?= $this->Html->css('frontend/styles.css') ?>
        <?= $this->Html->css('frontend/select2.css') ?>

        <!-- flag css -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- google fonts Montserrat, Poppins-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">

        <?= $this->Html->css('frontend/owl.carousel.min.css') ?>
        <?= $this->Html->css('frontend/owl.theme.default.min.css') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <!-- header -->
        <header class="d-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="brand-logo">
                            <?php echo $this->Html->image('frontend/darshboard-logo.png');  ?>
                        </div>
                    </div>
                    <div class="col-sm-10">
                      <div class="top-header-right">
                            <ul>
                                <li>
                                    <div class="dr-profile">
                                        <?php echo $this->Html->image('frontend/profile-pic.png');  ?>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown">
                                      <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <?= __("Welcome Dr, Michael"); ?> 
                                        <span class="caret"></span>
                                      </a>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#"><?= __("view profile")?></a></li>
                                        <li>
                                            <?php echo $this->Html->link('Logout' ,array('controller' => 'Users', 'action' => 'logout'), array('escape' => false)); ?>
                                        </li>
                                      </ul>
                                    </div>
                                </li>
                        </ul>
                      </div>
                    </div>
                </div>
            </div>
        </header>
        <?php
            echo $this->Flash->render();
            echo $this->fetch('content');
            echo $this->element('front/footer');
            echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
            echo $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
            echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js');
            echo $this->Html->script('frontend/jquery.validate.js');
            echo $this->Html->script('frontend/validate.js');
            echo $this->Html->script('frontend/select2.full.js');
            echo $this->Html->script('frontend/additional-methods.min.js');
            echo $this->Html->script('frontend/pwstrength-bootstrap.min.js');
            echo $this->fetch('script');
        ?>
    </body>
</html>