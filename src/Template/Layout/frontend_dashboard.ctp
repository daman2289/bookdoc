<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= __("Welcome to Bookdoc"); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
          <?= $this->Html->meta(
             'favicon.ico',
                '/favicon.ico',
            ['type' => 'icon']
    );
    ?>

        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css') ?>
        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
        
        <!-- Bootstap css -->
        <?= $this->Html->css([
                'bootstrap-min',
                'frontend/font-awesome.min',
                'frontend/themify-icons',
                'frontend/bootstrap-datetimepicker',
                'frontend/styles',
                'frontend/select2',
                'bootstrap-select.min',
                'frontend/owl.carousel.min',
                'frontend/owl.theme.default',
                'frontend/jquery-ui',
                'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css',
                'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css' 
            ]) ?>
        <!-- google fonts Montserrat, Poppins-->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <style>
            body {
                background: #eee;
                font-family: 'Poppins', sans-serif;
                color: #313538;
                font-weight: 400;
            }
        </style>
    </head>
    <body>
        <?php
            $session = $this->request->getSession();
            echo $this->element('front/User/header');
        ?>

        <section class="dashbroad-main">
            <div class="container-fluid">
                <?= $this->Flash->render() ?>
                <div class="d-flex-container">
                    <!-- left sidebar -->
                    <div class="dashbroad-left-sidebar">
                        <?php echo $this->element('front/my_profile_left_bar'); ?>
                        <div class="darshboard-nav">
                            <?php if($session->read('Auth.User.role_id') == 5) {
                                echo $this->element('front/hospital_sidebar');
                            } else {
                                echo $this->element('front/my_side_bar');
                            } ?>
                        </div>
                    </div>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </section>
        <?php
            echo $this->element('dashboard/footer');
            if (!$isPhoneVerified): 
                echo $this->element('front/verify_code');
            endif;

            echo $this->Html->script([
                'frontend/jquery-2.1.4.min',
                'bootstrap3.3.7.min',
                'jQuery-UI',
                'bootstrap-select',
                'underscore-min',
                'frontend/moment',
                'frontend/jquery.validate.min',
                'frontend/validate',
                'frontend/select2.full',
                'frontend/additional-methods.min',
                'frontend/pwstrength-bootstrap.min',
                'frontend/bootstrap-datetimepicker',
                'moment',
                'notify',
                'frontend/User/verify_phone',
                'fullcalendar.min',
                'scheduler.min',
                'notification',
                'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js',
                'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.js',
                'locale/de'
            ]);
            
            echo $this->fetch('script');
            echo $this->element('common/confirmationNotification');
            echo $this->element('common/working_day_notification');
        ?>
        <script>
            $(document).ready(function () {
                $('.darshboard-nav ul li').click(function () {
                    $('.darshboard-nav ul li').removeClass("active");
                    $(this).addClass("active");
                });

                $('#datetimepicker , #datetimepickerendtime').datetimepicker({
                    format: 'HH:mm'
                });
                
               
                $("a.toggle-hum").click(function() {
                    $(".dashbroad-left-sidebar").toggleClass("toggle-side");

                    if ($(".dashbroad-left-sidebar").hasClass("toggle-side")) {
                        $(".right-sidebar").click(function(){
                            $(".dashbroad-left-sidebar").removeClass("toggle-side");

                        });
                    }
                
                });

                $('.js-example-basic-multiple').select2();

                // $('#bookingForm').submit(function(event){
                //     event.preventDefault(); 
                //     var base_url = "<?= $this->Url->build('/', true); ?>"; 
                //     $.ajax({
                //         url: base_url+'doctors/doctor-create-booking',
                //         type: 'POST',
                //         data: $(this).serialize() ,
                //     }).done(function(response) {
                //         var resp = JSON.parse(response);
                         
                //         if (!resp.error) {
                          
                //             location.href = resp.login_url;
                //              return;
                //         }
                        
                //         alert(resp.message);
                //     });
                    
                // })

                $('#cancel_booking_form').submit(function(event){
                    event.preventDefault(); 
                    var base_url = "<?= $this->Url->build('/', true); ?>"; 
                    $.ajax({
                        url: base_url+'doctors/doctor-cancel-booking',
                        type: 'POST',
                        data: $(this).serialize() ,
                    }).done(function(response) {
                         var resp = JSON.parse(response);
                         console.log(resp);
                        if (!resp.error) {
                          
                            location.href = resp.login_url;
                             return;
                        }
                        
                        alert(resp.message);
                    });
                    
                })

                $('#myReForm').submit(function(event){
                    event.preventDefault(); 
                    var base_url = "<?= $this->Url->build('/', true); ?>"; 
                    var bid = $("#bookingId").val();
                    console.log(bid);
                    $.ajax({
                        url: base_url+'doctors/reschedule-appointment/' + bid,
                        type: 'POST',
                        data: $(this).serialize() ,
                    }).done(function(response) {
                         var resp = JSON.parse(response);
                         console.log(resp);
                        if (!resp.error) {
                            location.href = resp.login_url;
                            return;
                        }
                        
                        alert(resp.message);
                    });
                    
                })

                $('#book_search').submit(function(event){
                    event.preventDefault(); 
                    var url = $(this).attr('action'); 
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: $(this).serialize() ,
                    }).done(function(response) {
                         var resp = JSON.parse(response);
                         console.log(resp);
                        if (!resp.error) {
                            location.href = resp.login_url;
                            return;
                        }
                        
                        alert(resp.message);
                    });
                    
                })
            });     

        </script>

    </body>
</html>