<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= __("Welcome to Bookdoc"); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css') ?>
        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
            <!-- Bootstap css -->
            <?= $this->Html->css([
                'bootstrap-min',
                'frontend/font-awesome.min',
                'frontend/themify-icons',
                'frontend/bootstrap-datetimepicker',
                'frontend/styles',
                'frontend/star-rating',
                'frontend/select2',
                'bootstrap-select.min',
                'frontend/owl.carousel.min',
                'frontend/owl.theme.default',
                'frontend/jquery-ui'
            ]) ?>
                <!-- google fonts Montserrat, Poppins-->
                <link href="http://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">
                <?= $this->fetch('meta') ?>
                    <?= $this->fetch('css') ?>
                        <style>
                        body {
                            background: #eee;
                            font-family: 'Poppins', sans-serif;
                            color: #313538;
                            font-weight: 400;
                        }

                        </style>
</head>

<body>
    <?php
            $session = $this->request->getSession();
            echo $this->element('patients/header');
        ?>
        <section class="dashbroad-main">
            <div class="container-fluid">
                <?= $this->Flash->render() ?>
                    <div class="d-flex-container">
                        <!-- left sidebar -->
                        <div class="dashbroad-left-sidebar">
                            <?php echo $this->element('patients/my_profile_left_bar'); ?>
                            <div class="darshboard-nav">
                                <?php echo $this->element('patients/my_side_bar'); ?>
                            </div>
                        </div>
                        <?= $this->fetch('content') ?>
                    </div>
            </div>
        </section>
        <?php
            echo $this->element('dashboard/footer');
            if (!$isPhoneVerified): 
                echo $this->element('front/verify_code');
            endif;

            echo $this->Html->script([
                'frontend/jquery-2.1.4.min',
                'bootstrap3.3.7.min',
                'jQuery-UI',
                'bootstrap-select',
                'underscore-min',
                'frontend/moment',
                 'frontend/star-rating',
                'frontend/jquery.validate.min',
                'frontend/validate',
                'frontend/select2.full',
                'frontend/additional-methods.min',
                'frontend/pwstrength-bootstrap.min',
                'frontend/bootstrap-datetimepicker',
                'moment',
                'notify',
                'frontend/User/verify_phone'
            ]);
            echo $this->fetch('script');
        ?>
            <script>
            $(document).ready(function() {
                $('.darshboard-nav ul li').click(function() {
                    $('.darshboard-nav ul li').removeClass("active");
                    $(this).addClass("active");
                });

                $(function() {
                    $('#datetimepicker , #datetimepickerendtime').datetimepicker({
                        format: 'HH:mm'
                    });

                });

                $('.js-example-basic-multiple').select2();

                $('.kv-uni-star').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;'
                });

                $('.kv-uni-star1').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;'
                });

                $('.kv-uni-star2').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;'
                });

                $('.rate_overall').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true
                });

                $('.manner_bedside').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true
                });

                $('.time_wait').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true
                });

                // $('.kv-uni-star').on('change',
                //     function() {
                //         console.log($(this).val());
                //         $('#overall_rating').val($(this).val());
                //     });

                // $('.kv-uni-star1').on('change',function(){
                //     var bedside = $(this).val();
                //     $('#bedside_manner').val(bedside);
                // });

                // $('.kv-uni-star2').on('change',function(){
                //     $('#wait_time').val($(this).val());
                // });
                $("a.toggle-hum").click(function() {
                $(".dashbroad-left-sidebar").toggleClass("toggle-side");

                if ($(".dashbroad-left-sidebar").hasClass("toggle-side")) {
                    $(".right-sidebar").click(function(){
                        $(".dashbroad-left-sidebar").removeClass("toggle-side");

                    });
                }
            
            });

            });

            </script>
</body>

</html>
