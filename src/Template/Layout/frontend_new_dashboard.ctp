<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= __("Welcome to Bookdoc"); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
          <?= $this->Html->meta(
             'favicon.ico',
                '/favicon.ico',
            ['type' => 'icon']
    );
    ?>

        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css') ?>
        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
        
        <!-- Bootstap css -->
        <?= $this->Html->css([
                'bootstrap-min',
                'frontend/font-awesome.min',
                'frontend/themify-icons',
                'frontend/bootstrap-datetimepicker',
                'frontend/styles',
                'frontend/select2',
                'bootstrap-select.min',
                'frontend/owl.carousel.min',
                'frontend/owl.theme.default',
                'frontend/jquery-ui',
            ]) ?>
        <!-- google fonts Montserrat, Poppins-->
        <link href="http://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <style>
            body {
                background: #eee;
                font-family: 'Poppins', sans-serif;
                color: #313538;
                font-weight: 400;
            }
        </style>
    </head>
    <body>
        <?php
            $session = $this->request->getSession();
            echo $this->element('front/User/header');
        ?>  
        <section class="dashbroad-main">
            <div class="container-fluid">
                <?= $this->Flash->render() ?>
                <div class="d-flex-container">
                    
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </section>
        <?php
            echo $this->element('dashboard/footer');
            if (!$isPhoneVerified): 
                echo $this->element('front/verify_code');
            endif;

            echo $this->Html->script([
                'frontend/jquery-2.1.4.min',
                'bootstrap3.3.7.min',
                'jQuery-UI',
                'bootstrap-select',
                'underscore-min',
                'moment.min',
                'frontend/jquery.validate.min',
                'frontend/validate',
                'frontend/select2.full',
                'frontend/additional-methods.min',
                'frontend/pwstrength-bootstrap.min',
                'bootstrap-datetimepicker',
                'notify',
                'frontend/User/verify_phone',
                'fullcalendar.min',
                'gcal.min',
                'scheduler.min',
                'notification'
            ]);
            echo $this->fetch('script');
        ?>
        <script>
            $(document).ready(function () {
                
                $('.darshboard-nav ul li').click(function () {
                    $('.darshboard-nav ul li').removeClass("active");
                    $(this).addClass("active");
                });

                $(function () {
                    $('#datetimepicker , #datetimepickerendtime').datetimepicker({
                        format: 'HH:mm'
                    });

                });


            });
            $("a.toggle-hum").click(function() {
                $(".dashbroad-left-sidebar").toggleClass("toggle-side");

                if ($(".dashbroad-left-sidebar").hasClass("toggle-side")) {
                    $(".right-sidebar").click(function(){
                        $(".dashbroad-left-sidebar").removeClass("toggle-side");

                    });
                }
            
            });

        </script>
        <script>
            // $('#myModal').modal('show');
            $(document).ready(function () {
                $('.js-example-basic-multiple').select2();


            //    var winwidth = $(window).width(); 
            //    if( winwidth > 768 ){
            //     $(".dashbroad-left-sidebar").addClass("hidden-cal");
            //    } 
            //    else if( winwidth < 768 ){
            //     $(".dashbroad-left-sidebar").removeClass("hidden-cal");
            //    } 
            });

        </script>
    </body>
</html>