<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo __('Bookdoc :: Admin Login'); ?></title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="shortcut icon" href="favicon.ico"/>
		<!-- Bootstrap 3.3.5 -->     
		<?=
		 $this->Html->css(
			[
				'backend/bootstrap.min',
				'backend/login'
			],
			['block' => true]
		 );
		?>
		<?=
		 $this->Html->script(
			[
			   'backend/common/jquery-3.3.1.min',
			   'backend/common/bootstrap.min',
			],
			['block' => true]
		 );
		?>
		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
	</head>
	<body>
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="container">
					<div class="col-md-8 col-lg-offset-1 col-md-offset-1 login-inner-wraper padding-zero clearfix">
						<div class="col-md-12 padding-zero">
							<?= $this->Flash->render() ?>
						</div>
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
			</div>
		</div>
	<!-- Latest compiled and minified JavaScript -->
	  <?php echo $this->fetch('script'); ?>
	  <?php echo $this->fetch('scriptBottom'); ?> 
	</body>
</html>