<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title data-tid="elements_examples.meta.title"><?= __("Complete Payment ")?> | <?= __("Bookdoc"); ?></title>
    <meta data-tid="elements_examples.meta.description" name="description" content="Build beautiful, smart checkout flows.">

    <script src="https://js.stripe.com/v3/"></script>
    <script src="/js/stripe/index.js" data-rel-js></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" data-rel-css=""
    />
    <?= 
        $this->Html->css([
        'stripe/base.css',
        'stripe/example2'
    ])
    ?>
    <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css') ?>
        <?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') ?>
        
        <!-- Bootstap css -->
        <?= $this->Html->css([
                'frontend/font-awesome.min',
                'frontend/themify-icons',
                'frontend/styles',
                'frontend/jquery-ui',
            ]) ?>

        <!-- CSS for each example: -->
</head>

<body>
<style>
footer{
    max-width: unset;
}
body{
    min-height: unset;
}
</style>
    <?= $this->element('front/User/header'); ?>
    <section class="dashbroad-main">
        <div class="container-fluid">
            <?= $this->Flash->render() ?>
            <div class="d-flex-container">
                
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </section>
    <?= $this->element('dashboard/footer'); ?>
    <!-- Simple localization script for Stripe's examples page. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" data-rel-js></script>
    <script src="/js/stripe/l10n.js" data-rel-js></script>

    <!-- Scripts for each example: -->
    <script src="/js/stripe/example2.js" data-rel-js></script>
    
</body>

</html>
