<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo __('Bookdoc | dr online | arzttermin | zahnarzttermin | arztsuche wien'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msvalidate.01" content="6C60DBAB1DC98F9D6C833BA3BA523366" />
        <link rel="canonical" href="https://www.bookdoc.at/" />
        <meta name="google-site-verification" content="DbYUdZDFPx-TU5wYxJh-GQZ7SIM2bs40vWySf7n-IVQ" />
        <?php 
            if($this->request->getSession()->read('Config.locale') == 'en_US') { 
                echo $this->Html->meta(
                    'description',
                    'Bookdoc is online platform that connects patients with specialist doctors. Find & book dentist, physicians, and doctor appointment online in Vienna, Austria'
                ); 
            } else {            
                echo $this->Html->meta(
                    'description',
                    'Bookdoc is een online platform dat patiënten verbindt met gespecialiseerde artsen. Zoek en boek een afspraak voor artsen, artsen en artsen online in Wenen, Oostenrijk'
                );
            }
        ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128280446-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-128280446-1');
        </script>
        <?= $this->Html->css("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css") ?>
        <!-- <link rel="stylesheet" href="bower_components/themify-icons/themify-icons.css" /> -->
        <?= $this->Html->css('frontend/styles.css') ?>
        <?= $this->Html->css('frontend/select2.css') ?>
        <?= $this->Html->css('frontend/themify-icons.css') ?>
        <?= $this->Html->script('frontend/jquery-2.1.4.min.js') ?>

        <!-- flag css -->
        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css") ?>
        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css") ?>

        <?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css") ?>

        <!-- google fonts Montserrat, Poppins-->
      <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
      <link rel="canonical" href="https://www.bookdoc.at/" />
        <?= $this->Html->css('frontend/owl.carousel.min.css') ?>
        <?= $this->Html->css('frontend/owl.theme.default.css') ?>
        <?php echo $this->Html->css('frontend/star-rating.css') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?=  $this->element('common/push_notification'); ?>
        <meta name="google-site-verification" content="X13VEw98nyGAX49iX6xNzuJ5_jZ09tKKNWol4B3Evg8" />
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
           })(window,document,'script','dataLayer','GTM-PNQXGK3');</script>
        <!-- End Google Tag Manager -->
    </head>

    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNQXGK3"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <center><?= $this->Flash->render() ?></center>
    <?php
        echo $this->fetch('content');
        echo $this->element('front/footer');

        echo $this->Html->script([
            'frontend/jquery-2.1.4.min.js',
            'bootstrap3.3.7.min',
            'bootstrap-select',
            'frontend/select2.full',
            'frontend/owl.carousel',
            'frontend/home',
            'frontend/star-rating',
             'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js'

        ]);

        echo $this->fetch('script');
    ?>
        <script>
            $(document).ready(function() {
                $('.rate_overall').rating({
                    theme: 'krajee-uni',
                    filledStar: '&#x2605;',
                    emptyStar: '&#x2606;',
                    readonly : true
                });
            });
        </script> 
</body>
</html>