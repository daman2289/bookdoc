<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
$cakeDescription = __('Book Doc');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128280446-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-128280446-1');
        </script>
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <!-- Latest compiled and minified CSS -->
        <?= $this->Html->css('backend/bootstrap.min') ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?= $this->Html->css('backend/stylesheet'); ?>
        <?= $this->Html->css('backend/style') ?>
        <?= $this->Html->css('backend/custom-switch') ?>
        <?= $this->Html->css('backend/font-awesome.min') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?php 
        echo $this->Html->script(
            [
               'backend/common/jquery-3.3.1.min',
               'backend/common/bootstrap.min',
               'backend/common/bootstrap-switch.min.js',

            ],
            ['block' => true]
        );
        ?>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PNQXGK3');</script>
<!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNQXGK3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div class="wrapper" id="vue-container">
            <?= $this->Flash->render() ?>
            <?php
                echo $this->element('common/header_common');
                echo $this->element('common/side_bar_common');                
            ?>

            <div class="container-wrapper clearfix">
                <?php echo $this->fetch('content'); ?>
            </div>
            <footer class="main-footer text-center">               
                <strong>Copyright © 2018 All rights reserved.</strong>
            </footer>
            <?php //echo $this->fetch('modals'); ?>
        </div>        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <!-- Latest compiled and minified JavaScript -->
        <?php echo $this->fetch('script'); ?>
        <?php echo $this->fetch('scriptBottom'); ?> 
    </body>
</html>