<?php
    use Cake\Core\Configure;
    $session = $this->request->getSession();
?>
<header class="d-header">
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-sm-3 col-xs-12">
            
                <div class="brand-logo">
                    <?php
                        echo $this->Html->link($this->Html->image('frontend/darshboard-logo.png'), ['controller' => 'patients', 'action' => 'dashboard'], ['escape' => false]);
                        ?>
                </div>
            </div>
            
            <div class="col-sm-9 col-xs-12">
            <a href="#" type="button" class="toggle-hum"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg></a>
                <div class="top-header-right">
                    <ul>
                        <li>
                            <?php $profileLink = $this->Url->build(['controller' => 'Patients', 'action' => 'edit-profile'], ['escape' => false])?>
                            <a href="<?= $profileLink; ?>"> 
                                <div class="dr-profile">
                                    <?php 
                                        if ($session->read('Auth.User.user_profile.gender') == 'Male'):
                                            $pImg = 'frontend/Dummy-image.jpg';
                                        else:
                                            $pImg = 'frontend/woman_dummy.png';
                                        endif;
                                        
                                        if (!empty($session->read('Auth.User.user_profile.profile_pic'))) {
                                            $pImg = '../files/uploads/' . $session->read('Auth.User.user_profile.profile_pic');
                                        } else{
                                           $pImg = $session->read('Auth.User.social_profile.picture_url');
                                        }
                                        //pr($pImg);die;
                                        echo $this->Html->image($pImg);
                                    ?>

                                </div>
                            </a>
                        </li>
                        <li>
                            <div class="dropdown">
                                <?php
                                    $name = ($session->read('Auth.User.role_id') == Configure::read('UserRoles.Doctor')) ? 'Dr. ' : '';
                                    $name .= $session->read('Auth.User.user_profile.first_name') . " " . $session->read('Auth.User.user_profile.last_name');
                                    echo $this->Html->link(
                                        $name . '<span class="caret"></span>',
                                        'javascript:void(0)',
                                        [
                                            'id' => 'dLabel',
                                            'data-toggle' => 'dropdown',
                                            'role' => 'button',
                                            'aria-haspopup' => true,
                                            'aria-expanded' => false,
                                            'escape' => false
                                        ]
                                    );
                                ?>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                                    <li><a href="<?= $profileLink; ?>"><?= __('view profile') ?></a></li>
                                    <li>
                                        <?php echo $this->Html->link(__('Logout'), array('controller' => 'Users', 'action' => 'logout'), array('escape' => false)); ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<script type="text/javascript">
     
</script>