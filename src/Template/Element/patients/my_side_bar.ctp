<?php
$action = !empty($this->request->getParam('action')) ? $this->request->getParam('action') : null;
?>
<ul>
    <li class="<?= ($action == "dashboard") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-tachometer"></i> ' . __('Patient Dashboard'), ['controller' => 'Patients', 'action' => 'dashboard'], ['escape' => false]
        );
        ?>
    </li>

   <li  class="<?= ($action == "medicalTeam") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-users"></i> ' . __('Medical Team'), ['controller' => 'Patients', 'action' => 'medical-team'],['escape' => false]
        );
        ?>
    </li>


    <li  class="<?= ($action == "pastAppointment") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-history"></i> ' . __('Past Appointments'), ['controller' => 'Patients', 'action' => 'past-appointment'],['escape' => false]
        );
        ?>
    </li>
    <?php $class = ($this->request->getParam('controller') == 'Patients' && $this->request->getParam('action') == 'reports') ? 'active' : '' ?>
    <li class = '<?php echo $class?>'>
        <?php echo $this->Html->link('<i class="fa fa-file-text-o"></i>'.__('My Reports'),
            ['controller' => 'Patients','action' => 'reports'],
            ['escape' => false]
        );?>
    </li>
    <li  class="<?= ($action == "favoriteDoctors") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-user-md"></i> ' . __('My favorite doctors'), ['controller' => 'Patients', 'action' => 'favorite-doctors'],['escape' => false]
        );
        ?>
    </li>


    

    <li  class="<?= ($action == "editProfile") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-pencil-square-o"></i> ' . __('Edit Profile'), ['controller' => 'Patients', 'action' => 'edit-profile'], ['escape' => false]
        );
        ?>
    </li>
  
    <li class="<?= ($action == "changePassword") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-key"></i> ' . __('Change Password'), ['controller' => 'Patients', 'action' => 'change-password'], ['escape' => false]
        );
        ?>    
    </li>
   
  
   
   
  
    <li  class="<?= ($action == "logout") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-sign-out"></i> ' . __('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]
        );
        ?> 
    </li>
</ul>