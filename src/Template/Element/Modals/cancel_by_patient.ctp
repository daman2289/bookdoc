<style>
.table>thead>tr>th {
    border-bottom:none
}
.modal-footer{
    text-align: center;
}
</style>

<div id="cancelByPatientModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <?= 
                $this->Form->create(null,[
                        'type' => 'post',
                        'url' => ['controller' => 'patients', 'action' => 'cancelBooking'],
                        'id' => 'form',
                        'autocomplete' => "off"
                ]);
            ?>
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= __("Cancel Appointment"); ?></h4>
        </div>
        <div class="modal-body">
            <table class="table cancelappmodal">
                <thead>
                    <tr>
                        <th><?= __("Are you sure to cancel your Appointment on") ?> <span id="bookingDate"></span> <i class="fa fa-calendar-check-o"></i> ? </th>
                    </tr>
                </thead>
            </table>
            
            <div class="form-group">
                <?php
                    echo $this->Form->control('reason',[
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'required' => true,
                        'label' => __("But why..?"),
                    ]);
                ?>
                <div class="help-block with-errors"></div>
                <?=
                    $this->Form->control('booking_id',[
                        'type' => 'hidden',
                        'label' => false,
                        'id' => 'bid'
                    ]);
                ?>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-confirm"> <?= __("Confirm"); ?></button>
      </div>
    </div>
    <?= $this->Form->end()?>
  </div>
</div>