<div class="modal fade" id="review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= __("Leave a review"); ?></h4>
            </div>
            <?php 
                echo $this->Form->create('', [
                    'url'   => ['controller' => 'Patients', 'action' => 'addRating'],
                    'type'  => 'file',
                    'id'    => 'add-report-form'
                    ]
                );
            ?>  
            <?php echo $this->Form->control('doctor_id',['type' => 'hidden','class' => 'review_doctor']);?>
            <?php echo $this->Form->control('booking_id',['type' => 'hidden','class' => 'review_booking']);?>
            <div class="modal-body">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label><?= __("Enter your feedback"); ?></label>
                        <textarea class="form-control" name="review" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Overall rating"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star rating-loading myrate" id="overall_rating" name="overall_rating" value="" data-size="xs" title="">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Bedside Manner"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star1 rating-loading myrate" id="bedside_manner" name="bedside_manner" value="" data-size="xs" title="">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Wait time"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star2 rating-loading myrate" id="wait_time" name="wait_time" value="" data-size="xs" title="">
                    </div>
                </div>
                <div class="form-group clearfix text-center mt-5">
                    <button type="submit" class="btn btn-cancel"><?= __("Submit Review"); ?></button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
<?php $this->Html->scriptStart(['block' => true]); ?>
        //<script>
        // Rating Script(Star Rating Custom Attributes)
        $(".myrate").rating({
            'showCaption': false,
            'showClear': false,
            'size':'xxxs'
        });
    //</script>
<?php $this->Html->scriptEnd(); ?>