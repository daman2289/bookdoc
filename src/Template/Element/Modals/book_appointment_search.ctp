<!-- Modal -->
<div class="modal fade someonemodal" id="bookAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"><?= __("Book Appointment"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="right-side-from bg-white">
                        <?= 
                            $this->Form->create(null,[
                                'type' => 'file',
                                'url' => ['controller' => 'Doctors','action' => 'bookAppointment'],
                                'id' => 'book_search',
                                'autocomplete' => "off"
                            ]);
                        ?>
                            <div class="form-group old_patient">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <?php
                                        echo $this->Form->checkbox('old_patient', ['id' => 'my_patient','hiddenField' => false]);
                                    ?>
                                    <label class="control-label"><?php echo __('This Doctor just see only old patients and do not accept new patient, did you visit this doctor before ?'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php
                                            echo $this->Form->control('booking_date',[
                                                'type' => 'text',
                                                "id" => "bookingDate",
                                                'label' => __('Booking Date'), 
                                                'class' => 'form-control',
                                                "readonly"=>"readonly",
                                                'required' => true,
                                                'data-error' => __('Please select valid booking date')
                                                ]);
                                        ?>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <?=
                                            $this->Form->control('booking_time',[
                                                'type' => 'text',
                                                "id" => "bookingTime",
                                                'label' => __('Booking Time'), 
                                                "readonly"=>"readonly",
                                                'class' => 'form-control',
                                                'data-error' => __('Please select valid booking time')
                                            ]);
                                        ?>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                </div>

                                <?=
                                    $this->Form->control('doctor_id',[
                                        'type' => 'hidden',
                                        "id" => "doctor", 
                                        'class' => 'form-control',
                                        'value' => "",
                                    ]);
                                ?>
                                
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label"><?php echo __('Refer this Patient'); ?></label>
                                        <div class="col-sm-12">
                                            <?php $attributes['value'] = 1;
                                            echo $this->Form->radio('refer_yes', [__('Yes'),__('No')],$attributes);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group refer hide">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <label class="control-label"><?php echo __('Upload previous Prescription(if any)'); ?></label>
                                    <?php
                                        echo $this->Form->control('file', array(
                                            'class' => 'form-control',
                                            'label' => false,
                                            'type' => 'file'
                                            )
                                        );
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?=
                                    $this->Form->control('insurance',[
                                        'type' => 'select',
                                        'class' => 'form-control',
                                        'options' =>  $insurances,
                                        'lable' => __('Insurance'),
                                        'id' => 'myInsurance',
                                        'value' => 1,
                                        'required' => true,
                                        'label' => __('Insurance'), 
                                    ]);
                                ?>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <?=
                                    $this->Form->control('illness_reason_id',[
                                        'type' => 'select',
                                        'class' => 'form-control',
                                        'options' =>  $specialities,
                                        'required' => true,
                                        'label' => __('Illness Reason'), 
                                        'label' => __('What\'s the reason for your visit?'), 
                                    ]);
                                ?>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                               <label><?= __('Who will be seeing the doctor?'); ?>  </label> 
                                
                                <?= $this->Form->control('who_will_seeing',[ 
                                    'type' => 'radio',
                                    'lable' => __('Who will seeing'),
                                    'name' => 'who_will_seeing',
                                    'options' => $seeing_name,
                                    'label' => false,
                                    ]);
                                    ?>
                                <div class="help-block with-errors"></div>
                            </div>
                            
                            <div id="datahere">
                            </div>

                            <div class="form-group">
                               <?= $this->Form->control('insuranceMemId',[
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'label' => __('Insurance Member id (Optional)'),
                                    'placeholder' => __('Insurance Member id (Optional)')
                                ]);
                               ?>
                            </div>

                            <div class="form-group">
                               <?= $this->Form->control('doctor_note',[
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'label' => __('Note For Doctor (Optional)'),
                                    'placeholder' => __('Note For Doctor (Optional)')
                                    
                                ]);
                               ?>
                            </div>
                            
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?=
                                            $this->Form->control(__('Book Appointment'),[
                                                'class' => 'submit',
                                                'type' => 'submit'                    
                                            ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?= $this->Form->end(); ?>
                    </div>
            </div>
        </div>
    </div>
</div>


<!-- ****************************************  Dynamic Data ********************************************************** -->

 <div id="patientData" class="hide addPatient">                            
    <div class="form-group clearfix">
        <label>Patient name</label>
        <div class="row">    
            <div class="col-sm-6">
                <?=
                    $this->Form->control('patient_first_name',[
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => true,                                        
                        'placeholder' => __("First") 
                    ]);
                ?>
                <div class="help-block with-errors"></div> 
            </div>

            <div class="col-sm-6">
                <?=
                    $this->Form->control('patient_last_name',[
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => false,
                        'required' => true,                                        
                        'placeholder' => __("Last"), 
                    ]);
                ?>
                <div class="help-block with-errors"></div> 
            </div>
        </div>
    </div>
    <div class="form-group clearfix">
        <div class="row">    
            <div class="col-sm-12">
                <?= $this->Form->label('Patient\'s email (optional)'); ?>
            </div>
            <div class="col-sm-12 form-group">
                <?=
                    $this->Form->control('patient_email',[
                        'type' => 'email',
                        'class' => 'form-control',
                        'placeholder' => __("Email"),
                        'label' => false, 
                    ]);
                ?>
                 <div class="help-block with-errors"></div> 
            </div>
        </div>
    </div>
    <div class="form-group clearfix">
        <div class="row">    
            <div class="col-sm-12">
                <?= $this->Form->label('Patient\'s date of birth'); ?>                                            
            </div>
            <div class="col-sm-12">
                <div class="input-group date" data-provide="datepicker" data-date-format = "dd-mm-yyyy">
                    <?=
                        $this->Form->control('patient_dob',[
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false, 
                            "placeholder" => "dd-mm-yyyy",
                            "id" => "datepicker",
                            'required' => true,
                        ]);
                    ?>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                <div class="help-block with-errors"></div>
            </div>
            
        </div>
    </div>
    <div class="form-group clearfix">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->Form->label('Patient\'s sex'); ?>
            </div>
            <div class="col-sm-12">
                <?= $this->Form->control('patient_sex',[ 
                    'type' => 'radio',
                    'hiddenField' => false,
                    'options' => [
                        ['value' => 'Male', 'text' => __('Male'), 'class' => 'radio-inline', 'checked' => "checked"],
                        ['value' => 'Female', 'text' => __('Female'), 'class' => 'radio-inline']
                    ],
                    'label' => false ,
                    'required' => true,
                    ]);
                ?>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    
</div>


<?php
    echo $this->Html->script([
        "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"
    ],['block' => true]);
?>