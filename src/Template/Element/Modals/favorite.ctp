<div id="favourite" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p><?= __("Are you sure you want this doctor your favourite doctor ?"); ?></p>
      </div>
      <div class="modal-footer">
        <?php
        echo $this->Form->create('', [
            'url' => ['controller' => 'Doctors', 'action' => 'makeFavourite'],
            'type' => 'post',
            'id' => 'make-favourite',
            'novalidate' => true
                ]
        );
        ?> 
        <?php echo $this->Form->control('doctor_id',['value' => $doctor->id ,'type' => 'hidden']);?>
        <?php echo $this->Form->control('patient_id',['value' => $this->request->getSession()->read('Auth.User.id') ,'type' => 'hidden']);?>
        <button type="submit" class="btn btn-default"><?= __("Yes"); ?></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("No"); ?></button>
        </form>
      </div>
    </div>

  </div>
</div>