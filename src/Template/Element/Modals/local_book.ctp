<style>
.save-btn{
    text-align: center;
    margin: auto;
}
/* label{
        display: inline-block !important;
} */
</style>
<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" />
<!-- Modal -->
<div class="modal fade" id="localBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <?= 
        $this->Form->create(null,[
            'type' => 'post',
            'url' => ['controller' => 'Doctors','action' => 'bookAppointment'],
            'id' => 'form',
            'autocomplete' => "off"
        ]);
        ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLongTitle"><?= __("Book Local Appointment")?></h5>
      </div>
      <div class="modal-body">
            <?= $this->Form->label('Booking Date'); ?>
            <div class="form-group">
                <div class="input-group" id="datetimepicker4" data-id="<?= $this->request->getSession()->read('Auth.User.id'); ?>">
                    <?=
                        $this->Form->control('booking_date',[
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false, 
                            "placeholder" => "yyyy-mm-dd",
                            'required' => true,
                        ]);
                    ?>
                    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <div class="help-block with-errors"></div>
            </div>
            <div class="hide text-danger" id="errorMsg"><?= __("No Block timings specified for this weekday.") ?></div>
            <div class="hide" id="enterData">
            
                <div>
                    <div class="form-group">
                        <?= $this->Form->label('Booking Time'); ?>
                        <div id="availabletime">
                            
                        </div>
                    </div>
                </div> 


            
                <?= $this->Form->label('Patient\'s Name'); ?>            
                <div class="form-group">
                    <?=
                        $this->Form->control('patient_first_name',[
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => __("First Name"),
                            'required' => true,
                            'label' => false, 
                            ]);
                            ?>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <?=
                        $this->Form->control('patient_last_name',[
                            'type' => 'text',
                            'placeholder' => __("Last Name"),                            
                            'class' => 'form-control',
                            'required' => true,
                            'label' => false, 
                            ]);
                            ?>
                    <div class="help-block with-errors"></div>
                    <?=
                        $this->Form->control('doctor_id',[
                            'type' => 'hidden',
                            'label' => false, 
                            'value' => $this->request->getSession()->read('Auth.User.id')
                            ]);
                    ?>
                    <?=
                        $this->Form->control('booking_type',[
                            'type' => 'hidden',
                            'label' => false, 
                            'value' => '1'
                            ]);
                    ?>

                    <div class="help-block with-errors"></div>
                </div>


                <div class="form-group">    
                    <?= $this->Form->control('patient_email',[ 
                        'type' => 'email',
                        'label' => __("Email"),
                        'placeholder' => __("Patient's Email"),                    
                        'class' => 'form-control',
                        'required' => true,
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    
                    <?= $this->Form->control('illness_reason_id',[ 
                        'type' => 'select',
                        'class' => 'form-control',
                        'options' =>  $reasons,
                        'required' => true,                    
                        'label' => __("Reason"),
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                </div>
                <?= $this->Form->label(__('Gender')); ?>
                <div class="form-group">    
                    <?= $this->Form->control('gender',[ 
                        'type' => 'radio',
                        'class' => '',                    
                        'label' => false,
                        'options' => [
                            ['value' => 'Male', 'text' => __('Male')],
                            ['value' => 'Female', 'text' => __('Female')]
                        ]
                        ]);
                    ?>
                <div class="help-block with-errors"></div>
            </div>
            </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-12 save-btn">
         <?=
            $this->Form->button(__('Save'),[
                'type' => 'submit',  
                
                'class' => 'btn',
            ]);
        ?>
        </div>
      </div>
        <?= $this->Form->end(); ?>
      
    </div>
  </div>
</div>


<?php
    echo $this->Html->script([
        "https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js",
    ],['block' => true]);
?>
<script>

</script>