
<div class="modal fade myconfirm-modal" id="mySpecialModel" role="dialog">
    <div class="modal-dialog">
    
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                <h4 class="modal-title text-center"><?= __("Search Doctors Based on Specialities") ?> </h4>
            </div>
            <div class="modal-body">
                <ul>
                    <?php foreach ($specialities as $key => $speciality) { ?>
                        <li>
                            <!-- <a href="<?= $this->Url->build([
                                'controller' => 'search', 'action' => 'index', 
                                '?' => ['term' =>  $speciality]]); ?>">
                                <?= $speciality; ?>
                            </a> -->
                            <!-- <?php echo $this->Form->postLink($speciality,'/search/'.$speciality,['data' => ['term' => $speciality]]);?> -->
                            <a href="/speciality/<?= $key; ?>"><?= $speciality; ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close"); ?></button>
            </div>
        </div>
    
    </div>
</div>