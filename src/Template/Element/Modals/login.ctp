
<div id="loginModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?php echo $this->Form->create('',['url'=> ['controller' => 'Doctors','action' => 'login']]); ?>
                <div class="form-group">
                    <label><?php echo __('Email Address'); ?></label>
                    <?php echo $this->Form->control('email', [
                        'class' => 'form-control',
                        'placeholder' => __('Email Address'),
                        'autocomplete' => 'on',
                        'label' => false
                    ]); ?>
                </div>
                <div class="form-group">
                   <?php
                        echo $this->Form->hidden('selectedTime');
                        echo $this->Form->hidden('selectedDate');
                        echo $this->Form->hidden('doctorId');
                    ?>
                    <label><?php echo __('Password'); ?></label>
                   <?php
                        echo $this->Form->password('password', [
                                'class' => 'form-control',
                                'placeholder' => __('Password'),
                                 'autocomplete' => 'on'
                            ]);
                    ?>
                    <span id="result"></span>
                </div>
                <div class="form-group forget-pass">
                    <?php echo $this->Html->link(__('Forgot Your Password'),['controller'=>'Users','action'=>'forgetPassword'], ['class' => '']); ?>
                </div>
                <div class="form-group ">
                   <?php
                        echo $this->Form->button(__('Login'),
                            [
                                'type' => 'submit',
                                'class' => 'btn btn-block btn-signup'
                            ]
                        );
                        /*echo $this->Html->link("Forgot Password ?",['controller'=>'Users','action'=>'forgotPassword'], ['class' => 'pull-right']);*/
                    ?>
                </div>
        <?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close"); ?></button>
      </div>
    </div>

  </div>
</div>