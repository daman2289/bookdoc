
<div class="modal fade myconfirm-modal" id="myInsuranceModel" role="dialog">
    <div class="modal-dialog">
    
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                <h4 class="modal-title text-center"><?= __("Search Doctors Based on Insurances") ?> </h4>
            </div>
            <div class="modal-body">
                <ul>
                    <?php
                         unset($insurances[1]);    
                        foreach ($insurances as $key => $value) { ?>
                        <li>
                            <!-- <a href="<?= $this->Url->build([
                                'controller' => 'search', 'action' => 'index', 
                                '?' => ['insurance' =>  $key]]); ?>">
                                <?= $value; ?>
                            </a> -->
                            <a href="/insurance/<?= $key; ?>"><?= $value; ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Close"); ?></button>
            </div>
        </div>
    
    </div>
</div>