<div class="modal fade myconfirm-modal" id="myConfirmModel" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                <h4 class="modal-title text-center"><?= __("Confirm") ?> </h4>
            </div>
        <div class="modal-body">
            <p><?= __("Are you sure to confirm this Booking?"); ?></p>
        </div>
        <div class="modal-footer" style="text-align: center">
            <?= 
                $this->Form->create(null,[
                    'type' => 'post',
                    'url' => '#',
                    'id' => 'updateBookingForm',
                ]);
            ?>
            <?=
                $this->Form->control('status',[
                'type' => 'hidden',
                'value' => '2',
                ]);
            ?>
            <button type="submit" class="btn btn-model" ><?= __("Confirm") ?></button>
            <?= $this->Form->end(); ?>
        </div>
    </div>
      
</div>