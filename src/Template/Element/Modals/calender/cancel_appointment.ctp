<style>
.table>thead>tr>th {
    border-bottom:none
}
.modal-footer{
    text-align: center;
}
</style>

<div id="cancelAppModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <?= 
                $this->Form->create(null,[
                        'type' => 'post',
                        'id' => 'cancel_booking_form',
                        'autocomplete' => "off"
                ]);
            ?>
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= __("Cancel Appointment"); ?></h4>
        </div>
        <div class="modal-body">
            <table class="table cancelappmodal">
                <thead>
                    <tr>
                        <th><?= __("Appointment Date") ?> <i class="fa fa-calendar-check-o"></i> </th>
                        <td> <span class="appdate"></td>
                    </tr>
                    <tr>
                        <th><?= __("Patient's Name"); ?> </th>
                        <td><span class="pname"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Patient's Gender"); ?> </th>
                        <td><span class="pgender"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Patient's Email"); ?> </th>
                        <td><span class="pmail"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Patient's Mobile"); ?> </th>
                        <td><span class="pmob"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Patient's Illness Reason"); ?> </th>
                        <td><span class="illreason"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Doctor Note"); ?> </th>
                        <td><span class="docnote"></span></td>
                    </tr>
                </thead>
            </table>

            <div class="form-group">
                <?php
                    echo $this->Form->control('reason',[
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'required' => true,
                        'label' => __("Cancellation Reason"),
                    ]);
                ?>
                <div class="help-block with-errors"></div>
                <?=
                    $this->Form->control('booking_id',[
                        'type' => 'hidden',
                        'label' => false,
                        'id' => 'bid'
                    ]);
                ?>
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-confirm"> <?= __("Confirm"); ?></button>
      </div>
    </div>
    <?= $this->Form->end()?>
  </div>
</div>