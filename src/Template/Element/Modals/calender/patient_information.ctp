<div id="patient-info" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= __("Appointment Information")?> </h4>
      </div>
      <div class="modal-body">
        <table class="table cancelappmodal">
                <thead>
                    <tr>
                        <th><?= __("Current Status") ?> </th>
                        <td> <span class="status"></td>
                    </tr>
                    <tr>
                        <th><?= __("Appointment on"); ?> </th>
                        <td><span class="bdate"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Who will seeing"); ?> </th>
                        <td><span class="seeing"></span></td>
                    </tr>
                    <tr>
                        <th class="name_patient"><?= __("Patient's Name"); ?> </th>
                        <td><span class="name"></span></td>
                    </tr>
                    <tr>
                        <th class="primary_email"><?= __("Email"); ?> </th>
                        <td><span class="email"></span></td>
                    </tr>
                    <tr>
                        <th class="primary_gender"><?= __("Gender"); ?> </th>
                        <td><span class="gender"></span></td>
                    </tr>
                    <tr>
                        <th class="primary_number"><?= __("Phone Number"); ?> </th>
                        <td><span class="phone"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Illness Reason"); ?> </th>
                        <td><span class="illness"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Doctor Note"); ?> </th>
                        <td><span class="doctor"></span></td>
                    </tr>
                    <tr>
                        <th><?= __("Previous Prescription Report"); ?> </th>
                        <td><span class="report"></span></td>
                        <td>
                        <?php echo $this->Html->link(__('<i class="fa fa-download" aria-hidden="true"></i>'),
                                    '',
                                    ['class' => 'btn btn-default report_download','escape' => false]
                                );?>
                        </td>
                    </tr>
                    <tr class="cancelReason hide">
                        <th><?= __("Cancelation Reason"); ?> </th>
                        <td><span class="creason"></span></td>
                    </tr>
                    <tr class="else-name hide">
                        <th><?= __("Patient Name"); ?> </th>
                        <td><span class="else-patient"></span></td>
                    </tr>
                    <tr class ="else-patient-email hide">
                        <th><?= __("Patient Email"); ?> </th>
                        <td><span class="else-email"></span></td>
                    </tr>
                    <tr class ="else-patient-gender hide">
                        <th><?= __("Patient Gender"); ?> </th>
                        <td><span class="else-gender"></span></td>
                    </tr>
                    <tr class ="booked hide">
                        <th><?= __("Booked By"); ?> </th>
                        <td><span class="else-booked"></span></td>
                    </tr>
                </thead>
          </table>
        <!-- <p>Current Status : <span class="status"></span></p>
        <p>Appointment on : <span class="bdate"></span></p>
        <p>Who will seeing : <span class="seeing"></span></p>
        <p class="name_patient">Patient Name : <span class="name"></span></p>
        <p class="primary_email">Email : <span class="email"></span></p>
        <p class="primary_gender">Gender : <span class="gender"></span></p>
        <p class="primary_number">Phone Number : <span class="phone"></span></p>
        <p>Illness Reason : <span class="illness"></span></p>
        <p>Doctor Note : <span class="doctor"></span></p>
        <p class="else-name hide">Patient Name : <span class="else-patient"></span></p>
        <p class ="else-patient-email hide">Patient Email : <span class="else-email"></span></p>
        <p class="else-patient-gender hide">Patient Gender : <span class="else-gender"></span></p>
        <p class="booked hide">Booked By : <span class="else-booked"></span></p> -->
      </div>
      <div class="modal-footer mypinfo">
        <button type="button" class="btn cancelApp btn-cancel hide" id="cancelAppButton"> <?= __("Cancel Appointment"); ?></button>
        <button type="button" class="btn btn-confirm confirmApp hide" id="confirmAppButton"> <?= __("Make it Confirm"); ?></button>
        <button type="button" class="btn btn-reschedule rescheduleApp" id="reschedule"><?= __("Reschedule"); ?> </button>
      </div>
    </div>

  </div>
</div>