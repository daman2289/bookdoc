<style>
   .radio-custom-modal .form-group label {
   display:inline-block !important;
   }
   .radio-custom-modal .form-group input {
   height:auto !important;
   }
   .save-btn button.btn {
   background: #19ca68 !important;
   color: #fff;
   font-size: 15px;
   width: auto;
   height: 40px;
   border-radius: 25px;
   min-width: 140px;
   font-weight: 500;
   }
   .save-btn {
        margin-top: 0px;
    }
    .pad0{
        padding:0;
    }
    #timeerrors{
        text-align: left;
        color: red;
        font-size: smaller;
    }
</style>
<div id="createEventModal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> 
            <span class="sr-only"><?= __("close"); ?></span>
            </button>
            <h4><?= __("Book Appointment"); ?></h4>
         </div>
         <?= 
            $this->Form->create(null,[
            	'type' => 'post',
            	'url' => ['controller' => 'doctors', 'action' => 'doctorCreateBooking'],
            	'id' => 'bookingForm',
            	'class' => 'eventForm', 
            	'data-toggle' => "validator",
            	"role" => "form",
            	'autocomplete' => "off"
            ]);
            ?>
         <div id="modalBody" class="modal-body">
            <div class="redio_btn redio_btn2 avalableLabel">
               <label ><?= __("I am available"); ?>:</label>
               <label class="switch">
               <input type="checkbox" class="availablity" id="myavailablity" checked>
               <span class="slider round"></span>
               </label>
               <div class="clearfix"></div>
            </div>
            <!-- <div class="patientDetails"> -->
            <div class="row">
                
                <div class="col-sm-6">
                    <div class="form-group">
                    <?=
                        $this->Form->control('booking_date',[
                        'type' => 'text',
                        'class' => 'form-control',
                        'required' => true,
                        'id' => 'datetimepicker4', 
                        'label' => __('Date'), 
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                    </div>
                </div>
                <span id = "doctor_id_session" class="hide"><?php echo $duration->duration?></span>
                <div class="col-sm-6">
                    <div class="form-group form-inline">
                        <div id="search_time" class="input-group date" data-provide="datepicker">
                            <?php //$bookingTimes = $this->General->getTimes($date); 
                                //unset($bookingTimes[0]);
                                ?>
                            <?=
                                $this->Form->control('booking_time',[
                                'type' => 'select',
                                'class' => 'form-control browserTime',
                                'required' => true,
                                'label' => __('Time'), 
                                'id' => 'bookingTime'
                                ]);
                                ?>
                            
                            <span class="to_spn"><?= __("to"); ?></span> <span class="extendTime">9:30</span>
                        </div>
                        <?=
                            $this->Form->control('to_time_off',[
                            'type' => 'hidden',
                            'id' => 'toTimeOff',
                            ]);
                        ?>
                
                    </div>
                        <div id="timeerrors" class="help-block with-errors"></div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                    <?php $durationTimes = $this->General->getDurationTimes(); ?>
                    <?= 
                        $this->Form->control('duration',[
                        'type' => 'select',
                        'options' => $durationTimes,
                        'class' => 'form-control',
                        'value' => 40,
                        'required' => true,
                        'label' => __('Duration'), 
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-sm-12 patientDetails">
                    <div class="form-group">
                        <?= $this->Form->label(__("Patient")); ?>
                        <div class="row radio-custom-modal">
                            <div class="form-group col-sm-12">
                                
                                <label class="radio-inline">
                                    <input type="radio" name="patient" id="existingPatient" data-patient="existing" class="newPatient" checked value="existing">
                                        <?= __("Existing"); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="patient"  id="newPatient" data-patient="new" class="newPatient" value="new"><?= __("New"); ?>
                                </label>
                            </div>
                        </div>
                        <div class="help-block with-errors"></div>
                    
                    </div>
                </div>

                

                  
                <div class="clearfix"></div>
                <div id="inserthere">
                    <div class="col-sm-12 patientDetails pad0">
                        <div class="form-group">
                            <?=
                            $this->Form->control('patient_id',[
                                'type' => 'select',
                                'empty' => __('--Please Select--'),
                                'class' => 'form-control pad0',
                                'options' =>  $doctorPatients,
                                'id' => 'patients', 
                                'data-error' => 'Please select a patient in the list',										
                                'label' => __('Select Patient'), 
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
               <div class="row">
                  <div class="col-sm-12 patientDetails">
                     <div class="form-group">
                        <?=
                           $this->Form->control('illness_reason_id',[
                           'type' => 'select',
                               'class' => 'form-control',
                           'label' => __("Reason"),
                           'options' => $illnessReasons,
                           ]);
                           ?>
                        <div class="help-block with-errors"></div>
                     </div>
                  </div>
                  <div class="col-sm-12 patientDetails">
                     <div class="form-group">
                        <?=
                           $this->Form->control('doctor_note',[
                           'type' => 'textarea',
                               'class' => 'form-control',
                           'label' => __("Appointment Notes"),
                           'rows' => 3,
                           'placeholder' => 'Doctor Notes', 
                           'id' => "eventDescription"
                           ]);
                           ?>
                        <div class="help-block with-errors"></div>
                     </div>
                  </div>
               </div>
            <!-- </div> -->
         </div>
         <div class="modal-footer save-btn">
            <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> -->
            <div class="col-sm-12">
               <div class="save-btn">
                  <button class="btn submit" type="submit"><?php echo __('Save'); ?></button>
               </div>
            </div>
         </div>
         <?= $this->Form->end(); ?>
      </div>
   </div>
</div>




<!-- Dynamic Data -->
<div id="patientData" class="hide">
   <div class="choose_patient patientDetails ">
      <div class="row" style="text-align:left;">
         <div class="col-sm-12">
            <?= $this->Form->label(__("Patient's Name")); ?>
         </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
               <?=
                  $this->Form->control('patient_first_name',[
                  	'type' => 'text',
                  	'class' => 'form-control',
                  	'required' => true,
                  	'label' => false,
                  	"placeholder" => __("First Name")
                  	
                  ]);
                  ?>
               <div class="help-block with-errors"></div>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <?=
                  $this->Form->control('patient_last_name',[
                  	'type' => 'text',
                  	'class' => 'form-control',
                  	'required' => true,
                  	'label' => false,
                  	"placeholder" => __("Last Name")
                  	
                  ]);
                  ?>
               <div class="help-block with-errors"></div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="form-group col-sm-12">
            <?= $this->Form->label(__("Email (Optional)")); ?>
            <?=
               $this->Form->control(null,[
                'name' => 'patient_email',
               	'type' => 'email',
               	'class' => 'form-control',
               	'label' => false,
               	"placeholder" => __("Email Address")
               ]);
               ?>
         </div>
      </div>
      <div class="row radio-custom-modal">
         <div class="form-group col-sm-12">
            <label class="radio-inline"><input type="radio" name="patient_sex" value="Male"><?= __("Male"); ?></label>
            <label class="radio-inline"><input type="radio" name="patient_sex" value="Female"><?= __("Female"); ?></label>
            <div class="help-block with-errors"></div>
         </div>
      </div>
   </div>
</div>

<div id="selectPatient" class="hide">
    <div class="col-sm-12 patientDetails pad0">
        <div class="form-group">
            <?=
                $this->Form->control('booking_id',[
                    'type' => 'select',
                    'empty' => __('--Please Select--'),
                    'class' => 'form-control pad0',
                    'options' =>  $doctorPatients,
                    'required' => true,
                    'id' => 'patients', 
                    'data-error' => 'Please select a patient in the list',										
                    'label' => __('Select Patient'), 
                ]);
            ?>
        </div>
        <div class="help-block with-errors"></div>
    </div>
</div>

<?php
   echo $this->Html->script([
       'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js',
   ],['block' => true]);
   ?>