  <div id="daily_hours_override_dialog" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span> <span class="sr-only"><?= __("close"); ?></span></button>
				<h4>Monday, 4 June</h4>
              </div>
                <?= 
					$this->Form->create(null,[
						'type' => 'post',
						'url' => ['controller' => 'doctors', 'action' => 'manageDayOff'],
						'id' => 'form',
						'class' => 'eventForm', 
						'data-toggle' => "validator",
						 "role" => "form",
						'autocomplete' => "off"
					]);
				?>
              <!-- <form class="eventForm"> -->
              <div  class="modal-body">
            <div class="row">
				<div class="col-sm-12">
				  <div class="form-group">
					<div id="search_time2" class="input-group date search_tm" data-provide="datepicker">
						<div class="redio_btn">
							
							<?= $this->Form->label(__("Take the day off")); ?>
                            <label class="switch">
                                <?php
                                    $day_off = true;
                                ?>
                                <?=
                                    $this->Form->control('day_off',[
										'type' => 'checkbox',
                                        'label' => false,
                                        'templates' => [
                                            'inputContainer' => '{{content}}',
                                        ],
                                        'value' => true,
                                        'checked' => ($day_off)? 'checked':false 
                                    ]);
                                ?>

                                <span class="slider round"></span>
							</label>
                                <?=
                                    $this->Form->control('on_date',[
                                        'type' => 'hidden',
                                        'id' => 'onDate', 
                                        'label' => false,
                                    ]);
                                ?>

							<div class="clearfix"></div>
						</div>
					</div>
				  </div> 
				  
            
				</div>        
          </div>
                  <!-- <div class="form-group">
                      <textarea class="form-control" type="text" rows="4" placeholder="Event Description" id= "eventDescription"></textarea>
                  </div> -->
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true"><?= __("Cancel"); ?></button>
                  <button type="submit" class="btn btn-primary" id="submitButton2"><?= __("Save"); ?></button>
              </div>
              <?= $this->Form->end(); ?>
          </div>
      </div>
  </div>   