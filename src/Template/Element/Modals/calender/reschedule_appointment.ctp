<style>
   .radio-custom-modal .form-group label {
   display:inline-block !important;
   }
   .radio-custom-modal .form-group input {
   height:auto !important;
   }
   .save-btn button.btn {
   background: #19ca68 !important;
   color: #fff;
   font-size: 15px;
   width: auto;
   height: 40px;
   border-radius: 25px;
   min-width: 140px;
   font-weight: 500;
   }
   .save-btn {
        margin-top: 0px;
    }
    .pad0{
        padding:0;
    }
    .pNameLabel{
        text-align: left;
    }
    
</style>
<div id="rescheduleModel" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> 
            <span class="sr-only"><?= __("close"); ?></span>
            </button>
            <h4><?= __("Reschedule Appointment"); ?></h4>
         </div>
         <?= 
            $this->Form->create(null,[
            	'type' => 'post',
            	'id' => 'myReForm',
            	'class' => 'eventForm', 
            	'data-toggle' => "validator",
            	 "role" => "form",
            	'autocomplete' => "off"
            ]);
            ?>
         <div id="modalBody" class="modal-body">
            <!-- <div class="patientDetails"> -->
            <div class="row">
                
                <div class="col-sm-6">
                    <div class="form-group">
                    <?=
                        $this->Form->control('booking_date',[
                        'type' => 'text',
                            'class' => 'form-control datetimepicker44',
                        'required' => true,
                        'id' => 'bDate',
                            'label' => __('Date'), 
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-inline">
                    <div id="search_time" class="input-group date" data-provide="datepicker">
                        <?php $bookingTimes = $this->General->getTimings(); 
                            unset($bookingTimes[0]);
                            ?>
                        <?=
                            $this->Form->control('booking_time',[
                                'type' => 'select',
                                'options' => $bookingTimes,
                                'class' => 'form-control browserTime',
                                'required' => true,
                                'label' => __('Time'), 
                                 'id' => 'bTime',
                            ]);
                            ?>
                        <span class="to_spn">to</span> <span class="extendTime">9:30</span>
                    </div>
                        <?=
                            $this->Form->control('to_time_off',[
                            'type' => 'hidden',
                            'id' => 'toTimeOff',
                            ]);
                        ?>
                
                    <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                    <?php $durationTimes = $this->General->getDurationTimes(); ?>
                    <?=
                        $this->Form->control('duration',[
                        'type' => 'select',
                        'options' => $durationTimes,
                        'class' => 'form-control',
                        'value' => 30,
                        'required' => true,
                        'label' => __('Duration'), 
                        ]);
                        ?>
                    <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-sm-12 patientDetails">
                    <div class="pNameLabel">
                        <?= $this->Form->label(__("Patient Name"))?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <?=
                                $this->Form->control('patient_first_name',[
                                    'type' => 'text',
                                    'disabled' => 'disabled',
                                    'class' => 'form-control pname',
                                    'required' => true,
                                    'label' => false,
                                    'id' => 'patientfname',
                                    "placeholder" => __("First Name")
                                    
                                ]);
                                ?>
                            <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?=
                                    $this->Form->control('patient_last_name',[
                                        'type' => 'text',
                                        'class' => 'form-control',
                                        'disabled' => 'disabled',                                    
                                        'label' => false,
                                        'id' => 'patientlname',
                                        "placeholder" => __("Last Name")
                                        
                                    ]);
                                ?>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <?=
                            $this->Form->control('booking_id',[
                                'type' => 'hidden',
                                'id' => 'bookingId'
                            ]);
                        ?>
                    </div>
                </div>

                <div class="row radio-custom-modal">
                    <div class="form-group col-sm-12">
                        <label class="radio-inline"><input disabled type="radio" id="pgendermale" name="patient_sex" value="Male">Male</label>
                        <label class="radio-inline"><input disabled type="radio" id="pgenderfemale" name="patient_sex" value="Female">Female</label>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                

                  
               
            </div>
               <div class="row">
                  <div class="col-sm-12 patientDetails">
                     <div class="form-group">
                        <?=
                           $this->Form->control('illness_reason_id',[
                           'type' => 'select',
                            'class' => 'form-control',
                            'disabled' => 'disabled',                               
                           'label' => __("Reason"),
                           'options' => $illnessReasons,
                           ]);
                           ?>
                        <div class="help-block with-errors"></div>
                     </div>
                  </div>
                  <div class="col-sm-12 patientDetails">
                     <div class="form-group">
                        <?=
                           $this->Form->control('doctor_note',[
                           'type' => 'textarea',
                               'class' => 'form-control',
                           'label' => __("Appointment Notes"),
                           'rows' => 3,
                            'disabled' => 'disabled',                           
                           'placeholder' => 'Doctor Notes', 
                           'id' => "eventDescription"
                           ]);
                           ?>
                        <div class="help-block with-errors"></div>
                     </div>
                  </div>
               </div>
            <!-- </div> -->
         </div>
         <div class="modal-footer save-btn">
            <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> -->
            <div class="col-sm-12">
               <div class="save-btn">
                  <button class="btn submit" type="submit"><?php echo __('Save'); ?></button>
               </div>
            </div>
         </div>
         <?= $this->Form->end(); ?>
      </div>
   </div>
</div>




<!-- Dynamic Data -->
<div id="patientData" class="hide">
   <div class="choose_patient patientDetails ">
      <div class="row" style="text-align:left;">
         <div class="col-sm-12">
            <?= $this->Form->label(__("Patient's Name")); ?>
         </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
               <?=
                  $this->Form->control('patient_first_name',[
                  	'type' => 'text',
                  	'class' => 'form-control',
                  	'required' => true,
                  	'label' => false,
                  	"placeholder" => __("First Name")
                  	
                  ]);
                  ?>
               <div class="help-block with-errors"></div>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <?=
                  $this->Form->control('patient_last_name',[
                  	'type' => 'text',
                  	'class' => 'form-control',
                  	'label' => false,
                  	"placeholder" => __("Last Name")
                  	
                  ]);
                  ?>
               <div class="help-block with-errors"></div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="form-group col-sm-12">
            <?= $this->Form->label(__("Email (Optional)")); ?>
            <?=
               $this->Form->control('patient_email',[
               	'type' => 'email',
               	'class' => 'form-control',
               	'label' => false,
               	"placeholder" => __("Email Address")
               ]);
               ?>
         </div>
      </div>
      <div class="row radio-custom-modal">
         <div class="form-group col-sm-12">
            <label class="radio-inline"><input type="radio" name="patient_sex" value="Male"><?= __("Male"); ?></label>
            <label class="radio-inline"><input type="radio" name="patient_sex" value="Female"><?= __("Female"); ?></label>
            <div class="help-block with-errors"></div>
         </div>
      </div>
   </div>
</div>

<div id="selectPatient" class="hide">
    <div class="col-sm-12 patientDetails pad0">
        <div class="form-group">
            <?=
                $this->Form->control('booking_id',[
                    'type' => 'hidden',
                ]);
            ?>
        </div>
        <div class="help-block with-errors"></div>
    </div>
</div>

<?php
   echo $this->Html->script([
    //    'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js',
   ],['block' => true]);
   ?>