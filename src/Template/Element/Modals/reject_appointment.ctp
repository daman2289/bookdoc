
<div class="modal fade myconfirm-modal" id="myRejectModel" role="dialog">
    <div class="modal-dialog modal-sm">
    
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                <h4 class="modal-title text-center"><?= __("Decline") ?> </h4>
            </div>
            <div class="modal-body">
                <p><?= __("Are you sure to decline this booking?"); ?></p>
            </div>
            <div class="modal-footer" style="text-align: center">
                <?= 
                    $this->Form->create(null,[
                        'type' => 'post',
                        'url' => 'javascript:void(0);',
                        'id' => 'form',
                        'autocomplete' => "off"
                    ]);
                ?>
                <?=
                $this->Form->control('status',[
                    'type' => 'hidden',
                    'label' => false, 
                    'value' => 3
                    ]);
                ?>
                <?=
                    $this->Form->button(__('Decline'),[
                        'type' => 'submit',  
                        'class' => 'btn btn-model-danger',
                    ]);
                ?>

                <?= $this->Form->end(); ?>
            </div>
        </div>
    
    </div>
</div>