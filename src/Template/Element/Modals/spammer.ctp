<div id="spammer" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h5 class="modal-title" id="#"><?= __("You are marked as Spam")?></h5>
      </div>
      <div class="modal-body">
        <p><?= __("Sorry! Unfortunately you cannot do the further booking with"); ?>
            <strong><span id="mydoc"></span></strong>
        </p>
        <p>    
            <?= __("Please search and book other doctors")?>
         </p>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("OK"); ?></button>
      </div>
    </div>

  </div>
</div>