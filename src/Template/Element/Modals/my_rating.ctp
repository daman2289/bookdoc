<div class="modal fade" id="myRatingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= __("My Ratings for this Booking")?></h4>
            </div>
            <?php 
                echo $this->Form->create('', [
                    'url'   => "#",
                    'id'    => 'add-report-form'
                    ]
                );
            ?>  
            <div class="modal-body">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label><?= __("I Said: ") ?></label>
                        <textarea disabled id="myReview" class="form-control" name="review" rows="5"></textarea>
                    </div>
                </div>
                
                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Overall rating"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star rating-loading myrate" id="myOverAll" name="overall_rating" value="" data-size="xs" title="">
                    </div>
                </div>

                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Bedside Manner"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star1 rating-loading myrate" id="myBedSide" name="bedside_manner" value="" data-size="xs" title="">
                    </div>
                </div>
                
                <div class="form-group clearfix">
                    <div class="col-sm-4">
                        <div class=""><?= __("Wait time"); ?></div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="kv-uni-star2 rating-loading myrate" id="myWaitTime" name="wait_time" value="" data-size="xs" title="">
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>