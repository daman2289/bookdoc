<?php
$action = !empty($this->request->getParam('action')) ? $this->request->getParam('action') : null;
?>
<ul>
    <li class="<?= ($action == "dashboard") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-tachometer"></i> ' . __('Dashboard'), ['controller' => 'Users', 'action' => 'dashboard'], ['escape' => false]
        );
        ?>
    </li>
    <li class="<?= ($action == "bookingCalender") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-calendar-check-o"></i> ' . __('Booking Calender'), ['controller' => 'Doctors', 'action' => 'bookingCalender'], ['escape' => false]
        );
        ?>
    </li>
    <li  class="<?= ($action == "editProfile") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-pencil-square-o"></i> ' . __('Edit Profile'), ['controller' => 'Users', 'action' => 'edit-profile'], ['escape' => false]
        );
        ?>
    </li>
    <li class="<?= ($action == "editBio") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-user"></i> ' . __('My Bio'), ['controller' => 'Users', 'action' => 'edit-bio'], ['escape' => false]
        );
        ?>
    </li>
    <li class="<?= ($action == "changePassword") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-key"></i> ' . __('Change Password'), ['controller' => 'Users', 'action' => 'change-password'], ['escape' => false]
        );
        ?>    
    </li>
    <?php $class = ($this->request->getParam('controller') == 'Appointments' && $this->request->getParam('action') == 'index') ? 'active' : '' ?>
    <!-- <li class = '<?php echo $class?>'>
        <?php echo $this->Html->link('<i class="ti-notepad"></i>'.__('My Appointmentss'),
            ['controller' => 'Appointments','action' => 'index'],
            ['escape' => false]
        );?>
    </li> -->
    <?php $class = ($this->request->getParam('controller') == 'Appointments' && $this->request->getParam('action') == 'past') ? 'active' : '' ?>    
    <li  class='<?php echo $class?>'>
        <?php
        echo $this->Html->link('<i class="fa fa-history" aria-hidden="true"></i> ' . __('Past Appointments'), ['controller' => 'Appointments', 'action' => 'past'], ['escape' => false]
        );
        ?>
    </li>
    <?php $class = ($this->request->getParam('controller') == 'Reports' && $this->request->getParam('action') == 'index') ? 'active' : '' ?>
    <li class = '<?php echo $class?>'>
        <?php echo $this->Html->link('<i class="fa fa-file-text-o"></i>'.__('Patient Files/Reports'),
            ['controller' => 'Reports','action' => 'index'],
            ['escape' => false]
        );?>
    </li>
    <!-- <?php //$class = ($this->request->getParam('controller') == 'DoctorAvailabilityDays' && $this->request->getParam('action') == 'add') ? 'active' : '' ?>
    <li class="<?= $class ?>">
        <?php
        echo $this->Html->link('<i class="ti-calendar"></i> ' . __('Manage My Availability'), ['controller' => 'DoctorAvailabilityDays', 'action' => 'add'], ['escape' => false]
        );
        ?>
    </li> -->
    <!-- <li  class="<?= ($action == "changeMobile") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="ti-mobile"></i> ' . __('Change Mobile No'), ['controller' => 'Users', 'action' => 'change-mobile'], ['escape' => false]
        );
        ?>
    </li> -->
    <li   class="<?= ($action == "videoPicture") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-picture-o"></i> ' . __('Picture/Video'), ['controller' => 'Users', 'action' => 'video-picture'], ['escape' => false]
        );
        ?> 
    </li>
    <?php $myclass = ($this->request->getParam('controller') == 'Payments' && $this->request->getParam('action') == 'mySubscriptions') ? 'active' : 'dsd' ?>

    <li class= "<?= $myclass ?>" >
        <?php
        echo $this->Html->link('<i class="fa fa-picture-o"></i> ' . __('Manage Subscriptions'), ['controller' => 'Payments', 'action' => 'mySubscriptions'], ['escape' => false]
        );
        ?> 
    </li>

    <li  class="<?= ($action == "logout") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-sign-out"></i> ' . __('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]
        );
        ?> 
    </li>
</ul>