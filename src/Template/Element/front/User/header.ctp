<?php
    use Cake\Core\Configure;
    $session = $this->request->getSession();
?>
<style>
    .notification {
        margin-right: 20px;
    }

    .notification .glyphicon-bell {
        vertical-align: middle;
    }

    .notification .dropdown-menu {
        width: 350px;
    }

    .notification .dropdown-menu > li {
        display: block;
    }

    .notification .badge {
        position: absolute;
        margin: -8px 5px;
    }
    .wrapit{
        white-space: unset !important;
    }
    .badge{
        background-color: #ff7500bf;
    }
    .unread{
        background-color: #d1d10b26;
    }
    .padhead{
        padding-top:10px;
    }
</style>
<header class="d-header">
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-sm-3 col-xs-12">
                <div class="brand-logo">
                    <?php
                        echo $this->Html->link($this->Html->image('frontend/darshboard-logo.png'), ['controller' => 'users', 'action' => 'dashboard'], ['escape' => false]);
                        ?>
                </div>
            </div>
            
            <div class="col-sm-9 col-xs-12">
              <a href="#" type="button" class="toggle-hum"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg></a>
                <div class="top-header-right">
                    <ul>
                        <?php if ($session->check('Auth.User.hospital_uuid')) { ?>
                            <li>
                                
                                <?php echo $this->Html->link('Back to hospital',
                                    ['controller' => 'Hospitals','action' => 'viewDashboard',$session->read('Auth.User.hospital_uuid'),'1'],
                                    ['class' => 'btn btn-default']
                                );?>
                            </li>
                        <?php } ?>
                        <?php $notifications = $this->Notification->get(); ?>
                        <li class="dropdown notification">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="badge"><?= $this->Notification->getUnreadNotificationCount(); ?></span>
                                <span class="glyphicon glyphicon-bell"></span>
                                <span class="caret"></span>
                            </a>
                           
                            <ul class="dropdown-menu dropdown-menu-right">
                                <div class="row">
                                    <div class="col-sm-12 padhead">
                                        <div class="col-sm-6">
                                            <strong><small> <?= __("All Notifications"); ?> </small></strong>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <small> <a href="#" id="markasread"> <?= __("Mark All as Read"); ?> </a></small>
                                        </div>
                                    </div>    
                                </div>
                              
                                <?php 
                                    foreach ($notifications as $notification) { 
                                        $unread = ($notification->unread) ? "unread": "read"; 
                                        
                                        ?>
                                        <li role="separator" class="divider"></li>

                                        <li data-id = <?= $notification->id; ?> class="<?= $unread ?> notify-hover" >
                                            <a href="#" class="wrapit">
                                                <?= $notification->text ?>
                                            </a>
                                        </li>

                                <?php } ?>
                                    <?php if ($notifications->isEmpty()) { ?>
                                        <!-- <li role="separator" class="divider"></li> -->
                                        <li><a href="#"><?= __("No New Notification");?></a></li>
                                    <?php } ?>
                            
                            </ul>
                        </li>
                        
                        <li class="mobile-none">
                            <div class="dr-profile">
                                <?php
                                    if ($session->read('Auth.User.user_profile.gender') == 'Male'):
                                        $pImg = 'frontend/Dummy-image.jpg';
                                    else:
                                        $pImg = 'frontend/woman_dummy.png';
                                    endif;
                                    
                                    if (!empty($session->read('Auth.User.user_profile.profile_pic'))) {
                                        $pImg = '../files/uploads/' . $session->read('Auth.User.user_profile.profile_pic');
                                    } else{
                                       $pImg = $session->read('Auth.User.user_profile.social_profile_url');
                                    }
                                    //echo $pImg;die;
                                    echo $this->Html->image($pImg);
                                ?>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown">
                                <?php 
                                    if($session->read('Auth.User.role_id') == 5) {
                                        $name = $session->read('Auth.User.hospital_profile.hospital_name');
                                    } else {
                                        $name = ($session->read('Auth.User.role_id') == Configure::read('UserRoles.Doctor')) ? __('Dr.')  : '';
                                        $name .= $session->read('Auth.User.user_profile.first_name') . " " . $session->read('Auth.User.user_profile.last_name');
                                    }
                                    echo $this->Html->link(
                                        $name . '<span class="caret"></span>',
                                        'javascript:void(0)',
                                        [
                                            'id' => 'dLabel',
                                            'data-toggle' => 'dropdown',
                                            'role' => 'button',
                                            'aria-haspopup' => true,
                                            'aria-expanded' => false,
                                            'escape' => false
                                        ]
                                    );
                                ?>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                                    <li><a href="<?= $this->Url->build(['controller' => 'Doctors', 'action' => 'view', base64_encode($session->read('Auth.User.id'))], ['escape' => false])?>"><?= __("view profile"); ?></a></li>
                                    <li>
                                        <?php echo $this->Html->link(__('Logout'), array('controller' => 'Users', 'action' => 'logout'), array('escape' => false)); ?>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>