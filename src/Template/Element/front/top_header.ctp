  <div class="topheader">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"><?php echo __('Toggle navigation'); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <?php
                echo $this->Html->link($this->Html->image('logo-blue.png', ['alt' => 'logo']), '/', ['escapeTitle' => false, "class" => "navbar-brand"]);
                ?>

            </div>
            <div id="navbar" class="navbar-collapse collapse top-nav">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href=<?= $this->Url->build(['controller' => 'Users', 'action' => 'signUp','?' => ['type' => 'doctor']], ['escape' => false]); ?>><?= __("List your practice on Bookdoc");?> <i class="ti-angle-right"></i></a>
                     
                    </li>
                    <?php if(empty($this->request->getSession()->read('Auth.User'))) { ?>
                    <li class="dropdown custom-drop-top"> 
                        <a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Login / Join   <span class="caret"></span> </a>
                        <ul class="dropdown-menu" aria-labelledby="drop1"> 
                            <div class="col-sm-12">
                                <div class="col-sm-12 p-0 border-bottom">
                                    <div class="col-sm-5 p-0 label-text"><?= __('Patients ') ?></div>
                                    <div class="col-sm-7 p-0">
                                    <?php echo $this->Html->link(__('Login / Join'),
                                        ['controller' => 'Users', 'action' => 'PatientLogin','patient']
                                    ); ?></div>
                                </div>
                                <div class="col-sm-12 p-0 border-bottom">
                                    <div class="col-sm-5 p-0 label-text "><?= __('Doctors') ?></div>
                                    <div class="col-sm-7 p-0">
                                    <?php echo $this->Html->link(__('Log in'),
                                        ['controller' => 'Users', 'action' => 'login','doctor']
                                    ); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 p-0">
                                    <div class="col-sm-5 p-0 label-text "><?= __('Hospital ') ?></div>
                                    <div class="col-sm-7 p-0">
                                    <?php echo $this->Html->link(__('Log in'),
                                        ['controller' => 'Users', 'action' => 'login','hospital']
                                    ); ?>
                                    </div>
                                </div>
                            </div>
                        </ul> 
                    </li>

                    <li>
                   <!--  <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'login'])?>"> 
                        <?= __('Sign in / Join')?>
                    </a> -->


                    
                    <?php } else { ?>
                        <li class="dropdown user user-menu front-drop home-top-dropdwon">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <span class="name-lender">
                                  <?php 
                                    if(!empty($this->request->getSession()->read('Auth.User.user_profile'))) {
                                        echo $this->request->getSession()->read('Auth.User.user_profile.first_name').' '.$this->request->getSession()->read('Auth.User.user_profile.last_name');
                                    } else {
                                        echo $this->request->getSession()->read('Auth.User.social_profile.first_name').' '.$this->request->getSession()->read('Auth.User.social_profile.last_name');
                                    }
                                    ?>
                                </span>
                                <span class="text-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> 
                            </a>
                            <ul aria-labelledby="drop3" class="dropdown-menu front-drop-ul">
                                <li>
                                    <?php 
                                    if($this->request->getSession()->read('Auth.User.role_id') == 3) {
                                        $action = ['controller' => 'Users','action' => 'editProfile',base64_encode($this->request->getSession()->read('Auth.User.id'))];
                                    } else {
                                        $action = ['controller' => 'Patients','action' => 'editProfile',base64_encode($this->request->getSession()->read('Auth.User.id'))];
                                    }
                                    echo $this->Html->link(
                                        __('My Account'),
                                        $action
                                    );?>
                                </li>
                                <li>
                                    <?php 
                                    echo $this->Html->link(
                                        __('Logout'),
                                        ['controller' => 'Users','action' => 'logout']
                                    );?>
                                </li>
                            </ul>
                        </li>
                    <?php }?>
                    <li class="d-flag">
                        <a href="javascript:void(0);">
                            <?=
                                $this->Form->create(null,[
                                     'type' => 'post',
                                     'url'   => ['controller' => 'App', 'action' => 'setLocale'],
                                ]); 
                            ?>

                            <select name="locale" onchange="this.form.submit()" class="selectpicker" data-width="fit">
                                <option value="en_US" <?= $this->General->getLocaleSelecter('en_US'); ?> data-content='<span class="flag-icon flag-icon-us"></span> English'>Us</option>
                                <option value="en_DE" <?= $this->General->getLocaleSelecter('en_DE'); ?> data-content='<span class="flag-icon flag-icon-at"></span> Deutsch'></option>
                            </select>
                            <?= $this->Form->end(); ?>
                        </a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
        <div class="mob-flag">
            <!-- <select class="selectpicker" data-width="fit">
                <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
                <option  data-content='<span class="flag-icon flag-icon-fr"></span> French'>French</option>
                <option  data-content='<span class="flag-icon flag-icon-es"></span> Spanish'>Spanish</option>
            </select> -->
             <a href="javascript:void(0);">
                <?=
                    $this->Form->create(null,[
                         'type' => 'post',
                         'url'   => ['controller' => 'App', 'action' => 'setLocale'],
                    ]); 
                ?>

                <select name="locale" onchange="this.form.submit()" class="selectpicker" data-width="fit">
                    <option value="en_US" <?= $this->General->getLocaleSelecter('en_US'); ?> data-content='<span class="flag-icon flag-icon-us"></span> English'>Us</option>
                    <option value="en_DE" <?= $this->General->getLocaleSelecter('en_DE'); ?> data-content='<span class="flag-icon flag-icon-at"></span> Deutsch'></option>
                </select>
                <?= $this->Form->end(); ?>
            </a>
        </div>
    </nav>
</div>

<style type="text/css">
    .custom-drop-top ul {
        min-width: 238px;
        padding: 16px 0px !important;
    }

    .label-text{
        color:#12617b;
        font-weight: 600;
    }

    .border-bottom{
           border-bottom: 1px solid #ccc;
    margin-bottom: 13px;
    padding-bottom: 13px !important;
    }

   .custom-drop-top .dropdown-menu  a{
     color: #20657f !important;
    }


   .custom-drop-top .dropdown-menu{
    margin-top: 0px !important;
   }


   .navbar-inverse .navbar-nav>.open>a, 
   .navbar-inverse .navbar-nav>.open>a:focus, 
   .navbar-inverse .navbar-nav>.open>a:hover{
        background-color: #588fd8;
   }

</style> 