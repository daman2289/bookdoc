<?php
$action = !empty($this->request->getParam('action')) ? $this->request->getParam('action') : null;
?>
<ul>
    <li class="<?= ($action == "dashboard") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-tachometer"></i> ' . __('Dashboard'), ['controller' => 'Hospitals', 'action' => 'index'], ['escape' => false]
        );
        ?>
    </li>
    <li  class="<?= ($action == "doctorListing") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-user-md"></i> ' . __('Manage Doctors'), ['controller' => 'Hospitals', 'action' => 'doctorListing'], ['escape' => false]
        );
        ?>
    </li>
    <?php $myclass = ($this->request->getParam('controller') == 'Payments' && $this->request->getParam('action') == 'mySubscriptions') ? 'active' : 'dsd' ?>

    <li class= "<?= $myclass ?>" >
        <?php
        echo $this->Html->link('<i class="fa fa-picture-o"></i> ' . __('Manage Subscriptions'), ['controller' => 'Payments', 'action' => 'mySubscriptions'], ['escape' => false]
        );
        ?> 
    </li>

    <li  class="<?= ($action == "logout") ? 'active' : '' ?>">
        <?php
        echo $this->Html->link('<i class="fa fa-sign-out"></i> ' . __('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]
        );
        ?> 
    </li>
</ul>