<div class="footer hidden-xs">
    <?php if ($this->request->getParam('controller') == 'Dashboard' && $this->request->param('action') == 'index'): ?>
        <div class="subcription">
            <div class="container">
                <div class="col-md-6 col-sm-8 col-xs-12">
                    <div class="subcription_box">
                        <h2 class="subcription_heading"><?php echo __('Daily Health Tips to your Inbox'); ?></h2>
                        <div class="subcription_input_box">
                            <input type="text" class="subcription_input" placeholder="<?= __('Enter Your Email Address') ?>">
                            <button class="subc_btn"><?php echo __('Subscribe'); ?></button>                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="socialhelp">
                        <h2 class="socialtext"><?php echo __('Social Help'); ?></h2>
                        <div class="social_links">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-instagram"></i></a></li>
                                <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="footer_section">
        <div class="container">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="footer_links">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_box">
                            <h3 class="footer_text"><?php echo __('Look up your doctor'); ?></h3>

                            <ul>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Doctor name'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Specialty'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Location'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Insurance'); ?> </a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Gender'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index']); ?>"><?php echo __('Consultation'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="footer_box">
                            <h3 class="footer_text"><?php echo __('Search By'); ?></h3>
                            <ul>
                                <li><a href=""><?php echo __('Specialty'); ?></a></li>
                                <li><a href=""><?php echo __('Procedure'); ?></a></li>
                                <li><a href=""><?php echo __('Language'); ?></a></li>
                                <li><a href=""><?php echo __('Location'); ?></a></li>
                                <li><a href=""><?php echo __('Insurance'); ?></a></li>
                            </ul>
                        </div>
                    </div> -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_box">
                            <h3 class="footer_text"><?php echo __('Specialties'); ?></h3>
                            <ul>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' =>  'Chiropractor']]); ?>"><?php echo __('Chiropractor'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' => 'Dentists']]); ?>"><?php echo __('Dentists'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' =>  'Eye Doctors']]); ?>"><?php echo __('Eye Doctors'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' =>  'Gastroenterologist']]); ?>"><?php echo __('Gastroenterologist'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' =>  'Primary Care Doctor']]); ?>"><?php echo __('Primary care doctors'); ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'search', 'action' => 'index', '?' => ['term' =>  'Psychiatrist']]); ?>"><?php echo __('Psychiatrists'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="footer_box">
                            <h3 class="footer_text"><?php echo __('Partner with us'); ?></h3>
                            <ul>
                                <li><a href="/terms-conditions"><?php echo __('Terms & conditions'); ?></a></li>
                                <li><a href="/trust-safety"><?php echo __('Trust & Safety'); ?></a></li>
                                <li><a href="/privacy-policy"><?php echo __('Privacy Policy'); ?></a></li>
                                <li><a href="/careers"><?php echo __('Careers'); ?></a></li>
                                <!-- <li><a href="/press"><?php echo __('Press'); ?></a> -->
                                <li><a href="/about"><?php echo __('About Us'); ?></a>
                                <li><?php echo $this->Html->link(__('Contact Us'),
                                    ['controller' => 'Dashboard','action' => 'contactUs']
                                );?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 p-0">
                <div class="loginsbuttn">
                    <ul>
                        <li>

                            <div class="dentist">
                                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'signUp'])?>">
                                    <h2><?= __("Are you a top doctor or dentist"); ?>?</h2>
                                    <h4> <?= __("List your practice on Bookdoc"); ?></h4>
                                </a>
                                  
                            </div>
                        </li>
                        <li>
                            <div class="health">
                                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'signUp', '?' => ['on' => 'patient']])?>">
                                    <h2><?php echo __('Are you a looking for better Health?'); ?></h2>
                                    <h4><?php echo __('Join Perfect Health Partners'); ?></h4>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="copyright">
                    © 2018 <?php echo  __('Bookdoc. All Rights Reserved.'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
