<?php
    use Cake\Core\Configure;
    use Cake\I18n\Time;
    $session = $this->request->getSession(); 
?>
<div class="bg-white  profile-left">
    <div class="profile-left-img">
        <?php
            if ($session->read('Auth.User.user_profile.gender') == 'Male'):
                $pImg = 'frontend/Dummy-image.jpg';
            else:
                $pImg = 'frontend/woman_dummy.png';
            endif;

            if (!empty($session->read('Auth.User.user_profile.profile_pic'))):
                $pImg = '../files/uploads/' . $session->read('Auth.User.user_profile.profile_pic');
            endif;
            echo $this->Html->image($pImg);
        ?>
    </div>
    <div class="dr-name">
        <?php
            if($session->read('Auth.User.role_id') == 5) {
                echo $session->read('Auth.User.hospital_profile.hospital_name');
            } else {
                echo ($session->read('Auth.User.role_id') == Configure::read('UserRoles.Doctor')) ? 'Dr. ' : '';
                echo $session->read('Auth.User.user_profile.first_name') . " " . $session->read('Auth.User.user_profile.last_name');
            }  
        ?>
    </div>

    <div class="specialty">
        <?php if($session->read('Auth.User.role_id') == 5) {
            echo $session->read('Auth.User.hospital_profile.contact_name');
        } else {
            $dob = $session->read('Auth.User.user_profile.dob');
            echo $this->User->getUserAge($dob) . ' ' . __('yrs. old');
        }
        ?>
        <!-- 62 yrs. old -->
    </div>
    <div class="address">
        <h3><?php if($session->read('Auth.User.role_id') == 5) { ?>
            <?= $session->read('Auth.User.hospital_profile.address') . ','; 
        } else { ?>
            <?= $session->read('Auth.User.user_profile.address') . ','; } ?>
        </h3>
        <p><?php if($session->read('Auth.User.role_id') == 5) { ?>
            <?= $session->read('Auth.User.hospital_profile.zipcode'); 
        } else { ?>
            <?= $session->read('Auth.User.user_profile.zipcode'); }?>
        </p>
    </div>
</div>
<div class="bg-white">
    <div class="mobile-varifi">
        <h3><?php echo __('My mobile number'); ?>  </h3>
        <p>
        <?php if($session->read('Auth.User.role_id') == 5) { ?>
            <?= $session->read('Auth.User.hospital_profile.phone_number'); 
        } else { ?>
            <?php echo $session->read('Auth.User.user_profile.phone_number');} ?>
        </p>
        <?php if (!$isPhoneVerified): ?>
            <span data-id="phone-not-verified"><i class="fa fa-times-circle"></i></span>
            <u>
                <?php echo $this->Html->link(__('Verify Now'),
                            'javascript:void(0)',
                            [
                                'data-id' => 'verify-phone'
                            ]
                        );
                ?>
            </u>
        <?php else: ?>
            <span data-id="phone-verified"><i class="fa fa-check-circle ph one-verified-icon"></i> <?php echo __('Verified'); ?></span>
        <?php endif; ?>
    </div>
</div>
<div class="bg-white">
    <div class="mobile-varifi">
        <h3><?php echo __('My email address'); ?>  </h3>
        <p><?php echo $session->read('Auth.User.email'); ?></p>
        <?php if (!$isEmailVerified): ?>
            <span data-id="phone-not-verified"><i class="fa fa-times-circle"></i></span>
            <u>
                <?php echo $this->Html->link(__('Verify Now'),
                            ['controller' => 'Users','action' => 'sendEmail']
                            
                        );
                ?>
            </u>
        <?php else: ?>
            <span data-id="phone-verified"><i class="fa fa-check-circle phone-verified-icon"></i> <?php echo __('Verified'); ?></span>
        <?php endif; ?>
    </div>
</div>