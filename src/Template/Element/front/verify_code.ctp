<div class="modal fade" data-id="phone-verify" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <?php
                    echo $this->Form->button('<span aria-hidden="true">&times;</span>', [
                            'type' => 'button',
                            'class' => 'close',
                            'data-dismiss' => 'modal',
                            'aria-label' => 'Close'
                        ]);
                ?>
                <h3 class="modal-title"><?php echo __('Verify Your Phone Number'); ?></h3>
            </div>
            <?php
                echo $this->Form->create(null, [
                        'url' => [
                           'controller' => 'PhoneVerifications',
                           'action' => 'check.json',
                           'prefix' => 'api'
                        ],
                        'data-id' => 'phone-verify-form'
                    ]);
            ?>
                    <div class="modal-body clearfix">
                        <div class="col-xs-12 text-center">
                            <?php
                                echo __('To get verification code ');
                                echo $this->Html->link(
                                        __('click here'),
                                        'javascript:void(0)',
                                        [
                                            'data-id' => 'resend-code',
                                            'data-url' => $this->Url->build([
                                                            'controller' => 'PhoneVerifications',
                                                            'action' => 'send.json',
                                                            'prefix' => 'api'
                                                        ],true),
                                        ]
                                    );
                            ?>
                        </div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group clearfix">
                                <div class="col-sm-12">
                                    <label><?php echo __('Enter Verification Code'); ?></label>
                                </div>
                                <div class="col-sm-12">
                                    <?php
                                        echo $this->Form->text('code', [
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'data-id' => 'partitioned-input',
                                            'maxlength' => 4
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-xs-12 save-btn verify-phone-btn">
                            <?php
                                echo $this->Form->button(__('Verify'), [
                                    'type' => 'submit',
                                    'class' => 'btn',
                                    'data-id' => 'phone-verify-submit-btn'
                                ]);
                            ?>
                        </div>
                    </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<?php
    echo $this->Html->css('bootstrap-pincode-input');

    $this->Html->scriptStart(['block' => 'scriptBottom']);
?>
        var is_phone_verified = "<?php echo ($isPhoneVerified) ? true : false; ?>";
<?php
    $this->Html->scriptEnd();
    echo $this->Html->script([
        'bootstrap-pincode-input',
        'frontend/User/verify_phone'
    ], ['block' => 'script']);
?>