
<?php
	$isSignedIn = $this->request->getSession()->check('Auth.User');
	if (empty($unavailableTime)) {
		$unavailableTime = [];
	}
	
	if (empty($doctor)) {
		$doctor = "";
	}else{
		$doctor = base64_encode($doctor->id);
	}

	if ($isSignedIn) {
		$isSignedIn = "True";
	}else{
		$isSignedIn = "False";
	}

?>

<?php if(!empty($times)): 
	$noSlotsAvailable = false;
?>
		<?php 
			foreach($times as $key => $value): 
		?>	
		     		<?php if(!empty($value)) { 
							$dayAvailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
							if($dayAvailable) { 
								?>
							<li>
						
								<a href="javascript:void(0)" onclick='setTime(this, "<?= date('H:i',strtotime($value));  ?>", "<?= $timeDay['weekday']; ?>", "<?= $timeDay['timeObj']->format('Y-m-d'); ?>" ); 
									showBookingModal("<?= $isSignedIn; ?>", "<?= $doctor ?>");' data-day=" <?= $timeDay['weekday'] ?> "> 
									<?= date('H:i',strtotime($value)); ?> 
								</a>
							
							</li>
					<?php } }?>

		<?php
			endforeach; 
		?>
<?php endif;?>

