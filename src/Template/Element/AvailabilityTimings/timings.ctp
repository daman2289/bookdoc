<?php use Cake\Chronos\Chronos;
use Cake\I18n\Time;
$now = strtotime(Time::now('Europe/Vienna'));
$today = date('Y-m-d');
?>
<div class="col-md-4">
<?php
	$isSignedIn = $this->request->getSession()->check('Auth.User');
	if (empty($unavailableTime)) {
		$unavailableTime = [];
	}
	
	if (empty($doctor)) {
		$doctor = "";
		$my_patient = "";
	}else{
		$my_patient = $doctor->user_profile->my_patient_only;
		$doctor = base64_encode($doctor->id);
	}

	if ($isSignedIn) {
		$isSignedIn = "True";
	}else{
		$isSignedIn = "False";
	}

?>
<?php $countNonEmpty=0; $lastCountNonEmpty=0;

if(!empty($times)):?>
	<ul>		
		<?php 
			foreach($times as $key => $value):
				if($countNonEmpty < 3):
		?>	
					
						<?php if(!empty($value)) { 
							$lastCountNonEmpty = $key; 
							$dayAvailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
							if($dayAvailable) { 
								if($now < strtotime($value) || $today != $timeDay['timeObj']->format('Y-m-d')) {
								$countNonEmpty = $countNonEmpty+1;

						?>
							<li>
								<a href="javascript:void(0)" onclick='setTime(this, "<?= date('H:i',strtotime($value));  ?>", "<?= $timeDay['weekday']; ?>", "<?= $timeDay['timeObj']->format('Y-m-d'); ?>" ); 
									showBookingModal("<?= $isSignedIn; ?>", "<?= $doctor ?>","<?php echo $my_patient?>");' data-day=" <?= $timeDay['weekday'] ?> "> 
									<?= date('H:i',strtotime($value)); ?> 
								</a>
							</li>
						<?php } }
							 } else { ?>
							 <li>
								<a > <?= '----' ?> </a>
							</li>
						<?php } ?>
					

		<?php 
				endif;
				if(count($times) == $lastCountNonEmpty && $countNonEmpty == 3):
		?>
					
						<?php if (!empty($value)) { 
							$lastCountNonEmpty = $key;
							$dayAvailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
							if($dayAvailable) { 
								if($now < strtotime($value) || $today != $timeDay['timeObj']->format('Y-m-d')) {
								$countNonEmpty = $countNonEmpty+1;
							?>
							<li>
								<a href="javascript:void(0)" onclick='setTime(this,"<?= date('H:i',strtotime($value));?>", "<?= $timeDay['weekday']; ?>", "<?= $timeDay['timeObj']->format('Y-m-d') ?>" ); 
									showBookingModal("<?= $isSignedIn; ?>", "<?= $doctor ?>","<?php echo $my_patient?>");' data-day="<?= $timeDay['weekday'] ?>">
									<?= date('H:i',strtotime($value)); ?>
								</a>
							</li>
							<?php } }
						 } else { ?>
						<li>
								<a > <?= '----' ?> </a>		
						</li>						
						<?php } ?>
				    
		<?php
				endif;
			endforeach; 
		?>
	</ul>
<?php endif; ?>
	<?php   if(count($times) > $lastCountNonEmpty): ?>
		<?php 
			foreach ($times as $key => $value):
				if($key > $lastCountNonEmpty):	
		?>					
		<?php if(!empty($value)) {
			$dayUnavailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
			if($dayUnavailable) { 
				
		?>
			<ul class="more-link">
				<li><a href="javascript:void(0)" onclick="showMore(this)"><?= __("MORE"); ?></a></li>
			</ul>
		<?php	
					break;	
				
				}
			}
		?>
	<?php
			endif; 
		endforeach; 
	?>

		
		<ul class="more-times">
			<?php 
				foreach ($times as $key => $value):
					if($key > $lastCountNonEmpty):	
			?>
					
						<?php if(!empty($value)) {
							$dayUnavailable = $this->DayAvailability->isTimeAvailable($value, $timeDay['timeObj']->format('Y-m-d'), $unavailableTime);
							if($dayUnavailable) { 
								if($now < strtotime($value) || $today != $timeDay['timeObj']->format('Y-m-d')) {
							?>
								<li>
								<a href="javascript:void(0)" onclick='setTime(this, "<?= date('H:i',strtotime($value));  ?>", "<?= $timeDay['weekday']; ?>", "<?= $timeDay['timeObj']->format('Y-m-d') ?>" ); 
									showBookingModal("<?= $isSignedIn; ?>", "<?= $doctor ?>","<?php echo $my_patient?>");' data-day="<?= $timeDay['weekday'] ?>"> 
									<?= date('H:i',strtotime($value)); ?>
								</a>
								</li>
							<?php } }
						 } else { ?>
						<li>
								<a > <?= '----' ?> </a>
						</li>
						<?php } ?>
					
			<?php
					endif; 
				endforeach; 
			?>
			
		</ul>

	<?php endif; ?>
	
	<?php 
		if(empty($times)):
			echo '<ul>';
			for($i=4; $i>count($times); $i--){ ?>
				<li>
					<a ><?= '----' ?></a>
				</li>
	<?php	
			}
			echo '</ul>';
		endif;
	?>
</div>