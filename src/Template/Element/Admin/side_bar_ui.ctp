<aside class="sidebar">
    <section>
        <ul class="list-group">            
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-dashboard fa-fw"></i>' . __('Dashboard'),[
                        'controller' => 'users',
                        'action' => 'dashboard',                        
                    ],[
                        'class' => "list-group-item ". $this->Navigation->activeTabs('users', 'dashboard'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-user fa-fw"></i>' . __('Manage Subadmin'),[
                        'controller' => 'users',
                        'action' => 'index',                        
                    ],[
                        'class' => "list-group-item ". $this->Navigation->activeTabs('users', 'index'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-user-md fa-fw"></i>' . __('Manage Doctors/Hospitals'),[
                        'controller' => 'users',
                        'action' => 'doctors',                        
                    ],[
                        'class' => "list-group-item ". $this->Navigation->activeTabs('users', 'doctors'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-male fa-fw"></i>' . __('Manage Patients'),[
                        'controller' => 'users',
                        'action' => 'patients',                        
                    ],[
                        'class' => "list-group-item ". $this->Navigation->activeTabs('users', 'patients'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-user-plus fa-fw"></i>' . __('Manage Specialty'),[
                        'controller' => 'Specialities',
                        'action' => 'index',                        
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('Specialities', 'edit').$this->Navigation->activeTabs('Specialities', 'add') .$this->Navigation->activeTabs('Specialities', 'index'),
                        'escape' => false
                    ]);
                ?>
            </li>  
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-table fa-fw"></i>' . __('Manage Insurance'),[
                        'controller' => 'Insurances',
                        'action' => 'index',                        
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('Insurances', 'edit').$this->Navigation->activeTabs('Insurances', 'add') .$this->Navigation->activeTabs('Insurances', 'index'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php
                    // $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-table fa-fw"></i>' . __('Manage Free Subscription'),[
                        'controller' => 'Users',
                        'action' => 'manageSubscription',                        
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('Users', 'manageSubscription'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                 <?php
                    echo $this->Html->link('<i class="fa fa-table fa-fw"></i>' . __('Manage Appointments'),[
                        'controller' => 'Users',
                        'action' => 'bookings',                        
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('Users', 'bookings'),
                        'escape' => false
                    ]);
                ?>
            </li>                                                       
            <li>
                <?php 
                    echo $this->Html->link('<i class="fa fa-envelope fa-fw"></i>' . __('Manage Email Templates'),[
                        'controller' => 'EmailTemplates',
                        'action' => 'index',
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('EmailTemplates', 'index').$this->Navigation->activeTabs('EmailTemplates', 'edit'),
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php 
                    echo $this->Html->link('<i class="fa fa-file fa-fw"></i>' . __('Manage CMS Pages'),[
                        'controller' => 'CmsPages',
                        'action' => 'index',
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('CmsPages', 'index'),
                        'escape' => false
                    ]);
                ?>
            </li>                            
            <li>
                <?php 
                    echo $this->Html->link('<i class="fa fa-key fa-fw"></i>' . __('Change Password'),[
                        'controller' => 'users',
                        'action' => 'change-password',
                    ],[
                        'class' => "list-group-item ".$this->Navigation->activeTabs('users', 'changePassword'),
                        'escape' => false
                    ]);
                ?>
            </li>            
        </ul>
    </section>
</aside>