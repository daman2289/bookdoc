<footer class="footer user-dashbaord-footer">
    <div class="container">
        <div class="col-sm-4">
            <p>© 2018 <?= __("Bookdoc. . All Rights Reserved ")?>.</p>
        </div>
        <div class="col-sm-4 text-center">
            <ul class="list-inline">
                <li><a href="#"><?= __("Privacy"); ?></li>
        		<li><a href="#"><?= __("Terms of use "); ?></a></li>
            </ul>
        </div>
        <div class="col-sm-4 text-right">
            <ul class="list-inline">
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</footer>
