<header class="main-header">
    <?=
        $this->Html->link($this->Html->image('logo.jpg'),[
        ],[
            'escape' => false,
            'class' => 'logo'
        ]);
    ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only"><?= __("Toggle navigation"); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                <?php                     
                    echo __('Administrator Panel');                    
                ?>
                </div>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                       
                <ul class="nav navbar-nav navbar-right custom-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">    Welcome <?= !empty($this->request->getSession()->read('Auth.User.user_profile.first_name')) ? ucwords($this->request->getSession()->read('Auth.User.user_profile.first_name')): 'Admin'; ?>
                            <!-- <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= 
                                    $this->Html->link('<i class="fa fa-lock fa-fw"></i>' . __('Change Password'),[
                                        'controller' => 'Users',
                                        'action' => 'change-password'
                                    ],[
                                        'escape' => false
                                    ]);
                                ?>
                            </li>
                            <li>
                                <?= 
                                    $this->Html->link('<i class="fa fa-sign-out fa-fw"></i>' . __('Logout'),[
                                        'controller' => 'Users',
                                        'action' => 'logout'
                                    ],[
                                        'escape' => false
                                    ]);
                                ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>