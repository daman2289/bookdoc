<?php 
    use Cake\Core\Configure;

    echo $this->Html->css([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css' 
    ]);

    echo $this->Html->script([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.js',
    ]);
if($this->request->getSession()->check('Auth.User')){
   // pr(array_flip(Configure::read('Plans')));
    if(in_array($this->request->getSession()->read('Auth.User.plan_id'),Configure::read('Plans'))){?>

    <script>
        var notification = {
            'data' :  {
                title: false,
                width: "100%",
                hide: false,
                stack: {
                    "dir1": "down", 
                    "dir2": "left", 
                    "push": "top", 
                    "spacing1": 0, 
                    "spacing2": 0, 
                    "firstpos1": 0, 
                    "firstpos2": 0
                },
                styling: "bootstrap3",
                // addclass: "ui-pnotify-inline",
                buttons: {
                    sticker: false
                }
                
            }
        };
        
        getPlanTime();


        function upgrade(event){
            event.preventDefault();
            $(".resend-conf").prop('disabled', true);
            window.location.href="/users/dashboard#choosePlan"   
        }

        function getPlanTime(){
             $.get("/api/notifications/getPlanLeftTime.json")
                .done(function(data) {
                    notification.data.text  = `<div class="text-center"> <?= __("Your current plan is going to expire in")?> <strong>${data.results }.\
                        </strong> <a class='resend-conf' href='javascript:void(0);' onclick='upgrade(event)'>Upgrade Now</a> </div>`;
                    new PNotify(notification.data);
                }).always(function() {
                    $(".resend-conf").prop('disabled', false);
                });
                // .fail(function(data){
                //     notification.data.text  = `<?= __("Oops! Some Error has occured while fetching current Plan records"); ?>`;
                //     notification.data.type = 'error';
                //     new PNotify(notification.data);
                // })

        }

    </script>
<?php } ?>
<?php } ?>