<?= $this->Form->create('', ['class' => 'login-section', 'id' => 'reset-password']) ?>
<div class="form-group">
    <label><?php echo __('New Password'); ?></label>
    <?php
        echo $this->Form->control('new_password', [
                'class' => 'form-control',
                'placeholder' => __('New Password'),
                'type'=>'password',
                'required'=> 'required',
                'autocomplete' => 'off',
                'maxlength'=>100,
                'label' => false
            ]);
    ?>
</div> 
<div class="form-group">
    <label><?php echo __('Confirm Password'); ?></label>
    <?php
        echo $this->Form->control('confirm_password', [
                'class' => 'form-control',
                'placeholder' => __('New Password'),
                'type'=>'password',
                'required'=> 'required',
                'autocomplete' => 'off',
                'maxlength'=>100,
                'label' => false
            ]);
    ?>
</div>        
<?php
echo $this->Form->button(__('Submit'),
    [
        'type' => 'submit',
        'class' => 'btn btn-black btn-block'
    ]
);

?>
<?= $this->Form->end() ?>