<?php
echo $this->Form->create(null,[
	'url' => [
		'action' => 'login',
		'?' => ($this->request->getQuery('redirect')) ? ['redirect' => $this->request->getQuery('redirect')] : []
	],
	'class' => 'login-section',
	'id' => 'login-form'
]);
?>
<div class="form-group">
	<label><?php echo __('Email Address'); ?></label>
	<?php
		echo $this->Form->email('email', [
				'class' => 'form-control',
				'placeholder' => __('Email Address'),
				'autocomplete' => 'on'
			]);
	?>
</div>
<div class="form-group">
	<label><?php echo __('Password'); ?></label>
	<?php
		echo $this->Form->password('password', [
				'class' => 'form-control',
				'placeholder' => __('Password'),
				 'autocomplete' => 'on'
			]);
	?>
</div>    
<?php
echo $this->Form->button(__('Login'),
	[
		'type' => 'submit',
		'class' => 'btn btn-black btn-block'
	]
);
/*echo $this->Html->link("Forgot Password ?",['controller'=>'Users','action'=>'forgotPassword'], ['class' => 'pull-right']);*/

echo $this->Form->end(); 
?>