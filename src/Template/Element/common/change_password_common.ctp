<section>
    <div class="col-sm-12">
        <h3><?= __('Change Password') ?></h3>
        <ol class="breadcrumb">
            <li>
                <?php
                    echo $this->Html->link(__('Dashboard'), [
                        'controller' => 'users',
                        'action' => 'dashboard'
                    ]);
                ?>
            </li>
            <li class="active">
                <?= __('Change Password'); ?>
            </li>
        </ol>
    </div>
</section>
<section class="content">
    <div class="box">
        <div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
            <?php 
                echo $this->Form->create('', [
                    'url'   => ['controller' => 'Users', 'action' => 'change-password'],
                    'type'  => 'post',
                    'id'    => 'change-password'
                    ]
                );
                echo $this->Form->hidden('id', ['value' => $this->request->getSession()->read('Auth.User.id')]);
            ?>   
                <div class="form-group">
                    <label class="control-label"><?php echo __('Old Password'); ?><span>*</span></label>
                    <?php
                        echo $this->Form->control('old_password', array(
                            'class' => 'form-control',
                            'type' => 'password',
                            'label' => false,
                            'placeholder' => __('Old Password'),
                             'maxlength'=>100
                            )
                        );
                    ?>
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo __('New Password'); ?><span>*</span></label>
                    <?php
                        echo $this->Form->control('password', array(
                            'class' => 'form-control',
                            'type' => 'password',
                            'label' => false,
                            'placeholder' => __('New Password'),
                            'data-display' => 'mainPassword',
                             'maxlength'=>100
                            )
                        );
                    ?>
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo __('Confirm Password'); ?><span>*</span></label>
                    <?php
                        echo $this->Form->control('confirm_password', array(
                            'class' => 'form-control',
                            'type' => 'password',
                            'label' => false,
                            'placeholder' => __('Confirm Password'),
                             'maxlength'=>100
                            )
                        );
                    ?>
                </div>
                <div class="form-group">
                    <div class="clearfix">                               
                        <div class="text-center">                                
                            <?php
                                echo $this->Form->button(__('Save'), array(
                                    'class' => 'btn btn-lg btn-primary',
                                    'type' => 'submit'
                                ));
                            ?>
                        </div>
                    </div> 
                </div>     
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</section>