<div id="add-report" class="modal fade add-report" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo __('Add Report')?></h4>
      </div>
      <div class="modal-body">
        <?php 
                echo $this->Form->create('', [
                    'url'   => ['controller' => 'Reports', 'action' => 'addReport'],
                    'type'  => 'file',
                    'id'    => 'add-report-form'
                    ]
                );
        ?>  
        <div class="form-group">
        <label class="control-label"><?php echo __('Select Patient'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('patient_id', array(
                    'class' => 'form-control',
                    'label' => false,
                    'empty' => __('Select'),
                    'options' => $patient_name
                    )
                );
            ?>
        </div> 
        <div class="form-group">
        <label class="control-label"><?php echo __('Title'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('title', array(
                    'class' => 'form-control',
                    'label' => false,
                    'Placeholder' => __('Title')
                    )
                );
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Description'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('description', array(
                    'class' => 'form-control',
                    'label' => false,
                    'Placeholder' => __('Title'),
                    'type' => 'textarea'
                    )
                );
            ?>
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo __('Upload Report'); ?><span class="red-star">*</span></label>
            <?php
                echo $this->Form->control('file', array(
                    'class' => 'form-control',
                    'label' => false,
                    'type' => 'file'
                    )
                );
            ?>
        </div>
        <small class="text-info"><?php echo __('Upload only pdf, doc, docx, jpg, jpeg, png, gif files');?></small>
        <div class="form-group">
            <div class="clearfix">                               
                <div class="text-center save-btn">                                
                    <?php
                        echo $this->Form->button('Save', array(
                            'class' => 'btn submit',
                            'type' => 'submit'
                        ));
                    ?>
                </div>
            </div> 
        </div> 
        </form>
      </div>
    </div>

  </div>
</div>
<style type="text/css">
    .red-star {
        color : red;
    }
</style>