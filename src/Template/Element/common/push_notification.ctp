<?php use Cake\Core\Configure; ?>
<?php if($this->request->getSession()->check('Auth.User')) { ?>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        <?php
            $appId = Configure::read('PUSH_API');
            $secretKey = Configure::read('PUSH_API_SECRET');

            if(Configure::read('debug')){
                $appId = Configure::read('PUSH_LOCAL_API');
                $secretKey = Configure::read('PUSH_LOCAL_API_SECRET');
            }
        ?>
        var OneSignal = window.OneSignal || [];
        
        OneSignal.push(function() {
            
            OneSignal.init({
                appId: "<?= $appId; ?>",
            });
            
            OneSignal.isPushNotificationsEnabled().then(function(isEnabled) {
                
                if (isEnabled){
                    console.log("Push notifications are enabled!");
                    OneSignal.getUserId(function(token) {
                        console.log("OneSignal User ID:", token);
                        saveToken(token);                        
                    });

                }else{
                    OneSignal.showHttpPrompt().then(function (params) {
                        console.log("param", params);
                        
                    });
                    console.log("Push notifications are not enabled yet.");      
                }
            });


        
        });

        function saveToken(token){
            var url = '/api/notifications/saveToken.json';
            var data = {
                'user_token' : token
            }
            $.ajax({
                type:"POST",
                url : url,
                dataType:'json',
                data:data,
                success: function(response){
                    console.log(response);                              
                },
                        
            });
        }
    </script>
<?php } ?>
