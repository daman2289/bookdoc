<?php 
    use Cake\Core\Configure;

    echo $this->Html->css([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css' 
    ]);

    echo $this->Html->script([
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js',
        'https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.js',
    ]);

    if(empty($availability)){?>

    <script>
        var notification = {
            'data' :  {
                title: false,
                width: "100%",
                hide: false,
                stack: {
                    "dir1": "down", 
                    "dir2": "left", 
                    "push": "top", 
                    "spacing1": 0, 
                    "spacing2": 0, 
                    "firstpos1": 0, 
                    "firstpos2": 0
                },
                styling: "bootstrap3",
                // addclass: "ui-pnotify-inline",
                buttons: {
                    sticker: false
                }
                
            }
        };
       
        notification.data.text  = `<div class="text-center"> <?= __("Your have not mentioned your working days, please fill your availability/slots so that it can be searched by patients.")?></div>`;
        new PNotify(notification.data);

    </script>
<?php } ?>