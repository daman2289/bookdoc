<?= $this->Form->create('', ['class' => 'login-section', 'id' => 'forgot-password']) ?>
<div class="form-group">
    <label><?php echo __('Email Address'); ?></label>
    <?php
        echo $this->Form->email('email', [
                'class' => 'form-control',
                'placeholder' => __('Email Address'),
                'required'=> 'required',
                'autocomplete' => 'off',
                'maxlength'=>200
            ]);
    ?>
</div>        
<div class="form-group text-center ">
    <?=
        $this->Form->button(__('Submit'),[
            'type' => 'submit',
            'class' => 'btn btn-black'
        ]);
    ?>
    <?=
    $this->Html->link(__('Back'), [
        'action' => 'login'
    ], [
        'class' => 'btn btn-black'
    ]);
    ?>
</div>
<?= $this->Form->end() ?>