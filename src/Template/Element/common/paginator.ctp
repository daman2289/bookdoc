<?php  use Cake\Core\Configure; ?>

    <?php if($this->Paginator->counter('{{count}}') > Configure::read('recordsPerPage')){ ?>
       
        <div class="paginations">
            <ul class="pagination">
                <?= $this->Paginator->prev("<?= __(' Prev'); ?>" ); ?>
                <?= $this->Paginator->numbers(); ?>
                <?= $this->Paginator->next("__(' Next') "); ?>
            </ul>
        </div>

      <?php } ?>