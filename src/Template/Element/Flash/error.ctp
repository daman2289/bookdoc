<?php
// if (!isset($params['escape']) || $params['escape'] !== false) {
//     $message = h($message);
// }
?>
<div class="message error alert alert-danger main-alert-msg-box alert-dismissible" onclick="this.classList.add('hidden')">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
	<?= $message ?>
</div>

