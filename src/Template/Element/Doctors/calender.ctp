<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

<?php
use Cake\Routing\Router;
    echo $this->Html->css([
        'calender/datepicker.css',
        'calender/fullcalendar.min.css',
        //'calender/fullcalendar.print.min',
        'calender/style.css'
    ],['block' => true]);

    echo $this->Html->script([
        'calender/moment.min.js',
        'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',
        'calender/fullcalendar.min.js',
        'locale/locale-all',
        'locale/de',
         'gcal.min',
        'calender/bootstrapmodal.min.js',
        'calender/datepicker.js',
        'calender/mycalender.js',
    ], ['block' => true]);
?>

<style>
   table > tbody > tr  {
     height: 50px ;
}
.right-sidebar {
    padding-bottom: 80px;
}
</style>
<script>
  //This reads data from URL and sets in window for calendar default Week
  var date = "<?= $this->request->getQuery('on'); ?>";
  if (date == "") {
    date = Date();    
  }else{
    date = "<?= base64_decode($this->request->getQuery('on')); ?>"
  }
  window.myBookingDate = date;  
  
  
</script>

<div class="col-md-3 no-print">
    <div class="form-group">
        <div class="col-xs-12 form-group">
            <h2 class="dark-blue"><?= __('Booking Calender') ?></h2>
        </div>
        <div id="datetimepicker12"></div>
    </div>
    
    <!-- <p>Date: <input type="text" id="datetimepicker1"></p> -->
    <div class="col-xs-12 form-group">
        <h4 class="form-group dark-blue"><?= __('Booking info') ?></h4>
    </div>
    <div class="clr_box"> 
        <span><b style="background:#ffaf48;"></b><?= __("Pending for Approval"); ?> </span> 
        <span><b style="background:#76ff90;"></b><?= __("Confirmed (Local Bookings)"); ?> </span> 
        <span><b style="background:#fb5570;"></b><?= __("Decline by Doctor"); ?> </span> 
        <span><b style="background:#44fcd3;"></b><?= __("Confirmed (Site Bookings)"); ?> </span> 
        <span><b style="background:#6c96ff;;"></b><?= __("Cancelled by Patient"); ?> </span> 
        <span><b style="background:#a8c3c3;"></b> <?= __("Unavailable"); ?></span> 
        <div class="clear"></div>
    </div>  
</div>
<div class="col-md-9 print-w-full">
    <div class="calendar_div">
    	<div class="dropdown">
    		  <button id="dLabel"	class="btn btn-default new_appo"	 type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    			<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    			
    		  </button>
    		  <ul class="dropdown-menu" aria-labelledby="dLabel">
    			<li>
    				<button type="button" class="btn btn-primary ManageWorkingHours"> <?= __("Manage working hours"); ?></button>
                   <!--  <?php echo $this->Html->link('Manage working hours',
                        ['controller' => 'Doctors', 'action' => 'manageWorkingHours'],
                        ['class' => 'btn btn-primary']
                    );?> -->
    			</li>
    			<li>
    				<button type="button" class="btn btn-primary NewAppointment"> <?= __("New Appointment"); ?></button>
    			</li> <!-- <a href="#"></a> </li> -->
                        <li>
    				<button type="button" id="authorize_button" class="btn btn-primary"> <?= __("Sync Google Calendar"); ?></button><button id="signout_button" style="display: none;">Sign Out</button>

    			</li>
                <?php 
                    $client_id = '1083594538233-vqkllggvaiefenfamkfuret6j1osgqlv.apps.googleusercontent.com';
                    $client_redirect_url = Router::url(array(
                           'controller' => 'Doctors',
                           'action' => 'addEvent',
                           'prefix' => 'api',
                        ), true); 
                    $login_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($client_redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=online';?>
		          <li>
                    <a id="sync-to-gcal" href="<?php echo $login_url?>" class="btn btn-primary"> <?= __("Sync to Google Calendar"); ?></a>
                </li>
    		  </ul>
    	</div>
    	<!-- Button trigger modal -->
    	<div class="clear"></div>
        <div id='mycalendar'></div>
    </div>
</div>
 
   
<?= $this->Element('Modals/calender/book_appointment') ?>
<?= $this->Element('Modals/calender/cancel_appointment') ?>
<?= $this->Element('Modals/calender/reschedule_appointment') ?>
<?= $this->Element('Modals/calender/manage_working_hours') ?>
<?= $this->Element('Modals/calender/overide_daily_hours') ?>
<?= $this->Element('Modals/calender/patient_information') ?>
<?= $this->element('Modals/confirm_appointment'); ?>


