<div>
    <strong><?= __("Photos "); ?>:</strong>
    <div class="owl-carousel photo-carousel owl-theme">
        <?php 
            foreach ($images as $image) { ?>
                <div class="item">
                    <?php
                    $getimagePath = $this->User->checkAndGetDoctorImages($image->images);
                    if ($getimagePath) {
                        echo $this->Html->link(
                            $this->Html->image(
                                $getimagePath, 
                                ["alt" => "Doctor Images", 'class' => 'doctor-pic-custom-dimensions']
                            ),
                            $getimagePath,
                            ['escape' => false, 'class' => 'thumbnail']
                        );
                    }?>
                </div>
            <?php
            } ?>
        </div>
</div>
<?=
    $this->Html->script(['carousal'],['block' => true]);
?>
