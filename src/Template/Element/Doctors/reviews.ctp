<div class="doctor_profile_reviews">
        <div class="container-fluid">
            <div class="doctor_review_slider">
                <div class="review_heading">
                    <h2 class="heading"><?= __("Verified Patient Reviews"); ?></h2>
                    <h3 class="subheading"><?php echo $overall_rating_doctor?> <?= __("out of 5 stars"); ?></h3>
                </div>
            
                <div class="doctor_rating_slider">
                        <div class="owl-carousel owl-carouselsss">
                        <?php 
                            if(!empty($rating)) {
                            foreach($rating as $rate) { 
                        ?>
                        <div class="testimonial">
                            <p class="description"><?php echo $rate->review?></p>
                            <h3 class="date"><?php echo date('M d Y',strtotime($rate->created));?></h3>
                            <h3 class="title">by a <?php echo $rate->patient->user_profile->full_name?></h3>
                            <div class="doctor_rating_profile">
                                <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                                    <div class="rating">
                                        <div class="groupstar">
                                        <input type="text" class="rate_overall_doctor" name="overall_rating" value="<?= $rate->overall_rating ?>" data-size="xxs" title="">

                                    </div>
                                </div>
                                <div class="star_for_name"><?= __("Overall Rating"); ?></div>
                              </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                                    <div class="rating">
                                        <div class="groupstar">
                                        <input type="text" class="rate_overall_doctor" name="overall_rating" value="<?= $rate->bedside_manner ?>" data-size="xxs" title="">
                                    </div>
                                </div>
                                <div class="star_for_name"><?= __("Bedside Manner"); ?></div>
                              </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 p-0">
                                    <div class="rating">
                                        <div class="groupstar">
                                        <input type="text" class="rate_overall_doctor" name="overall_rating" value="<?= $rate->wait_time ?>" data-size="xxs" title="">
                                    </div>
                                </div>
                                <div class="star_for_name"><?= __("Wait Time"); ?></div>
                              </div>
                            </div>
                          </div>
                          <?php } } else { ?>
                          <div class="testimonial">
                                <?php echo __('No rating added yet');?>
                          </div>
                          <?php } ?>
                      </div>
              </div>
            </div>
        </div>
     </div>  




