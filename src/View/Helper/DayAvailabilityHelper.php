<?php
/**
 * User Helper
 * @package Helpers
 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\I18n\Time;
use Cake\Utility\Hash; 
use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;

class DayAvailabilityHelper extends Helper
{

    public $lastAvailableDay = null;
 
    /**
     * convertFrozenTimeToHumanUnserstandable method to convert frozen time to human understansable time
     * 
     * @param $time FrozenTime 
     * @return string
     */
    public function convertFrozenTimeToHumanUnserstandable($time) {

        $timeObj = Time::parse($time);
        return $timeObj->i18nFormat('HH:mm');
    }

    public function unavailableSlots($unavailableSlots) {
        $data  = [];
        foreach ($unavailableSlots as $key => $value) {
            $weekday = date('N', strtotime($value->formatted_time));
            $data[$value->formatted_time][$value->id] = Hash::extract($value->unavailable_slots, '{n}.timing');
        }
        return $data;
    }

    public function isTimeAvailable($timeSlot, $date, $unavailableSlots){
        $dayofweek = date('N', strtotime($date));
        $exists = false;
        if (empty($unavailableSlots)) {
            return true;
        }
        $allSlots = (isset($unavailableSlots[$date]) ? $unavailableSlots[$date] :[]);
        if (!empty($allSlots)) {
            $data = Hash::extract($allSlots, '{n}.{n}');
            $exists = in_array($timeSlot, $data);
        }

        return !$exists;
    }

    public function setLastAvailableDay($lastAvailableDay) {
        if (is_null($this->lastAvailableDay)) {
            $this->lastAvailableDay = $lastAvailableDay;
        }
    }

    public function getLastAvailableDay() {
        return $this->lastAvailableDay;
    }

    public function nextAvailableDay($value, $weekDays) {
        $availableWeekDay = null;
        $currentWeekDay = $value["weekday"];

        foreach ($weekDays as $weekDay => $slot) {
            if (!empty($slot)) {
                $availableWeekDay = $weekDay;

                break;
            }
        }

        if (!is_null($availableWeekDay)) {
            $time = new Chronos($value["timeObj"]->format("Y-m-d h:i:s"));
            $newDateTime = $time->next($availableWeekDay);
            
            return $newDateTime->format('D, M d');
        }

        return null;
    }
}