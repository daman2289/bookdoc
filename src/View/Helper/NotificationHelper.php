<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\InternalErrorException;
use Cake\Routing\Router;

/**
 * Notification helper
 */
class NotificationHelper extends Helper
{

		/**
		 * Default configuration.
		 *
		 * @var array
		 */
		protected $_defaultConfig = [];

		 public function get(){

        $notificationsTable = TableRegistry::get('Notifications');
        $user_id = $this->request->getSession()->read('Auth.User.id');


        $results = $notificationsTable->find('all')
                        ->where(['Notifications.user_id' => $user_id])
                        ->order(['Notifications.created' => 'DESC'])
                        ->limit(10);

        return $results;
    }

		public function getModel(){
				return "<!-- Modal -->
						<div class='modal fade' id='notficationModal'>
							<div class='modal-dialog modal-lg'>
								<div class='modal-content'>
									<div class='modal-header'>
										<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											<span aria-hidden='true'>&times;</span></button>
										<h4 class='modal-title'>__('Notifications') </h4>
									</div>
									<div class='modal-body' id='notification-content-list'>
										<p> __('Loading please wait')...</p>
									</div>
									<div class='modal-footer'>
										<div id='notification-pagination' class='pagination-sm pagination' style='margin:0;'></div>
									</div>
								</div>
							</div>
						</div>";
		}

		public function getVarifiedStatus($userId){
			$users = TableRegistry::get('Users');
			$isVerified = $users->get($userId)->is_varified;
			return $isVerified;

		}
		public function getUnreadNotificationCount(){
			$notificationsTable = TableRegistry::get('Notifications');
			$user_id = $this->request->getSession()->read('Auth.User.id');
			$uread_count = $notificationsTable->find('all')
									->where(['Notifications.user_id' => $user_id])
									->andWhere(['Notifications.unread' => 1])
									->count();
			
			return $uread_count;
		}

}

