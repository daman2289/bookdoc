<?php
/**
 * User Helper
 * @package Helpers
 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Utility\Hash;

class UserHelper extends Helper
{
 
    /**
     * getUserAge method to get user age fron their date of birth
     * 
     * @return integere
     */
    public function getUserAge($dob) {
    	@$date1 = $dob;// if format mm/dd/yyyy
        $date2 = date("Y-m-d");//change m,d,y according to input data format
        $diff = abs(strtotime($date2) - strtotime($date1));
        $years = floor($diff / (365*60*60*24));
        return $years;
    }

    /**
     * method checkAndGetImage to check image exists on server or not. Then return its path
     *
     * @param $imageName string
     * @return string
     */
    public function checkAndGetImage($imageName, $isFemale=false) {
        $defaultPic = Configure::read('UserDummyImage');
        if ($isFemale) {
            $defaultPic = Configure::read('DummyFemaleImage');
        }
        if (!empty($imageName)) {
            $file = new File(Configure::read('UserProfilePicPathToCheck') . $imageName);
            if ($file->exists()) {
                return '../' . Configure::read('UserProfilePicPathToShow') . $file->name;
            }
        }
        return $defaultPic;
    }

    public function checkAndGetDoctorImages($imageName, $isFemale=false) {
        if (!empty($imageName)) {
            $file = new File(Configure::read('doctorAddedImagesCheckPath') . $imageName);
            if ($file->exists()) {
                return '../' . Configure::read('doctorAddedImagesPath') . $file->name;
            }
        }
        return false;
    }
    

    /**
     * method getCommaSeparatedUserSpecialities to convert user specialities into comma separated string to show in detail
     *
     * @param $userSpecialities array obj
     * @return string
     */
    public function getCommaSeparatedUserSpecialities($userSpecialities) {
        if(!empty($userSpecialities)) {    
            $specialitiesIntoKeyValue = Hash::extract($userSpecialities, '{n}.speciality.title_eng');
            $specialitiesIntoCommaSeparatedString = str_replace(',', ', ', implode($specialitiesIntoKeyValue, ','));
            return $specialitiesIntoCommaSeparatedString;
        } else {
            return __('No specialities added yet');
        }
    }

    /**
     * method getUserEducationsToShowInStringFormat to convert user specialities into string to show in detail
     *
     * @param $userEducations array obj
     * @return string
     */
    public function getUserEducationsToShowInStringFormat($userEducations) {
        $userDegreeAndUniversities = Hash::combine(
                                $userEducations,
                                '{n}.id',
                                ['%s - %s', '{n}.degree', '{n}.university']
                            );

        $userEducationFormatToShow = '';
        if (!empty($userDegreeAndUniversities)) {
            foreach ($userDegreeAndUniversities as $key => $value) {
                $userEducationFormatToShow .= '<p>' . $value . '</p>';
            }
        } else {
            return '';
        }

        return $userEducationFormatToShow;
    }

    /**
     * method getCommaSeparatedUserLanguages to convert user languages into comma separated string to show in detail
     *
     * @param $userLanguages array obj
     * @return string
     */
    public function getCommaSeparatedUserLanguages($userLanguages) {
        $languagesIntoKeyValue = Hash::extract($userLanguages, '{n}.language.name');
        $languagesIntoCommaSeparatedString = str_replace(',', ', ', implode($languagesIntoKeyValue, ','));
        return $languagesIntoCommaSeparatedString;
    }

    /**
     * method getUserHohspitalAffiliationsToShowInStringFormat to convert user hospital affilications into string to show in detail
     *
     * @param $userHospitalAffiliations array obj
     * @return string
     */
    public function getUserHohspitalAffiliationsToShowInStringFormat($userHospitalAffiliations) {
        $hospitalAffiliationsIntoKeyValue = Hash::extract($userHospitalAffiliations, '{n}.name');

        $userHospitalAffiliationFormatToShow = '';
        if (!empty($hospitalAffiliationsIntoKeyValue)) {
            foreach ($hospitalAffiliationsIntoKeyValue as $key => $value) {
                $userHospitalAffiliationFormatToShow .= '<p>' . $value . '</p>';
            }
        }

        return $userHospitalAffiliationFormatToShow;
    }

    /**
     * method getCommaSeparatedUserInsurances to convert user insurances into comma separated string to show in detail
     *
     * @param $userInsurances array obj
     * @return string
     */
    public function getCommaSeparatedUserInsurances($userInsurances) {
        $languagesIntoKeyValue = Hash::extract($userInsurances, '{n}.insurance.title_eng');
        $insurancesIntoCommaSeparatedString = str_replace(',', ', ', implode($languagesIntoKeyValue, ','));
        return $insurancesIntoCommaSeparatedString;
    }
}