<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * Menu helper
 */
class GeneralHelper extends Helper
{
    public function isSelected($type, $value){
        if ($type === 'gender') {
            $gender = $this->request->getQuery('gender',null);

            if ($gender === $value) {
                return 'checked';
            }
        }
        
        if ($type === 'day') {
            $day = $this->request->getQuery('day',null);
            
            if ($day === $value) {
                return 'checked';
            }
        }

        return null;
    }

    public function isActive($type, $item){
        if ($type === 'gender') {
            $gender = $this->request->getQuery('gender','no');
            if ($item === $gender) {
                return 'active';
            }
            if ($gender == '' && $gender == $item) {
                    return 'active';                    
            }
        }
        if ($type === 'day') {
            $day = $this->request->getQuery('day','no');
            if ($item === $day) {
                return 'active';
            }
            if ($day == '' && $day == $item) {
                return 'active';                    
            }
        }
        return;
    }

    public function getDoctorTimings($data) {
        if (! isset($days)) {
            $days = [];
        }
        
        foreach ($data as $key => $value) {
            if(!empty($days[$value['weekday']]) ) {
                $allTiming = $days[$value['weekday']];
                $days[$value['weekday']] = array_merge($allTiming,Hash::extract($value, 'available_slots.{n}.timing'));
            } else {
                $days[$value['weekday']] = Hash::extract($value, 'available_slots.{n}.timing');
            }
            
        }
        return $days;
    }

    public function getBlockTime($weekday){
        $blockTime = TableRegistry::get('DoctorBlockTimings');
        $bt = $blockTime->findByWeekday($weekday);
        return $bt;
    }

    public function getBookingDateAndTime($bookingDate, $bookingTime){
        $now = Time::parse($bookingDate->format('Y-m-d') .' ' .$bookingTime->format('H:i:s'));
        $zonesuffix = "CEST";
        return $now->nice().' '.$zonesuffix;
    }

    public function getTimings(){
        $timings = [
            0 => 'Out',
            '00:00' => '00:00',
            '00:30' => '00:30',
            '01:00' => '01:00',
            '01:30' => '01:30',
            '02:00' => '02:00',
            '02:30' => '02:30', 
            '03:00' => '03:00',
            '03:30' => '03:30',
            '04:00' => '04:00',
            '04:30' => '04:30',
            '05:00' => '05:00',
            '05:30' => '05:30',

            '06:00' => '06:00',
            '06:30' => '06:30',
            '07:00' => '07:00',
            '07:30' => '07:30',
            '08:00' => '08:00',
            '08:30' => '08:30',
            '09:00' => '09:00',
            '09:30' => '09:30',
            '10:00' => '10:00',
            '10:30' => '10:30',
            '11:00' => '11:00',
            '11:30' => '11:30',

            '12:00' => '12:00',
            '12:30' => '12:30',
            '13:00' => '13:00',
            '13:30' => '13:30',
            '14:00' => '14:00',
            '14:30' => '14:30',
            '15:00' => '15:00',
            '15:30' => '15:30',
            '16:00' => '16:00',
            '16:30' => '16:30',
            '17:00' => '17:00',
            '17:30' => '17:30',

            '18:00' => '18:00',
            '18:30' => '18:30',
            '19:00' => '19:00',
            '19:30' => '19:30',
            '20:00' => '20:00',
            '20:30' => '20:30',
            '21:00' => '21:00',
            '21:30' => '21:30',
            '22:00' => '22:00',
            '22:30' => '22:30',
            '23:00' => '23:00',
            '23:30' => '23:30',

        ];
        return $timings;
    }

    public function getDuration() {
        $durations = [
            0 => 'Out',
            '00' => '00:00',
            '10' => '00:10',
            '20' => '00:20',
            '30' => '00:30',
            '40' => '00:40',
            '50' => '00:50', 
            '60' => '01:00',
            '70' => '01:10',
            '80' => '01:20',
            '90' => '01:30',
            '100' => '01:40',
            '110' => '01:50',
            '120' => '02:00',
            

        ];
        return $durations;
    }
    public function getTimes($startTime="00:00", $endTime="23:59"){
        $begin = new \DateTime($startTime);
        $end = new \DateTime($endTime);
        $duration = TableRegistry::get('Durations');
        $time_duration = $duration->find()
                        ->where(['Durations.user_id' => $this->request->getSession()->read('Auth.User.id')])
                        ->first();
        $timespan = $time_duration->duration*60;
        //$timespan = 30*60;
        $interval = new \DateInterval('PT'.$timespan.'S');
        $dateRange = new \DatePeriod($begin, $interval, $end);
        $range = [];
        foreach ($dateRange as $date) {
            $new = [];
            $new[$date->format('H:i')] = $date->format('H:i');
            $range = array_merge($range, $new);
        }
        return $range;
    }

    public function getDurationTimes(){
        $duration = [];
        $key = __("Minutes");
        $durations = TableRegistry::get('Durations');
        $time_duration = $durations->find()
                        ->where(['Durations.user_id' => $this->request->getSession()->read('Auth.User.id')])
                        ->first();
        foreach (range(0, 180, $time_duration->duration) as $number) {

            $yes = [ $number => "{$number} {$key}"];
            $duration = $duration + $yes;
        }
        
        return $duration;
    }

    public function formatTime($bookingDate, $bookingTime){
        $myDate = new Time($bookingDate->i18nFormat('yyyy-MM-dd') .' ' .$bookingTime->i18nFormat('HH:mm'));
        return $myDate->format('d. F Y, H:i') ;
    }

    public function isPlanActive($activePlan, $plan){
        if ($plan == $activePlan) {
            return true;
        }
        return false;
    }

    public function getWeekday(){
        $days = [
            '1' => __('Monday'),
            '2' => __('Tuesday'),
            '3' => __('Wednesday'),
            '4' => __('Thursday'),
            '5' => __('Friday'),
            '6' => __('Saturday'),
            '7' => __('Sunday'),
        ];
        return $days;
    }

    public function getActiveTab($item){
        $day = $this->request->pass[0];
        if ($item === $day || (empty($day) && $item == 'patient')) {
            return 'active';
        }
        return '';
    }

    public function getLocaleSelecter($locale){
        $sessionLocale = $this->request->getSession()->read('Config.locale');
        if (empty($sessionLocale)) {
            $sessionLocale = 'en_US';
        }
        if ($sessionLocale == $locale) {
            return 'selected';
        }
        return '';
    }

    public function getTimimgSlots() {
        $result = array();
        for ($n = 0; $n < 24 * 60; $n+=60)
        {
            $date = sprintf('%02d:%02d', $n / 60, $n % 60);
            $timestamp = strtotime($date) + 60*60;
            $time = date('H:i', $timestamp);
            $options = $date . '-' . $time;
            $result[$options] = $options;
        }
        return $result;
    }
}