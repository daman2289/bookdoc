<?php
 
/**
 * Navigation Helper
 * @package Helpers
 */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
class NavigationHelper extends Helper
{
    
 
    /**
     * Active the tab on match condition
     * 
     * @return active class
     */
    public function activeTabs($controller, $action) {
        return (ucfirst($this->request->getParam('controller')) == ucfirst($controller) && $this->request->getParam('action') == $action) ? 'active' : '';
    }

    public function getSignedInUserName(){
        
        if ($this->request->getSession()->check('Auth.User')) {
            return $this->request->getSession()->read('Auth.User.user_profile.full_name');
        }
        return null;
    }
    
         
}
 
?>