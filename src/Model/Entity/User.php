<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\Utility\Hash; 
use Cake\ORM\TableRegistry;

/**
 * User Entity
 *
 * @property int $id
 * @property int $role_id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property int $status
 * @property int $is_approved
 * @property int $is_email_verified
 * @property int $is_deleted
 * @property string $persist_code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $password_reset_token
 * @property string $hashval
 *
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Insurance[] $insurances
 * @property \App\Model\Entity\Speciality[] $specialities
 * @property \App\Model\Entity\UserProfile[] $user_profiles
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'password_reset_token',
        'hashval'
    ];

    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }

    protected $_virtual = ['all_unavailable_slots', 'doctor_final_rating'];

    
    protected function _getAllUnavailableSlots(){
        $allSlots = [];
        if (!empty($this->_properties['doctor_unavailability'])){
            $allSlots = Hash::combine($this->_properties['doctor_unavailability'], '{n}.id','{n}.unavailable_slots', "{n}.formatted_time");
            //  $allSlots = Hash::combine($allSlots, ['']);
        }
        return $allSlots;
    }

    protected function _getDoctorFinalRating(){
        $overAllrating = 0;
        if (!empty($this->_properties['doctor_ratings'])){
            $rating_doctor = Hash::extract($this->_properties['doctor_ratings'], '{n}.overall_rating');
            if (!$rating_doctor) {
                $rating_doctor = [1];
            }
            $overAllrating = array_sum($rating_doctor) / count($rating_doctor);
            return $overAllrating;
        }
        return $overAllrating;
    }


    public function parentNode(){
        if (!$this->id) {
            return null;
        }
        if (isset($this->role_id)) {
            $roleId = $this->role_id;
        } else {
            // $Users = TableRegistry::get('Users');
            // $user = $Users->find('all', ['fields' => ['role_id']])->where(['id' => $this->id])->last();
            $roleId = 4;
        }
        if (!$roleId) {
            return null;
        }
        return ['Roles' => ['id' => $roleId]];
    }
}
