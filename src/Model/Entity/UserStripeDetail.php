<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserStripeDetail Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $customer_id
 * @property \Cake\I18n\FrozenTime $current_period_start
 * @property \Cake\I18n\FrozenTime $current_period_end
 * @property string $subscription_id
 * @property int $is_paid_plan_subscribed
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Plan $plan
 */
class UserStripeDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'customer_id' => true,
        'current_period_start' => true,
        'current_period_end' => true,
        'subscription_id' => true,
        'is_paid_plan_subscribed' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'plan' => true,
        'auto_renew' => true
    ];
}
