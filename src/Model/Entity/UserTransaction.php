<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserTransaction Entity
 *
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property int $plan_id
 * @property float $amount
 * @property int $is_paid
 * @property string $invoice_url
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Plan $plan
 */
class UserTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'user_id' => true,
        'plan_id' => true,
        'amount' => true,
        'is_paid' => true,
        'invoice_url' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'plan' => true
    ];
}
