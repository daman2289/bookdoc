<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Routing\Router;


/**
 * UserProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property \Cake\I18n\FrozenDate $dob
 * @property string $gender
 * @property int $zipcode
 * @property string $address
 * @property string $state
 * @property string $country
 * @property string $profile_pic
 * @property string $specialty
 *
 * @property \App\Model\Entity\User $user
 */
class UserProfile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    protected $_virtual = ['full_name', 'display_url'];

    
    protected function _getFullName(){
        if (!empty($this->_properties['first_name']) && !empty($this->_properties['last_name'])){
            return $this->_properties['first_name'] .' '. $this->_properties['last_name'];
        }
        return null;
    }

    protected function _getDisplayUrl(){

        if(!empty($this->_properties['profile_pic'])){
            $file = new File(Configure::read('UserProfilePicPathToCheck') . $this->_properties['profile_pic']);
            if ($file->exists()) {
                return Router::url('/',true) . Configure::read('UserProfilePicPathToShow') . $file->name;
            }

        }
        if(!empty($this->_properties['gender'])){
            if ($this->_properties['gender'] == 'Female') {
                return Router::url('/',true).Configure::read('doctorDummyPath').Configure::read('DummyFemaleImage');
            }
        }
        return Router::url('/',true).Configure::read('doctorDummyPath').Configure::read('UserDummyImage');

    }   
}
