<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Booking Entity
 *
 * @property int $id
 * @property int $patient_id
 * @property int $doctor_id
 * @property \Cake\I18n\FrozenDate $booking_date
 * @property \Cake\I18n\FrozenTime $booking_time
 * @property int $illness_reason_id
 * @property string $who_will_seeing
 * @property string $patient_first_name
 * @property string $patient_last_name
 * @property \Cake\I18n\FrozenDate $patient_dob
 * @property string $patient_sex
 * @property string $patient_email
 * @property string $insurance_id
 * @property string $insurance_member_id
 * @property string $doctor_note
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\IllnessReason $illness_reason
 * @property \App\Model\Entity\Insurance $insurance
 * @property \App\Model\Entity\InsuranceMember $insurance_member
 */
class Booking extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    protected $_virtual = ['weekday', 'full_name'];

    
    protected function _getWeekday(){
        if (!empty($this->_properties['bookingTime'])){
            return date('w', strtotime($this->_properties['bookingTime']));
        }
        return null;
    }

    // protected function _getFullName()
    // {
    //     return $this->_properties['patient_first_name'] . '  ' .
    //         $this->_properties['patient_last_name'];
    // }

    
    protected $_accessible = [
        'patient_id' => true,
        'doctor_id' => true,
        'booking_date' => true,
        'booking_time' => true,
        'duration' => true,
        'illness_reason_id' => true,
        'who_will_seeing' => true,
        'patient_first_name' => true,
        'patient_last_name' => true,
        'patient_dob' => true,
        'patient_sex' => true,
        'patient_email' => true,
        'insurance_id' => true,
        'insurance_member_id' => true,
        'doctor_note' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'patient' => true,
        'doctor' => true,
        'illness_reason' => true,
        'insurance' => true,
        'insurance_member' => true,
        'file' => true
    ];
}
