<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Routing\Router;

/**
 * DoctorImage Entity
 *
 * @property int $id
 * @property string $images
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class DoctorImage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'images' => true,
        'user_id' => true,
        'user' => true
    ];

    protected $_virtual = ['display_url'];


    protected function _getDisplayUrl(){

        if(!empty($this->_properties['images'])){
            $file = new File(Configure::read('doctorAddedImagesPath') . $this->_properties['images']);
            return Router::url('/',true) . Configure::read('doctorAddedImagesPath') . $file->name;
        }
        
        return null;

    }   
}
