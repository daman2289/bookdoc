<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plan Entity
 *
 * @property int $id
 * @property string $name
 * @property string $stripe_id
 * @property float $price
 * @property int $plan_type
 * @property int $trial_period_in_days
 * @property int $is_default
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Stripe $stripe
 */
class Plan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'stripe_id' => true,
        'price' => true,
        'plan_type' => true,
        'trial_period_in_days' => true,
        'is_default' => true,
        'created' => true,
        'modified' => true,
        'stripe' => true
    ];
}
