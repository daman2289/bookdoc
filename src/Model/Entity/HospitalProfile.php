<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HospitalProfile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $hospital_name
 * @property string $contact_name
 * @property string $address
 * @property int $zipcode
 * @property int $state_id
 * @property int $country_id
 * @property string $phone_number
 * @property string $lat
 * @property string $lng
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Country $country
 */
class HospitalProfile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'hospital_name' => true,
        'contact_name' => true,
        'address' => true,
        'zipcode' => true,
        'state_id' => true,
        'country_id' => true,
        'phone_number' => true,
        'lat' => true,
        'lng' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'state' => true,
        'country' => true
    ];
}
