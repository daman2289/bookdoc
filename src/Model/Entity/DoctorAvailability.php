<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DoctorAvailability Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $available_from
 * @property \Cake\I18n\FrozenTime $available_upto
 * @property int $weekday
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class DoctorAvailability extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'available_from' => true,
        'available_upto' => true,
        'weekday' => true,
        'full_day_off' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'available_slots' => true
    ];

    //protected $_virtual = ['available_slots'];

    protected $duration = 40;
    
    // protected function _getAvailableSlots(){
    //     // $range = [];
    //     // $duration=$this->duration;
    //     // if (!empty($this->_properties['available_from']) && !empty($this->_properties['available_upto'])){
    //     //     $begin = new \DateTime($this->_properties['available_from']);
    //     //     $end = new \DateTime($this->_properties['available_upto']);
    //     //     $timespan = $duration*60;
    //     //     $interval = new \DateInterval('PT'.$timespan.'S');
    //     //     $dateRange = new \DatePeriod($begin, $interval, $end);
    //     //     foreach ($dateRange as $date) {
    //     //         $new = [];
    //     //         $new['timing'] = $date->format('H:i');
    //     //         array_push($range, $new);
    //     //     }
    //     // }
    //     // return $range;
    // }

}
