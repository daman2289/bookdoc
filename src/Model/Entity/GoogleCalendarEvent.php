<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GoogleCalendarEvent Entity
 *
 * @property int $id
 * @property int $booking_id
 * @property int $doctor_id
 * @property string $event_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Booking $booking
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\Event $event
 */
class GoogleCalendarEvent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'booking_id' => true,
        'doctor_id' => true,
        'event_id' => true,
        'created' => true,
        'modified' => true,
        'booking' => true,
        'doctor' => true,
        'event' => true
    ];
}
