<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DoctorVideo Entity
 *
 * @property int $id
 * @property string $video_link_one
 * @property string $video_link_two
 * @property string $video_link_three
 * @property string $video_link_four
 * @property string $video_link_five
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class DoctorVideo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'video_link_one' => true,
        'video_link_two' => true,
        'video_link_three' => true,
        'video_link_four' => true,
        'video_link_five' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
