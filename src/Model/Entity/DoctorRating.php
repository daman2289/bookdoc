<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DoctorRating Entity
 *
 * @property int $id
 * @property int $doctor_id
 * @property int $patient_id
 * @property int $booking_id
 * @property string $review
 * @property float $overall_rating
 * @property float $bedside_manner
 * @property float $wait_time
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Booking $booking
 */
class DoctorRating extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'doctor_id' => true,
        'patient_id' => true,
        'booking_id' => true,
        'review' => true,
        'overall_rating' => true,
        'bedside_manner' => true,
        'wait_time' => true,
        'created' => true,
        'modified' => true
    ];
}
