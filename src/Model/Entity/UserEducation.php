<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserEducation Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $degree
 * @property string $university
 *
 * @property \App\Model\Entity\User $user
 */
class UserEducation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'degree' => true,
        'university' => true,
        'user' => true
    ];
}
