<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Insurance Entity
 *
 * @property int $id
 * @property string $title_french
 * @property string $title_eng
 * @property string $title_italic
 * @property int $user_id
 * @property int $status
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Insurance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title_french' => true,
        'title_eng' => true,
        'title_italic' => true,
        'user_id' => true,
        'status' => true,
        'is_deleted' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
