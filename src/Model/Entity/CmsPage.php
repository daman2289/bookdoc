<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CmsPage Entity
 *
 * @property int $id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $page_title
 * @property string $page_name
 * @property int $is_activated
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $is_header
 * @property int $page_order
 * @property string $additional_key
 * @property int $is_deleted
 */
class CmsPage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'meta_title' => true,
        'meta_description' => true,
        'meta_keyword' => true,
        'page_title' => true,
        'page_name' => true,
        'is_activated' => true,
        'created' => true,
        'modified' => true,
        'is_header' => true,
        'page_order' => true,
        'additional_key' => true,
        'is_deleted' => true
    ];
}
