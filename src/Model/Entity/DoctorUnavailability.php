<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Hash; 

/**
 * DoctorUnavailability Entity
 *
 * @property int $id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $unavailable_from
 * @property \Cake\I18n\FrozenTime $unavailable_upto
 * @property int $unavailable_type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 */
class DoctorUnavailability extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'unavailable_from' => true,
        'unavailable_upto' => true,
        'unavailable_type' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
    protected $_virtual = ['formatted_time'];

    
    // protected function _getUnavailableSlots(){
    //     $range = [];
    //     $duration=30;
    //     if (!empty($this->_properties['unavailable_from']) && !empty($this->_properties['unavailable_upto'])){
    //         $begin = new \DateTime($this->_properties['unavailable_from']->format("Y-m-d H:i:s"));
    //         $end = new \DateTime($this->_properties['unavailable_upto']->format("Y-m-d H:i:s"));
    //         $timespan = $duration*60;
    //         $interval = new \DateInterval('PT'.$timespan.'S');
    //         $dateRange = new \DatePeriod($begin, $interval, $end);
    //         foreach ($dateRange as $date) {
    //             $new = [];
    //             $new['timing'] = $date->format('H:i');
    //             array_push($range, $new);
    //         }
    //         $range = Hash::extract($range, '{n}.timing');
    //     }
    //     return $range;
    // }
    protected function _getFormattedTime(){
        if (!empty($this->_properties['unavailable_from'])){
            return $this->_properties['unavailable_from']->format('Y-m-d');
        }
        return '';
    }

}
