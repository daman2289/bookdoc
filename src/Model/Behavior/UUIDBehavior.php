<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;


use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;

use Cake\Utility\Text;

/**
 * UUID behavior
 */
class UUIDBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
    	'field' => 'uuid',

    ];

    public function uuid(Entity $entity)
    {
        $fieldConfig = $this->getConfig('field');
        $value = $entity->get($fieldConfig);
        if (!$value || strlen($value) < 32){
            $entity->set($fieldConfig, Text::uuid());
        }
       
    }

    public function beforeSave($event, $entity, $options)
    {
        if($entity->isNew()) {
            $this->uuid($entity);
        }
    } 
}
