<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * DoctorUnavailability Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DoctorUnavailability get($primaryKey, $options = [])
 * @method \App\Model\Entity\DoctorUnavailability newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DoctorUnavailability[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DoctorUnavailability|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DoctorUnavailability|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DoctorUnavailability patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DoctorUnavailability[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DoctorUnavailability findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DoctorUnavailabilityTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('doctor_unavailability');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('unavailable_from')
            ->requirePresence('unavailable_from', 'create')
            ->notEmpty('unavailable_from');

        $validator
            ->dateTime('unavailable_upto')
            ->requirePresence('unavailable_upto', 'create')
            ->notEmpty('unavailable_upto');

        $validator
            ->integer('unavailable_type')
            ->requirePresence('unavailable_type', 'create')
            ->notEmpty('unavailable_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function afterSave($event, $entity, $options = []){

        if($entity->isNew()){

                $data = [
                            'vars' => [
                                'from_time_off' => $entity->unavailable_from->format('H:i'),
                                'to_time_off' => $entity->unavailable_upto->format('H:i'),
                                'on_date' => $entity->unavailable_from->format('M d, Y')
                            ],
                            'doctor_id' => $entity->user_id
                        ];
                        
                $event = new Event('Model.DoctorTimeOffs.new', $this, ['options' => $data]);
                $this->eventManager()->dispatch($event);
        }

    }
}
