<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Insurances Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Insurance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Insurance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Insurance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Insurance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Insurance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Insurance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Insurance findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InsurancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('insurances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title_french')
            ->maxLength('title_french', 55)
            ->requirePresence('title_french', 'create')
            ->notEmpty('title_french');

        $validator
            ->scalar('title_eng')
            ->maxLength('title_eng', 55)
            ->requirePresence('title_eng', 'create')
            ->notEmpty('title_eng');

        $validator
            ->scalar('title_italic')
            ->maxLength('title_italic', 55)
            ->requirePresence('title_italic', 'create')
            ->notEmpty('title_italic');

        /*$validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function loadInsurances($lngTitle) {
        $query = $this->find('list', [
                    'keyField' => 'id',
                    'valueField' => 'title_'.$lngTitle
                ])
                ->toArray();
        return $query;
    }
}
