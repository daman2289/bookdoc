<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\InsurancesTable|\Cake\ORM\Association\HasMany $Insurances
 * @property \App\Model\Table\SpecialitiesTable|\Cake\ORM\Association\HasMany $Specialities
 * @property \App\Model\Table\UserProfilesTable|\Cake\ORM\Association\HasMany $UserProfiles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('UUID');
        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        \Cake\Event\EventManager::instance()->on('HybridAuth.newUser', [$this, 'createUser']);

        $this->addBehavior('Timestamp');
        
        $this->hasOne('UserStripeDetails');
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id'
        ]);
        $this->belongsTo('Plans', [
            'foreignKey' => 'plan_id'
        ]);
        
        $this->hasMany('UserInsurances', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('UserTokens', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('UserSpecialities', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('DoctorImages', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('UserLanguages', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('UserEducations', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('UserAwards', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('DoctorUnavailability', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('DoctorAvailability', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('UserHospitalAffiliations', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);

        $this->hasMany('DoctorRatings', [
            'foreignKey' => 'doctor_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('DoctorBlockTimings', [
            'foreignKey' => 'user_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasOne('SpamPatients', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasOne('UserProfiles', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('SocialProfiles', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('HospitalProfiles', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('HospitalDoctors', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('PatientProfiles', [
            'className' => 'UserProfiles',
            'foreignKey' => 'user_id',
        ]);

        $this->addBehavior('Acl.Acl', ['type' => 'requester']);
    }


    // public function createUser(\Cake\Event\Event $event) 
    // {
    //     // Entity representing record in social_profiles table
    //     $profile = $event->data()['profile'];
    //     // Make sure here that all the required fields are actually present

    //     $user = $this->newEntity(['role_id' => 4,'email' => $profile->email,'user_profile' => ['first_name' => $profile->first_name,'last_name' => $profile->last_name,'gender' => $profile->gender,'social_profile_url' => $profile->photo_url]]);

    //     $user = $this->save($user);

    //     if (!$user) {
    //         throw new \RuntimeException('Unable to save new user');
    //     }

    //     return $user;
    // }

    public function getUser(\Cake\Datasource\EntityInterface $profile) {
        // Make sure here that all the required fields are actually present
        if (empty($profile->email)) {
            throw new \RuntimeException('Could not find email in social profile.');
        }

        // Check if user with same email exists. This avoids creating multiple
        // user accounts for different social identities of same user. You should
        // probably skip this check if your system doesn't enforce unique email
        // per user.
        $user = $this->find()
            ->where(['email' => $profile->email])
            ->first();

        if ($user) {
            return $user;
        }

        // Create new user account
        $user = $this->newEntity(['role_id' => 4,'email' => $profile->email,'user_profile' => ['first_name' => $profile->full_name,'gender' => $profile->gender,'social_profile_url' => $profile->photo_url]]);

        $user = $this->save($user);

        if (!$user) {
            throw new \RuntimeException('Unable to save new user');
        }

        return $user;
    }
 

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /*
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmpty('password');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->allowEmpty('is_approved');

        $validator
            ->allowEmpty('is_email_verified');

        $validator
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->scalar('persist_code')
            ->maxLength('persist_code', 255)
            ->allowEmpty('persist_code');

        $validator
            ->scalar('password_reset_token')
            ->maxLength('password_reset_token', 255)
            ->allowEmpty('password_reset_token');

        $validator
            ->scalar('hashval')
            ->maxLength('hashval', 255)
            ->allowEmpty('hashval');

        return $validator;
    } */

    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->notEmpty('password')
            ->add('password', [
                    'length' => [
                        'rule' => ['minLength', 8],
                        'message' => 'password need to be at least 8 characters long',
                    ]
                ])
            ->maxLength('password', 255);

        $validator
            ->email('email')
            ->allowEmpty('email');
        
        $validator
            ->add('email', 'email', [
                'rule' => 'validateUnique', 
                'provider' => 'table',
                'message' => __("This Email is already registered.")
            ]);

        // $validator->add('email','custom', [
        //     'rule' => function ($value, $context){
        //         $condition = ['Users.email' => $value];
                
        //         if (!empty($context['data']['id'])) {
        //             $condition['Users.id <>'] = $context['data']['id'];
        //         }

        //         $data = $this->find('all',[
        //                 'conditions' => $condition
        //             ]
        //         )->toArray();
               
        //         if (count($data) > 0) {
        //             return false;
        //         }
        //         return true;
        //     },
        //     'message' => 'This Email is already registered.',            
        // ]);

        $validator
            ->integer('role_id')
            ->notEmpty('role_id');
    
        return $validator;
    }

    public function validationPassword(Validator $validator)
    {
        $validator
                ->add('old_password','custom',[
                    'rule' => function($value, $context){
                        $user = $this->get($context['data']['id']);
                        if($user)
                        {
                            if((new DefaultPasswordHasher)->check($value, $user->password))
                            {
                                return true;
                            }
                        }
                        return false;
                    },
                    'message' => __('Invalid old password. Please try again.'),
                ])
                ->notEmpty('old_password');
        
        $validator
                ->add('password',[
                    'length' => [
                        'rule' => ['minLength',8],
                        'message' => __('Password should be atleast 8 characters long.')
                    ]
                ])
                
                ->notEmpty('password');


        $validator
                ->add('confirm_password',[
                    'length' => [
                        'rule' => ['minLength',8],
                        'message' => __('Password and Confirm Password must be same')
                    ]
                ])
                 ->add('confirm_password',[
                    'match' => [
                        'rule' => ['compareWith','password'],
                        'message' => __('Sorry! Password doesn\'t not match. Please try again!')
                    ]
                ])
                
                ->notEmpty('password');
       
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));

        return $rules;
    }

    /**
     * return User details.
     *
     * @param array $data user fields.
     */

    public function getUserDetail($data) {
        $query = $this->find()
                ->where($data)
                ->contain(['UserProfiles']);
        return $query->first();
    }


    public function updateUserRecord($fields, $data, $record) {
        $usersTable = TableRegistry::get('Users');
        $users = $usersTable->get($data);
        foreach ($fields as $value) {
            $users->$value = $record[$value];
        }
        if ($usersTable->save($users)) {
            return true;
        }
        return false;
    }

    /**
     * return User details.
     * @future use if customize auth contain conditions
     * @param array $data user fields.
     */
    // public function findAuth(\Cake\ORM\Query $query, array $options)
    // {
    //     $query
    //         ->select(['Users.id', 'Users.email', 'Users.password', 'Users.role_id'])
    //         ->where(['Users.status' => 0])
    //         ->contain(['UserProfiles' => function($q) {
    //             return $q
    //                 ->select(['first_name', 'last_name']);
    //         }]);
    //     return $query;
    // }
    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        return $query->contain(['UserProfiles','HospitalProfiles']);
    }


    
}
