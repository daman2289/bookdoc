<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Bookings Model
 *
 * @property \App\Model\Table\PatientsTable|\Cake\ORM\Association\BelongsTo $Patients
 * @property \App\Model\Table\DoctorsTable|\Cake\ORM\Association\BelongsTo $Doctors
 * @property \App\Model\Table\IllnessReasonsTable|\Cake\ORM\Association\BelongsTo $IllnessReasons
 * @property \App\Model\Table\InsurancesTable|\Cake\ORM\Association\BelongsTo $Insurances
 * @property \App\Model\Table\InsuranceMembersTable|\Cake\ORM\Association\BelongsTo $InsuranceMembers
 *
 * @method \App\Model\Entity\Booking get($primaryKey, $options = [])
 * @method \App\Model\Entity\Booking newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Booking[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Booking|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Booking patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Booking[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Booking findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BookingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bookings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Patients', [
        //     'foreignKey' => 'patient_id',
        //     'joinType' => 'INNER'
        // ]);
        $this->belongsTo('Patient', [
            'className' => 'Users',
            'foreignKey' => 'patient_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'doctor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('IllnessReasons', [
            'foreignKey' => 'illness_reason_id'
        ]);
        $this->belongsTo('Insurances', [
            'foreignKey' => 'insurance_id'
        ]);
        // $this->belongsTo('InsuranceMembers', [
        //     'foreignKey' => 'insurance_member_id'
        // ]);

        $this->hasOne('DoctorRatings',[
            'foreignKey' => 'booking_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('booking_date')
            ->requirePresence('booking_date', 'create')
            ->notEmpty('booking_date');

        $validator
            ->requirePresence('duration', 'create')
            ->notEmpty('duration');

        $validator
            ->time('booking_time')
            ->requirePresence('booking_time', 'create')
            ->notEmpty('booking_time');

        $validator
            ->scalar('who_will_seeing')
            ->maxLength('who_will_seeing', 255)
            ->allowEmpty('who_will_seeing');

        $validator
            ->scalar('patient_first_name')
            ->maxLength('patient_first_name', 255)
            ->allowEmpty('patient_first_name');

        $validator
            ->scalar('patient_last_name')
            ->maxLength('patient_last_name', 255)
            ->allowEmpty('patient_last_name');

        $validator
            ->date('patient_dob')
            ->allowEmpty('patient_dob');

        $validator
            ->scalar('patient_sex')
            ->allowEmpty('patient_sex');

        $validator
            ->scalar('patient_email')
            ->maxLength('patient_email', 45)
            ->allowEmpty('patient_email');

        $validator
            ->scalar('doctor_note')
            ->allowEmpty('doctor_note');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['patient_id'], 'Users'));
        $rules->add($rules->existsIn(['doctor_id'], 'Users'));
        $rules->add($rules->existsIn(['illness_reason_id'], 'IllnessReasons'));
        $rules->add($rules->existsIn(['insurance_id'], 'Insurances'));
        // $rules->add($rules->existsIn(['insurance_member_id'], 'InsuranceMembers'));

        return $rules;
    }


    public function afterSave($event, $entity, $options = []){
        $doctor_name = $this->Users->UserProfiles->findByUserId($entity->doctor_id)->first();

        if(!$entity->isNew()){
            // CASE : RESCHEDULE 
            if ($entity->getOriginal('booking_date') != $entity->booking_date) {
                $bookingDate = new Time($entity->booking_date .' ' .$entity->booking_time);
                $data = [
                            'vars' => [
                                    'booking_id' => $entity->id,
                                    'patient_name' => $entity->patient_first_name.' '.$entity->patient_last_name,
                                    'doctor_name' => $doctor_name->first_name.' '.$doctor_name->last_name,
                                    'booking_date' => $bookingDate->nice()
                            ],
                            'doctor_id' => $entity->doctor_id
                        ];
                        

                $event = new Event('Model.Bookings.rescheduled', $this, ['options' => $data]);
                return $this->eventManager()->dispatch($event);
            }

            if($entity->getOriginal('status') != $entity->status){ // save notification on only status change
                // CASE : PENDING->CONFIRM, PENDING->DECLINE, APPROVED -> DECLINED 
                $bookingDate = new Time($entity->booking_date->format('Y-m-d') .' ' .$entity->booking_time->format('H:i:s'));

                $data = [
                            'vars' => [
                                    'booking_id' => $entity->id,
                                    'patient_name' => $entity->patient_first_name.' '.$entity->patient_last_name,
                                    'doctor_name' => $doctor_name->first_name.' '.$doctor_name->last_name,
                                    'booking_date' => $bookingDate->nice()
                                    ],
                            'status' => [
                                'from' => $entity->getOriginal('status'), 
                                'to' => $entity->status
                            ],
                            'doctor_id' => $entity->doctor_id
                        ];
                        
                $this->UserTokens = TableRegistry::get('UserProfiles');
                $doctor = $this->UserTokens->findByUserId($data['doctor_id'])->first();

                $pushData = [
                            'vars' => [
                                    'booking_id' => $entity->id,
                                    'patient_name' => $entity->patient_first_name.' '.$entity->patient_last_name,
                                    'doctor_name' => $doctor->full_name,
                                    'booking_date' => $bookingDate->format('M d, Y H:i')
                                    ],
                            'status' => [
                                'from' => $entity->getOriginal('status'), 
                                'to' => $entity->status
                            ],
                            'doctor_id' => $entity->doctor_id,
                            'patient_id' => $entity->patient_id,
                ];
                        

                $event1 = new Event('Push.Bookings.status', $this, ['options' => $pushData]);
                $event2 = new Event('Model.Bookings.status', $this, ['options' => $data]);
                $this->eventManager()->dispatch($event1);
                return $this->eventManager()->dispatch($event2);
            }
        }else{
            // CASE : NEW APPOINTMENT
            $bookingDate = new Time($entity->booking_date->format('Y-m-d') .' ' .$entity->booking_time);

            $data = [
                'vars' => [
                        'booking_id' => $entity->id,
                        'patient_name' => $entity->patient_first_name.' '.$entity->patient_last_name,
                        'doctor_name' => $doctor_name->first_name.' '.$doctor_name->last_name,
                        'booking_date' => $bookingDate->format('M d, Y H:i')
                ],
                
                'doctor_id' => $entity->doctor_id
            ];
            $this->UserTokens = TableRegistry::get('UserProfiles');
            $doctor = $this->UserTokens->findByUserId($data['doctor_id'])->first();

            $pushData = [
                'vars' => [
                        'booking_id' => $entity->id,
                        'patient_name' => $entity->patient_first_name.' '.$entity->patient_last_name,
                        'doctor_name' => $doctor->full_name,
                        'booking_date' => $bookingDate->format('M d, Y H:i')
                        ],
                'doctor_id' => $entity->doctor_id,
                'patient_id' => $entity->patient_id,
            ];
                        
            $event1 = new Event('Model.Bookings.new', $this, ['options' => $data]);
            $event2 = new Event('Push.Bookings.new', $this, ['options' => $pushData]);
            $this->eventManager()->dispatch($event2);
            return $this->eventManager()->dispatch($event1);
        }

    }
}
