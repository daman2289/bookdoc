<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * DoctorAvailability Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DoctorAvailability get($primaryKey, $options = [])
 * @method \App\Model\Entity\DoctorAvailability newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DoctorAvailability[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DoctorAvailability|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DoctorAvailability|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DoctorAvailability patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DoctorAvailability[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DoctorAvailability findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DoctorAvailabilityTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('doctor_availability');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->time('available_from')
            ->requirePresence('available_from', 'create')
            ->notEmpty('available_from');

        $validator
            ->time('available_upto')
            ->requirePresence('available_upto', 'create')
            ->notEmpty('available_upto');

        $validator
            ->integer('weekday')
            ->allowEmpty('weekday');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    // public function afterSave($event, $entity, $options = []){

    //     // if(!$entity->isNew()){

    //     //         $data = [
    //     //                     'vars' => [
    //     //                         'from_time_off' => $entity->available_from->format('H:i'),
    //     //                         'to_time_off' => $entity->available_upto->format('H:i'),
    //     //                         'on_date' => $entity->available_upto->format('M d, Y')
    //     //                     ],
    //     //                     'doctor_id' => $entity->user_id
    //     //                 ];
                        
    //     //         $event = new Event('Model.DoctorTimeOffs.new', $this, ['options' => $data]);
    //     //         $this->eventManager()->dispatch($event);
    //     // }

    // }
}
